//
//  Service.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/09/21.
//

import Foundation
import Alamofire
import SwiftyJSON


class Service {
    //https://restcountries.eu/rest/v2
    fileprivate var baseUrl = ""
    typealias countriesCallBack = (_ countries:[UserInfos]?, _ status: Bool, _ message:String) -> Void
    
    var callBack:countriesCallBack?
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    //MARK:- getAllCountryNameFrom
    func getAllCountryNameFrom(endPoint:String) {
        guard let Xauthorization = UserDefaults.standard.string(forKey: "authToken") else { return }
        
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : Xauthorization,
                                   "focus_club_id": "135"]
        
        Alamofire.request(self.baseUrl + endPoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (responseData) in
            print("Service responseData is:\(responseData)")
            guard let data = responseData.data else {
                self.callBack?(nil, false, "")
                
                return}
            print(data)
            if let array = responseData.result.value as? [Any] {
                print("Got an array with \(array.count) objects")
            }
            else if let dictionary = responseData.result.value as? [String: Any] {
                print("Got a dictionary: \(dictionary)")
            }
        }
    }
    
    
    
    func completionHandler(callBack: @escaping countriesCallBack) {
        self.callBack = callBack
    }
}
