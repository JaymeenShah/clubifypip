//
//  ServiceCalls.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 31/08/21.
//

import UIKit
import Alamofire

class ServiceCalls: NSObject {
    

    static let sharedInstance: ServiceCalls = {
        let instance = ServiceCalls()
        
        return instance
    }()
    
     let Xauthorization = UserDefaults.standard.string(forKey: "authToken")

    
    //MARK:- for get API call
    func commonGETRequestCall(urlStr: String!, completionHandler:@escaping ( _ success: Bool, _ resultVal: String) -> Void) {
        Alamofire.request(urlStr, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil ).responseString { response in
            
            print("### commonGETRequestCall response :: \(response)")
            if let JSON = response.result.value {
                
                completionHandler(true, JSON)
            } else {
                if let message = response.result.error?.localizedDescription {
                    // Handle API error message
                    print("Error Found :: \(message)")
                    completionHandler(false, message)
                }
            }
        }
    }
    func getApiResponseWithDictionary(urlVal:String,method:HTTPMethod, parameters:[String:Any],headers:HTTPHeaders,completionHandler:@escaping (_ Result:[String:Any])->Void)
    {
        Alamofire.request(urlVal, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers ).responseJSON { response in
            print(response)
            if let responseData = response.result.value as? [String:Any]{


            }
            
        }

    }
    //MARK:- for get API call With header values
    
    func GETAPICallWithHeaderValues (url: String, headersData: [String: Any]!, completionHandler:@escaping ( _ success: Bool, _ resultVal: [String: Any]) -> Void) {
        
        Alamofire.request(url, headers: headersData as? HTTPHeaders)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    print("success GETAPICallWithHeaderValues")
                    //decode data
                    guard let dataVal = response.result.value as? [Any] else { return }
                    guard let json = dataVal.first as?[String: Any] else { return }
                    completionHandler(true, json)
                } else {
                }
            }
    }
    
    func postAPICallForData(urlStr: String!, postData: [String: Any]!, completionHandler:@escaping ( _ success: Bool, _ resultVal: [String: Any]) -> Void) {
        Alamofire.request(urlStr, method: .post, parameters: postData,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("print what ever you want here")
                    //              completionHandler(false, response)
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("Error: \(String(describing: response.result.error))")
                    
                    //                            completionHandler(false, json)
                    
                    return
                }
                
                if let resultDict: [String: Any] =  json["result"] as? [String: Any] {
                    completionHandler(true, resultDict)
                    return
                } else if var resultDict: [String: Any] =  json["error"] as? [String: Any] {
                    
                    completionHandler(false, [:])
                    
                }
            }
    }
    //MARK:- for post request with header values
    func PostAPICallWithHeaderValues (url: String, postData: [String: Any]!, headersData: [String: Any]!, completionHandler:@escaping ( _ success: Bool, _ resultVal: [String: Any]) -> Void) {
        
        Alamofire.request(url, method: .post, parameters: postData, encoding: JSONEncoding.default, headers: headersData as? HTTPHeaders).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any] {
                completionHandler(true, json)
            } else {
                completionHandler(false, [:])
            }
        }
    }
   
    func commonApiCall (url: String, postData: [String: Any]!, headersData: HTTPHeaders, completionHandler:@escaping ( _ success: Bool, _ resultVal: [String: Any]) -> Void) {
        
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headersData).responseJSON { (response:DataResponse<Any>) -> Void in
                    switch response.result {
                    case .success:
                        if let data = response.result.value as? Dictionary<String, AnyObject> {
                        }
                        break
                    case .failure:
                        print("Error")
                    }
                   

                }

        
    }
    
    //MARK:- upload an image
    func uploadWithAlamofire() {
      let image = UIImage(named: "bodrum")!

      // define parameters
      let parameters = [
        "hometown": "yalikavak",
        "living": "istanbul"
      ]

      Alamofire.upload(multipartFormData: { multipartFormData in
                        if let imageData = image.jpegData(compressionQuality: 1) {
          multipartFormData.append(imageData, withName: "file", fileName: "file.png", mimeType: "image/png")
        }

        for (key, value) in parameters {
            multipartFormData.append((value.data(using: .utf8))!, withName: key)
        }}, to: "upload_url", method: .post, headers: ["Authorization": "auth_token"],
            encodingCompletion: { encodingResult in
              switch encodingResult {
              case .success(let upload, _, _):
                upload.response { [weak self] response in
                  
                  debugPrint(response)
                }
              case .failure(let encodingError):
                print("error:\(encodingError)")
              }
      })
    }
    
}

