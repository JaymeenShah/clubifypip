//
//  ColorControlContentViewDelegate.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 22/09/21.
//

import Foundation

import UIKit

protocol ColorControlContentViewDelegate: class {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
}

/// Content holder for all color controls derived from `AbstractColorControl`. Delegates its `gestureRecognizerShouldBegin(:)` method to settable delegate. Use it as a regular `UIView`.
class ColorControlContentView: UIView {
    weak var delegate: ColorControlContentViewDelegate?

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let delegate = delegate {
            return delegate.gestureRecognizerShouldBegin(gestureRecognizer)
        }
        return true
    }
}
