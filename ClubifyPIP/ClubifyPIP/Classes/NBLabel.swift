//
//  NBLabel.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 12/10/21.
//

import Foundation
import UIKit

class NBLabel : UILabel {
    override func awakeFromNib() {
        
        var lblColor: UIColor!
        let strTheme: String = UserDefaults.standard.string(forKey:"lightTheme") ?? ""
        if !strTheme.isEmpty {
            let isLightTheme = UserDefaults.standard.string(forKey:"lightTheme")
            if isLightTheme == "1" {
                lblColor = .black//[UIColor.clear, UIColor.white]
            } else {
                lblColor = .white
            }
        } else {
            UserDefaults.standard.set("0", forKey: "lightTheme")
            UserDefaults.standard.synchronize()
            lblColor = .white
        }
        
        //Font size auto resizing in different devices
//        if checkIfiPad(){
//            self.font = self.font.withSize(self.font.pointSize * CGFloat(DeviceScaleiPad.SCALE_X))
//            self.textColor = lblColor
//        }else {
//            self.font = self.font.withSize(self.font.pointSize * DeviceScale.SCALE_X)
//            self.textColor = lblColor
//        }
    }
}
