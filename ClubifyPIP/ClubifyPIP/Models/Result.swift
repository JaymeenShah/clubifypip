//
//  Result.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 19/10/21.
//

import Foundation
import SwiftyJSON

public struct DataResult {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let caption = "caption"
    static let name = "name"
    static let title = "title"
    static let address = "address"
  }

  // MARK: Properties
  public var caption: String?
  public var name: String?
  public var title: String?
  public var address: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    caption = json[SerializationKeys.caption].string
    name = json[SerializationKeys.name].string
    title = json[SerializationKeys.title].string
    address = json[SerializationKeys.address].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = caption { dictionary[SerializationKeys.caption] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    return dictionary
  }

}
