//
//  YouTubeVideoDetails.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation

class YouTubeVideoDetails : NSObject {
    
    var title : String?
    var descriptionText : String?
    var url : String?
    var thumbnail : String?
    var channelTitle : String?
    var publishTime : String?
    
    override init() {
        
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(dictJSON:NSDictionary) {
        title = dictJSON.getString(key:"title")
        descriptionText = dictJSON.getString(key:"description")
        url = dictJSON.getString(key:"url")
        thumbnail = dictJSON.getString(key:"thumbnail")
        channelTitle = dictJSON.getString(key:"channelTitle")
        publishTime = dictJSON.getString(key:"publishTime")
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if title != nil{
            dictionary["title"] = title
        }
        if description != nil{
            dictionary["description"] = description
        }
        if url != nil{
            dictionary["url"] = url
        }
        if thumbnail != nil{
            dictionary["thumbnail"] = thumbnail
        }
        if channelTitle != nil{
            dictionary["channelTitle"] = channelTitle
        }
        if publishTime != nil{
            dictionary["publishTime"] = publishTime
        }
        return dictionary
    }
}
