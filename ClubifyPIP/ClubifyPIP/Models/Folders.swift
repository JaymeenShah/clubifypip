//
//  Folders.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
import UIKit

class FolderList : NSObject {
    
    var id: String = ""
    var caption: String = ""
    var clubid: String = ""
    var cost: String = ""
    var points: String = ""
    var createdtimestamp: String = ""
    var deleted: String = ""
    var desc: String = ""
    var isactive: String = ""
    var modifiedtimestamp: String = ""
    var owneraccountid: String = ""
    var title: String = ""
    var visible: String = ""
    var isPurchased : Bool = false
    var membershipLevel: [MembershipLevel]?
    var parentId : String = ""
    var galleryId : String = ""
    var galleryColor: String = ""
    var galleryFont: String = ""
    var isLargeBanner: String = ""
    var largeBannerImageURL: String = ""
    var percentage: Int = 0
    var sortBy: String = ""
    var galleryOrder: String = ""
    
    override init() {
        
    }
    
    init(dictJSON:NSDictionary) {
        self.galleryOrder = dictJSON.getString(key: "gallery_order")
        self.sortBy = dictJSON.getString(key: "sort_by")
        self.percentage = dictJSON.getInt(key: "percentage")
        self.galleryColor = dictJSON.getString(key: "gallery_color")
        self.galleryFont = dictJSON.getString(key: "gallery_font")
        self.id = dictJSON.getString(key: "id")
        self.caption = dictJSON.getString(key: "caption")
        self.clubid = dictJSON.getString(key: "clubid")
        self.cost = dictJSON.getString(key: "cost")
        self.points = dictJSON.getString(key: "points")
        self.createdtimestamp = dictJSON.getString(key: "createdtimestamp")
        self.deleted = dictJSON.getString(key: "deleted")
        self.desc = dictJSON.getString(key: "description")
        self.isactive = dictJSON.getString(key: "isactive")
        self.modifiedtimestamp = dictJSON.getString(key: "modifiedtimestamp")
        self.owneraccountid = dictJSON.getString(key: "owneraccountid")
        self.title = dictJSON.getString(key: "title")
        self.visible = dictJSON.getString(key: "visible")
        self.isPurchased = dictJSON.getBool(key: "is_purchased")
        self.parentId = dictJSON.getString(key: "parent_id")
        self.galleryId = dictJSON.getString(key: "gallery_id")
        self.isLargeBanner = dictJSON.getString(key: "is_large_banner")
        self.largeBannerImageURL = dictJSON.getString(key: "gallery_large_banner_image")
        
        if let data = dictJSON["membership_level"] as? [[String: Any]] {
            var arrMembershipLevel: [MembershipLevel] = []
            for permData in data {
                arrMembershipLevel.append(MembershipLevel(fromDictionary: permData))
            }
            membershipLevel = arrMembershipLevel
        }
    }
}

