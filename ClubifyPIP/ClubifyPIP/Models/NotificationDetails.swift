//
//  NotificationDetails.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
import UIKit

class NotificationDetails : NSObject {

    var id: String = ""
    var firstname: String = ""
    var lastname: String = ""
    var gender: String = ""
    var profilepicture: String = ""
    var username: String = ""
    var email: String = ""
    var title: String = ""
    var message: String = ""
    var type: String = ""
    var clubId: String = ""
    var userId: String = ""
    var isRead: String = ""
    var payload: String = ""
    var createdAt: String = ""
    var deleted: String = ""

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        self.title = dictJSON.getString(key: "title")
        self.message = dictJSON.getString(key: "message")
        self.type = dictJSON.getString(key: "type")
        self.clubId = dictJSON.getString(key: "club_id")
        self.userId = dictJSON.getString(key: "user_id")
        self.isRead = dictJSON.getString(key: "is_read")
        self.payload = dictJSON.getString(key: "payload")
        self.createdAt = dictJSON.getString(key: "created_at")
        self.deleted = dictJSON.getString(key: "deleted")
        self.id = dictJSON.getString(key: "id")
        self.firstname = dictJSON.getString(key: "firstname")
        self.lastname = dictJSON.getString(key: "lastname")
        self.gender = dictJSON.getString(key: "gender")
        self.profilepicture = dictJSON.getString(key: "profilepicture")
        self.username = dictJSON.getString(key: "username")
        self.email = dictJSON.getString(key: "email")
    }
}
