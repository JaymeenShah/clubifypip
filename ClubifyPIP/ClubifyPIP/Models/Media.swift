//
//  Media.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/10/21.
//

import Foundation
import UIKit


class MembershipLevel : NSObject  {

    var id: String?
    var plan_name : String?
    var profile_icon_url : String?
    
    override init() {
        
    }
    
    init(fromDictionary dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        plan_name = dictionary["plan_name"] as? String
        profile_icon_url = dictionary["profile_icon_url"] as? String
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if plan_name != nil{
            dictionary["plan_name"] = plan_name
        }
        if profile_icon_url != nil{
            dictionary["profile_icon_url"] = profile_icon_url
        }
        return dictionary
    }
}

class GalleryMedia : NSObject  {
  
    var id: String = ""
    var clubid : String = ""
    var caption : String = ""
    var cost : String = ""
    var points : String = ""
    var createdtimestamp : String = ""
    var deleted : String = ""
    var desc : String = ""
    var fileid : String = ""
    var galleryid : String = ""
    var importedfromurl : String = ""
    var isactive : String = ""
    var isfavourite : String = ""
    var lastviewedtimestamp : String = ""
    var modifiedtimestamp : String = ""
    var owneruserid : String = ""
    var size : String = ""
    var title : String = ""
    var type : String = ""
    var url : String = ""
    var videoThumbnail = UIImage()
    var isDoneThumbnail = false
    var visible : String = ""
    var thumbnail_url : String = ""
    var isPurchased : Bool = false
    var membershipLevel: [MembershipLevel]?
    var owneraccountid: String = ""
    var parentId: String = ""
    var galleryLevel: String = ""
    var galleryresourceId: String = ""
    var galleryColor: String = ""
    var galleryFont: String = ""
    var allowExport: String = ""
    var isLargeBanner: String = ""
    var largeBannerImageURL: String = ""
    var percentage: Int = 0
    var item_url: String = ""
    var externalBrowser: String = ""
    var sortBy: String = ""
    var galleryOrder: String = ""
    override init() {

    }
    
    init(dictJSON:NSDictionary) {
        
        self.galleryOrder = dictJSON.getString(key: "gallery_order")
        self.sortBy = dictJSON.getString(key: "sort_by")
        self.externalBrowser = dictJSON.getString(key: "external_browser")
        self.item_url = dictJSON.getString(key: "item_url")
        self.percentage = dictJSON.getInt(key: "percentage")
        self.largeBannerImageURL = dictJSON.getString(key: "gallery_large_banner_image")
        self.isLargeBanner = dictJSON.getString(key: "is_large_banner")
        self.allowExport = dictJSON.getString(key: "allow_export")
        self.points = dictJSON.getString(key: "points")
        self.galleryColor = dictJSON.getString(key: "gallery_color")
        self.galleryFont = dictJSON.getString(key: "gallery_font")
        self.galleryresourceId = dictJSON.getString(key: "galleryresource_id")
        self.owneraccountid = dictJSON.getString(key: "owneraccountid")
        self.parentId = dictJSON.getString(key: "parent_id")
        self.galleryLevel = dictJSON.getString(key: "gallery_level")
        
        self.id = dictJSON.getString(key: "id")
        self.clubid = dictJSON.getString(key: "clubid")
        self.caption = dictJSON.getString(key: "caption")
        self.cost = dictJSON.getString(key: "cost")
        self.createdtimestamp = dictJSON.getString(key: "createdtimestamp")
        self.deleted = dictJSON.getString(key: "deleted")
        self.desc = dictJSON.getString(key: "description")
        self.fileid = dictJSON.getString(key: "fileid")
        self.galleryid = dictJSON.getString(key: "galleryid")
        self.importedfromurl = dictJSON.getString(key: "importedfromurl")
        self.isactive = dictJSON.getString(key: "isactive")
        self.isfavourite = dictJSON.getString(key: "isfavourite")
        self.lastviewedtimestamp = dictJSON.getString(key: "lastviewedtimestamp")
        self.modifiedtimestamp = dictJSON.getString(key: "modifiedtimestamp")
        self.owneruserid = dictJSON.getString(key: "owneruserid")
        self.size = dictJSON.getString(key: "size")
        self.title = dictJSON.getString(key: "title")
        self.type = dictJSON.getString(key: "type")
        self.url = dictJSON.getString(key: "url")
        self.visible = dictJSON.getString(key: "visible")
        self.thumbnail_url = dictJSON.getString(key: "thumbnail_url")
        self.isPurchased = dictJSON.getBool(key: "is_purchased")
        
        if let data = dictJSON["membership_level"] as? [[String: Any]] {
            var arrMembershipLevel: [MembershipLevel] = []
            for permData in data {
                arrMembershipLevel.append(MembershipLevel(fromDictionary: permData))
            }
            membershipLevel = arrMembershipLevel
        }
    }
}

