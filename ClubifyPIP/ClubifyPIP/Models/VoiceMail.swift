//
//  VoiceMail.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
class VoiceMail: NSObject, NSCoding{

    var voiceMessage: [VoiceMessage] = []
    var message: String?
    var status: Int?
    var morePages: Bool?


    override init() {
          
      }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        message = dictionary["message"] as? String
        status = dictionary["status"] as? Int
        morePages = dictionary["more_pages"] as? Bool
        voiceMessage = [VoiceMessage]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = VoiceMessage(fromDictionary: dic)
                voiceMessage.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if morePages != nil{
            dictionary["more_pages"] = morePages
        }

        if voiceMessage != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in voiceMessage {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        voiceMessage = (aDecoder.decodeObject(forKey: "data") as? [VoiceMessage])!
        message = aDecoder.decodeObject(forKey: "message") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if voiceMessage != nil{
            aCoder.encode(voiceMessage, forKey: "data")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }
}
