//
//  ClubPlan.swift
//  Clubify
//
//  Created  on 2017-07-26.
//  Copyright © 2021 Clubify Inc. All rights reserved.
//

import Foundation

class ClubifyCoinsPackges : NSObject  {

    var name : String = ""
    var productIdentifry : String = ""
    var price : String = ""
    var sortPrice : Int = 0
    
    override init() {

    }
    init(name: String,price : String,sortPrice : Int,productIdentifry : String)
    {
        self.name = name
        self.price = price
        self.sortPrice = sortPrice
        self.productIdentifry = productIdentifry
    }
   
}

class ClubPlans: NSObject {
    
    var clubAuth = ClubAuthority()
    var clubPlanDetail = ClubPlanDetail()
    var screenPermissions = ScreenPermissions()
    
    override init() {
        
    }
    init (clubAuth: ClubAuthority,clubPlanDetail: ClubPlanDetail , screenPermissions :ScreenPermissions)
    {
        self.clubAuth = clubAuth
        self.clubPlanDetail = clubPlanDetail
        self.screenPermissions = screenPermissions
    }
}
class ClubAuthority: NSObject {
    
        var id: String = ""
        var canInviteMembers: String = ""
        var canUploaddDocuments: String = ""
        var canUploadVideos: String = ""
        var canUploadImages: String = ""
        var isaActive: String = ""
        var isAdmin: String = ""
        var clubPlanId: String = ""
        var createdtimestamp: String = ""
        var canChooseMembershipLevel: String = ""
        var member_editable_listings: String = "0"

    override init() {
        
    }

    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.canInviteMembers = dictJSON.getString(key: "caninvitemembers")
        self.canUploaddDocuments = dictJSON.getString(key: "canuploaddocuments")
        self.canUploadVideos = dictJSON.getString(key: "canuploadvideos")
        self.canUploadImages = dictJSON.getString(key: "canuploadimages")
        self.isaActive = dictJSON.getString(key: "isactive")
        self.isAdmin = dictJSON.getString(key: "isadmin")
        self.clubPlanId = dictJSON.getString(key: "clubplanid")
        self.createdtimestamp = dictJSON.getString(key: "createdtimestamp")
        self.canChooseMembershipLevel = dictJSON.getString(key: "canchoosemembershiplevel")
        self.member_editable_listings = dictJSON.getString(key: "member_editable_listings")

    }
}

class ClubPlanDetail: NSObject {
    
    var id: String = ""
    var clubId: String = ""
    var color: String = ""
    var cost: String = ""
    var createdtimestamp : String = ""
    var imageid : String = ""
    var memberName: String = ""
    var desc: String = ""
    var imageurl: String = ""
    var planInterval: String = ""
    var plan_sub_interval : String = ""
    var isActive: String = ""
    var planName: String = ""
    var profile_icon_url: String = ""
    var members : [User] = []
    var status: String = ""
    var requires_invite: String = "0"
    var upgradeText: String = "0"

    override init() {
        
    }
    init(dictJSON:NSDictionary) {
        
        self.upgradeText = dictJSON.getString(key: "upgrade_text")
        self.requires_invite = dictJSON.getString(key: "requires_invite")
        self.status = dictJSON.getString(key: "status")
        self.id = dictJSON.getString(key: "id")
        self.clubId = dictJSON.getString(key: "clubid")
        self.color = dictJSON.getString(key: "color")
        self.createdtimestamp = dictJSON.getString(key: "canuploadvideos")
        self.cost = dictJSON.getString(key: "cost")
        self.imageid = dictJSON.getString(key: "imageid")
        self.memberName = dictJSON.getString(key: "membername")
        self.desc = dictJSON.getString(key: "description")
        self.imageurl = dictJSON.getString(key: "imageurl")
        self.planInterval = dictJSON.getString(key: "paymentinterval")
        self.plan_sub_interval = dictJSON.getString(key: "plan_sub_interval")
        self.isActive = dictJSON.getString(key: "isactive")
        self.planName = dictJSON.getString(key: "planname")
        self.profile_icon_url = dictJSON.getString(key: "profile_icon_url")
        
        members = [User]()
        if let membersArray = dictJSON.getArray(key: "members") as? NSArray {
            for dic in membersArray{
                let value = User.init(fromDictionary: dic as! [String : Any])
                members.append(value)
            }
        }
    }
}
class ScreenPermissions: NSObject {

    var id: String = ""
    var calendarscreen: String = ""
    var clubinformation: String = ""
    var clubplanid: String = ""
    var createdtimestamp: String = ""
    var documentsscreen: String = ""
    var feedscreen: String = ""
    var forumscreen: String = ""
    var imagescreen: String = ""
    var isactive: String = ""
    var memberscreen: String = ""
    var messagescreen: String = ""
    var modifiedtimestamp: String = ""
    var modulesscreen: String = ""
    var socialscreen: String = ""
    var videosscreen: String = ""
    var createFeedPost: String = ""
    var addFeedComments: String = ""
    var webpage: [String: Any] = [:]
    var product: [String: Any] = [:]
    var referScreen: [String: Any] = [:]
    var dynamicCourse: [String: Any] = [:]
    var contactMeScreen: String = ""
    var chatShowAllMembers: String = ""
    var chatScreen: String = ""

    /*"webpage" : {
    "223" : "1",
    "254" : "1",
    "255" : "1"
    },

    "product" : {
    "18" : "1"
    }*/

    override init() {
        
    }
     
    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.calendarscreen = dictJSON.getString(key: "calendarscreen")
        self.clubinformation = dictJSON.getString(key: "clubinformation")
        self.clubplanid = dictJSON.getString(key: "clubplanid")
        self.createdtimestamp = dictJSON.getString(key: "createdtimestamp")
        self.documentsscreen = dictJSON.getString(key: "documentsscreen")
        self.feedscreen = dictJSON.getString(key: "feedscreen")
        self.forumscreen = dictJSON.getString(key: "forumscreen")
        self.imagescreen = dictJSON.getString(key: "imagescreen")
        self.isactive = dictJSON.getString(key: "isactive")
        self.memberscreen = dictJSON.getString(key: "memberscreen")
        self.messagescreen = dictJSON.getString(key: "messagescreen")
        self.modifiedtimestamp = dictJSON.getString(key: "modifiedtimestamp")
        self.modulesscreen = dictJSON.getString(key: "modulesscreen")
        self.socialscreen = dictJSON.getString(key: "socialscreen")
        self.videosscreen = dictJSON.getString(key: "videosscreen")
        self.createFeedPost = dictJSON.getString(key: "canpostfeeds")
        self.addFeedComments = dictJSON.getString(key: "canpostcomments")
        self.contactMeScreen = dictJSON.getString(key: "contactme_screen")
        self.chatShowAllMembers = dictJSON.getString(key: "chat_show_all_members")
        self.chatScreen = dictJSON.getString(key: "chat_screen")

        if let product = dictJSON["product"] as? [String: Any] {
            self.product = product
        }
        if let webpage = dictJSON["webpage"] as? [String: Any] {
            self.webpage = webpage
        }
        if let referScreen = dictJSON["refer_screen"] as? [String: Any] {
            self.referScreen = referScreen
        }
        if let dynamicCourse = dictJSON["dynamic_course"] as? [String: Any] {
            self.dynamicCourse = dynamicCourse
        }
    }
}

class Role: NSObject {
    
    var key: String?
    var value: String?
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String: Any]) {
        key = dictionary["id"] as? String
        value = dictionary["role_name"] as? String
    }
}

class UserPointDetails : NSObject {

    var clubId: String = ""
    var account_id: String = ""
    var action: String = ""
    var desc: String = ""
    var reward_balance: String = ""

    override init() {
        
    }
    init(dictJSON:NSDictionary) {
        
        self.clubId = dictJSON.getString(key: "club_id")
        self.account_id = dictJSON.getString(key: "account_id")
        self.action = dictJSON.getString(key: "action")
        self.desc = dictJSON.getString(key: "description")//"token_balance")
        self.reward_balance = dictJSON.getString(key: "reward_balance")
    }
}

class WalletDetails : NSObject {
    
    var id: String = ""
    var account_id: String = ""
    var created: String = ""
    var token_balance: String = ""
    var transactionType: String = ""
    var direction: String = ""
    var payment_type: String = ""
    var paypal_transaction_id: String = ""
    var title: String = ""
    var type: String = ""

    var currency_name: String = ""
    var currency_name_short: String = ""
    var coins: String = ""

    override init() {
        
    }
    init(dictJSON:NSDictionary) {
        
        self.currency_name = dictJSON.getString(key: "currency_name")
        self.currency_name_short = dictJSON.getString(key: "currency_name_short")

        self.id = dictJSON.getString(key: "id")
        self.account_id = dictJSON.getString(key: "account_id")
        self.created = dictJSON.getString(key: "created")
        self.token_balance = dictJSON.getString(key: "account_balance")//"token_balance")
//        if self.token_balance.isEmpty
//        {
//            self.token_balance = dictJSON.getString(key: "coins")
//        }
        self.transactionType = dictJSON.getString(key: "tansactionType")
        self.direction = dictJSON.getString(key: "direction")
        self.payment_type = dictJSON.getString(key: "payment_type")
        self.paypal_transaction_id = dictJSON.getString(key: "paypal_transaction_id")
        self.title = dictJSON.getString(key: "title")
        self.type = dictJSON.getString(key: "type")
        self.type = dictJSON.getString(key: "type")
        self.coins = dictJSON.getString(key: "coins")

    }
}

class InAppPurchaseSubscription : NSObject {
    
    var product_id: String = ""
    var purchase_date: String = ""
    var expires_date: String = ""
    var original_purchase_date: String = ""
    var transaction_id: String = ""
    var auto_renew_status: String = ""
    var expiration_intent: String = ""
    var auto_renew_product_id: String = ""
    
    
    override init() {
        
    }
    init (product_id: String,purchase_date: String,expires_date: String,original_purchase_date: String,transaction_id: String,auto_renew_status: String,expiration_intent: String,auto_renew_product_id: String) {
        
        self.product_id = product_id
        self.purchase_date = purchase_date
        self.expires_date = expires_date
        self.original_purchase_date = original_purchase_date
        self.transaction_id = transaction_id
        self.auto_renew_status = auto_renew_status
        self.expiration_intent = expiration_intent
        self.auto_renew_product_id = auto_renew_product_id
    }
}
