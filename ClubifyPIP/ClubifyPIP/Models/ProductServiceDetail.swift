//
//  ProductServiceDetail.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation

class ProductServiceDetail : NSObject{

    var callPhoneNumber : String?
    var callToActionButton : String?
    var caption : String?
    var cost : String?
    var points: String?
    var created : String?
    var deleted : String?
    var descriptionField : String?
    var emails : String?
    var id : String?
    var memberDetails : [MemberList]!
    var phoneNumber : String?
    var physicalProduct : String?
    var productPackageId : String?
    var sendEmailNotification : String?
    var sendNotification : String?
    var title : String?
    var url : String?
    var visibility : String?
    var webLink : String?
    var membershipLevel: [MembershipLevel]?
    var productId: String?
    
    var isPublic: String?
    var subCategory: [SubCategoryDetail]?
    var parentCategory: [ParentCategoryDetail]?
    var address: String?
    var longitude: String?
    var latitude: String?
    var company: String?
    var name: String?
    var PAWebSiteLink: String?
    var PAPhoneNo: String?
    var clicks: String?
    var owner_id: String?
    var priceDisplayOnly: String?
    
    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){

        priceDisplayOnly = dictionary["price_display_only"] as? String
        owner_id = dictionary["owner_id"] as? String
        clicks = dictionary["clicks"] as? String
        isPublic = dictionary["is_public"] as? String
        address = dictionary["address"] as? String
        longitude = dictionary["longitude"] as? String
        latitude = dictionary["latitude"] as? String
        name = dictionary["NAME"] as? String
        company = dictionary["company"] as? String
        PAWebSiteLink = dictionary["pa_webiste"] as? String
        PAPhoneNo = dictionary["pa_phone"] as? String

        if let data = dictionary["sub_category"] as? [[String: Any]] {
            var arrSubCategoryDetail: [SubCategoryDetail] = []
            for permData in data {
                arrSubCategoryDetail.append(SubCategoryDetail(fromDictionary: permData))
            }
            subCategory = arrSubCategoryDetail
        }
        
        if let data = dictionary["parent_category"] as? [[String: Any]] {
            var arrParentCategoryDetail: [ParentCategoryDetail] = []
            for permData in data {
                arrParentCategoryDetail.append(ParentCategoryDetail(fromDictionary: permData))
            }
            parentCategory = arrParentCategoryDetail
        }
        productId = dictionary["product_id"] as? String
        callPhoneNumber = dictionary["call_phone_number"] as? String
        callToActionButton = dictionary["call_to_action_button"] as? String
        caption = dictionary["caption"] as? String
        cost = dictionary["cost"] as? String
        points = dictionary["points"] as? String
        created = dictionary["created"] as? String
        deleted = dictionary["deleted"] as? String
        descriptionField = dictionary["description"] as? String
        emails = dictionary["emails"] as? String
        id = dictionary["id"] as? String
        phoneNumber = dictionary["phone_number"] as? String
        physicalProduct = dictionary["physical_product"] as? String
        productPackageId = dictionary["product_package_id"] as? String
        sendEmailNotification = dictionary["send_email_notification"] as? String
        sendNotification = dictionary["send_notification"] as? String
        title = dictionary["title"] as? String
        url = dictionary["url"] as? String
        visibility = dictionary["visibility"] as? String
        webLink = dictionary["web_link"] as? String
        
        memberDetails = [MemberList]()
        if let memberDetailsArray = dictionary["memberDetails"] as? [NSDictionary]{
            for dic in memberDetailsArray{
                let value = MemberList(dictJSON: dic)
                memberDetails.append(value)
            }
        }
        
        if let data = dictionary["membership_level"] as? [[String: Any]] {
            var arrMembershipLevel: [MembershipLevel] = []
            for permData in data {
                arrMembershipLevel.append(MembershipLevel(fromDictionary: permData))
            }
            membershipLevel = arrMembershipLevel
        }
    }
}

class SubCategoryDetail : NSObject{

    var parentCategoryId : String?
    var created : String?
    var deleted : String?
    var updated : String?
    var id : String?
    var name: String?

    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){

        parentCategoryId = dictionary["parent_category_id"] as? String
        created = dictionary["created"] as? String
        deleted = dictionary["deleted"] as? String
        updated = dictionary["updated"] as? String
        id = dictionary["id"] as? String
        name = dictionary["name"] as? String
    }
}

class ParentCategoryDetail : NSObject{

    var created : String?
    var deleted : String?
    var updated : String?
    var id : String?
    var name: String?
    var subCategory: [SubCategoryDetail]?

    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){

        created = dictionary["created"] as? String
        deleted = dictionary["deleted"] as? String
        updated = dictionary["updated"] as? String
        id = dictionary["id"] as? String
        name = dictionary["name"] as? String
        
        if let data = dictionary["sub_category"] as? [[String: Any]] {
            var arrSubCategoryDetail: [SubCategoryDetail] = []
            for permData in data {
                arrSubCategoryDetail.append(SubCategoryDetail(fromDictionary: permData))
            }
            subCategory = arrSubCategoryDetail
        }
    }
}

