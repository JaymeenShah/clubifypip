//
//  User.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/09/21.
//

import Foundation
class User : NSObject, NSCoding  {

    // required properties var id: String = ""
    var userID: String?
    var sessionToken: String?
    // optional values
    var username: String?
    var firstname: String?
    var lastname: String?
    var profileImageURL: String?
    var email: String?
    var gender: String?
    var bio: String?
    var isMultiClub: String?
    var phone: String?
    var firebaseId: String?
    var firebasePassword: String?
    var membershipLevel: String?
    var profileVideoURL: String?
    var followStatus: Int?
    var followersCount: Int?
    var totalFeeds: Int?

    struct KeyUserProfile    {
        static let userID = "id"
        static let sessionToken = "sessionToken"
        static let username = "username"
        static let firstname = "firstname"
        static let lastname = "lastname"
        static let profileImageURL = "profilepicture"
        static let email = "email"
        static let gender = "gender"
        static let bio = "bio"
        static let isMultiClub = "is_multiclub"
        static let phone = "phone"
        static let firebaseId = "firebase_id"
        static let firebasePassword = "firebase_password"
        static let membershipLevel = "membership_level"
        static let profileVideoURL = "profile_video"
        static let followStatus = "follow_status"
        static let followersCount = "followers_count"
        static let totalFeeds = "total_feeds"
   }
    override init() {
    
    }
    
    init(fromDictionary dictionary: [String:Any]){
        
        firebaseId = dictionary["firebase_id"] as? String
        firebasePassword = dictionary["firebase_password"] as? String

        userID = dictionary["id"] as? String
        sessionToken = dictionary["sessionToken"] as? String
        username = dictionary["username"] as? String
        firstname = dictionary["firstname"] as? String
        lastname = dictionary["lastname"] as? String
        profileImageURL = dictionary["profilepicture"] as? String
        email = dictionary["email"] as? String
        gender = dictionary["gender"] as? String
        bio = dictionary["biography"] as? String
        isMultiClub = dictionary["is_multiclub"] as? String
        phone = dictionary["phone"] as? String
        membershipLevel = dictionary["membership_level"] as? String
        profileVideoURL = dictionary["profile_video"] as? String
        followStatus = dictionary["follow_status"] as? Int
        followersCount = dictionary["followers_count"] as? Int
        totalFeeds = dictionary["total_feeds"] as? Int
    }

      func toDictionary() -> [String:Any]
      {
        var dictionary = [String:Any]()
        if userID != nil{
            dictionary["id"] = userID
        }
        if sessionToken != nil{
            dictionary["sessionToken"] = sessionToken
        }
        if username != nil{
            dictionary["username"] = username
        }
        if firstname != nil{
            dictionary["firstname"] = firstname
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if lastname != nil{
            dictionary["lastname"] = lastname
        }
        if profileImageURL != nil{
            dictionary["profilepicture"] = profileImageURL
        }
        if email != nil{
            dictionary["email"] = email
        }
        if bio != nil{
            dictionary["biography"] = bio
        }
        if isMultiClub != nil{
            dictionary["is_multiclub"] = isMultiClub
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if firebaseId != nil{
            dictionary["firebase_id"] = phone
        }
        if firebasePassword != nil{
            dictionary["firebase_password"] = phone
        }
        if membershipLevel != nil{
            dictionary["membership_level"] = phone
        }
        if profileVideoURL != nil{
            dictionary["profile_video"] = profileVideoURL
        }
        if followStatus != nil{
            dictionary["follow_status"] = followStatus
        }
        if followersCount != nil{
            dictionary["followers_count"] = followersCount
        }
        if totalFeeds != nil{
            dictionary["total_feeds"] = totalFeeds
        }
        return dictionary
    }
    
    init(id: String, sessionToken: String, username :String,firstname: String, lastname: String, profileImageURL :String, email: String, gender :String ,bio :String, isMultiClub: String, phone: String, firebaseId: String, firebasePassword: String, membershipLevel: String, profileVideoURL: String ) {
        
        self.userID = id
        self.sessionToken = sessionToken
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.profileImageURL = profileImageURL
        self.email = email
        self.gender = gender
        self.bio = bio
        self.isMultiClub = isMultiClub
        self.phone = phone
        self.firebaseId = firebaseId
        self.firebasePassword = firebasePassword
        self.membershipLevel = membershipLevel
        self.profileVideoURL = profileVideoURL

    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(userID, forKey: KeyUserProfile.userID)
        aCoder.encode(sessionToken, forKey: KeyUserProfile.sessionToken)
        aCoder.encode(username, forKey: KeyUserProfile.username)
        aCoder.encode(firstname, forKey: KeyUserProfile.firstname)
        aCoder.encode(lastname, forKey: KeyUserProfile.lastname)
        aCoder.encode(profileImageURL, forKey: KeyUserProfile.profileImageURL)
        aCoder.encode(email, forKey: KeyUserProfile.email)
        aCoder.encode(gender, forKey: KeyUserProfile.gender)
        aCoder.encode(bio, forKey: KeyUserProfile.bio)
        aCoder.encode(isMultiClub, forKey: KeyUserProfile.isMultiClub)
        aCoder.encode(phone, forKey: KeyUserProfile.phone)
        aCoder.encode(firebaseId, forKey: KeyUserProfile.firebaseId)
        aCoder.encode(firebasePassword, forKey: KeyUserProfile.firebasePassword)
        aCoder.encode(membershipLevel, forKey: KeyUserProfile.membershipLevel)
        aCoder.encode(profileVideoURL, forKey: KeyUserProfile.profileVideoURL)
        aCoder.encode(followersCount, forKey: KeyUserProfile.followersCount)
        aCoder.encode(followStatus, forKey: KeyUserProfile.followStatus)
        aCoder.encode(totalFeeds, forKey: KeyUserProfile.totalFeeds)

    }
    required convenience init?(coder aDecoder: NSCoder)
    {
        let userID = aDecoder.decodeObject(forKey: KeyUserProfile.userID) as? String
        let sessionToken = aDecoder.decodeObject(forKey: KeyUserProfile.sessionToken) as? String
        let username = aDecoder.decodeObject(forKey: KeyUserProfile.username) as? String
        let firstname = aDecoder.decodeObject(forKey: KeyUserProfile.firstname) as? String
        let lastname = aDecoder.decodeObject(forKey: KeyUserProfile.lastname) as? String
        let profileImageURL = aDecoder.decodeObject(forKey: KeyUserProfile.profileImageURL) as? String
        let email = aDecoder.decodeObject(forKey: KeyUserProfile.email) as? String
        let gender = aDecoder.decodeObject(forKey: KeyUserProfile.gender) as? String
        let bio = aDecoder.decodeObject(forKey: KeyUserProfile.bio) as? String
        let isMultiClub = aDecoder.decodeObject(forKey: KeyUserProfile.isMultiClub) as? String
        let phone = aDecoder.decodeObject(forKey: KeyUserProfile.phone) as? String
        let firebaseId = aDecoder.decodeObject(forKey: KeyUserProfile.firebaseId) as? String
        let firebasePassword = aDecoder.decodeObject(forKey: KeyUserProfile.firebasePassword) as? String
        let membershipLevel = aDecoder.decodeObject(forKey: KeyUserProfile.membershipLevel) as? String
        let profileVideoURL = aDecoder.decodeObject(forKey: KeyUserProfile.profileVideoURL) as? String

        self.init(id: userID ?? "", sessionToken: sessionToken ?? "", username: username ?? "", firstname: firstname ?? "", lastname: lastname ?? "", profileImageURL: profileImageURL ?? "", email: email ?? "", gender: gender ?? "", bio: bio ?? "", isMultiClub: isMultiClub ?? "", phone: phone ?? "", firebaseId: firebaseId ?? "", firebasePassword: firebasePassword ?? "", membershipLevel: membershipLevel ?? "", profileVideoURL: profileVideoURL ?? "")
    }
}

class UserDetail: NSObject  {
    
    // required properties var id: String = ""
    var userID: String?
    var sessionToken: String?
    // optional values
    var username: String?
    var firstname: String?
    var lastname: String?
    var profileImageURL: String?
    var email: String?
    var gender: String?
    var bio: String?
    var isMultiClub: String?
    var phone: String?
    var firebaseId: String?
    var firebasePassword: String?
    var profileVideoURL: String?
    var followStatus: Int?
    var followersCount: Int?
    var totalFeeds: Int?
    var createdAt: String?
        
    override init() {
        
    }
    
    init(fromDictionary dictionary: [String:Any]){
        createdAt = dictionary["created_at"] as? String
        userID = dictionary["id"] as? String
        sessionToken = dictionary["sessionToken"] as? String
        username = dictionary["username"] as? String
        firstname = dictionary["firstname"] as? String
        lastname = dictionary["lastname"] as? String
        profileImageURL = dictionary["profilepicture"] as? String
        email = dictionary["email"] as? String
        gender = dictionary["gender"] as? String
        bio = dictionary["bio"] as? String
        isMultiClub = dictionary["is_multiclub"] as? String
        phone = dictionary["phone"] as? String

        firebaseId = dictionary["firebase_id"] as? String
        firebasePassword = dictionary["firebase_password"] as? String
        profileVideoURL = dictionary["profile_video"] as? String
        followStatus = dictionary["follow_status"] as? Int
        followersCount = dictionary["followers_count"] as? Int
        totalFeeds = dictionary["total_feeds"] as? Int
    }

      func toDictionary() -> [String:Any]
      {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if userID != nil{
            dictionary["id"] = userID
        }
        if sessionToken != nil{
            dictionary["sessionToken"] = sessionToken
        }
        if username != nil{
            dictionary["username"] = username
        }
        if firstname != nil{
            dictionary["firstname"] = firstname
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if lastname != nil{
            dictionary["lastname"] = lastname
        }
        if profileImageURL != nil{
            dictionary["profilepicture"] = profileImageURL
        }
        if email != nil{
            dictionary["email"] = email
        }
        if bio != nil{
            dictionary["bio"] = bio
        }
        if isMultiClub != nil{
            dictionary["is_multiclub"] = isMultiClub
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if firebaseId != nil{
            dictionary["firebase_id"] = phone
        }
        if firebasePassword != nil{
            dictionary["firebase_password"] = phone
        }
        if profileVideoURL != nil{
            dictionary["profile_video"] = profileVideoURL
        }
        if followStatus != nil{
            dictionary["follow_status"] = followStatus
        }
        if followersCount != nil{
            dictionary["followers_count"] = followersCount
        }
        if totalFeeds != nil{
            dictionary["total_feeds"] = totalFeeds
        }
        return dictionary
    }
}
