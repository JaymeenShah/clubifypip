//
//  UsersDataModel.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/09/21.
//

import Foundation
struct UsersDataModel {
    
    var id:String = ""
    var firstname: String = ""
    var username: String = ""
    var profilepicture: String = ""
    var biography: String = ""
    var lastname: String = ""
    var gender: String = ""
    var email: String = ""
    var phone: String = ""
}
