//
//  Comments.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
import UIKit

class Comments: NSObject {
    
    var comment_id : String?
    var reply : String?
    var id : String?
    var user_id : String?
    var feed_id : String?
    var club_id : String?
    var comment : String?
    var created : String?
    var updated : String?
    var deleted : String?
    var userDetails : UserDetails?
    var replies : [Replies]?
    var hasRepies: Bool = false
    var comment_like: Bool = false
    var comment_likes: Int = 0

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String: Any]) {
        
        comment_like = dictionary["comment_like"] as? Bool ?? false
        comment_likes = dictionary["comment_likes"] as? Int ?? 0

        reply = dictionary["reply"] as? String
        comment_id = dictionary["comment_id"] as? String
        user_id = dictionary["user_id"] as? String
        id = dictionary["id"] as? String
        feed_id = dictionary["feed_id"] as? String
        club_id = dictionary["club_id"] as? String
        comment = dictionary["comment"] as? String
        created = dictionary["created"] as? String
        updated = dictionary["updated"] as? String
        deleted = dictionary["deleted"] as? String
        if let userData = dictionary["userDetails"] as? [String: Any] {
            userDetails = UserDetails(fromDictionary: userData)
        }
        if let data = dictionary["replies"] as? [[String: Any]] {
            var arrReplyList: [Replies] = []
            for permData in data {
                hasRepies = true
                arrReplyList.append(Replies(fromDictionary: permData))
            }
            replies = arrReplyList
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String: Any] {
        var dictionary = [String: Any]()
        if comment_id != nil {
            dictionary["comment_id"] = comment_id
        }
        if reply != nil {
            dictionary["reply"] = reply
        }
        if user_id != nil {
            dictionary["user_id"] = user_id
        }
        if id != nil {
            dictionary["id"] = id
        }
        if feed_id != nil {
            dictionary["feed_id"] = feed_id
        }
        if club_id != nil {
            dictionary["club_id"] = club_id
        }
        if comment != nil {
            dictionary["comment"] = feed_id
        }
        if created != nil {
            dictionary["created"] = created
        }
        if updated != nil {
            dictionary["updated"] = updated
        }
        if deleted != nil {
            dictionary["deleted"] = deleted
        }
        if userDetails != nil {
            dictionary["userDetails"] = userDetails?.toDictionary()
        }
        
        if comment_like != nil {
            dictionary["comment_like"] = comment_like
        }

        if comment_likes != nil {
            dictionary["comment_likes"] = comment_likes
        }
        return dictionary
    }
}

class Replies: NSObject {
    var id : String?
    var user_id : String?
    var comment_id : String?
    var feed_id : String?
    var club_id : String?
    var reply : String?
    var created : String?
    var updated : String?
    var deleted : String?
    var userDetails : UserDetails?
    var reply_like: Bool = false
    var reply_likes: Int = 0
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String: Any]) {
        
        reply_like = dictionary["reply_like"] as? Bool ?? false
        reply_likes = dictionary["reply_likes"] as? Int ?? 0

        comment_id = dictionary["comment_id"] as? String
        user_id = dictionary["user_id"] as? String
        id = dictionary["id"] as? String
        feed_id = dictionary["feed_id"] as? String
        club_id = dictionary["club_id"] as? String
        reply = dictionary["reply"] as? String
        created = dictionary["created"] as? String
        updated = dictionary["updated"] as? String
        deleted = dictionary["deleted"] as? String
        if let userData = dictionary["userDetails"] as? [String: Any] {
            userDetails = UserDetails(fromDictionary: userData)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String: Any] {
        var dictionary = [String: Any]()
        if comment_id != nil {
            dictionary["comment_id"] = comment_id
        }
        if user_id != nil {
            dictionary["user_id"] = user_id
        }
        if id != nil {
            dictionary["id"] = id
        }
        if feed_id != nil {
            dictionary["feed_id"] = feed_id
        }
        if club_id != nil {
            dictionary["club_id"] = club_id
        }
        if reply != nil {
            dictionary["reply"] = reply
        }
        if created != nil {
            dictionary["created"] = created
        }
        if updated != nil {
            dictionary["updated"] = updated
        }
        if deleted != nil {
            dictionary["deleted"] = deleted
        }
        if userDetails != nil {
            dictionary["userDetails"] = userDetails?.toDictionary()
        }
        
        if reply_like != nil {
            dictionary["reply_like"] = reply_like
        }

        if reply_likes != nil {
            dictionary["reply_likes"] = reply_likes
        }
        return dictionary
    }
}

class UserDetails : NSObject {
    var firstname : String?
    var lastname : String?
    var username : String?
    var gender : String?
    var profilepicture : String?
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String: Any]) {
        firstname = dictionary["firstname"] as? String
        lastname = dictionary["lastname"] as? String
        username = dictionary["username"] as? String
        gender = dictionary["gender"] as? String
        profilepicture = dictionary["profilepicture"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String: Any] {
        var dictionary = [String: Any]()
        if firstname != nil {
            dictionary["firstname"] = firstname
        }
        if lastname != nil {
            dictionary["lastname"] = lastname
        }
        if username != nil {
            dictionary["username"] = username
        }
        if gender != nil {
            dictionary["gender"] = gender
        }
        if profilepicture != nil {
            dictionary["profilepicture"] = profilepicture
        }
        return dictionary
    }
}
