//
//  TasklistData.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 19/10/21.
//

import Foundation
import SwiftyJSON

public struct TasklistData {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let clubId = "club_id"
    static let name = "name"
    static let result = "data"
    static let deleted = "deleted"
    static let id = "id"
    static let created = "created"
    static let message = "message"
    static let caption = "caption"
  }

  // MARK: Properties
  public var clubId: String?
  public var name: String?
  public var result: [DataResult]?
  public var deleted: String?
  public var id: String?
  public var created: String?
  public var message: String?
  public var caption: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    clubId = json[SerializationKeys.clubId].string
    name = json[SerializationKeys.name].string
    if let items = json[SerializationKeys.result].array { result = items.map { DataResult(json: $0) } }
    deleted = json[SerializationKeys.deleted].string
    id = json[SerializationKeys.id].string
    created = json[SerializationKeys.created].string
    message = json[SerializationKeys.message].string
    caption = json[SerializationKeys.caption].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = clubId { dictionary[SerializationKeys.clubId] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = result { dictionary[SerializationKeys.result] = value.map { $0.dictionaryRepresentation() } }
    if let value = deleted { dictionary[SerializationKeys.deleted] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = caption { dictionary[SerializationKeys.caption] = value }
    return dictionary
  }

}
