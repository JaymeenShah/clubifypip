//
//  FeedItem.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
import UIKit

class ClubFeedsElement : NSObject {
    var id: String = ""
    var type: String = ""
    var ownerID: String = ""
    var clubID: String = ""
    var created: String = ""
    var desc: String = ""
    var imageURL: String = ""
    var videoURL: String = ""
    var videoThumbnail = UIImage()
    var isDoneThumbnail = false
    var user_firstname: String = ""
    var user_lastname: String = ""
    var user_id: String = ""
    var user_profilepicture: String = ""
    var file_url: String = ""
    var thumbnail_file_url: String = ""
    var file_id: String = ""
    var thumbnail_file_id: String = ""
    var feed_muted : Bool = false
    var feed_like : Bool = false
    var feed_count : Int = 0
    var can_access_image : Bool = false
    var can_access_video : Bool = false
    var can_access_file : Bool = false
    var user_name: String = ""
    var user_gender: String = ""
    var comment_count : Int = 0
    var feed_id : String = ""
    var feed_reported: Bool = false
    var membershipLevel: [MembershipLevel]?
    var planName: String = ""
    var planIconUrl: String = ""
    var disable_likes: String = ""
    var can_share_feed: String = ""
    var live_video_channel: String = ""
    var user_profileVideo: String = ""
    var send_sms: String = ""
    var presigned_url: String = ""
    var enable_comments: String = ""
    var rec_sid: String = ""
    var like_users: [User] = []
    override init() {
        
    }

    init(dictJSON:NSDictionary) {
        
        self.send_sms = dictJSON.getString(key: "send_sms")
        self.user_profileVideo = dictJSON.getString(key: "user_profile_video")
        self.can_share_feed = dictJSON.getString(key: "can_share_feed")
        self.disable_likes = dictJSON.getString(key: "disable_likes")
        self.planIconUrl = dictJSON.getString(key: "plan_image")
        self.planName = dictJSON.getString(key: "plan_name")
        self.id = dictJSON.getString(key: "id")
        self.type = dictJSON.getString(key: "type")
        self.ownerID = dictJSON.getString(key: "owner_id")
        self.clubID = dictJSON.getString(key: "club_id")
        self.created = dictJSON.getString(key: "created")
        self.desc = dictJSON.getString(key: "description")
        self.imageURL = dictJSON.getString(key: "image")
        self.videoURL = dictJSON.getString(key: "video")
        self.user_firstname = dictJSON.getString(key: "user_firstname")
        self.user_lastname = dictJSON.getString(key: "user_lastname")
        self.user_id = dictJSON.getString(key: "user_id")
        self.user_profilepicture = dictJSON.getString(key: "user_profilepicture")
        self.file_url = dictJSON.getString(key: "file_url")
        self.thumbnail_file_url = dictJSON.getString(key: "thumbnail_file_url")
        self.file_id = dictJSON.getString(key: "file_id")
        self.thumbnail_file_id = dictJSON.getString(key: "thumbnail_file_id")
        self.feed_muted = dictJSON.getBool(key: "feed_muted")
        self.feed_like = dictJSON.getBool(key: "feed_like")
        self.feed_count = dictJSON.getInt(key: "feed_count")
        self.can_access_image = dictJSON.getBool(key: "can_access_image")
        self.can_access_video = dictJSON.getBool(key: "can_access_video")
        self.can_access_file = dictJSON.getBool(key: "can_access_file")
        self.user_name = dictJSON.getString(key: "user_name")
        self.user_gender = dictJSON.getString(key: "user_gender")
        self.comment_count = dictJSON.getInt(key: "commentsCount")
        self.feed_id = dictJSON.getString(key: "feed_id")
        self.feed_reported = dictJSON.getBool(key: "feed_reported")
        self.live_video_channel = dictJSON.getString(key: "live_video_channel")
        self.presigned_url = dictJSON.getString(key: "presigned_url")
        self.enable_comments = dictJSON.getString(key: "enable_comments")
        self.rec_sid = dictJSON.getString(key: "rec_sid")

        if let data = dictJSON["membership_level"] as? [[String: Any]] {
            var arrMembershipLevel: [MembershipLevel] = []
            for permData in data {
                arrMembershipLevel.append(MembershipLevel(fromDictionary: permData))
            }
            membershipLevel = arrMembershipLevel
        }
        
        if let data = dictJSON["like_users"] as? [[String: Any]] {
            var arrLikeUsers: [User] = []
            for likeUserData in data {
                arrLikeUsers.append(User(fromDictionary: likeUserData))
            }
            like_users = arrLikeUsers
        }
    }
}
