//
//  SideMenu.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 21/09/21.
//

import Foundation
import UIKit
import SwiftyJSON

//struct SideMenu {
//    var id:String = ""
//    var club_id:String = ""
//    var menu_name:String = ""
//    var menu_unique_id:String = ""
//    var menu_reference_id:String = ""
//    var is_active:String = ""
//}

//MARK: Menu Unique Ids
struct MenuUniquId {
    static let menuWallet = "my_wallet"
    static let clubWall = "club_wall"
    static let videoGallery = "video_gallery"
    static let documentGallery = "document_gallery"
    static let courses = "courses"
    static let privateEvents = "private_events"
    static let memberList = "member_list"
    static let inviteMembers = "invite_members"
    static let clubInformation = "club_information"
    static let chooseMembership = "choose_membership"
    static let upgradeMembership = "upgrade_membership"
    static let clubStore = "club_store"
    static let webPage = "web_page"
    static let taskList = "task_list"
    static let photoGallery = "photo_gallery"
    static let clubPowerups = "club_powerups"
    static let donateNow = "donate_now"
    static let myToDoList = "my_to_do_list"
    static let clubGoals = "club_goals"
    static let productPackage = "product_package"
    static let visualVoicemail = "visual_voicemail"
    static let contest = "contest"
    static let inviteReferFriend = "invite_refer_friend"
    static let homeScreen = "home_screen"
    static let dynamicCourse = "dynamic_course"
    static let contactMe = "contact_me"
    static let chatScreen = "chat_screen"
    static let leaderboard = "game_leaderboard"
    static let chatrooms = "predefined_chat"
    static let iDeserveItHomeScreen = "landing_screen"
    static let SMSText = "sms_text"

}

public struct SideMenu {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let deleted = "deleted"
        static let updatedAt = "updated_at"
        static let isActive = "is_active"
        static let tasklistData = "tasklist_data"
        static let visibleTo = "visible_to"
        static let menuUniqueId = "menu_unique_id"
        static let menuOrder = "menu_order"
        static let clubId = "club_id"
        static let storeData = "store_data"
        static let id = "id"
        static let webpageData = "webpage_data"
        static let createdAt = "created_at"
        static let menuName = "menu_name"
        static let menuReferenceId = "menu_reference_id"
        static let productPackageData = "product_package_data"
        static let referData = "refer_data"
        static let dynamicCourseData = "dynamic_course_data"
        static let sortBy = "sort_by"
    }
    
    // MARK: Properties
    public var deleted: String?
    public var updatedAt: String?
    public var isActive: String?
    public var tasklistData: TasklistData?
    public var visibleTo: String?
    public var menuUniqueId: String?
    public var menuOrder: String?
    public var clubId: String?
    public var storeData: StoreData?
    public var id: String?
    public var webpageData: WebpageData?
    public var createdAt: String?
    public var menuName: String?
    public var menuIcon: UIImage?
    public var menuFilledIcon: UIImage?
    public var menuIconForSidebar: UIImage?
    public var menuReferenceId: String?
    public var productPackageData: WebpageData?
    public var referData: WebpageData?
    public var dynamicCourseData: WebpageData?
    public var sortBy: String?

    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        deleted = json[SerializationKeys.deleted].string
        updatedAt = json[SerializationKeys.updatedAt].string
        isActive = json[SerializationKeys.isActive].string
        tasklistData = TasklistData(json: json[SerializationKeys.tasklistData])
        visibleTo = json[SerializationKeys.visibleTo].string
        menuUniqueId = json[SerializationKeys.menuUniqueId].string
        menuOrder = json[SerializationKeys.menuOrder].string
        clubId = json[SerializationKeys.clubId].string
        storeData = StoreData(json: json[SerializationKeys.storeData])
        id = json[SerializationKeys.id].string
        webpageData = WebpageData(json: json[SerializationKeys.webpageData])
        createdAt = json[SerializationKeys.createdAt].string
        menuName = json[SerializationKeys.menuName].string
        menuIcon = self.getMenuIconFromUniquId(menuUniqueId ?? "")
        menuIconForSidebar = self.getMenuNewIconFromUniquId(menuUniqueId ?? "")
        menuFilledIcon = self.getMenuFilledNewIconFromUniquId(menuUniqueId ?? "")

        menuReferenceId = json[SerializationKeys.menuReferenceId].string
        productPackageData = WebpageData(json: json[SerializationKeys.productPackageData])
        referData = WebpageData(json: json[SerializationKeys.referData])
        dynamicCourseData = WebpageData(json: json[SerializationKeys.dynamicCourseData])
        sortBy = json[SerializationKeys.sortBy].string

    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = deleted { dictionary[SerializationKeys.deleted] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = isActive { dictionary[SerializationKeys.isActive] = value }
        if let value = tasklistData { dictionary[SerializationKeys.tasklistData] = value.dictionaryRepresentation() }
        if let value = visibleTo { dictionary[SerializationKeys.visibleTo] = value }
        if let value = menuUniqueId { dictionary[SerializationKeys.menuUniqueId] = value }
        if let value = menuOrder { dictionary[SerializationKeys.menuOrder] = value }
        if let value = clubId { dictionary[SerializationKeys.clubId] = value }
        if let value = storeData { dictionary[SerializationKeys.storeData] = value.dictionaryRepresentation() }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = webpageData { dictionary[SerializationKeys.webpageData] = value.dictionaryRepresentation() }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = menuName { dictionary[SerializationKeys.menuName] = value }
        if let value = menuReferenceId { dictionary[SerializationKeys.menuReferenceId] = value }
        if let value = productPackageData { dictionary[SerializationKeys.productPackageData] = value.dictionaryRepresentation() }
        if let value = referData { dictionary[SerializationKeys.referData] = value.dictionaryRepresentation() }
        if let value = dynamicCourseData { dictionary[SerializationKeys.dynamicCourseData] = value.dictionaryRepresentation() }
        if let value = sortBy { dictionary[SerializationKeys.sortBy] = value }

        
        return dictionary
    }
    fileprivate func getMenuIconFromUniquId(_ uniqueId : String) -> UIImage? {
        switch uniqueId {
        case MenuUniquId.menuWallet: return UIImage(named: "icon_walletmenu") ?? nil
        case MenuUniquId.clubWall: return UIImage(named: "icon_forum") ?? nil
        case MenuUniquId.videoGallery: return UIImage(named: "video_menu") ?? nil
        case MenuUniquId.documentGallery: return UIImage(named: "documents_menu") ?? nil
        case MenuUniquId.courses: return UIImage(named: "icon_folder") ?? nil
        case MenuUniquId.privateEvents: return UIImage(named: "icon_eventCalender") ?? nil
        case MenuUniquId.memberList: return UIImage(named: "icon_memberList") ?? nil
        case MenuUniquId.inviteMembers: return UIImage(named: "icon_invitemenu") ?? nil
        case MenuUniquId.clubInformation: return UIImage(named: "icon_about") ?? nil
        case MenuUniquId.chooseMembership: return UIImage(named: "icon_powerupmenu") ?? nil
        case MenuUniquId.upgradeMembership: return UIImage(named: "icon_powerupmenu") ?? nil
        case MenuUniquId.clubStore: return UIImage(named: "clubify_store_icon") ?? nil
        case MenuUniquId.webPage: return UIImage(named: "icon_webpage") ?? nil
        case MenuUniquId.taskList: return UIImage(named: "icon_taskListing") ?? nil
        case MenuUniquId.photoGallery: return UIImage(named: "photogallery_menu") ?? nil
        case MenuUniquId.clubPowerups: return UIImage(named: "icon_powerupmenu") ?? nil
        case MenuUniquId.donateNow: return UIImage(named: "icon_donate") ?? nil
        case MenuUniquId.myToDoList: return UIImage(named: "icon_taskListing") ?? nil
        case MenuUniquId.clubGoals: return UIImage(named: "icon_taskListing") ?? nil
        case MenuUniquId.productPackage: return UIImage(named: "clubify_store_icon") ?? nil
        case MenuUniquId.contest: return UIImage(named: "clubify_store_icon") ?? nil
        case MenuUniquId.visualVoicemail: return UIImage(named: "icon_forum") ?? nil
        case MenuUniquId.inviteReferFriend: return UIImage(named: "icon_memberList") ?? nil
        case MenuUniquId.homeScreen: return UIImage(named: "icon_forum") ?? nil
        case MenuUniquId.dynamicCourse: return UIImage(named: "icon_folder") ?? nil
        case MenuUniquId.contactMe: return UIImage(named: "icon_memberList") ?? nil
        case MenuUniquId.chatScreen: return UIImage(named: "icon_forum") ?? nil
        case MenuUniquId.leaderboard: return UIImage(named: "icon_taskListing") ?? nil
        case MenuUniquId.chatrooms: return UIImage(named: "icon_forum") ?? nil
        case MenuUniquId.iDeserveItHomeScreen: return UIImage(named: "icon_forum") ?? nil
        case MenuUniquId.SMSText: return UIImage(named: "icon_forum") ?? nil

        default: return nil
        }
    }
    
    fileprivate func getMenuNewIconFromUniquId(_ uniqueId : String) -> UIImage? {
        switch uniqueId {
        case MenuUniquId.menuWallet: return UIImage(named: "icon_wallet") ?? nil
        case MenuUniquId.clubWall: return UIImage(named: "icon_announcements") ?? nil
        case MenuUniquId.videoGallery: return UIImage(named: "icon_videoGallery") ?? nil
        case MenuUniquId.documentGallery: return UIImage(named: "icon_gallery") ?? nil
        case MenuUniquId.courses: return UIImage(named: "icon_content") ?? nil
        case MenuUniquId.privateEvents: return UIImage(named: "icon_events") ?? nil
        case MenuUniquId.memberList: return UIImage(named: "icon_members") ?? nil
        case MenuUniquId.inviteMembers: return UIImage(named: "icon_inviteMembers") ?? nil
        case MenuUniquId.clubInformation: return UIImage(named: "icon_information") ?? nil
        case MenuUniquId.chooseMembership: return UIImage(named: "icon_powerups") ?? nil
        case MenuUniquId.upgradeMembership: return UIImage(named: "icon_powerups") ?? nil
        case MenuUniquId.clubStore: return UIImage(named: "icon_store") ?? nil
        case MenuUniquId.webPage: return UIImage(named: "icon_live") ?? nil
        case MenuUniquId.taskList: return UIImage(named: "icon_taskList") ?? nil
        case MenuUniquId.photoGallery: return UIImage(named: "icon_photoGallery") ?? nil
        case MenuUniquId.clubPowerups: return UIImage(named: "icon_powerups") ?? nil
        case MenuUniquId.donateNow: return UIImage(named: "icon_donateNow") ?? nil
        case MenuUniquId.myToDoList: return UIImage(named: "icon_todoList") ?? nil
        case MenuUniquId.clubGoals: return UIImage(named: "icon_clubGoals") ?? nil
        case MenuUniquId.productPackage: return UIImage(named: "icon_globe") ?? nil
        case MenuUniquId.contest: return UIImage(named: "icon_contestRewards") ?? nil
        case MenuUniquId.visualVoicemail: return UIImage(named: "icon_announcements") ?? nil
        case MenuUniquId.inviteReferFriend: return UIImage(named: "icon_referAFriend") ?? nil
        case MenuUniquId.homeScreen: return UIImage(named: "icon_myClubs") ?? nil
        case MenuUniquId.dynamicCourse: return UIImage(named: "icon_content") ?? nil
        case MenuUniquId.contactMe: return UIImage(named: "icon_phone") ?? nil
        case MenuUniquId.chatScreen: return UIImage(named: "icon_messages") ?? nil
        case MenuUniquId.leaderboard: return UIImage(named: "icon_leaderboard") ?? nil
        case MenuUniquId.chatrooms: return UIImage(named: "icon_messages") ?? nil
        case MenuUniquId.iDeserveItHomeScreen: return UIImage(named: "icon_myClubs") ?? nil
        case MenuUniquId.SMSText: return UIImage(named: "icon_sms") ?? nil

        default: return nil
        }
    }
    
    fileprivate func getMenuFilledNewIconFromUniquId(_ uniqueId : String) -> UIImage? {
        switch uniqueId {
        case MenuUniquId.menuWallet: return UIImage(named: "icon_wallet") ?? nil
        case MenuUniquId.clubWall: return UIImage(named: "icon_announcementsBold") ?? nil
        case MenuUniquId.videoGallery: return UIImage(named: "icon_videoGalleryBold") ?? nil
        case MenuUniquId.documentGallery: return UIImage(named: "icon_galleryBold") ?? nil
        case MenuUniquId.courses: return UIImage(named: "icon_contentBold") ?? nil
        case MenuUniquId.privateEvents: return UIImage(named: "icon_eventsBold") ?? nil
        case MenuUniquId.memberList: return UIImage(named: "icon_members") ?? nil
        case MenuUniquId.inviteMembers: return UIImage(named: "icon_inviteMembersBold") ?? nil
        case MenuUniquId.clubInformation: return UIImage(named: "icon_information") ?? nil
        case MenuUniquId.chooseMembership: return UIImage(named: "icon_powerups") ?? nil
        case MenuUniquId.upgradeMembership: return UIImage(named: "icon_powerups") ?? nil
        case MenuUniquId.clubStore: return UIImage(named: "icon_storeBold") ?? nil
        case MenuUniquId.webPage: return UIImage(named: "icon_liveBold") ?? nil
        case MenuUniquId.taskList: return UIImage(named: "icon_taskListBold") ?? nil
        case MenuUniquId.photoGallery: return UIImage(named: "icon_photoGalleryBold") ?? nil
        case MenuUniquId.clubPowerups: return UIImage(named: "icon_powerups") ?? nil
        case MenuUniquId.donateNow: return UIImage(named: "icon_donateNowBold") ?? nil
        case MenuUniquId.myToDoList: return UIImage(named: "icon_todoListBold") ?? nil
        case MenuUniquId.clubGoals: return UIImage(named: "icon_clubGoalsBold") ?? nil
        case MenuUniquId.productPackage: return UIImage(named: "icon_globe_bold") ?? nil
        case MenuUniquId.contest: return UIImage(named: "icon_contestRewardsBold") ?? nil
        case MenuUniquId.visualVoicemail: return UIImage(named: "icon_announcementsBold") ?? nil
        case MenuUniquId.inviteReferFriend: return UIImage(named: "icon_referAFriendBold") ?? nil
        case MenuUniquId.homeScreen: return UIImage(named: "icon_myClubsBold") ?? nil
        case MenuUniquId.dynamicCourse: return UIImage(named: "icon_contentBold") ?? nil
        case MenuUniquId.contactMe: return UIImage(named: "icon_phoneBold") ?? nil
        case MenuUniquId.chatScreen: return UIImage(named: "icon_messagesBold") ?? nil
        case MenuUniquId.leaderboard: return UIImage(named: "icon_leaderboardBold") ?? nil
        case MenuUniquId.chatrooms: return UIImage(named: "icon_messagesBold") ?? nil
        case MenuUniquId.iDeserveItHomeScreen: return UIImage(named: "icon_myClubsBold") ?? nil
        case MenuUniquId.SMSText: return UIImage(named: "icon_smsBold") ?? nil

        default: return nil
        }
    }
}

