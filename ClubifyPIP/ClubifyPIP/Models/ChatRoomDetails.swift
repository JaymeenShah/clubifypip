//
//  ChatRoomDetails.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation

class ChatRoomDetails : NSObject {
    
    var clubId : String?
    var firebaseGroupId : String?
    var isPublic : String?
    var largeBannerImage : String?
    var name : String?
    var created : String?
    var deleted : String?
    var id : String?
    var membershipLevel : [MembershipLevel] = []
    var ownerId : String?
    var updated : String?
    var isJoined : Bool = false
    
    override init() {
        
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(dictJSON:NSDictionary) {
        isJoined = dictJSON.getBool(key: "is_join")
        clubId = dictJSON.getString(key:"club_id")
        firebaseGroupId = dictJSON.getString(key:"firebase_group_id")
        isPublic = dictJSON.getString(key:"is_public")
        largeBannerImage = dictJSON.getString(key:"large_banner_image")
        name = dictJSON.getString(key:"name")
        created = dictJSON.getString(key:"created")
        deleted = dictJSON.getString(key:"deleted")
        id = dictJSON.getString(key:"id")
        ownerId = dictJSON.getString(key:"owner_id")
        updated = dictJSON.getString(key:"updated")
        
        membershipLevel = [MembershipLevel]()
        if let membershipLevelArray = dictJSON.getArray(key: "membership_level") as? NSArray {
            for dic in membershipLevelArray{
                let value = MembershipLevel(fromDictionary: dic as! [String : Any])
                membershipLevel.append(value)
            }
        }
    }

    /*
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if clubId != nil{
            dictionary["club_id"] = clubId
        }
        if firebaseGroupId != nil{
            dictionary["firebase_group_id"] = firebaseGroupId
        }
        if isPublic != nil{
            dictionary["is_public"] = isPublic
        }
        if largeBannerImage != nil{
            dictionary["large_banner_image"] = largeBannerImage
        }
        if name != nil{
            dictionary["name"] = name
        }
        if created != nil{
            dictionary["created"] = created
        }
        if deleted != nil{
            dictionary["deleted"] = deleted
        }
        if id != nil{
            dictionary["id"] = id
        }
        if ownerId != nil{
            dictionary["owner_id"] = ownerId
        }
        if updated != nil{
            dictionary["updated"] = updated
        }
        

        if membershipLevel != nil {
            var dictionaryElements = [[String:Any]]()
            for membershipLevelElement in membershipLevel {
                dictionaryElements.append(membershipLevelElement.toDictionary())
            }
            dictionary["membershipLevel"] = dictionaryElements
        }
        return dictionary
    }*/
}

class SMSChatDetails : NSObject {
    
    var sid : String?
    var from : String?
    var to : String?
    var sent_date : String?
    var status : String?
    var body : String?
    
    override init() {
        
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(dictJSON:NSDictionary) {
        sid = dictJSON.getString(key:"sid")
        from = dictJSON.getString(key:"from")
        to = dictJSON.getString(key:"to")
        sent_date = dictJSON.getString(key:"sent_date")
        status = dictJSON.getString(key:"status")
        body = dictJSON.getString(key:"body")
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if sid != nil{
            dictionary["sid"] = sid
        }
        if from != nil{
            dictionary["from"] = from
        }
        if to != nil{
            dictionary["to"] = to
        }
        if sent_date != nil{
            dictionary["sent_date"] = sent_date
        }
        if status != nil{
            dictionary["status"] = status
        }
        if body != nil{
            dictionary["body"] = body
        }
        return dictionary
    }
}
