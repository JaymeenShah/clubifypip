//
//  VoiceMessage.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation

class VoiceMessage : NSObject {

    var clubId : String?
    var createdAt : String?
    var deleted : String?
    var firstname : String?
    var gender : String?
    var id : String?
    var isSender : Bool?
    var lastname : String?
    var photo : String?
    var reciverUser : String?
    var sender : String?
    var url : String?
    var username : String?
    var users : [UserDetail] = []
    var isForwarded : String?
    var parentMessageId : String?
    var type: String?
    var msgId: String?
    var isForwardedMsg: String?
    var isSavedMsg: Bool = false

    override init() {
          
      }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        isForwarded = dictionary["is_forwarded"] as? String
        parentMessageId = dictionary["parent_message_id"] as? String
        clubId = dictionary["club_id"] as? String
        createdAt = dictionary["created_at"] as? String
        deleted = dictionary["deleted"] as? String
        firstname = dictionary["firstname"] as? String
        gender = dictionary["gender"] as? String
        id = dictionary["id"] as? String
        isSender = dictionary["is_sender"] as? Bool
        lastname = dictionary["lastname"] as? String
        photo = dictionary["photo"] as? String
        reciverUser = dictionary["reciver_user"] as? String
        sender = dictionary["sender"] as? String
        url = dictionary["url"] as? String
        username = dictionary["username"] as? String
        type = dictionary["type"] as? String
        msgId = dictionary["msg_id"] as? String
        isForwardedMsg = dictionary["is_forwarded"] as? String
        users = [UserDetail]()
        if let usersArray = dictionary["userData"] as? [[String:Any]]{
            for dic in usersArray{
                let value = UserDetail(fromDictionary: dic)
                users.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if isForwarded != nil{
            dictionary["is_forwarded"] = isForwarded
        }
        if parentMessageId != nil{
            dictionary["parent_message_id"] = parentMessageId
        }
        if clubId != nil{
            dictionary["club_id"] = clubId
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if deleted != nil{
            dictionary["deleted"] = deleted
        }
        if firstname != nil{
            dictionary["firstname"] = firstname
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isSender != nil{
            dictionary["is_sender"] = isSender
        }
        if lastname != nil{
            dictionary["lastname"] = lastname
        }
        if photo != nil{
            dictionary["photo"] = photo
        }
        if reciverUser != nil{
            dictionary["reciver_user"] = reciverUser
        }
        if sender != nil{
            dictionary["sender"] = sender
        }
        if url != nil{
            dictionary["url"] = url
        }
        if username != nil{
            dictionary["username"] = username
        }
        if type != nil{
            dictionary["type"] = type
        }
        if msgId != nil{
            dictionary["msg_id"] = msgId
        }
        if isForwardedMsg != nil{
            dictionary["is_forwarded"] = isForwardedMsg
        }

        if users != nil{
            var dictionaryElements = [[String:Any]]()
            for usersElement in users {
                dictionaryElements.append(usersElement.toDictionary())
            }
            dictionary["userData"] = dictionaryElements
        }
        return dictionary
    }
}
