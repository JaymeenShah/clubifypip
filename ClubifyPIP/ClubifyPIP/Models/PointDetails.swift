//
//  pointDetails.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 21/09/21.
//

import Foundation
import UIKit

class PointDetails : NSObject {
    
    var id: String?
    var updated: String?
    var club_id: String?
    var deleted: String?
    var created: String?
    var invite_user: String?
    var max_invite_user: String?
    var upload_photo: String?
    var max_upload_photo: String?
    var upload_document: String?
    var max_upload_document: String?
    var coin_spent: String?
    var max_coin_spent: String?
    var view_document: String?
    var max_view_document: String?
    var comment_points: String?
    var max_comment_points: String?
    var view_video: String?
    var max_view_video: String?
    var view_photo: String?
    var max_view_photo: String?
    var register_to_event: String?
    var max_register_to_event: String?
    var upload_video: String?
    var max_upload_video: String?
    var buy_coins: String?
    var max_buy_coins: String?
    var product_services: String?
    var max_product_services: String?
    var member_join_club: String?
    var max_member_join_club: String?
    
    var add_gallery_item_url: String?
    var view_gallery_item_url: String?
    var max_add_gallery_item_url: String?
    var max_view_gallery_item_url: String?
    var create_feed: String?
    var max_create_feed: String?

    var share_feed: String?
    var max_share_feed: String?

    override init() {
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        id = dictionary["id"] as? String
        updated = dictionary["updated"] as? String
        club_id = dictionary["club_id"] as? String
        deleted = dictionary["deleted"] as? String
        created = dictionary["created"] as? String
        invite_user = dictionary["invite_user"] as? String
        max_invite_user = dictionary["max_invite_user"] as? String
        upload_photo = dictionary["upload_photo"] as? String
        max_upload_photo = dictionary["max_upload_photo"] as? String
        upload_document = dictionary["upload_document"] as? String
        max_upload_document = dictionary["max_upload_document"] as? String
        coin_spent = dictionary["coin_spent"] as? String
        max_coin_spent = dictionary["max_coin_spent"] as? String
        view_document = dictionary["view_document"] as? String
        max_view_document = dictionary["max_view_document"] as? String
        comment_points = dictionary["comment_points"] as? String
        max_comment_points = dictionary["max_comment_points"] as? String
        view_video = dictionary["view_video"] as? String
        max_view_video = dictionary["max_view_video"] as? String
        view_photo = dictionary["view_photo"] as? String
        max_view_photo = dictionary["max_view_photo"] as? String
        register_to_event = dictionary["register_to_event"] as? String
        max_register_to_event = dictionary["max_register_to_event"] as? String
        upload_video = dictionary["upload_video"] as? String
        max_upload_video = dictionary["max_upload_video"] as? String
        buy_coins = dictionary["buy_coins"] as? String
        max_buy_coins = dictionary["max_buy_coins"] as? String
        
        max_product_services = dictionary["max_product_services"] as? String
        product_services = dictionary["product_services"] as? String
        
        max_member_join_club = dictionary["max_member_join_club"] as? String
        member_join_club = dictionary["member_join_club"] as? String
    
        add_gallery_item_url = dictionary["add_gallery_item_url"] as? String
        view_gallery_item_url = dictionary["view_gallery_item_url"] as? String
        max_add_gallery_item_url = dictionary["max_add_gallery_item_url"] as? String
        max_view_gallery_item_url = dictionary["max_view_gallery_item_url"] as? String
        create_feed = dictionary["create_feed"] as? String
        max_create_feed = dictionary["max_create_feed"] as? String

        share_feed = dictionary["share_feed"] as? String
        max_share_feed = dictionary["max_share_feed"] as? String

    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if id != nil {
            dictionary["id"] = id
        }
        if updated != nil {
            dictionary["updated"] = updated
        }
        if club_id != nil {
            dictionary["club_id"] = club_id
        }
        if deleted != nil {
            dictionary["deleted"] = deleted
        }
        if created != nil {
            dictionary["created"] = created
        }
        if invite_user != nil {
            dictionary["invite_user"] = invite_user
        }
        if max_invite_user != nil {
            dictionary["max_invite_user"] = max_invite_user
        }
        
        if upload_photo != nil {
            dictionary["upload_photo"] = upload_photo
        }
        if max_upload_photo != nil {
            dictionary["max_upload_photo"] = max_upload_photo
        }
        
        if upload_document != nil {
            dictionary["upload_document"] = upload_document
        }
        if max_upload_document != nil {
            dictionary["max_upload_document"] = max_upload_document
        }
        
        if coin_spent != nil {
            dictionary["coin_spent"] = coin_spent
        }
        if max_coin_spent != nil {
            dictionary["max_coin_spent"] = max_coin_spent
        }
        
        if view_document != nil {
            dictionary["view_document"] = view_document
        }
        if max_view_document != nil {
            dictionary["max_view_document"] = max_view_document
        }
        
        if comment_points != nil {
            dictionary["comment_points"] = comment_points
        }
        if max_comment_points != nil {
            dictionary["max_comment_points"] = max_comment_points
        }
        
        if view_video != nil {
            dictionary["view_video"] = view_video
        }
        if max_view_video != nil {
            dictionary["max_view_video"] = max_view_video
        }
        
        if view_photo != nil {
            dictionary["view_photo"] = view_photo
        }
        if max_view_photo != nil {
            dictionary["max_view_photo"] = max_view_photo
        }
        
        if register_to_event != nil {
            dictionary["register_to_event"] = register_to_event
        }
        if max_register_to_event != nil {
            dictionary["max_register_to_event"] = max_register_to_event
        }
        
        if upload_video != nil {
            dictionary["upload_video"] = upload_video
        }
        if max_upload_video != nil {
            dictionary["max_upload_video"] = max_upload_video
        }
        
        if buy_coins != nil {
            dictionary["buy_coins"] = buy_coins
        }
        if max_buy_coins != nil {
            dictionary["max_buy_coins"] = max_buy_coins
        }
        
        if max_product_services != nil {
            dictionary["max_product_services"] = max_product_services
        }
        if product_services != nil {
            dictionary["product_services"] = product_services
        }
       
        if max_member_join_club != nil {
            dictionary["max_member_join_club"] = max_member_join_club
        }
        if member_join_club != nil {
            dictionary["member_join_club"] = member_join_club
        }
        if add_gallery_item_url != nil {
            dictionary["add_gallery_item_url"] = add_gallery_item_url
        }
        if view_gallery_item_url != nil {
            dictionary["view_gallery_item_url"] = view_gallery_item_url
        }
        if max_add_gallery_item_url != nil {
            dictionary["max_add_gallery_item_url"] = max_add_gallery_item_url
        }
        if max_view_gallery_item_url != nil {
            dictionary["max_view_gallery_item_url"] = max_view_gallery_item_url
        }
        if create_feed != nil {
            dictionary["create_feed"] = create_feed
        }
        if max_create_feed != nil {
            dictionary["max_create_feed"] = max_create_feed
        }

        if share_feed != nil {
            dictionary["share_feed"] = share_feed
        }
        if max_share_feed != nil {
            dictionary["max_share_feed"] = max_share_feed
        }

        return dictionary
    }
}
