//
//  ClubPowerUp.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
import UIKit

class ClubPowerUp: NSObject {
    
    var listpage = listPage()
    var page = listPage()
    
    override init() {
        
    }
    init (listpage: listPage,page: listPage)
    {
        self.listpage = listpage
        self.page = page
    }
}
class listPage: NSObject {
    
    var id: String = ""
    var additionalpageid: String = ""
    var clubid: String = ""
    var createdtimestamp: String = ""
    var isactive: String = ""
    var modifiedtimestamp: String = ""
    var remoteurl: String = ""
    var name: String = ""
    var pagetype: String = ""
    var listtypeid: String = ""
    var title: String = ""

    override init() {
        
    }
    
    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.additionalpageid = dictJSON.getString(key: "additionalpageid")
        self.clubid = dictJSON.getString(key: "clubid")
        self.createdtimestamp = dictJSON.getString(key: "createdtimestamp")
        self.isactive = dictJSON.getString(key: "isactive")
        self.modifiedtimestamp = dictJSON.getString(key: "modifiedtimestamp")
        self.remoteurl = dictJSON.getString(key: "remoteurl")
        self.name = dictJSON.getString(key: "name")
        self.pagetype = dictJSON.getString(key: "pagetype")
        self.listtypeid = dictJSON.getString(key: "listtypeid")
        self.title = dictJSON.getString(key: "title")
    }

}

class TaskList: NSObject {
    
    var id: String = ""
    var listpageid: String = ""
    var name: String = ""
    var caption: String = ""
    var isdone: String = ""
    var finisherid: String = ""
    var isactive: String = ""
    var createdtimestamp: String = ""
    var modifiedtimestamp: String = ""
    var imageid: String = ""
    var desc: String = ""
    var cost: String = ""
    var actionurl: String = ""
    
    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.listpageid = dictJSON.getString(key: "listpageid")
        self.name = dictJSON.getString(key: "name")
        self.caption = dictJSON.getString(key: "caption")
        self.isdone = dictJSON.getString(key: "isdone")
        self.finisherid = dictJSON.getString(key: "finisherid")
        self.isactive = dictJSON.getString(key: "isactive")
        self.createdtimestamp = dictJSON.getString(key: "createdtimestamp")
        self.modifiedtimestamp = dictJSON.getString(key: "modifiedtimestamp")
        self.imageid = dictJSON.getString(key: "imageid")
        self.desc = dictJSON.getString(key: "description")
        self.cost = dictJSON.getString(key: "cost")
        self.actionurl = dictJSON.getString(key: "actionurl")

    }
}

class ClubPowerUpList: NSObject {

    var id: String = ""
    var type: String = ""
    var name: String = ""
    var desc: String = ""
    var icon: String = ""
    var coins: String = ""
    var created: String = ""
    var deleted: String = ""
    var status: String = ""
    var subscription_period: String = ""
    var subscription_id: String = ""
    
    override init() {
        
    }
 
    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.type = dictJSON.getString(key: "type")
        self.name = dictJSON.getString(key: "name")
        self.desc = dictJSON.getString(key: "description")
        self.icon = dictJSON.getString(key: "icon")
        self.coins = dictJSON.getString(key: "coins")
        self.created = dictJSON.getString(key: "created")
        self.deleted = dictJSON.getString(key: "deleted")
        self.status = dictJSON.getString(key: "status")
        self.subscription_period = dictJSON.getString(key: "subscription_period")
        self.subscription_id = dictJSON.getString(key: "subscription_id")
    }
}
class ClubPowerUpSettings: NSObject {
    var id: String = ""
    var club_id: String = ""
    var title: String = ""
    var address: String = ""
    var created: String = ""
    var deleted: String = ""
    var name: String = ""
    var caption: String = ""
    
    override init() {
        
    }
    init (id: String,club_id: String,title: String,address: String,created :String,deleted: String,name :String,caption: String) {
        self.id = id
        self.club_id = club_id
        self.title = title
        self.address = address
        self.created = created
        self.deleted = deleted
        self.name = name
        self.caption = caption

    }
}
class ClubStore: NSObject {
    var id: String = ""
    var club_id: String = ""
    var title: String = ""
    var desc: String = ""
    var image: String = ""
    var cost: String = ""
    var created: String = ""
    var modified: String = ""
    var deleted: String = ""
    var category_id: String = ""
    var category_name: String = ""

    override init() {
        
    }
    init (id: String,club_id: String,title: String,desc: String,image :String,cost: String,created: String,modified : String ,deleted: String,category_id : String,category_name : String) {
        self.id = id
        self.club_id = club_id
        self.title = title
        self.desc = desc
        self.image = image
        self.cost = cost
        self.created = created
        self.modified = modified
        self.deleted = deleted
        self.category_id = category_id
         self.category_name = category_name
    }
}
class Categories: NSObject {
    var id: String = ""
    var club_id: String = ""
    var type: String = ""
    var name: String = ""
    var created: String = ""
    var deleted: String = ""
    
    override init() {
        
    }
    init (id: String,club_id: String,type: String,name: String,created: String,deleted: String) {
        self.id = id
        self.club_id = club_id
        self.type = type
        self.name = name
        self.created = created
        self.deleted = deleted
    }
}
class WebPageData: NSObject {
    var id: String = ""
    var club_id: String = ""
    var title: String = ""
    var addressUrl: String = ""
    var created: String = ""
    var deleted: String = ""
    var status: String = ""
    var course: String = ""
    var navigation: String = ""
    var externalBrowser: String = ""
    var type: String = ""

    override init() {
        
    }
    init (id: String,club_id: String,title: String,addressUrl: String,created: String,deleted: String, status: String, course: String, navigation: String, externalBrowser: String, type: String) {
       
        self.id = id
        self.club_id = club_id
        self.title = title
        self.addressUrl = addressUrl
        self.created = created
        self.deleted = deleted
        self.status = status
        self.course = course
        self.navigation = navigation
        self.externalBrowser = externalBrowser
        self.type = type
    }
}

