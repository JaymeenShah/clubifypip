//
//  LeaderboardPointsDetails.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
import UIKit

class LeaderboardMembers : NSObject {

    var id: String = ""
    var biography: String = ""
    var firstname: String = ""
    var lastname: String = ""
    var gender: String = ""
    var profilepicture: String = ""
    var state: String = ""
    var username: String = ""
    var isSelected: Bool = false
    var email: String = ""
    var isOwner: Bool = false
    var userId: String = ""
    var totalPoints: String = ""
    var planName: String = ""
    var planIconUrl: String = ""
    //var clubPlanDetail = ClubPlanDetail()

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        //
        self.planIconUrl = dictJSON.getString(key: "plan_image")
        self.planName = dictJSON.getString(key: "plan_name")
        self.userId = dictJSON.getString(key: "user_id")
        self.id = dictJSON.getString(key: "id")
        self.biography = dictJSON.getString(key: "biography")
        self.firstname = dictJSON.getString(key: "first_name")
        self.lastname = dictJSON.getString(key: "last_name")
        self.gender = dictJSON.getString(key: "gender")
        self.profilepicture = dictJSON.getString(key: "profilepicture")
        self.state = dictJSON.getString(key: "state")
        self.username = dictJSON.getString(key: "username")
        self.email = dictJSON.getString(key: "email")
        self.isOwner = dictJSON.getBool(key: "is_owner")
        self.totalPoints = dictJSON.getString(key: "total_points")
        //self.clubPlanDetail = ClubPlanDetail.init(dictJSON: dictJSON.getDictionary(key: "club_plan_details"))
    }
}

class LeaderboardPointsDetails : NSObject {

    var id: String = ""
    var desc: String = ""
    var updated: String = ""
    var clubId: String = ""
    var rewardBalance: String = ""
    
    var reply_id: String = ""
    var action: String = ""
    var gallery_resource_id: String = ""
    var product_id: String = ""
    var comment_id: String = ""
    var gallery_id: String = ""
    var invite_id: String = ""
    var created: String = ""
    var account_id: String = ""
    var event_id: String = ""
    var contest_id: String = ""
    var point_details = PointsData()

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        self.point_details = PointsData.init(dictJSON: dictJSON.getDictionary(key: "point_details"))
        self.reply_id = dictJSON.getString(key: "reply_id")
        self.id = dictJSON.getString(key: "id")
        self.desc = dictJSON.getString(key: "description")
        self.updated = dictJSON.getString(key: "updated")
        self.clubId = dictJSON.getString(key: "club_id")
        self.rewardBalance = dictJSON.getString(key: "reward_balance")
        self.action = dictJSON.getString(key: "action")
        self.gallery_resource_id = dictJSON.getString(key: "gallery_resource_id")
        self.product_id = dictJSON.getString(key: "product_id")
        self.comment_id = dictJSON.getString(key: "comment_id")
        self.gallery_id = dictJSON.getString(key: "gallery_id")
        self.invite_id = dictJSON.getString(key: "invite_id")
        self.created = dictJSON.getString(key: "created")
        self.account_id = dictJSON.getString(key: "account_id")
        self.event_id = dictJSON.getString(key: "event_id")
        self.contest_id = dictJSON.getString(key: "contest_id")
    }
}

class PointsData : NSObject {

    var title: String = ""
    var url: String = ""
    var thumb_url: String = ""
    var name: String = ""
    var desc: String = ""
    
    var contest_caption: String = ""
    var contest_name: String = ""
    var contest_image: String = ""
    var contest_description: String = ""

    var comment: String = ""
    var comment_reply: String = ""
    var feed_type: String = ""
    var feed_description: String = ""
    
    var profile_image: String = ""
    var game = GameDetails()
    var question = QuestionDetails()
    var answer = AnswerDetails()
    var item_url: String = ""
    var type: String = ""
    var image: String = ""
    var video: String = ""

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        self.game = GameDetails.init(dictJSON: dictJSON.getDictionary(key: "game"))
        self.question = QuestionDetails.init(dictJSON: dictJSON.getDictionary(key: "question"))
        self.answer = AnswerDetails.init(dictJSON: dictJSON.getDictionary(key: "answer"))

        self.thumb_url = dictJSON.getString(key: "thumb_url")
        self.title = dictJSON.getString(key: "title")
        self.url = dictJSON.getString(key: "url")
        self.name = dictJSON.getString(key: "name")
        self.desc = dictJSON.getString(key: "description")
        self.contest_caption = dictJSON.getString(key: "contest_caption")
        self.contest_name = dictJSON.getString(key: "contest_name")
        self.contest_image = dictJSON.getString(key: "contest_image")
        self.contest_description = dictJSON.getString(key: "contest_description")

        self.comment = dictJSON.getString(key: "comment")
        self.comment_reply = dictJSON.getString(key: "comment_reply")
        self.feed_type = dictJSON.getString(key: "feed_type")
        self.feed_description = dictJSON.getString(key: "feed_description")

        self.profile_image = dictJSON.getString(key: "profile_image")
        self.item_url = dictJSON.getString(key: "item_url")
        self.type = dictJSON.getString(key: "type")
        self.image = dictJSON.getString(key: "image")
        self.video = dictJSON.getString(key: "video")

    }
}

class GameDetails : NSObject {

    var id: String = ""
    var game_image: String = ""
    var name: String = ""

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.game_image = dictJSON.getString(key: "game_image")
        self.name = dictJSON.getString(key: "name")
    }
}

class QuestionDetails : NSObject {

    var id: String = ""
    var question_image: String = ""
    var question: String = ""

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.question_image = dictJSON.getString(key: "question_image")
        self.question = dictJSON.getString(key: "question")
    }
}

class AnswerDetails : NSObject {

    var id: String = ""
    var answer: String = ""
    var num_points: String = ""

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {

        self.id = dictJSON.getString(key: "id")
        self.answer = dictJSON.getString(key: "answer")
        self.num_points = dictJSON.getString(key: "num_points")
    }
}


