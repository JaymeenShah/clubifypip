//
//  WebpageData.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 19/10/21.
//

import Foundation
import SwiftyJSON

public struct WebpageData {

  // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let clubId = "club_id"
        static let result = "data"
        static let deleted = "deleted"
        static let id = "id"
        static let created = "created"
        static let address = "address"
        static let title = "title"
        static let message = "message"
        static let status = "status"
        static let course = "course"
        static let navigation = "navigation"
        static let externalBrowser = "external_browser"
        static let type = "type"
        static let amount = "amount"
    }

  // MARK: Properties
    public var clubId: String?
    public var result: [DataResult]?
    public var deleted: String?
    public var id: String?
    public var created: String?
    public var address: String?
    public var title: String?
    public var message: String?
    public var status: String?
    public var course: String?
    public var navigation: String?
    public var externalBrowser: String?
    public var type: String?
    public var amount: String?


  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        clubId = json[SerializationKeys.clubId].string
        if let items = json[SerializationKeys.result].array { result = items.map { DataResult(json: $0) } }
        deleted = json[SerializationKeys.deleted].string
        id = json[SerializationKeys.id].string
        created = json[SerializationKeys.created].string
        address = json[SerializationKeys.address].string
        title = json[SerializationKeys.title].string
        message = json[SerializationKeys.message].string
        status = json[SerializationKeys.status].string
        course = json[SerializationKeys.course].string
        navigation = json[SerializationKeys.navigation].string
        externalBrowser = json[SerializationKeys.externalBrowser].string
        type = json[SerializationKeys.type].string
        amount = json[SerializationKeys.amount].string
        
    }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = clubId { dictionary[SerializationKeys.clubId] = value }
        if let value = result { dictionary[SerializationKeys.result] = value.map { $0.dictionaryRepresentation() } }
        if let value = deleted { dictionary[SerializationKeys.deleted] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = created { dictionary[SerializationKeys.created] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = message { dictionary[SerializationKeys.message] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = course { dictionary[SerializationKeys.course] = value }
        if let value = navigation { dictionary[SerializationKeys.navigation] = value }
        if let value = externalBrowser { dictionary[SerializationKeys.externalBrowser] = value }
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = amount { dictionary[SerializationKeys.amount] = value }
        
        return dictionary
    }

}
