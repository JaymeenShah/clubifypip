//
//  Club.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation

class CategoryWiseClubList : NSObject{

    var name : String?
    var count : String?
    var club: [ClubList]?

    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        
        name = dictionary.getString(key: "name")
        count = dictionary.getString(key: "count")
        
        if let data = dictionary["club"] as? [NSDictionary] {
            var arrMembershipLevel: [ClubList] = []
            for permData in data {
                arrMembershipLevel.append(ClubList(dictJSON: permData))
            }
            club = arrMembershipLevel
        }
    }
}


class ClubList : NSObject {
    
    var id: String = ""
    var desc: String = ""
    var anonymous: String = ""
    var background_image: String = ""
    var name: String = ""
    var num_members: String = ""
    var owner_id: String = ""
    var clubPrivate: String = ""
    var profile_image: String = ""
    var requires_invite: String = ""
    var isPublic: String = ""
    var subtitle: String = ""
    var type: String = ""
    var is_member: String = ""
    var is_club_member: String = ""
    var is_club_owner: String = ""
    var invite_status: String = ""
    var invite_type: String = ""
    var invite_token: String = ""
    var category_name: String = ""
    var category_type: String = ""
    var club_category : String = ""
    var plan_name: String = ""
    var cost: String = ""
    var plan_profile_icon: String = ""
    var facebook_link: String = ""
    var twitter_link: String = ""
    var linkedin_link: String = ""
    var instagram_link: String = ""
    var can_access_club_information : Bool = false
    var redirect_to_powerups : Bool = false
    var deleted: String = ""
    
    var clubTheme: String = ""
    var default_startup_screen: String = ""
    var default_menu_name: String = ""
    var contest: String = ""
    var user: User?
    var default_menu_id: String = ""
    var default_menu_name_default_startup_screen: String = ""
    
    var color_type: String = "1"
    var start_color: String = ""
    var end_color: String = ""
    var club_info_access: Bool = true
    var button_text_color: String = ""
    var phone: String = ""
    var feed_id: String?
    var resource_type: String?
    var app_link: String = ""
    var ios_use_iap: String = "1"
    var welcomeText: String = ""
    var app_highlight_color: String = ""
    var currency: String = ""
    var icon_url: String = ""
    var welcome_image: String = ""
    var welcomeButton_text: String = ""

    override init() {
        
    }
   
    init(dictJSON:NSDictionary) {
        
        self.icon_url = dictJSON.getString(key: "icon_url")
        self.currency = dictJSON.getString(key: "currency")
        self.app_highlight_color = dictJSON.getString(key: "app_highlight_color")
        self.welcomeText = dictJSON.getString(key: "welcome_text")
        self.ios_use_iap = dictJSON.getString(key: "ios_use_iap")
        self.app_link = dictJSON.getString(key: "app_link")
        self.resource_type = dictJSON.getString(key: "resource_type")
        self.feed_id = dictJSON.getString(key: "feed_id")
        self.phone = dictJSON.getString(key: "phone")
        self.button_text_color = dictJSON.getString(key: "button_text_color")
        self.club_info_access = dictJSON.getBool(key: "club_info_access")
        self.color_type = dictJSON.getString(key: "color_type")
        self.start_color = dictJSON.getString(key: "start_color")
        self.end_color = dictJSON.getString(key: "end_color")

        self.default_menu_name_default_startup_screen = dictJSON.getString(key: "default_menu_name_default_startup_screen ")
        self.default_menu_id = dictJSON.getString(key: "default_menu_id ")

        self.contest = dictJSON.getString(key: "contest")
        self.id = dictJSON.getString(key: "id")
        self.desc = dictJSON.getString(key: "description")
        self.anonymous = dictJSON.getString(key: "anonymous")
        self.background_image = dictJSON.getString(key: "background_image")
        self.name = dictJSON.getString(key: "name")
        self.num_members = dictJSON.getString(key: "num_members")//dictJSON.getString(key: "num_of_members")
        self.owner_id = dictJSON.getString(key: "owner_id")
        self.clubPrivate = dictJSON.getString(key: "private")
        self.profile_image = dictJSON.getString(key: "profile_image")
        self.requires_invite = dictJSON.getString(key: "requires_invite")
        self.subtitle = dictJSON.getString(key: "subtitle")
        self.type = dictJSON.getString(key: "type")
        self.is_member = dictJSON.getString(key: "is_member")
        self.is_club_member = dictJSON.getString(key: "is_club_member")
        self.is_club_owner = dictJSON.getString(key: "is_club_owner")
        self.invite_status = dictJSON.getString(key: "invite_status")
        self.invite_type = dictJSON.getString(key: "invite_type")
        self.invite_token = dictJSON.getString(key: "invite_token")
        self.category_name = dictJSON.getString(key: "category_name")
        self.category_type = dictJSON.getString(key: "category_type")
        self.plan_name = dictJSON.getString(key: "plan_name")
        self.cost = dictJSON.getString(key: "cost")
        self.plan_profile_icon = dictJSON.getString(key: "plan_profile_icon")
        self.facebook_link = dictJSON.getString(key: "facebook_link")
        self.twitter_link = dictJSON.getString(key: "twitter_link")
        self.linkedin_link = dictJSON.getString(key: "linkedin_link")
        self.instagram_link = dictJSON.getString(key: "instagram_link")

        self.isPublic = dictJSON.getString(key: "public")
        self.club_category = dictJSON.getString(key: "club_category")
        //FIXME: Check Info Can accessible for members or not
        self.can_access_club_information = true//dictJSON.getBool(key: "can_access_club_information")
        self.redirect_to_powerups = dictJSON.getBool(key: "redirect_to_powerups")
        self.clubTheme = dictJSON.getString(key: "club_theme")
        self.default_startup_screen = dictJSON.getString(key: "default_startup_screen")
        self.default_menu_name = dictJSON.getString(key: "default_menu_name")
        self.deleted = dictJSON.getString(key: "deleted")
        self.welcome_image = dictJSON.getString(key: "welcome_image")
        self.welcomeButton_text = dictJSON.getString(key: "button_text")
       
        if let userData = dictJSON["user"] as? [String: Any] {
            self.user = User(fromDictionary: userData)
        }
    }
    
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        
        if self.app_highlight_color != nil {
            dictionary.setValue(self.app_highlight_color, forKey: "app_highlight_color")
        }
        
        if self.welcomeText != nil {
            dictionary.setValue(self.welcomeText, forKey: "welcome_text")
        }

        if self.app_link != nil {
            dictionary.setValue(self.app_link, forKey: "app_link")
        }

        if self.resource_type != nil {
            dictionary.setValue(self.resource_type, forKey: "resource_type")
        }
        
        if self.feed_id != nil {
            dictionary.setValue(self.feed_id, forKey: "feed_id")
        }
        
        if phone != nil{
            dictionary.setValue(phone, forKey: "phone")

        }
        
        if button_text_color != nil{
            dictionary.setValue(button_text_color, forKey: "button_text_color")
        }
        
        if club_info_access != nil{
            dictionary.setValue(club_info_access, forKey: "club_info_access")
        }
        
        if color_type != nil{
            dictionary.setValue(color_type, forKey: "color_type")
        }

        if start_color != nil{
            dictionary.setValue(start_color, forKey: "start_color")
        }
        
        if end_color != nil{
            dictionary.setValue(end_color, forKey: "end_color")
        }
        
        if default_menu_name_default_startup_screen != nil{
            dictionary.setValue(default_menu_name_default_startup_screen, forKey: "default_menu_name_default_startup_screen")
        }

        if default_menu_id != nil{
            dictionary.setValue(default_menu_id, forKey: "default_menu_id")
        }

        if contest != nil{
            dictionary.setValue(contest, forKey: "contest")
        }

        if id != nil{
            dictionary.setValue(id, forKey: "id")
        }

        if desc != nil{
            dictionary.setValue(desc, forKey: "description")
        }

        if anonymous != nil{
            dictionary.setValue(anonymous, forKey: "anonymous")
        }
        
        if background_image != nil{
            dictionary.setValue(background_image, forKey: "background_image")
        }
        
        if name != nil{
            dictionary.setValue(name, forKey: "name")
        }

        if num_members != nil{
            dictionary.setValue(num_members, forKey: "num_members")
        }

        if owner_id != nil{
            dictionary.setValue(owner_id, forKey: "owner_id")
        }

        if clubPrivate != nil{
            dictionary.setValue(clubPrivate, forKey: "private")
        }

        if profile_image != nil{
            dictionary.setValue(profile_image, forKey: "profile_image")
        }

        if requires_invite != nil{
            dictionary.setValue(requires_invite, forKey: "requires_invite")
        }

        if subtitle != nil{
            dictionary.setValue(subtitle, forKey: "subtitle")
        }
        
        if type != nil{
            dictionary.setValue(type, forKey: "type")
        }

        if is_club_member != nil{
            dictionary.setValue(is_club_member, forKey: "is_club_member")
        }

        if is_club_owner != nil{
            dictionary.setValue(is_club_owner, forKey: "is_club_owner")
        }
        
        if invite_status != nil{
            dictionary.setValue(invite_status, forKey: "invite_status")
        }

        if invite_type != nil{
            dictionary.setValue(invite_type, forKey: "invite_type")
        }
        
        if invite_token != nil{
            dictionary.setValue(invite_token, forKey: "invite_token")
        }
        
        if category_name != nil{
            dictionary.setValue(category_name, forKey: "category_name")
        }
        
        if category_type != nil{
            dictionary.setValue(category_type, forKey: "category_type")
        }
        
        if plan_name != nil{
            dictionary.setValue(plan_name, forKey: "plan_name")
        }
        
        if cost != nil{
            dictionary.setValue(cost, forKey: "cost")
        }
        
        if plan_profile_icon != nil{
            dictionary.setValue(plan_profile_icon, forKey: "plan_profile_icon")
        }
        
        if facebook_link != nil{
            dictionary.setValue(facebook_link, forKey: "facebook_link")
        }
        
        if twitter_link != nil{
            dictionary.setValue(twitter_link, forKey: "twitter_link")
        }
        
        if linkedin_link != nil{
            dictionary.setValue(linkedin_link, forKey: "linkedin_link")
        }

        if instagram_link != nil{
            dictionary.setValue(instagram_link, forKey: "instagram_link")
        }

        if isPublic != nil{
            dictionary.setValue(isPublic, forKey: "isPublic")
        }
        
        if club_category != nil{
            dictionary.setValue(club_category, forKey: "club_category")
        }

        if can_access_club_information != nil{
            dictionary.setValue(can_access_club_information, forKey: "can_access_club_information")
        }

        if redirect_to_powerups != nil{
            dictionary.setValue(redirect_to_powerups, forKey: "redirect_to_powerups")
        }

        if clubTheme != nil{
            dictionary.setValue(clubTheme, forKey: "clubTheme")
        }
        
        if default_startup_screen != nil{
            dictionary.setValue(default_startup_screen, forKey: "default_startup_screen")
        }
        
        if default_menu_name != nil{
            dictionary.setValue(default_menu_name, forKey: "default_menu_name")
        }

        if deleted != nil{
            dictionary.setValue(deleted, forKey: "default_menu_name")
        }
        
        if default_menu_name != nil{
            dictionary.setValue(default_menu_name, forKey: "default_menu_name")
        }
        
        if welcome_image != nil{
            dictionary.setValue(welcome_image, forKey: "welcome_image")
        }
        
        if welcomeButton_text != nil{
            dictionary.setValue(welcomeButton_text, forKey: "button_text")
        }
//        if userData != nil{
//            var dictionaryElements = [[String:Any]]()
//            for dataElement in voiceMessage {
//                dictionaryElements.append(dataElement.toDictionary())
//            }
//            dictionary["user"] = dictionaryElements
//        }
//
//        if let userData = dictJSON["user"] as? [String: Any] {
//            self.user = User(fromDictionary: userData)
//        }

        return dictionary
    }
}



