//
//  discoverClub.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 01/09/21.
//

import Foundation


struct discoverId: Codable {
    
    var id:String = ""
    var owner_id: String = ""
    var type: String = ""
    var name: String = ""
    var subtitle: String = ""
    var description: String = ""
    var anonymous: String = ""
    var requires_invite: String = ""
    var profile_image: String = ""
    
}
