//
//  MemberList.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 07/10/21.
//

import Foundation

class MemberList : NSObject {

    var id: String = ""
    var biography: String = ""
    var firstname: String = ""
    var lastname: String = ""
    var gender: String = ""
    var profilepicture: String = ""
    var state: String = ""
    var username: String = ""
    var isSelected: Bool = false
    var email: String = ""
    var isOwner: Bool = false
    var firebaseId: String = ""
    var currentPlanDetails : ClubPlanDetail?
    var profileVideo: String = ""

    override init() {
        
    }
  
    init(dictJSON:NSDictionary) {
        
        self.profileVideo = dictJSON.getString(key: "profile_video")
        self.id = dictJSON.getString(key: "id")
        self.biography = dictJSON.getString(key: "biography")
        self.firstname = dictJSON.getString(key: "firstname")
        self.lastname = dictJSON.getString(key: "lastname")
        self.gender = dictJSON.getString(key: "gender")
        self.profilepicture = dictJSON.getString(key: "profilepicture")
        self.state = dictJSON.getString(key: "state")
        self.username = dictJSON.getString(key: "username")
        self.email = dictJSON.getString(key: "email")
        self.isOwner = dictJSON.getBool(key: "is_owner")
        self.firebaseId = dictJSON.getString(key: "firebase_id")
        
        if let planDetail = dictJSON["current_plan"] as? NSDictionary {
            currentPlanDetails = ClubPlanDetail(dictJSON: planDetail)
        }
    }
}


class RequestInvitesList : NSObject {
    
    var id: String = ""
    var clubid: String = ""
    var inviter: String = ""
    var invitee: String = ""
    var status: String = ""
    var token: String = ""
    var email: String = ""
    var username: String = ""
    var profilepicture: String = ""
    var created: String = ""

    
    override init() {
        
    }

    init(dictJSON:NSDictionary) {
        
        self.id = dictJSON.getString(key: "id")
        self.clubid = dictJSON.getString(key: "account_id")
        self.inviter = dictJSON.getString(key: "inviter")
        self.invitee = dictJSON.getString(key: "invitee")
        self.status = dictJSON.getString(key: "status")
        self.token = dictJSON.getString(key: "token")
        self.email = dictJSON.getString(key: "email")
        self.username = dictJSON.getString(key: "username")
        self.profilepicture = dictJSON.getString(key: "profilepicture")
        self.created = dictJSON.getString(key: "created")
    }
}
