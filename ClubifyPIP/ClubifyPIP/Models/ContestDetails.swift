//
//  ContestDetails.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
class ContestDetails : NSObject {

    var clubId : String?
    var contestCaption : String?
    var contestDescription : String?
    var contestEndDate : String?
    var contestImage : String?
    var contestName : String?
    var contestStartDate : String?
    var created : String?
    var deleted : String?
    var entryCoins : String?
    var entryPoints : String?
    var id : String?
    var maxEntries : String?
    var membershipLevel : [MembershipLevel] = []
    var ownerId : String?
    var updated : String?
    var notificationFrequency: String?
    var contestId : String?
    var fileId : String?
    var clubPlanId: String?
    var isJoined: Bool?
    var numberOfEntries: String?
    var contestReferenceNumber: String?

    override init() {
        
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        contestReferenceNumber = dictionary["contest_reference_number"] as? String
        numberOfEntries = dictionary["number_of_entries"] as? String
        isJoined = dictionary["is_joined"] as? Bool
        clubPlanId = dictionary["club_plan_id"] as? String
        fileId = dictionary["file_id"] as? String
       
        clubId = dictionary["club_id"] as? String
        contestCaption = dictionary["contest_caption"] as? String
        contestDescription = dictionary["contest_description"] as? String
        contestEndDate = dictionary["contest_end_date"] as? String
        contestImage = dictionary["contest_image"] as? String
        contestName = dictionary["contest_name"] as? String
        contestStartDate = dictionary["contest_start_date"] as? String
        created = dictionary["created"] as? String
        deleted = dictionary["deleted"] as? String
        entryCoins = dictionary["entry_coins"] as? String
        entryPoints = dictionary["entry_points"] as? String
        id = dictionary["id"] as? String
        maxEntries = dictionary["max_entries"] as? String
        ownerId = dictionary["owner_id"] as? String
        updated = dictionary["updated"] as? String
        notificationFrequency = dictionary["notification_frequency"] as? String
        contestId = dictionary["contest_id"] as? String
        
        membershipLevel = [MembershipLevel]()
        if let membershipLevelArray = dictionary["membership_level"] as? [[String:Any]]{
            for dic in membershipLevelArray{
                let value = MembershipLevel(fromDictionary: dic)
                membershipLevel.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if clubId != nil{
            dictionary["club_id"] = clubId
        }
        if contestCaption != nil{
            dictionary["contest_caption"] = contestCaption
        }
        if contestDescription != nil{
            dictionary["contest_description"] = contestDescription
        }
        if contestEndDate != nil{
            dictionary["contest_end_date"] = contestEndDate
        }
        if contestImage != nil{
            dictionary["contest_image"] = contestImage
        }
        if contestName != nil{
            dictionary["contest_name"] = contestName
        }
        if contestStartDate != nil{
            dictionary["contest_start_date"] = contestStartDate
        }
        if created != nil{
            dictionary["created"] = created
        }
        if deleted != nil{
            dictionary["deleted"] = deleted
        }
        if entryCoins != nil{
            dictionary["entry_coins"] = entryCoins
        }
        if entryPoints != nil{
            dictionary["entry_points"] = entryPoints
        }
        if id != nil{
            dictionary["id"] = id
        }
        if maxEntries != nil{
            dictionary["max_entries"] = maxEntries
        }
        if ownerId != nil{
            dictionary["owner_id"] = ownerId
        }
        if updated != nil{
            dictionary["updated"] = updated
        }
        if notificationFrequency != nil {
            dictionary["notification_frequency"] = notificationFrequency
        }
        if contestId != nil {
            dictionary["contest_id"] = contestId
        }
        if contestReferenceNumber != nil {
            dictionary["contest_reference_number"] = contestReferenceNumber
        }
        if numberOfEntries != nil {
            dictionary["number_of_entries"] = numberOfEntries
        }
        if isJoined != nil {
            dictionary["is_joined"] = isJoined
        }
        if clubPlanId != nil {
            dictionary["club_plan_id"] = clubPlanId
        }
        if fileId != nil {
            dictionary["file_id"] = fileId
        }

        if membershipLevel != nil {
            var dictionaryElements = [[String:Any]]()
            for membershipLevelElement in membershipLevel {
                dictionaryElements.append(membershipLevelElement.toDictionary())
            }
            dictionary["membershipLevel"] = dictionaryElements
        }
        return dictionary
    }
}
