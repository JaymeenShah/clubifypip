//
//  userInfo.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 07/09/21.
//

import Foundation
import SwiftyJSON
import Alamofire

struct data {
    var users : Users?
    
    init(json : JSON?) {
        
        if let value = json?["data"].dictionaryObject {
            
            let new = Users(json: JSON(value))
            self.users = new
        }
    }
}
struct Users {
    var userinfo : UserInfo?
    
    init(json : JSON?) {
        
        if let value = json?["user"].dictionaryObject {
            
            let new = UserInfo(json: JSON(value))
            self.userinfo = new
        }
    }
    
}

struct UserInfo {
    
    var id:String?
    var firstname: String?
    var username: String?
    var profilepicture: String?
    var biography: String?
    var lastname: String?
    var gender: String?
    var email: String?
    var phone: String?
    
    init(json : JSON?) {
        
        self.id = json?["id"].string
        self.firstname = json?["firstname"].string
        self.username = json?["username"].string
        self.profilepicture = json?["profilepicture"].string
        self.biography = json?["biography"].string
        self.lastname = json?["lastname"].string
        self.gender = json?["gender"].string
        self.email = json?["email"].string
        self.phone = json?["phone"].string
        
    }
}


struct UserInfos:Decodable {
    var id:String?
    var firstname: String?
    var username: String
    var profilepicture: String?
    var biography: String?
    var lastname: String?
    var gender: String?
    var email: String?
    var phone: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case firstname = "firstname"
        case username = "username"
        case profilepicture = "profilepicture"
        case biography = "biography"
        case lastname = "lastname"
        case gender = "gender"
        case email = "email"
        case phone = "phone"
    }

}
