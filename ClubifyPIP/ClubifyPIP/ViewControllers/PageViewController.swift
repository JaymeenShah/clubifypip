//
//  PageViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.
//

import UIKit
import  Foundation
class PageViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var discoverClubButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        loginButton.setBtnGradientBackground(colorTrail: UIColor.init(displayP3Red: 18/255, green: 137/255, blue: 77/255, alpha: 1.0), colorLead: UIColor.init(displayP3Red: 23/255, green: 171/255, blue:95/255, alpha: 1.0))

    }
    


}

//MARK:- Gradient Functions
extension UIButton
{
    func setBtnGradientBackground(colorTrail: UIColor, colorLead: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorLead.cgColor, colorTrail.cgColor]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
