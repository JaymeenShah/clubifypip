//
//  ClubEventCalenderVC.swift
//  Clubify
//
//  Created on 04/05/18.
//  Copyright © 2021 Clubify Inc. All rights reserved.
//

import UIKit
import BubblePictures
import DataCache
import iOSDropDown
import FSCalendar
import SDWebImage
import EventKit

class ClubEventCalenderVC: UIViewController {
    
    //MARK:  Properties
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var tbleEventList: UITableView!
    @IBOutlet weak var btnAddEvent: UIButton!
    @IBOutlet weak var btnAddEventBottom: ActualGradientButton!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var cons_heightAddEvent: NSLayoutConstraint!
    @IBOutlet weak var calEvent: FSCalendar!
    @IBOutlet weak var lblHeaderTitle: NBLabel!
    @IBOutlet weak var topBarView: UIView!
    
    @IBOutlet weak var tblForPlanList: UITableView!
    @IBOutlet weak var viewMembershipLevel: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var btnFutureEvents: UIButton!
    @IBOutlet weak var cons_widthAddBtnTop: NSLayoutConstraint!
    
    //MARK:  Variables
    var arrMembershipLevel: [MembershipLevel] = []
    var currentDate = Date()
    var event_start_date = Date()
    var strCost: String = ""
    let rightBarDropDown = DropDown()
    let upComing = "Upcoming"
    let onGoing = "Ongoing"
    let past = "Past"
    var datesWithEvent = [String]()
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate lazy var eventDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    var selectedDate : String = ""
    var refresher:UIRefreshControl!
    var isComesFrom : String = ""
    var morepagges = "0"
    var lastmorepage = "0"
    var isFromRefresh = false
    var isLoadMore = false
    var isCellSwipe = false
    var arr_eventList = [ClubEvent]()
    var arr_eventMainList = [ClubEvent]()
    var currentStringSytemDate : String = ""
    var isRightDate = true
    let now = Date()
    var newStartDate = Date()
    var newEndDate = Date()
    var newSelectedDate = Date()
    var newCurrentDate = Date()
    var selectedIndexPath = IndexPath()
    //MARK:  View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        self.arr_eventList = self.arr_eventMainList
        self.tableViewReload()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        btnFutureEvents.imageView?.setImageColor(color: UIColor.white)
        btnFutureEvents.tintColor = UIColor.white

        if isClubLightTheme() {
            btnAddEvent.imageView?.setImageColor(color: UIColor.appIconsDarkColor())
            btnSideMenu.imageView?.setImageColor(color: UIColor.appIconsDarkColor())
            btnFutureEvents.imageView?.setImageColor(color: UIColor.appIconsDarkColor())
            btnFutureEvents.tintColor = UIColor.appIconsDarkColor()
        }
        
        if AppState.sharedInstance.ClubList?.owner_id != AppState.sharedInstance.user?.userID{
            cons_heightAddEvent.constant = 0
            cons_widthAddBtnTop.constant = 0
        }
        
        btnFutureEvents.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.topBarView?.backgroundColor = setTopBarColor()
        btnFutureEvents.imageView?.setImageColor(color: UIColor.white)
        btnFutureEvents.tintColor = UIColor.white

        if isClubLightTheme() {
            btnAddEvent.imageView?.setImageColor(color: UIColor.appIconsDarkColor())
            btnSideMenu.imageView?.setImageColor(color: UIColor.appIconsDarkColor())
            btnFutureEvents.imageView?.setImageColor(color: UIColor.appIconsDarkColor())
            btnFutureEvents.tintColor = UIColor.appIconsDarkColor()
        }
        if !appDelegate.sideMenuData.isEmpty {
            if let index = appDelegate.sideMenuData.index(where:{$0.menuUniqueId == MenuUniquId.privateEvents}){
                self.lblHeaderTitle?.text = appDelegate.sideMenuData[index].menuName?.decode()
            }
        }
        
        self.arr_eventList = self.arr_eventMainList
        self.tableViewReload()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        appDelegate.currentOpenScreen = ""
    }
    
    //MARK:  Setup UI
    func setUpUI() {
        let customView = GradientBGView(frame: CGRect(x: 0, y: 70, width: self.view.bounds.width, height: self.view.bounds.height))
        self.view.addSubview(customView)
        self.view.sendSubviewToBack( customView)
        
        
        if checkIfiPad() {
            self.lblHeaderTitle.font = FontName.fontNavBarIPad
        } else {
//            bottomView.applyGradientView(colours: [UIColor.clear, UIColor.black])
            self.lblHeaderTitle.font = FontName.fontNavBar
        }
        
        self.InitializationControlAndButtons()
        
        lblNoData.lineBreakMode = .byWordWrapping
        lblNoData.numberOfLines = 0
        lblNoData.attributedText = setNoDataFoundLabelString(firstLine: "There are no events yet.", secondLine: "Check back soon!")
        
        lblNoData.isHidden = true
        viewMembershipLevel.isHidden = true
        
        //Calendar Setup
        self.calEvent.appearance.selectionColor = UIColor.appGradiantButton()
        self.calEvent.backgroundColor = .clear
        self.calEvent.appearance.eventSelectionColor = UIColor.appGradiantButton()
        self.calEvent.calendarHeaderView.backgroundColor = .clear
        self.calEvent.calendarWeekdayView.backgroundColor = .clear
        self.calEvent.appearance.eventDefaultColor = UIColor.appGradiantButton()
        tbleEventList.delegate = self
        tbleEventList.dataSource = self
        tbleEventList.register(UINib(nibName: "UpcomingEventCell", bundle: nil), forCellReuseIdentifier: "UpcomingEventCell")
        
        
        let strTheme: String = UserDefaults.standard.string(forKey:"lightTheme") ?? ""
        if !strTheme.isEmpty {
            let isLightTheme = UserDefaults.standard.string(forKey:"lightTheme")
            if isLightTheme == "1" {
                self.calEvent.appearance.titleDefaultColor = .black
            } else {
                self.calEvent.appearance.titleDefaultColor = .white
            }
        } else {
            UserDefaults.standard.set("0", forKey: "lightTheme")
            UserDefaults.standard.synchronize()
            self.calEvent.appearance.titleDefaultColor = .white
        }
        
        
        if AppState.sharedInstance.ClubList?.owner_id != AppState.sharedInstance.user?.userID{
            btnAddEvent.isHidden = true
            btnAddEventBottom.isHidden = true
        }
        
        self.selectedDate = "\(self.dateFormatter.string(from: Date()))"
        //load saved cached data
        self.loadPastListCache()
        self.webserviceCall_getEvents(pagging: "0", aStrDate: self.selectedDate)
        
        //Set observer for local notification calling when push notification call
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushNotiCome(notification:)), name: Notification.Name("pushNotiForReloadEventList"), object: nil)
        
        appDelegate.setMenuTitle = {
            if !appDelegate.sideMenuData.isEmpty {
                if let index = appDelegate.sideMenuData.index(where:{$0.menuUniqueId == MenuUniquId.privateEvents}){
                    self.lblHeaderTitle?.text = appDelegate.sideMenuData[index].menuName?.decode()
                }
            }
        }
    }
   
    //MARK: - App CLub Theme
    func isClubLightTheme() -> Bool {
        let clubTheme = AppState.sharedInstance.ClubList?.clubTheme
        if clubTheme == appTheme.Light.rawValue {
            return true
        }
        return false
    }
    //MARK:- Check If device is iPad or iPhone
    func checkIfiPad() -> Bool {
        
        if DeviceTypes.IS_IPAD_AIR || DeviceTypes.IS_IPAD_PRO || DeviceTypes.IS_IPAD_MINI {
            return true
        }
        return false
    }

    func setTopBarColor() -> UIColor {
        
        let topBarColor = isClubLightTheme() ? .white : UIColor.AppGradiantBlue1()
        return topBarColor
    }

    //MARK:  Load Past list Cache Methods
    func loadPastListCache() {
        if let dataDict = DataCache.instance.readData(forKey: "EventList"), let response: Dictionary = NSKeyedUnarchiver.unarchiveObject(with: dataDict) as? [String : Any] {
            handleAPIResponse(response)
        }
    }
    
    fileprivate func handleAPIResponse(_ response: [String: Any?]?) {
        
        var eventData = [ClubEvent]()
        
        if let morePages = response!["more_pages"] as? String {
            self.morepagges = morePages
        }
        if response != nil, let data = response!["data"] as? [[String:Any?]] {
            for prodDetail in data {
                let data = creatDictnory(value:prodDetail as AnyObject)
                eventData.append(ClubEvent.init(dictJSON: data))
            }
        }
        
        if eventData.count > 0 {
            eventData.sort(by: { Int($0.id)! < Int($1.id)! })
            eventData.sort(by: { Int($0.id)! > Int($1.id)! })
        }
        
        print(eventData.count)
        if eventData.count > 0 {
            if !self.isLoadMore {
                self.arr_eventMainList.removeAll()
                self.arr_eventMainList = eventData
            }else {
                for dict in eventData {
                    self.arr_eventMainList.append(dict)
                }
            }
            
            self.getEventDateFromList()
            
//            self.arr_eventList = self.arr_eventMainList.filter { item in
//                let eventDate = self.eventDateFormatter.date(from: item.event_start_date)
//                let eventStartDate = "\(self.dateFormatter.string(from: eventDate ?? Date()))"
//                return eventStartDate == self.selectedDate }
            print(self.arr_eventList)
            
        }
        self.tableViewReload()
        self.stopRefresher()
    }
    
    //MARK:  Local Notification Call from Push Notification
    @objc func pushNotiCome(notification: Notification){
        appDelegate.notificationType = ""
        self.lastmorepage = "0"
        isFromRefresh = true
        isLoadMore = false
        self.webserviceCall_getEvents(pagging: "0", aStrDate: self.selectedDate)
    }
    
    //MARK:  Initializations data
    func InitializationControlAndButtons() {
        lblNoData.isHidden = true
        tbleEventList.tableFooterView = UIView()
        tbleEventList.estimatedRowHeight = 100
        self.refresher = UIRefreshControl()
        self.tbleEventList!.alwaysBounceVertical = true
        self.refresher.tintColor = isClubLightTheme() ? .black : .white
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tbleEventList!.addSubview(refresher)
        self.navigationController?.isNavigationBarHidden = true
//        if self.revealViewController() != nil {
//            self.btnSideMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside )
//            self.revealViewController().delegate = self
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        }
    }
    
//    //MARK:  Side menu controller delegate
//    func revealControllerPanGestureShouldBegin(_ revealController: SWRevealViewController!) -> Bool {
//        let allow = true
//        var velocity = CGPoint()
//        velocity = revealController.panGestureRecognizer().velocity(in: self.view)
//        if velocity.x < 0 && revealController.frontViewPosition == FrontViewPosition.left {
//            return false
//        }
//        return allow
//    }
    
    //MARK:  Pull To Refresh Methods
    @objc func loadData() {
        if !isFromRefresh {
            self.lastmorepage = "0"
            isFromRefresh = true
            isLoadMore = false
            self.webserviceCall_getEvents(pagging: "0", aStrDate: self.selectedDate)
        }else {
            stopRefresher()
        }
    }
    
    func stopRefresher() {
        if self.refresher != nil {
            isFromRefresh = false
            self.refresher.endRefreshing()
        }
    }
    
    //MARK:  Click Events
    @IBAction func tapToOpenFutureEvent(_ sender: UIButton) {
//        let objClubFutureEventListVC:ClubFutureEventListVC = storyBoards.EventsStoryboard.instantiateViewController(withIdentifier: "ClubFutureEventListVC") as! ClubFutureEventListVC
//
//        let navigationController = UINavigationController(rootViewController: objClubFutureEventListVC)
//        navigationController.modalPresentationStyle = .fullScreen
//        navigationController.isNavigationBarHidden = true
//        self.present(navigationController, animated: true)
    }
    
    @IBAction func tapToAddevent(_ sender: UIButton) {
//        if self.isRightDate {
//            let objCreateClubEventsVC = storyBoards.EventsStoryboard.instantiateViewController(withIdentifier: "CreateClubEventsVC") as! CreateClubEventsVC
//            objCreateClubEventsVC.strSelectedDate = self.selectedDate
//            self.navigationController?.pushViewController(objCreateClubEventsVC, animated: true)
//        }else {
//            showAlert(title: "Alert", message: "Please select another date. You can't create on past date.")
//        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        viewMembershipLevel.isHidden = true
    }
    
    @IBAction func tapToWebLink(_ sender: UIButton) {
//        redirectURL(strURL: self.arr_eventList[sender.tag].videoLink)
        print("redirectWebLink() working")
    }
    
    @IBAction func tapToJoinNow(_ sender: UIButton) {
        
        let dictEvent = self.arr_eventList[sender.tag]
        
        if AppState.sharedInstance.ClubList?.contest == "1" {
            
            if self.arr_eventList[sender.tag].points == "0" || self.arr_eventList[sender.tag].points.isEmpty {
                
                //Coins only
                if self.arr_eventList[sender.tag].amount != "0" && !self.arr_eventList[sender.tag].amount.isEmpty{
                    self.openCoinsPopup(index: sender.tag)
                } else {
                    self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.arr_eventList[sender.tag].eventId, purchaseType:                        AppState.sharedInstance.ClubList?.contest == "1" ? "1" : "0", code: "", dictEvent: dictEvent)
                }
            } else if self.arr_eventList[sender.tag].amount == "0" || self.arr_eventList[sender.tag].amount.isEmpty {
                
                //Points only
                if self.arr_eventList[sender.tag].points != "0" && !self.arr_eventList[sender.tag].points.isEmpty{
                    self.openPointsPopup(index: sender.tag)
                } else {
                    self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.arr_eventList[sender.tag].eventId, purchaseType:                        AppState.sharedInstance.ClubList?.contest == "1" ? "1" : "0", code: "", dictEvent: dictEvent)
                }
            } else {
                //Both Points and Coins
                if (self.arr_eventList[sender.tag].amount != "0" && !self.arr_eventList[sender.tag].amount.isEmpty) && (self.arr_eventList[sender.tag].points != "0" && !self.arr_eventList[sender.tag].points.isEmpty) {
                    
                    if AppState.sharedInstance.ClubList?.contest == "1" {
                        self.openSelectOptionPopup(index: sender.tag)
                    } else {
                        self.openCoinsPopup(index: sender.tag)
                    }
                } else {
                    self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.arr_eventList[sender.tag].eventId, purchaseType:                        AppState.sharedInstance.ClubList?.contest == "1" ? "1" : "0", code: "", dictEvent: dictEvent)

                }
            }
        } else {
            //Coins only
            if self.arr_eventList[sender.tag].amount != "0" && !self.arr_eventList[sender.tag].amount.isEmpty{
                self.openCoinsPopup(index: sender.tag)
            } else {
                self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.arr_eventList[sender.tag].eventId, purchaseType: "0", code: "", dictEvent: dictEvent)
            }
        }
    }
    
    func openPointsPopup(index: Int) {
        let strAlertTitle = "Redeem Points"//"Purchase For * Coins"
        //strAlertTitle = strAlertTitle.replacingOccurrences(of: "*", with: imgDetails.cost)
        let alert = UIAlertController.init(title: strAlertTitle, message: "You must redeem \(self.arr_eventList[index].points) points to view this item. Would you like to continue?".localized, preferredStyle: .alert)
        
        let actionCouponCode = UIAlertAction.init(title: "Enter Coupon Code".localized, style: UIAlertAction.Style.default, handler: { (action) in
            self.openAlertForCouponCode(aClubId: getClubId(), aEventId: self.arr_eventList[index].eventId, purchaseType: "1", code: "", dictEvent: self.arr_eventList[index])
        })
        
        let action = UIAlertAction.init(title: "No Thanks".localized, style: UIAlertAction.Style.cancel, handler: { (action) in
        })
        let actionOk = UIAlertAction.init(title: "Yes".localized, style: UIAlertAction.Style.default, handler: { (action) in
            self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.arr_eventList[index].eventId, purchaseType: "1", code: "", dictEvent: self.arr_eventList[index])
        })

        alert.addAction(action)
        alert.addAction(actionOk)
        alert.addAction(actionCouponCode)

        self.present(alert, animated: true, completion: {
        })
    }
    
    func openCoinsPopup(index: Int) {
//        let strCurrencyName = " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coins")"
//        var strAlertTitle = "Purchase For * \(strCurrencyName)"
//        strAlertTitle = strAlertTitle.replacingOccurrences(of: "*", with: self.arr_eventList[index].amount)
//        let alert = UIAlertController.init(title: strAlertTitle, message: "Would you like to continue?".localized, preferredStyle: .alert)
//
//        let actionCouponCode = UIAlertAction.init(title: "Enter Coupon Code".localized, style: UIAlertAction.Style.default, handler: { (action) in
//            self.openAlertForCouponCode(aClubId: getClubId(), aEventId: self.arr_eventList[index].eventId, purchaseType: "0", code: "", dictEvent: self.arr_eventList[index])
//        })
//
//        let action = UIAlertAction.init(title: "No Thanks".localized, style: UIAlertAction.Style.cancel, handler: { (action) in
//        })
//        let actionOk = UIAlertAction.init(title: "Yes".localized, style: UIAlertAction.Style.default, handler: { (action) in
//            self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.arr_eventList[index].eventId, purchaseType: "0", code: "", dictEvent: self.arr_eventList[index])
//        })
//
//        alert.addAction(action)
//        alert.addAction(actionOk)
//        alert.addAction(actionCouponCode)
//
//        self.present(alert, animated: true, completion: {
//        })
    }
    
    func openSelectOptionPopup(index: Int) {
//        let strCurrencyName = " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coins")"
//        let strAlertTitle = "Purchase For \(self.arr_eventList[index].amount) \(strCurrencyName) or \(self.arr_eventList[index].points) Points "
//        //strAlertTitle = strAlertTitle.replacingOccurrences(of: "*", with: imgDetails.cost)
//        let actionSheetController: UIAlertController = UIAlertController(title: strAlertTitle, message: "Would you like to continue with one of these options?", preferredStyle: .alert)
//
//        let saveActionButton = UIAlertAction(title: "By \(strCurrencyName)", style: .default)
//        { _ in
//            print("Choose Membership Level")
//            self.openAlertForCouponCode(aClubId: getClubId(), aEventId: self.arr_eventList[index].eventId, purchaseType: "0", code: "", dictEvent: self.arr_eventList[index])
//
//            //self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.eventDetails.eventId, purchaseType: "0", code: "")
//        }
//        actionSheetController.addAction(saveActionButton)
//
//        let deleteActionButton = UIAlertAction(title: "By Points", style: .default)
//        { _ in
//            print("Choose Individual/Group")
//            self.openAlertForCouponCode(aClubId: getClubId(), aEventId: self.arr_eventList[index].eventId, purchaseType: "1", code: "", dictEvent: self.arr_eventList[index])
//
//            //self.webservice_JoinEvent(aClubId: getClubId(), aEventId: self.eventDetails.eventId, purchaseType: "1", code: "")
//        }
//        actionSheetController.addAction(deleteActionButton)
//
//        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
//            print("Cancel")
//        }
//        actionSheetController.addAction(cancelActionButton)
//        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK:  Coupon Code Alert
    func openAlertForCouponCode(aClubId : String , aEventId : String, purchaseType : String, code: String, dictEvent: ClubEvent) {
       
        showAlertForCouponCode(self, title: "Coupon Code", message: "Please enter the coupon code to grab the discount".localized, cancelTitle: LocalizeKeys.cancel, cancelCompletion: {
            self.webservice_JoinEvent(aClubId: aClubId, aEventId: aEventId, purchaseType: purchaseType,code: "", dictEvent: dictEvent)
        }, okTitle: LocalizeKeys.yes, okCompletion: { (code) in
            self.webservice_JoinEvent(aClubId: aClubId, aEventId: aEventId, purchaseType: purchaseType,code: code, dictEvent: dictEvent)
        })
    }
    
    @IBAction func lockBtnTapped(_ sender: UIButton) {
        showUpgradePlanAlert()
    }
    
    //MARK:  Show Upgrade Plan Alert
    func showUpgradePlanAlert() {
        let upgradeText = WHITELABEL_APP_NAME == "CABA" ? "CABA_Upgrade_Your_Life".localized : "Upgrade_Your_Life".localized
        showAlertAction(self, title: LocalizeKeys.upgradeLife, message: upgradeText, cancelTitle: LocalizeKeys.cancel, cancelCompletion: {}, okTitle: "Upgrade Your Package".localized, okCompletion: {
            
//            if let revealViewController: SideMenuRootVC = storyBoards.Main.instantiateViewController(withIdentifier: "SideMenuRootVC") as? SideMenuRootVC {
//                
//                let mainStoryboard:UIStoryboard = UIStoryboard(name: "ClubPlans", bundle: nil)
//                let destinationController = mainStoryboard.instantiateViewController(withIdentifier: "UpgradeClubPlansVC")
//                let newFrontViewController = UINavigationController.init(rootViewController:destinationController)
//                revealViewController.setContentViewController(newFrontViewController, animated: true)
//                self.navigationController?.pushViewController(revealViewController, animated: true)
//
//            }
        })
    }
    
    @IBAction func btnMoreTapped(_ sender: UIButton) {
//
//        rightBarDropDown.anchorView = sender
//        rightBarDropDown.selectionBackgroundColor = UIColor.init(hex: getAppHightLightColor())
//        rightBarDropDown.corner_Radius = 10
//        rightBarDropDown.selectedTextColor = UIColor.white
//
//        rightBarDropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
//        rightBarDropDown.shadowOpacity = 0.9
//        rightBarDropDown.shadowRadius = 20
//        rightBarDropDown.animationduration = 0.25
//        rightBarDropDown.textColor = .darkGray
//
//        rightBarDropDown.direction = .any
//        rightBarDropDown.backgroundColor = UIColor.white
//        rightBarDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
//
//        if DeviceType.IS_IPAD || DeviceType.IS_IPAD_10INCH || DeviceType.IS_IPAD_PRO {
//            rightBarDropDown.textFont = UIFont.init(name: FontName.SFProTextRegular, size: 22) ?? UIFont.systemFont(ofSize: 22)
//        } else {
//            rightBarDropDown.textFont = UIFont.init(name: FontName.SFProTextRegular, size: 16) ?? UIFont.systemFont(ofSize: 16)
//        }
//
//        let dict = arr_eventList[sender.tag]
//        rightBarDropDown.dataSource = [
//            LocalizeKeys.edit,
//            LocalizeKeys.delete,
//        ]
//
//        rightBarDropDown.show()
//
//        // Action triggered on selection
//        rightBarDropDown.selectionAction = { [unowned self] (index, item) in
//
//            switch item {
//            case LocalizeKeys.edit:
//
//                let objCreateClubEventsVC = storyBoards.EventsStoryboard.instantiateViewController(withIdentifier: "CreateClubEventsVC") as! CreateClubEventsVC
//                objCreateClubEventsVC.isForEdit = true
//                objCreateClubEventsVC.eventDetails = dict
//                objCreateClubEventsVC.strSelectLastIndex = sender.tag
//                self.navigationController?.pushViewController(objCreateClubEventsVC, animated: true)
//
//                break
//            case LocalizeKeys.delete:
//
//                let alertView = UIAlertController(title: "Delete Event", message: "Are you sure you wish to delete?".localized, preferredStyle: .alert)
//                let actionYes = UIAlertAction(title: "Yes".localized, style: .default, handler: { (alert) in
//                    self.webservice_DeleteClubEvent(aClubId: dict.club_id, aEventId:dict.id, aIndex: sender.tag)
//                })
//                let actionCancel = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (alert) in
//                })
//                alertView.addAction(actionYes)
//                alertView.addAction(actionCancel)
//                self.present(alertView, animated: true, completion: nil)
//
//                break
//            default:
//                break
//            }
//        }
    }
    
    //Api Calling For Join Club Event
    func webservice_JoinEvent(aClubId : String , aEventId : String, purchaseType : String, code: String, dictEvent: ClubEvent) {
        IJProgressView.shared.showProgressView(self.view)
        
        var parameters: [String : Any] = ["club_id": aClubId,
                                          "event_id":aEventId,
                                          "purchase_type":purchaseType,
                                          "app_name": IN_APPPURCHASE_APP_NAME]
        
        if !code.isEmpty {
            parameters["coupon_code"] = code
            parameters["coupon_category"] = "3"
        }
        
        WebServicesEvent.sharedInstanceEvent.webservice_joinClubEvent(url: WebURL.joinClubEvent, parameters: parameters) { (code,message) in
            
            IJProgressView.shared.hideProgressView()
            
            if code != 200 {
                if code == 403 {
                    if purchaseType == "1" {
                        let alert = UIAlertController.init(title: "Insufficient Points", message: message, preferredStyle: .alert)
                       
                        let action1 = UIAlertAction.init(title:"Ok", style: .default, handler: { (action) in
                            
                        })
                        alert.addAction(action1)
                        self.present(alert, animated: true, completion: {
                        })

                    } else {
                        let strCurrencyName = "\(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "coins")"
                        let alert = UIAlertController.init(title: "Insufficient Funds", message: "You have insufficient funds to buy these \(strCurrencyName). Would you like to buy \(strCurrencyName)?", preferredStyle: .alert)
                        let action = UIAlertAction.init(title: "No", style: .cancel, handler: { (action) in
                        })
                        let action1 = UIAlertAction.init(title:"Yes", style: .default, handler: { (action) in
//                            if isStripeEnabled() {
//                                weservice_getNewStripeToken(view: self)
//                            } else {
//                                let objWalletCoinVC:UINavigationController = storyBoards.AccountsStoryboard.instantiateViewController(withIdentifier: "ClubWalletCoinNav") as! UINavigationController
//                                objWalletCoinVC.modalPresentationStyle = .overCurrentContext
//                                self.present(objWalletCoinVC, animated: true, completion: nil)
//                            }
                        })
                        alert.addAction(action)
                        alert.addAction(action1)
                        self.present(alert, animated: true, completion: {
                        })
                    }
                } else {
                    showAlert(title:"Alert".localized as NSString, message: message)
                }
            }else {
                dictEvent.isJoined = true

                if !dictEvent.event_start_date.isEmpty && !dictEvent.event_end_date.isEmpty {
                    
                    self.addEventInCalender(dictEvent: dictEvent)
                }else {
                    showAlert(title:"Alert".localized as NSString, message: message)
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: Notification.Name("pushNotiForReloadEventList"), object: nil, userInfo: nil)

                }
            }
        }
    }
    
    func addEventInCalender(dictEvent: ClubEvent) {
        let alert = UIAlertController.init(title: "Permission".localized, message: "You want to add this event in you Phone calendar?".localized, preferredStyle: .alert)
        let action = UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
        })
        let action1 = UIAlertAction.init(title: "Confirm".localized, style: .default, handler: { (action) in
            let startDate = getUTCDateFromString(aDate: dictEvent.event_start_date,aDateFormate :"yyyy-MM-dd HH:mm:ss") as Date
            let endDate = getUTCDateFromString(aDate: dictEvent.event_end_date,aDateFormate :"yyyy-MM-dd HH:mm:ss") as Date
            
            if endDate != nil && startDate != nil{
                //Add event in phone default calender
                addEventToCalendar(title: dictEvent.name, description: dictEvent.desc, startDate: startDate, endDate: endDate)
            }
            showAlert(title:"Alert".localized as NSString, message: "Event added to Calendar successfully.")
            self.navigationController?.popViewController(animated: true)
            NotificationCenter.default.post(name: Notification.Name("pushNotiForReloadEventList"), object: nil, userInfo: nil)

        })
        alert.addAction(action)
        alert.addAction(action1)
        self.present(alert, animated: true, completion: { })
    }

}

//MARK:  UITableview Delegates and Datasource
extension ClubEventCalenderVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblForPlanList {
            return UITableView.automaticDimension
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblForPlanList {
            return arrMembershipLevel.count
        }
        return self.arr_eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblForPlanList {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellForClubPlanList") as! CellForClubPlanList
            
            let dict = arrMembershipLevel[indexPath.row]
            cell.lblTitle.text = dict.plan_name?.decode()
            
            if !dict.profile_icon_url!.isEmpty {
                
                cell.imgPlan.sd_imageIndicator = SDWebImageActivityIndicator.gray
                
                if !WHITELABEL_APP_ID.isEmpty && WHITELABEL_APP_ID != "0" {
                    cell.imgPlan.sd_setImage(with: URL.init(string:dict.profile_icon_url ?? ""), placeholderImage: UIImage.init(named: getAppIconImageName()), options: .refreshCached)
                }else {
                    cell.imgPlan.sd_setImage(with: URL.init(string:dict.profile_icon_url ?? ""), placeholderImage: UIImage.init(named: "placeholder"), options: .refreshCached)
                }
            } else {
                let index = indexPath.row + 1
                let planImage = [#imageLiteral(resourceName: "icon_freeplan"), #imageLiteral(resourceName: "icon_silverMember"), #imageLiteral(resourceName: "icon_goldMember")]
                let reminder = (index % 3)
                print(reminder)
                if reminder != 0 {
                    cell.imgPlan.image = planImage[reminder - 1]
                } else {
                    cell.imgPlan.image = planImage[2]
                }
            }
            return cell
        }  else {
            let dictEvent = self.arr_eventList[indexPath.row]
            let cell: UpcomingEventCell = tableView.dequeueReusableCell(withIdentifier: "UpcomingEventCell") as! UpcomingEventCell
            
            if selectedIndexPath == indexPath {
                cell.viewContainer.backgroundColor = UIColor(hex: ColorConstant.clubifyDefaultHighLightColor).withAlphaComponent(0.5)
            } else {
                cell.viewContainer.backgroundColor = UIColor.clear
            }

            let blurEffect = UIBlurEffect(style: isClubLightTheme() ? UIBlurEffect.Style.extraLight : UIBlurEffect.Style.dark)
            cell.blurEffectView.effect = blurEffect
            
            cell.pictures.removeAll()
            var configFile: BPCellConfigFile?
            
            cell.picturesMembership = dictEvent.membershipLevel
            
            var index = 0
            for image in dictEvent.membershipLevel {
                let strUrl: String = image.profile_icon_url ?? ""
                if !strUrl.isEmpty {
                    configFile = BPCellConfigFile(imageType: BPImageType.URL(URL(string: strUrl)!), title: "")
                }else {
                    index = index + 1
                    let planImage = [#imageLiteral(resourceName: "icon_freeplan"), #imageLiteral(resourceName: "icon_clubifyGold"), #imageLiteral(resourceName: "icon_goldMember")]
                    let reminder = (index % 3)
                    print(reminder)
                    if reminder != 0 {
                        configFile = BPCellConfigFile(imageType: BPImageType.image(planImage[reminder - 1]), title: "")
                    } else {
                        configFile = BPCellConfigFile(imageType: BPImageType.image(planImage[2]), title: "")
                    }
                }
                cell.pictures.append(configFile!)
            }
            cell.bubblePictures = BubblePictures(collectionView: cell.collectionView, configFiles: cell.pictures, layoutConfigurator: cell.layoutConfigurator)
            cell.bubblePictures.delegate = cell
            cell.collectionView.reloadData()
            
            cell.delegate = self
            
            cell.lblTitle.textColor = UIColor.init(hex: getAppHightLightColor())
            cell.lblTitle.text = dictEvent.name.isEmpty ? "No Name Available".localized : dictEvent.name.decode()
            cell.lblLocation.text = dictEvent.address
            //cell.lblStatus.text = "Upcoming" //FIX ME: To change the logic based on start date and display the label accordingly
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            let startDate = dateFormatter.date(from: dictEvent.event_start_date)
            let endDate = dateFormatter.date(from: dictEvent.event_end_date)

            cell.lblStatus.text = compareDatesForStatus(currentDate: Date(), startDate: startDate ?? Date(), endDate: endDate ?? Date())
            cell.lblStatus?.textColor = UIColor.init(hex: getAppHightLightColor())

            cell.btnWebLink.tag = indexPath.row
            cell.btnWebLink?.addTarget(self, action: #selector(tapToWebLink(_:)), for: .touchUpInside)
            cell.btnWebLink.setTitle("", for: .normal)
            
            cell.lblEventPage.textColor = UIColor.init(hex: getAppHightLightColor())
            cell.lblEventPage.text = dictEvent.videoLink.isEmpty ? "" : dictEvent.videoLink
            cell.heightConstraintEventPage.constant = dictEvent.videoLink.isEmpty ? 0 : 40
            cell.lblEventPageTitle.isHidden = dictEvent.videoLink.isEmpty ? true : false

//            cell.lblStartDate.text = utcToLocalTimeFromDate(dateStr: dictEvent.event_start_date)
//            cell.lblEndDate.text = utcToLocalTimeFromDate(dateStr: dictEvent.event_end_date)
            cell.lblStartDate.text = UTCToCurrentDateFormatWithOutput(date: dictEvent.event_start_date,aDateFormate :"yyyy-MM-dd HH:mm:ss", outDateFormat: "MM-dd-yyyy h:mm a")
            cell.lblEndDate.text = UTCToCurrentDateFormatWithOutput(date: dictEvent.event_end_date,aDateFormate :"yyyy-MM-dd HH:mm:ss", outDateFormat: "MM-dd-yyyy h:mm a")
            
            cell.btnJoin?.tag = indexPath.row
            cell.btnJoin?.addTarget(self, action: #selector(tapToJoinNow(_:)), for: .touchUpInside)
            cell.heightConstraintBtnJoined.constant = dictEvent.isJoined ? 0 : 30
            
            
            cell.btnLockIcon?.tag = indexPath.row
            cell.btnLockIcon?.addTarget(self, action: #selector(lockBtnTapped(_:)), for: .touchUpInside)
            cell.btnLockIcon.setTitle("", for: .normal)

            if AppState.sharedInstance.ClubList?.owner_id == AppState.sharedInstance.user?.userID {
                cell.btnJoin.isHidden = true
                cell.heightConstraintBtnJoined.constant = 0
                cell.collectionView.isHidden = false
                cell.viewForLock.isHidden = true
            } else {
                cell.btnJoin.isHidden = dictEvent.isJoined ? true : false
                cell.heightConstraintBtnJoined.constant = dictEvent.isJoined ? 0 : 30
                cell.collectionView.isHidden = true
                cell.viewForLock.isHidden = dictEvent.upgrade_package == "0" ? true : false
            }
            
            let moreIconImage = #imageLiteral(resourceName: "icon_more_event").withRenderingMode(.alwaysTemplate)
            cell.btnMore?.setBackgroundImage( moreIconImage, for: .normal)
            cell.btnMore?.tintColor = UIColor.init(hex: getAppHightLightColor())
            cell.btnMore?.tag = indexPath.row
            cell.btnMore?.isHidden = (AppState.sharedInstance.ClubList?.owner_id == AppState.sharedInstance.user?.userID) ? false : true
            cell.btnMore?.addTarget(self, action: #selector(btnMoreTapped(_:)), for: .touchUpInside)

            if AppState.sharedInstance.ClubList?.contest == "1" {
                
                if dictEvent.amount == "0" || dictEvent.amount == "" {
                    cell.viewCoins.isHidden = true
                    cell.lblCost.text = ""
                } else if dictEvent.amount == "1" {
                    cell.viewCoins.isHidden = false
                    cell.lblCost.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coin")".localized)
                    
                } else {
                    cell.viewCoins.isHidden = false
                    cell.lblCost.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coins")".localized)
                }
               
                if dictEvent.points == "0" || dictEvent.points == "" {
                    cell.viewPoints.isHidden = true
                    cell.lblPoints.text = ""
                } else if dictEvent.points == "1" {
                    cell.viewPoints.isHidden = false
                    cell.lblPoints.text = (dictEvent.points + " " + "Point".localized)
                } else {
                    cell.viewPoints.isHidden = false
                    cell.lblPoints.text = (dictEvent.points + " " + "Points".localized)
                }
                setCurrencyIconIfStripe(imageView: cell.imgCoins)
            } else {
                cell.lblPoints.text = ""
                
                if dictEvent.amount == "0" || dictEvent.amount == "" {
                    cell.viewCoins.isHidden = true
                    cell.lblCost.text = ""
                } else if dictEvent.amount == "1" {
                    cell.viewCoins.isHidden = false
                    cell.lblCost.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coin")".localized)
                    setCurrencyIconIfStripe(imageView: cell.imgCoins)
                    
                } else {
                    cell.viewCoins.isHidden = false
                    cell.lblCost.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coins")".localized)
                    setCurrencyIconIfStripe(imageView: cell.imgCoins)
                }
            }
            
            if AppState.sharedInstance.ClubList?.owner_id != AppState.sharedInstance.user?.userID{
                cell.collectionView.isHidden = true
                cell.heightCollectionView.constant = 0
            } else {
                if dictEvent.membershipLevel.count == 0 {
                    cell.collectionView.isHidden = true
                    cell.heightCollectionView.constant = 0
                } else {
                    cell.collectionView.isHidden = false
                    cell.heightCollectionView.constant = checkIfiPad() ? 40 : 25
                }
            }
            DispatchQueue.main.async {
//                cell.viewContainer.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 15)
//                cell.btnJoin.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: cell.btnJoin.frame.height/2)
            }
            
            return cell
        }
        /*else {
            let cell: UpcomingEventCell = tableView.dequeueReusableCell(withIdentifier: "UpcomingEventCell") as! UpcomingEventCell
            
            cell.contentView.backgroundColor = UIColor.clear
            cell.backgroundColor = .clear
                        let dictEvent = self.arr_eventList[indexPath.row]
           
            cell.lblStartDate.text = dictEvent.event_start_date
            cell.lblTitle.text = dictEvent.name
            cell.lblEndDate.text = dictEvent.event_end_date
            cell.lblLocation.text = dictEvent.locationName
            cell.lblPoints.text = dictEvent.points
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            event_start_date = dateFormatter.date(from: dictEvent.event_start_date)!
            print("Event Start Date for cell is:\(String(describing: event_start_date))")
            cell.lblStatus.text = describeComparison(currentDate: currentDate, event_start_date: event_start_date)
            
            cell.lblCost.text = dictEvent.amount
            return cell
        }*/
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dictEvent = self.arr_eventList[indexPath.row]

        if tableView == tbleEventList {
            /*if AppState.sharedInstance.ClubList?.owner_id == AppState.sharedInstance.user?.userID{
                let objCreateClubEventsVC = storyBoards.EventsStoryboard.instantiateViewController(withIdentifier: "CreateClubEventsVC") as! CreateClubEventsVC
                objCreateClubEventsVC.isForEdit = true
                objCreateClubEventsVC.eventDetails = self.arr_eventList[indexPath.row]
                objCreateClubEventsVC.strSelectLastIndex = indexPath.row
                self.navigationController?.pushViewController(objCreateClubEventsVC, animated: true)
            }else {*/
//                if dictEvent.upgrade_package == "0" {
//                    let objClubEventDetailsVC = storyBoards.EventsStoryboard.instantiateViewController(withIdentifier: "ClubEventDetailsVC") as! ClubEventDetailsVC
//                    objClubEventDetailsVC.eventDetails = self.arr_eventList[indexPath.row]
//                    self.navigationController?.pushViewController(objClubEventDetailsVC, animated: true)
//                } else {
//                    showUpgradePlanAlert()
//                }
            
//            }
        }
    }
    
    /*func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (AppState.sharedInstance.ClubList?.owner_id)! == AppState.sharedInstance.user?.userID {
            self.isCellSwipe = true
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if self.isCellSwipe {
            self.isCellSwipe = false
            let cellMain = self.tbleEventList.cellForRow(at: indexPath) as! CellForClubevents
            let kCellActionWidth = CGFloat(70.0)// The width you want of delete button
            let kCellHeight = cellMain.contentView.bounds.size.height // The height you want of delete button
            let whitespace = whitespaceString(width: kCellActionWidth) // add the padding
            let deleteAction = UITableViewRowAction(style: .default, title: whitespace) {_,_ in
                
                let alertView = UIAlertController(title: "", message: "Are you sure you wish to delete this?".localized, preferredStyle: .alert)
                let actionYes = UIAlertAction(title: "Yes".localized, style: .default, handler: { (alert) in
                    self.webservice_DeleteClubEvent(aClubId: self.arr_eventList[indexPath.row].club_id, aEventId:self.arr_eventList[indexPath.row].id, aIndex: indexPath.row)
                })
                let actionCancel = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (alert) in
                })
                alertView.addAction(actionYes)
                alertView.addAction(actionCancel)
                self.present(alertView, animated: true, completion: nil)
            }
            
            //create a color from patter image and set the color as a background color of action
            let viewForDelete = UIView(frame: CGRect(x: 0, y: 1, width: 70, height: kCellHeight - 2))
            viewForDelete.backgroundColor = UIColor(red: 255.0/255.0, green: 103.0/255.0, blue: 104.0/255.0, alpha: 1.0)
            let imageViewForDelete = UIImageView(frame: CGRect(x: 20,y: (kCellHeight/2) - 20, width: 35, height: 40))
            imageViewForDelete.image = UIImage(named: "icon_deleteInvite")! // required image
            viewForDelete.addSubview(imageViewForDelete)
            let imageForDelete = viewForDelete.image()
            deleteAction.backgroundColor = UIColor.init(patternImage: imageForDelete)
            return [deleteAction]
        }
        return [UITableViewRowAction]()
    }*/
}

//MARK:  FSCalendar Delegates and Datasource methods
extension ClubEventCalenderVC: FSCalendarDelegate,FSCalendarDataSource{
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = self.dateFormatter.string(from: date)
        if self.datesWithEvent.contains(dateString) {
            return 1
        }
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if self.selectedDate != "\(self.eventDateFormatter.string(from: Date()))" {
            let dateInUTC = getUTCDateFromString(aDate: "\(self.eventDateFormatter.string(from: Date()))",aDateFormate :"yyyy-MM-dd HH:mm:ss")
            
            
            let currentDateInUTC = getUTCDateFromString(aDate: "\(self.eventDateFormatter.string(from: Date()))",aDateFormate :"yyyy-MM-dd HH:mm:ss")
            print(dateInUTC)
            print(currentDateInUTC)
            currentDate = currentDateInUTC
            currentStringSytemDate = "\(currentDate)"
            print("Your sytem date is:\(currentDate)")
            print("Your sytem date string is:\(currentStringSytemDate)")

            if dateInUTC >= currentDateInUTC {
                self.isRightDate = true
            }else {
                self.isRightDate = false
            }
            
            self.selectedDate = "\(self.dateFormatter.string(from: date))"
             print("Your selected date is:\(selectedDate)")
            if monthPosition == .next || monthPosition == .previous {
                calendar.setCurrentPage(date, animated: true)
            }
            
//            arr_eventList = arr_eventMainList.filter { item in
//                let eventDate = eventDateFormatter.date(from: item.event_start_date)
//                let eventStartDate = "\(self.dateFormatter.string(from: eventDate ?? Date()))"
//                return eventStartDate == self.selectedDate }
//            print(arr_eventList)
            
            if let index = arr_eventList.index(where:{
                let eventDate = eventDateFormatter.date(from: $0.event_start_date)
                let eventStartDate = "\(self.dateFormatter.string(from: eventDate ?? Date()))"
                return eventStartDate == self.selectedDate })
            {
                let indexPath = IndexPath(row: index, section: 0)
                selectedIndexPath = indexPath
                tbleEventList.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
//            self.tableViewReload()
        }
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.selectedDate = "\(self.dateFormatter.string(from: calendar.currentPage))"
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
        self.arr_eventList.removeAll()
        self.webserviceCall_getEvents(pagging: "0", aStrDate: "\(self.dateFormatter.string(from: calendar.currentPage))")
    }
}

//MARK:  Api Calling Methods
extension ClubEventCalenderVC {
    
    //WebServices Calling for get Event List
    func webserviceCall_getEvents(pagging : String , aStrDate : String) {
        if !self.isLoadMore && !self.isFromRefresh {
            self.activityIndicator.startAnimating()
        }
        
        DispatchQueue.global(qos: .utility).async {
            
            let urlToRequest = "\(WebURL.eventListing)\(getClubId())/\(aStrDate)/\(pagging)"
            
            WebServicesEvent.sharedInstanceEvent.getCalenderEventList(url: urlToRequest) { (eventData,morepages,code,message ) in
                
                DispatchQueue.main.async {
                    
                    self.activityIndicator.stopAnimating()
                    IJProgressView.shared.hideProgressView()
                    
                    if code != 200 {
                        if code == 406 {
                            //To Navigate in other view controller
                            func navigateToSidebarControllers(strControllerId : String ,storyboardName : String) {
                                appDelegate.openMenuUniqueId = MenuUniquId.upgradeMembership
                                
//                                if let revealViewController: SideMenuRootVC = storyBoards.Main.instantiateViewController(withIdentifier: "SideMenuRootVC") as? SideMenuRootVC {
//
//                                    let mainStoryboard:UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
//                                    let destinationController = mainStoryboard.instantiateViewController(withIdentifier: strControllerId)
//                                    let newFrontViewController = UINavigationController.init(rootViewController:destinationController)
//                                    revealViewController.setContentViewController(newFrontViewController, animated: true)
//                                    self.navigationController?.pushViewController(revealViewController, animated: true)
//                                }
                                
                            }
                            
                            let upgradeText = WHITELABEL_APP_NAME == "CABA" ? "CABA_Upgrade_Your_Life".localized : "Upgrade_Your_Life".localized

                            showAlertAction(self, title: LocalizeKeys.upgradeLife, message: upgradeText, cancelTitle: LocalizeKeys.cancel, cancelCompletion: {
                                if appDelegate.moveToClubWall != nil {
                                    appDelegate.moveToClubWall!()
                                }else {
                                    navigateToSidebarControllers(strControllerId: "ClubNewFeedsVC", storyboardName: "Main")
                                }
                            }, okTitle: "Upgrade Your Package".localized, okCompletion: {
                                if appDelegate.upgradeYourPackage != nil {
                                    appDelegate.upgradeYourPackage!()
                                }else {
                                    navigateToSidebarControllers(strControllerId: "UpgradeClubPlansVC", storyboardName: "ClubPlans")
                                }
                            })
                        }else {
                            if !self.isLoadMore && !self.isFromRefresh {
                                showAlert(title:"Alert".localized as NSString, message: message)
                            }
                        }
                    }else {
                        self.morepagges = morepages
                        if eventData.count > 0 {
                            if !self.isLoadMore {
                                self.arr_eventMainList.removeAll()
                                self.arr_eventMainList = eventData
                            }else {
                                for dict in eventData {
                                    self.arr_eventMainList.append(dict)
                                }
                            }
                            
                            self.getEventDateFromList()
                            
//                            self.arr_eventList = self.arr_eventMainList.filter { item in
//                                let eventDate = self.eventDateFormatter.date(from: item.event_start_date)
//                                let eventStartDate = "\(self.dateFormatter.string(from: eventDate ?? Date()))"
//                                return eventStartDate == self.selectedDate }
                            self.arr_eventList = self.arr_eventMainList
                            self.tableViewReload()
                            print(self.arr_eventList)
                            
                        }
                    }
                    self.tableViewReload()
                }
                self.stopRefresher()
            }
        }
    }
    //Date compare methods
    
    func compareDatesForStatus(currentDate: Date,startDate: Date, endDate: Date) -> String {
        
        //        let upComing = "Upcoming"
        //        let onGoing = "Ongoing"
        //        let past = "Past"
        newCurrentDate = currentDate
        newStartDate = startDate
        newEndDate = endDate
        
        if endDate < currentDate {
            return past
        } else if (startDate < currentDate) && (endDate > currentDate) {
            return onGoing
        } else if (startDate > currentDate) && (endDate > currentDate) {
            return upComing
        }
        return ""
    }
        
    func eventsDates(currentDateString:Date,event_start_date:String,event_end_date:String)->String{
    if event_start_date < event_end_date
    {
        
    }
     
        return event_start_date
    }
    //Reload Table data after get list from api side
    func tableViewReload() {
        self.isLoadMore = false
        self.isFromRefresh = false
        
        if self.arr_eventList.count > 0 {
            self.tbleEventList.isHidden = false
            lblNoData.isHidden = true
        }else {
            self.tbleEventList.isHidden = true
            lblNoData.isHidden = false
        }
        
        if self.tbleEventList != nil{
            self.tbleEventList.reloadData()
        }
        
        appDelegate.currentOpenScreen = CurrentOpenScreen.ClubEvents.rawValue
    }
    
    //Scrolling Delegate For Load more data from api
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if scrollView == tbleEventList {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                if scrollView.contentOffset.y > 0{
                    if !self.isLoadMore {
                        if morepagges != "0" && !morepagges.isEmpty {
                            var morePaged = createStringToint(value: lastmorepage as AnyObject)
                            morePaged = morePaged + 1
                            lastmorepage = "\(morePaged)"
                            if self.arr_eventList.count > 0 {
                                self.isLoadMore = true
                                self.webserviceCall_getEvents(pagging: "\(morePaged)", aStrDate: self.selectedDate)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //Api Calling For Delete Club event
    func webservice_DeleteClubEvent(aClubId : String , aEventId : String, aIndex : Int) {
        IJProgressView.shared.showProgressView(self.view)
        
        let urlToRequest = WebURL.deleteEvent + aClubId + "/" + aEventId
        WebServices.sharedInstance.DeleteMediaDetails(requestUrl:urlToRequest) { (code,message) in
            IJProgressView.shared.hideProgressView()
            if code != 200 {
                showAlert(title:"Alert".localized as NSString, message: message)
            }else {
                self.arr_eventList.remove(at: aIndex)
                self.tableViewReload()
            }
        }
    }
    
    //Filter and get all dates from array
    func getEventDateFromList() {
        self.datesWithEvent.removeAll()
        for eventData in self.arr_eventMainList {
            let strDate = getFormatedDate(eventData.event_start_date, numericDates: false , dateFormate : "yyyy-MM-dd")
            self.datesWithEvent.append(strDate)
        }
        self.calEvent.reloadData()
    }
}

extension ClubEventCalenderVC : FutureEventPictureSelectedProtocol {
    func pictureSelected(_ item: Int, _ arrMembership: [MembershipLevel], _ cost: String) {
        
        print("Test Tapped")
        arrMembershipLevel.removeAll()
        arrMembershipLevel = arrMembership
        strCost = cost
        viewMembershipLevel.isHidden = false
        viewMembershipLevel.bringSubviewToFront(self.view)
        tblForPlanList.delegate = self
        tblForPlanList.dataSource = self
        tblForPlanList.reloadData()
    }
}

//MARK:  Add Club event in Phone calender
func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
    let eventStore = EKEventStore()
    eventStore.requestAccess(to: .event, completion: { (granted, error) in
        if (granted) && (error == nil) {
            let event = EKEvent(eventStore: eventStore)
            event.title = title
            event.startDate = startDate
            event.endDate = endDate
            event.notes = description
            event.calendar = eventStore.defaultCalendarForNewEvents
            do {
                try eventStore.save(event, span: .thisEvent)
            } catch let e as NSError {
                completion?(false, e)
                return
            }
            completion?(true, nil)
        } else {
            completion?(false, error as NSError?)
        }
    })
}


//extension String {
//    var toDate: Date {
//        return Date.Formatter.customDate.date(from: self)!
//    }
//}
//            let cell = tableView.dequeueReusableCell(withIdentifier: "CellForClubevents") as! CellForClubevents
//            cell.contentView.backgroundColor = UIColor.clear
//
//            let dictEvent = self.arr_eventList[indexPath.row]
//
//            cell.pictures.removeAll()
//            var configFile: BPCellConfigFile?
//
//            cell.picturesMembership = dictEvent.membershipLevel
//            cell.strCost = dictEvent.amount
//
//            var index = 0
//            for image in dictEvent.membershipLevel {
//                let strUrl: String = image.profile_icon_url ?? ""
//                if !strUrl.isEmpty {
//                    configFile = BPCellConfigFile(imageType: BPImageType.URL(URL(string: strUrl)!), title: "")
//                }else {
//                    index = index + 1
//                    let planImage = [#imageLiteral(resourceName: "icon_freeplan"), #imageLiteral(resourceName: "icon_silverMember"), #imageLiteral(resourceName: "icon_goldMember")]
//                    let reminder = (index % 3)
//                    print(reminder)
//                    if reminder != 0 {
//                        configFile = BPCellConfigFile(imageType: BPImageType.image(planImage[reminder - 1]), title: "")
//                    } else {
//                        configFile = BPCellConfigFile(imageType: BPImageType.image(planImage[2]), title: "")
//                    }
//                }
//                cell.pictures.append(configFile!)
//            }
//            cell.bubblePictures = BubblePictures(collectionView: cell.collectionView, configFiles: cell.pictures, layoutConfigurator: cell.layoutConfigurator)
//            cell.bubblePictures.delegate = cell
//            cell.collectionView.reloadData()
//
//            cell.delegate = self
//
//            cell.lblTitle.text = dictEvent.name.isEmpty ? "No Name Available".localized : dictEvent.name.decode()
//
//
//            cell.btnWebLink.tag = indexPath.row
//            cell.lblWebLink.text = dictEvent.videoLink.isEmpty ? "" : dictEvent.videoLink
//
//            cell.lblStartTime.text = utcToLocalTimeFromDate(dateStr: dictEvent.event_start_date)
//            cell.lblEndTime.text = utcToLocalTimeFromDate(dateStr: dictEvent.event_end_date)
//
//            if AppState.sharedInstance.ClubList?.contest == "1" {
//
//                if dictEvent.amount == "0" || dictEvent.amount == "" {
////                    cell.heightImgCoins.constant = 0
//                    cell.imgCoins.isHidden = true
//                    cell.lblCoins.text = ""
//                } else if dictEvent.amount == "1" {
////                    cell.heightImgCoins.constant = 20
//                    cell.imgCoins.isHidden = false
//                    cell.lblCoins.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coin")".localized)
//
//                } else {
////                    cell.heightImgCoins.constant = 20
//                    cell.imgCoins.isHidden = false
//                    cell.lblCoins.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coins")".localized)
//
//                }
//
//                if dictEvent.points == "0" || dictEvent.points == "" {
////                    cell.heightImgPoints.constant = 0
//                    cell.imgPoints.isHidden = true
//                    cell.lblPoints.text = ""
//                } else if dictEvent.points == "1" {
////                    cell.heightImgPoints.constant = 20
//                    cell.imgPoints.isHidden = false
//                    cell.lblPoints.text = (dictEvent.points + " " + "Point".localized)
//                } else {
////                    cell.heightImgPoints.constant = 20
//                    cell.imgPoints.isHidden = false
//                    cell.lblPoints.text = (dictEvent.points + " " + "Points".localized)
//                }
//                setCurrencyIconIfStripe(imageView: cell.imgCoins)
//            } else {
//                cell.heightImgPoints.constant = 0
//                cell.lblPoints.text = ""
//
//                if dictEvent.amount == "0" || dictEvent.amount == "" {
////                    cell.heightImgCoins.constant = 0
//                    cell.imgCoins.isHidden = true
//                    cell.lblCoins.text = ""
//                } else if dictEvent.amount == "1" {
////                    cell.heightImgCoins.constant = 20
//                    cell.imgCoins.isHidden = false
//                    cell.lblCoins.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coin")".localized)
//
//                    setCurrencyIconIfStripe(imageView: cell.imgCoins)
//
//                } else {
////                    cell.heightImgCoins.constant = 20
//                    cell.imgCoins.isHidden = false
//                    cell.lblCoins.text = (dictEvent.amount + " " + " \(isStripeEnabled() ? AppState.sharedInstance.ClubList?.currency ?? "USD" : "Coins")".localized)
//
//                    setCurrencyIconIfStripe(imageView: cell.imgCoins)
//                }
//
//            }
//
//            if AppState.sharedInstance.ClubList?.owner_id != AppState.sharedInstance.user?.userID{
//                cell.collectionView.isHidden = true
//                cell.heightCollectionView.constant = 0
//            } else {
//                if dictEvent.membershipLevel.count == 0 {
//                    cell.collectionView.isHidden = true
//                    cell.heightCollectionView.constant = 0
//                } else {
//                    cell.collectionView.isHidden = false
//                    cell.heightCollectionView.constant = checkIfiPad() ? 40 : 25
//                }
//            }
//            return cell
