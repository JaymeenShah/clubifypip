//
//  File.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 22/09/21.
//

import Foundation
import UIKit
import FlexColorPicker

class PickerInTableDemoViewController: UITableViewController {
    @IBOutlet var pickerController: ColorPickerController!
    
    @IBAction func touchOnDone(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

extension PickerInTableDemoViewController: ColorPickerControllerProtocol {
    var delegate: ColorPickerDelegate? {
        get { return pickerController.delegate }
        set { pickerController.delegate = newValue}
        
    }

    var selectedColor: UIColor {
        get { return pickerController.selectedColor }
        set { pickerController.selectedColor = newValue }
    }
}
