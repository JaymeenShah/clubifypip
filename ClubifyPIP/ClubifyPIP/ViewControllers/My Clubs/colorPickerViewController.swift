//
//  colorPickerViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 22/09/21.
//

import UIKit
import FlexColorPicker

class colorPickerViewController: UIViewController {
    
    //MARK:- IBoutlets
    
    @IBOutlet weak var viewColorSliderControl: ColorSliderControl!
    @IBOutlet weak var colorPaletteControl: ColorPaletteControl!
    @IBOutlet weak var viewGreenSliderControl: GreenSliderControl!
    
    //MARK:- LifeCycleMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func touchOnDone(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
