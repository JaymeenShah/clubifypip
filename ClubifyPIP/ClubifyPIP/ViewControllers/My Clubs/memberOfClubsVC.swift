//
//  memberOfClubsVC.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 27/08/21.
//

import UIKit
import Alamofire

class memberOfClubsVC: UIViewController {
    
    //MARK:- IBoutlets
    
    @IBOutlet weak var buttonDiscoverClub: UIButton!
    @IBOutlet weak var memberCollectionView: UICollectionView!
 
    //MARK:- Variables
    var titleString = ["Clubify Club","Contest Tests","Critter Care Brothers...","Everything Tennis","Clubify Club","Contest Tests"]
    var subTittleString = ["PARENTHOOD","PARENTHOOD","PARENTHOOD","PARENTHOOD","PARENTHOOD","PARENTHOOD"]
    let myView = SpinnerView(frame: CGRect(x: screenSize.width/2, y: screenSize.height/2, width: 50, height: 50))

    //MARK:- LifeCycleMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        guard let Xauthorization = UserDefaults.standard.string(forKey: "authToken") else { return }


        buttonLayouts()
        getMemberInfo(Xauthorization: Xauthorization)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        buttonLayouts()
        
    }
}

//MARK:- Functions
extension memberOfClubsVC
{
    func buttonLayouts()
    {
        buttonDiscoverClub.layer.masksToBounds = true
        buttonDiscoverClub.layer.cornerRadius = 30
        
        buttonDiscoverClub.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
    
}
//MARK:- CollectionviewDelegate and CollectionviewDatasource

extension memberOfClubsVC:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch  collectionView {
        
        case memberCollectionView:
            
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "memberClubCell", for: indexPath) as! memberClubCell
            cellA.lblTitle.text = titleString[indexPath.row]
            cellA.lblSubtitle.text =  subTittleString[indexPath.row]
            
            
            return cellA
        default:
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "dicoverClubCell", for: indexPath) as! dicoverClubCell
            return cellA
        }
        
    }
}
//MARK:- CollectionviewFlowLayoutDelegate
extension memberOfClubsVC:UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       
            if  DeviceTypes.IS_IPHONE_12
            {
                return CGSize(width: 172, height: 210)
                
            }
            else if DeviceTypes.IS_IPHONE_7
            {
                return CGSize(width: 160, height: 210)

            }
            else
            {
                return CGSize(width: UIScreen.main.bounds.width / 2 - 30 , height: UIScreen.main.bounds.width / 2)
            }
            
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
            if DeviceTypes.IS_IPHONE_12
            {
                return 12
                
            }
            else if DeviceTypes.IS_IPHONE_7
            {
                return 4
                
            }
            else
            {
                return 12
            }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
            if DeviceTypes.IS_IPHONE_12
            {
                return 5
                
            }
            return 5
            
            
            
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
            if DeviceTypes.IS_IPHONE_12
            {
                return UIEdgeInsets(top: 20, left:15, bottom: 10, right: 15)
                
            }
       else if DeviceTypes.IS_IPHONE_7
        {
            return UIEdgeInsets(top: 20, left:15, bottom: 10, right: 15)
            
        }
            return UIEdgeInsets(top: 20, left:18, bottom: 10, right: 18)
            
      
    }
    
}
//MARK:- ApiClub
extension memberOfClubsVC
{
    func getMemberInfo(Xauthorization:String){
    
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                           "X-authorization" : Xauthorization,
                           "focus_club_id": "135"]
        
        let urlVal: String =  baseUrl.development.rawValue + "/clubs/memberof"
        print(urlVal)
        
        ServiceCalls.sharedInstance.commonApiCall(url: urlVal, postData: nil, headersData: headers) { (success, resultValue) in
            print(resultValue)
            if success == true
            {
                
            }
            else
            {
                
            }
        }

        
}
}
