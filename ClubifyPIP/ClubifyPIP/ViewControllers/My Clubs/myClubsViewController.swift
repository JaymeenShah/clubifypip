//
//  myClubsViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 27/08/21.
//

import UIKit
import Alamofire

class myClubsViewController: UIViewController {

    //MARK:- IBoutlets
    @IBOutlet weak var buttonMember: UIButton!
    @IBOutlet weak var buttonOwner: UIButton!
    @IBOutlet weak var memberContainerView: UIView!
    @IBOutlet weak var ownerConainerView: UIView!
    @IBOutlet weak var buttonPlusIcon: UIButton!
    @IBOutlet weak var buttonSettingIcon: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:- LifeCycleMethods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

      
        self.scrollView .setContentOffset(self.memberContainerView.frame.origin, animated: true)
       buttonLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        self.scrollView .setContentOffset(self.memberContainerView.frame.origin, animated: true)
        buttonLayouts()
    }
    
    
}
//MARK:- Functions
extension myClubsViewController
{
    func buttonLayouts()
    {
        buttonMember.titleLabel?.textColor = UIColor.black
        buttonMember.backgroundColor = UIColor.white
        buttonMember.setTitleColor(UIColor.black, for: .normal)
        buttonOwner.backgroundColor = UIColor.gray
        buttonOwner.setTitleColor(UIColor.white, for: .normal)

    }
}
//MARK:- Actions
extension myClubsViewController
{
    @IBAction func touchOnMember(_ sender: UIButton) {
        
        if buttonMember.isEnabled == true
        {
            buttonMember.titleLabel?.textColor = UIColor.black
            buttonMember.backgroundColor = UIColor.white
            buttonMember.setTitleColor(UIColor.black, for: .normal)
            self.scrollView .setContentOffset(self.memberContainerView.frame.origin, animated: true)
            buttonPlusIcon.isHidden = true
            buttonOwner.backgroundColor = UIColor.gray
            buttonOwner.setTitleColor(UIColor.white, for: .normal)

    }
    }
    
    @IBAction func touchOnOwner(_ sender: UIButton) {
        
        if buttonOwner.isEnabled == true
        {
            ownerConainerView.isHidden = false
            self.scrollView.setContentOffset(self.ownerConainerView.frame.origin, animated: true)
            buttonOwner.backgroundColor = UIColor.white
            buttonOwner.setTitleColor(UIColor.black, for: .normal)
            buttonPlusIcon.isHidden = false
            buttonPlusIcon.isEnabled = true
            buttonMember.backgroundColor = UIColor.gray
            buttonMember.setTitleColor(UIColor.white, for: .normal)
        
        }
    }
    
    @IBAction func touchOnSetting(_ sender: UIButton) {
        let myClubStoryBoard = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let myClubvC = myClubStoryBoard.instantiateViewController(identifier: "userSettingsViewController") as! userSettingsViewController
        self.navigationController?.pushViewController(myClubvC, animated: true)
        
    }
    
    @IBAction func toouchOnPlus(_ sender: UIButton) {
        
                let myClubStoryBoard = UIStoryboard(name: "DiscoverClub", bundle: nil)
                let myClubvC = myClubStoryBoard.instantiateViewController(identifier: "createClubViewController") as! createClubViewController
                self.navigationController?.pushViewController(myClubvC, animated: true)
    }
}
//MARK:- ScrolViewDelegate
extension myClubsViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width = scrollView.frame.width
        let page = Int(round(scrollView.contentOffset.x/width))
        print("CurrentPage:\(page)")
        
        if page == 1
        
        
        {
            ownerConainerView.isHidden = false
            self.scrollView.setContentOffset(self.ownerConainerView.frame.origin, animated: true)
            buttonOwner.backgroundColor = UIColor.white
            buttonOwner.setTitleColor(UIColor.black, for: .normal)
            buttonPlusIcon.isHidden = false
            buttonPlusIcon.isEnabled = true
            buttonMember.backgroundColor = UIColor.gray
            buttonMember.setTitleColor(UIColor.white, for: .normal)
            
        }
        else if page == 0
        {
            ownerConainerView.isHidden = false
            self.scrollView.setContentOffset(self.ownerConainerView.frame.origin, animated: true)
            buttonOwner.backgroundColor = UIColor.white
            buttonOwner.setTitleColor(UIColor.black, for: .normal)
            buttonPlusIcon.isHidden = false
            buttonPlusIcon.isEnabled = true
            buttonMember.backgroundColor = UIColor.gray
            buttonMember.setTitleColor(UIColor.white, for: .normal)
            guard let Xauthorization = UserDefaults.standard.string(forKey: "authToken") else { return }

            getOwnerInfo(Xauthorization: Xauthorization)
        }
    }
    
}
//MARK:- api functions
extension myClubsViewController
{
    func getOwnerInfo(Xauthorization:String){
    
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                           "X-authorization" : Xauthorization,
                           "focus_club_id": "135"]
        
        let urlVal: String =  baseUrl.development.rawValue + "/clubs/ownerof"
        print(urlVal)
        
        ServiceCalls.sharedInstance.commonApiCall(url: urlVal, postData: nil, headersData: headers) { (success, resultValue) in
            print(resultValue)
            if success == true
            {
                
            }
            else
            {
                
            }
        }

        
}
}

