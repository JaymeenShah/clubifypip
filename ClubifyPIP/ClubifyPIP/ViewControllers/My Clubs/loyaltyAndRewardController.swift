//
//  loyaltyAndRewardController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/09/21.
//

import UIKit
import Alamofire

class loyaltyAndRewardController: UIViewController {
    
    //MARK:- IBoutlets
    
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var topContentView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    //MARK:- Variables
    var arrLoyaltiData = [[String:Any]]()
    var arrPointLoyaltiData = [PointDetails]()
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        buttonLayouts()
        mainContentView.setMainGradientBackground()
        topContentView.setMainGradientBackground()
        loyaltiDetailInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        buttonLayouts()
        mainContentView.setMainGradientBackground()
        topContentView.setMainGradientBackground()
        
    }
    
    @IBAction func touchOnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- tableview controller delegate and datasource
extension loyaltyAndRewardController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPointLoyaltiData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "loyaltyAndRewardCell",for: indexPath) as! loyaltyAndRewardCell
        cell.lblProgress.text = arrPointLoyaltiData[indexPath.row].buy_coins
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK:- Functions
extension loyaltyAndRewardController
{
    func buttonLayouts()
    {
        buttonSave.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 170/255, green:132/255, blue: 176/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 107/255, green: 123/255, blue:131/255, alpha: 1.0))
        buttonSave.layer.masksToBounds = true
        buttonSave.layer.cornerRadius = 32
    }
}

//MARK:- ApiCalling
extension loyaltyAndRewardController
{
    func loyaltiDetailInfo(){
        
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : ServiceCalls.sharedInstance.Xauthorization!,
                                   "focus_club_id": "135"]
        let parameters = ["club_id":192]
        
        let urlVal: String =  baseUrl.development.rawValue + "/club/points/details/192"
        print(urlVal)
        Alamofire.request(urlVal,
                          method: .get,
                          parameters: parameters,encoding: URLEncoding.default,
                          headers: headers)
            .responseJSON { response in
                if let responseData = response.result.value as? [String:Any]{
                    var arrPointDetailsData = [PointDetails]()
                    print("responseData of owner is:\(responseData)")
                    guard let data = responseData["data"] as? [String:Any] else { return }
                    print(data)
                    guard let buy_coins = data["buy_coins"] as? String else { return }
                    print(buy_coins)
//                    arrPointDetailsData.append(PointDetails.init(fromDictionary: <#T##[String : Any]#>))
//                    arrPointDetailsData.append(pointDetails.init(product_services: (data["product_services"] as? String)!, buy_coins: (data["buy_coins"] as? String)!, club_id: (data["club_id"] as? String)!, coin_spent: (data["coin_spent"] as? String)!, comment_points: (data["comment_points"] as? String)!, create_feed: (data["create_feed"] as? String)!, deleted: (data["deleted"] as? String)!, id: (data["id"] as? String)!, invite_user: (data["invite_user"] as? String)!, max_add_gallery_item_url: (data["max_add_gallery_item_url"] as? String)!, max_buy_coins: (data["max_buy_coins"] as? String)!, max_coin_spent: (data["max_coin_spent"] as? String)!))
                    self.arrPointLoyaltiData = arrPointDetailsData
                    
                    print(arrPointDetailsData)
                    print(self.arrPointLoyaltiData)
                    self.tblView.reloadData()
                }
            }
    }
    
}
