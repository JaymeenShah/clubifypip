//
//  avatarViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 27/08/21.
//

import UIKit

class avatarViewController: UIViewController {

    @IBOutlet var mainContentView: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageProfileLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        mainContentView.setMainGradientBackground()
        super.viewWillAppear(animated)
        imageProfileLayout()
    }
    @IBAction func touchOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func imageProfileLayout()
    {
        imageProfile.layer.masksToBounds = true
        imageProfile.layer.cornerRadius = 12
    }

}
