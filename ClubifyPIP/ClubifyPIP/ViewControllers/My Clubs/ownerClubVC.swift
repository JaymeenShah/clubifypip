//
//  ownerClubVc.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 27/08/21.
//
import Foundation
import UIKit
import Alamofire
import SDWebImage

class ownerClubVc: UIViewController {
    
    //MARK:- IBoutlets
    @IBOutlet weak var buttonCreateClub: UIButton!
    @IBOutlet weak var ownerCollectionView: UICollectionView!
    
    //MARK:- Variables
    var titleString = ["Clubify Club","Contest Tests","Critter Care Brothers...","Everything Tennis","Clubify Club","Contest Tests"]
    var subTittleString = ["PARENTHOOD","PARENTHOOD","PARENTHOOD","PARENTHOOD","PARENTHOOD","PARENTHOOD"]
    var arrOwnerClub = [ownerClubsData]()
    var statusData = [[String:Any]]()
    
    //MARK:- LifeCycleMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        buttonLayouts()
        guard let Xauthorization = UserDefaults.standard.string(forKey: "authToken") else { return }
        
        getOwnerInfo(Xauthorization: Xauthorization)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        buttonLayouts()
        guard let Xauthorization = UserDefaults.standard.string(forKey: "authToken") else { return }
        
        getOwnerInfo(Xauthorization: Xauthorization)
        
        
    }
    
}
//MARK:- Functions
extension ownerClubVc
{
    func buttonLayouts()
    {
        buttonCreateClub.layer.masksToBounds = true
        buttonCreateClub.layer.cornerRadius = 30
        
        buttonCreateClub.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
}
//MARK:- CollectionviewDelegate and CollectionviewDatasource

extension ownerClubVc:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return statusData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch  collectionView {
        
        case ownerCollectionView:
            let dict = statusData[indexPath.row]
            
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "ownerCell", for: indexPath) as! ownerCell
            print("collection cell dict is:\(dict)")
            cellA.lblTitle.text = dict["name"]  as? String
            cellA.lblSubtitle.text =  subTittleString[indexPath.row]
            if let url: NSURL = NSURL(string: dict["profile_image"] as! String) {
                cellA.imgMemeberCell.sd_setImage(with: url as URL) { (image, error, cache, urls) in
                }
            }
            return cellA
        default:
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "dicoverClubCell", for: indexPath) as! dicoverClubCell
            return cellA
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            let contentViewController = UINavigationController(rootViewController: viewController)

            UIApplication.shared.keyWindow?.rootViewController = viewController
        }
        else if indexPath.row == 1
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            let contentViewController = UINavigationController(rootViewController: viewController)

            UIApplication.shared.keyWindow?.rootViewController = viewController
        }
        else if indexPath.row == 2{
            print("SideMenu opened")
//            let storyBoards = UIStoryboard(name: "SideMenu", bundle: nil)
//            let secondVC = storyBoards.instantiateViewController(withIdentifier: "firstViewController")
//                let contentViewController = UINavigationController(rootViewController: secondVC)
//                sideMenuViewController?.setContentViewController(contentViewController, animated: true)
//                sideMenuViewController?.hideMenuViewController()
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            let contentViewController = UINavigationController(rootViewController: viewController)

            UIApplication.shared.keyWindow?.rootViewController = viewController
            
        }
    }
    
}
//MARK:- CollectionviewFlowLayoutDelegate
extension ownerClubVc:UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  DeviceTypes.IS_IPHONE_12
        {
            
            return CGSize(width: 172, height: 210)
            
            
        }
        return CGSize(width: UIScreen.main.bounds.width / 2 - 30 , height: UIScreen.main.bounds.width / 2.2)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        switch collectionView {
        case ownerCollectionView:
            if DeviceTypes.IS_IPHONE_12
            {
                return 12
                
            }
            else
            {
                return 12
            }
        default:
            return 12
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        switch collectionView {
        
        case ownerCollectionView:
            if DeviceTypes.IS_IPHONE_12
            {
                return 0
                
            }
            return 0
            
            
            
        default:
            return 0
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if DeviceTypes.IS_IPHONE_12
        {
            return UIEdgeInsets(top: 8, left:20, bottom: -14, right: 20)
            
        }
        return UIEdgeInsets(top: 8, left:23, bottom: -14, right: 23)
    }
}
//MARK:- Ownerclub api call
extension ownerClubVc
{
    func getOwnerInfo(Xauthorization:String){
        
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : Xauthorization,
                                   "focus_club_id": "135"]
        
        let urlVal: String =  baseUrl.development.rawValue + "/clubs/ownerof"
        print(urlVal)
        Alamofire.request(urlVal,
                          method: .get,
                          parameters: nil,encoding: URLEncoding.default,
                          headers: headers)
            .responseJSON { [self] response in
                if let responseData = response.result.value as? [String:Any]{
                    print("responseData of owner is:\(responseData)")
                    
                    if let statusData = responseData["data"]  as? [[String:Any]] {
                        self.statusData = responseData["data"] as! [[String:Any]]
                        print("Array is:\(statusData)")
                        for review in statusData {
                            if let authorName = review["name"] as? String {
                                // do something with authorName
                                print(authorName)
                            }
                            guard let profile_image_url = review["profile_image"] else { return }
                            print("profile_image is:\(profile_image_url)")
                        }
                        ownerCollectionView.reloadData()
                    }
                    
                }
            }
    }
}



