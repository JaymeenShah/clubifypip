//
//  editGlobalSideMenuController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 17/09/21.
//

import UIKit
import Alamofire
class editGlobalSideMenuController: UIViewController {
    //MARK:- Iboutlests
    @IBOutlet weak var btnAddCoins: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    
    //icon_forum,icon_webpage
    
    //MARK:- Variables
    
    var arrayTitle = ["Club Wall","Test","Test Page","Test Playlist","Playlist","Test Playlist"]
    var imageProfile = [UIImage(named: "icon_forum"),
                        UIImage(named: "icon_webpage"),
                        UIImage(named: "icon_webpage"),
                        UIImage(named: "icon_webpage"),
                        UIImage(named: "icon_webpage"),
                        UIImage(named: "icon_webpage"),]
    var arrSideMenu = [SideMenu]()
    
    //MARK:-LifeCycleMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.setEditing(true, animated: false)
        buttonLayouts()
        guard let Xauthorization = UserDefaults.standard.string(forKey: "authToken") else { return }
        getSideMenuInfo(Xauthorization: Xauthorization)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        buttonLayouts()
        
    }
    
}
//MARK:- Tableview delegate and datasource

extension editGlobalSideMenuController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "editGlobalCell",for: indexPath) as! editGlobalCell
        cell.lblTitle.text = arrSideMenu[indexPath.row].menuName
        cell.lblSubtitle.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceObject = self.arrSideMenu[sourceIndexPath.row]
        self.arrSideMenu.remove(at: sourceIndexPath.row)
        self.arrSideMenu.insert(sourceObject, at: destinationIndexPath.row)
        self.tblView.reloadData()
    }
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    
}

//MARK:- Functions
extension editGlobalSideMenuController
{
    func buttonLayouts()
    {
        
        btnAddCoins.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        btnAddCoins.layer.masksToBounds = true
        btnAddCoins.layer.cornerRadius = 25
    }
    
    func tableCellOrdered()
    {
        if tblView.isEditing == true
        {
            tblView.isEditing = false
        }
        else
        {
            tblView.isEditing = true
        }
        
    }
}
//MARK:- Actions
extension editGlobalSideMenuController
{
    
    @IBAction func buttonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:-API Handling
extension editGlobalSideMenuController
{
    func getSideMenuInfo(Xauthorization:String){
        
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : Xauthorization,
                                   "focus_club_id": "135"]
        let parameters = ["club_id":192]
        
        let urlVal: String =  baseUrl.development.rawValue + "/additional_sidemenu/192"
        print(urlVal)
        var userData = [SideMenu]()
        Alamofire.request(urlVal,
                          method: .get,
                          parameters: parameters,encoding: URLEncoding.default,
                          headers: headers)
            .responseJSON { [self] response in
                if let responseData = response.result.value as? [String:Any]{
                    guard let dataValue = try? NSKeyedArchiver.archivedData(withRootObject: responseData, requiringSecureCoding: false) else { return }
                    
                    print("responseData of owner is:\(responseData)")
                    print("responseData of owner is:\(dataValue)")
                    
                    if let arrMenuCCategoriesData = responseData["side_menu"] as? [[String:Any]]
                    {
                        print("array of data is:\(String(describing: arrMenuCCategoriesData))")
                        for review in arrMenuCCategoriesData {
                            //                            userData.append(SideMenu.init(json: <#JSON#>, id: (review["id"] as? String)!, club_id: (review["club_id"] as? String)!, menu_name: (review["menu_name"] as? String)!, menu_unique_id: (review["menu_unique_id"] as? String)!, menu_reference_id: (review["menu_reference_id"] as? String)!, is_active: (review["is_active"] as? String)!))
                            print(userData)
                            arrSideMenu = userData
                            
                        }
                        print(arrSideMenu)
                        tblView.reloadData()
                    }
                    
                }
            }
    }
}
