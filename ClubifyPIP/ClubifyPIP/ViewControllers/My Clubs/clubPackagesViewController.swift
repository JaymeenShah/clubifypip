//
//  clubPackagesViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 17/09/21.
//
import UIKit
import Alamofire
import SDWebImage

class clubPackagesViewController: UIViewController {
    
    //MARK:- Iboutlets
    @IBOutlet weak var buttonCreatePackage: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var topContentView: UIView!
    
    
    //MARK:-Variables
    var arrPackageData = [[String:Any]]()
//    var arrClubPlan = ClubPlan()

    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        buttonLayouts()
        mainContentView.setMainGradientBackground()
        topContentView.setMainGradientBackground()
        clubPackagesAPI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        buttonLayouts()
        mainContentView.setMainGradientBackground()
        
    }
    
    @IBAction func touchOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- tableview controller delegate and datasource
extension clubPackagesViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPackageData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "clubPackageCell",for: indexPath) as! clubPackageCell
        let dict = arrPackageData[indexPath.row]["clubPlan"] as? [String:Any]
        cell.lblTitle.text = dict!["planname"] as? String
        let url: NSURL = NSURL(string: dict!["profile_icon_url"] as! String)!
        cell.imgProfile.sd_setImage(with: url as URL, completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK:- funcions
extension clubPackagesViewController
{
    func buttonLayouts()
    {
        buttonCreatePackage.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 170/255, green:132/255, blue: 176/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 107/255, green: 123/255, blue:131/255, alpha: 1.0))
        buttonCreatePackage.layer.masksToBounds = true
        buttonCreatePackage.layer.cornerRadius = 32
    }
}
//MARK:- apiFunctions()
extension clubPackagesViewController
{
    func clubPackagesAPI() {
        let urlString = baseUrl.development.rawValue + "/clubplan/clubplans/0"
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : ServiceCalls.sharedInstance.Xauthorization!,
                                   "focus_club_id": "135"]
        
        let parameters = ["club_id": 192] as [String : Any]
        
        Alamofire.request(urlString,
                          method: .post,
                          parameters: parameters,encoding: JSONEncoding.default,
                          headers: headers)
            .responseJSON { response in
                if let responseData = response.result.value as? [String:Any]{
                    print("responseData of owner is:\(responseData)")
                    if let packageData = responseData["data"] as? [[String:Any]]
                    {
                        self.arrPackageData = responseData["data"] as! [[String : Any]]
                        print(packageData)
                        for review in packageData
                        
                        {
                            let clubPlans = review["clubPlan"] as? [String:Any]
                            print(clubPlans!)
                            
                            guard let id = clubPlans!["id"] else { return }
                            guard let clubid = clubPlans!["clubid"] else { return }
                            guard let planname = clubPlans!["planname"] else { return }
                            guard let membername = clubPlans!["membername"] else { return }
                            
                            guard let paymentinterval = clubPlans!["paymentinterval"] else { return }
                            guard let description = clubPlans!["description"] else { return }
                            guard let imageurl = clubPlans!["imageurl"] else { return }
                            guard let profile_icon_url = clubPlans!["profile_icon_url"] else { return }

//                            print(planname)
//                            let packageClubData = ClubPlan.init(id: id as! String, clubid: clubid as! String, planname: planname as! String, membername: membername as! String, paymentinterval: paymentinterval as! String, description: description as! String, imageurl: description as! String, profile_icon_url: description as! String)
//                            print(packageClubData)
//                            self.arrClubPlan = packageClubData
//                            print("Your array clubplans is :\(self.arrClubPlan)")
                            
                    }
                        self.tblView.reloadData()
                        print(self.arrPackageData)
                }
            }
        }
    }
}
