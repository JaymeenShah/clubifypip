//
//  accountSettingViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 27/08/21.
//

import UIKit

class accountSettingViewController: UIViewController {

    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var viewWalletControl: UIControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        buttonLayouts()
        walletViewLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mainContentView.setMainGradientBackground()
        super.viewWillAppear(animated)
        buttonLayouts()
        walletViewLayout()
    }

    @IBAction func touchOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func buttonLayouts()
    {
        buttonLogout.layer.masksToBounds = true
        buttonLogout.layer.cornerRadius = 30
        
       
        buttonLogout.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
    func walletViewLayout()
    {
        viewWalletControl.layer.masksToBounds = true
        viewWalletControl.layer.cornerRadius = 30
        
       
        viewWalletControl.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
    
    @IBAction func tapOnLogOut(_ sender: UIButton) {
        let storyBoards = UIStoryboard(name: "Main", bundle: nil)
        let logOut = storyBoards.instantiateViewController(identifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(logOut, animated: true)
        
    }
    
}
