//
//  createClubViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 13/09/21.
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField

class createClubViewController: UIViewController {

    //MARK:- IBOutlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var otherClubs: DropDown!
    @IBOutlet weak var createClubButton: UIButton!
    @IBOutlet weak var labelChooseClubTypes: UILabel!
    @IBOutlet weak var viewCreateClub: UIView!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var editClubView: UIView!

    @IBOutlet weak var placeHolderImage: UIImageView!
    @IBOutlet weak var videoUrlImage: UIImageView!

    @IBOutlet weak var editGlobalControl: UIControl!
    @IBOutlet weak var editClubControl: UIControl!
    @IBOutlet weak var editLoyalty: UIControl!
    @IBOutlet weak var editAppControl: UIControl!
    @IBOutlet weak var buttonTrue: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    
    @IBOutlet weak var txtFacebookLink: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTwitterLink: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOtherClubs: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLinkedInLink: SkyFloatingLabelTextField!
    @IBOutlet weak var txtInstagramLink: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPublicLink: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtClubNames: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCaptions: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtWelcomeText: UITextView!
    
    @IBOutlet weak var lblClubProfileIcon: UILabel!
    @IBOutlet weak var lblClubBackground: UILabel!
    @IBOutlet weak var lblCreateClub: UILabel!
    @IBOutlet weak var lblPublic: UILabel!
    @IBOutlet weak var lblPrivate: UILabel!
    @IBOutlet weak var lblAnonymous: UILabel!
    @IBOutlet weak var lblRequiresInvite: UILabel!
    @IBOutlet weak var lblRewards: UILabel!
        
    @IBOutlet weak var editGlobalHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var editClubHieghtConstarints: NSLayoutConstraint!
    @IBOutlet weak var editLoyaltyHeihtConstariants: NSLayoutConstraint!
    @IBOutlet weak var editAppHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtFacebookLinkHeight: NSLayoutConstraint!
    @IBOutlet weak var txtTwtterLinkHeight: NSLayoutConstraint!
    @IBOutlet weak var txtLinkedInHeight: NSLayoutConstraint!
    @IBOutlet weak var txtPhoneNumberHeight: NSLayoutConstraint!
    @IBOutlet weak var txtInstgramLinkHeight: NSLayoutConstraint!
    @IBOutlet weak var txtPublicHeight: NSLayoutConstraint!
    @IBOutlet weak var discriptionBottomView: UIView!
    @IBOutlet weak var welcometextBottomview: UIView!
    
    //MARK:- Variables
    
    var isFlag : Bool = false
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        placeHolderImage.layer.masksToBounds = true
        placeHolderImage.layer.cornerRadius = 16
        videoUrlImage.layer.masksToBounds = true
        videoUrlImage.layer.cornerRadius = 16
        otherClubs.optionArray = ["Car Club", "Coaching Club","Other Club","Real Estate Club","Public Speaker Club"]
        
        otherClubs.didSelect{(selectedText , index ,id) in
            //            self.optionalInformation.text = "\(selectedText)"
            self.txtOtherClubs.placeholder = ""
            self.txtOtherClubs.text = " "
            self.txtOtherClubs.isEnabled = false
            self.txtOtherClubs.selectedLineColor = UIColor.clear
            self.txtOtherClubs.lineColor = UIColor.clear
            self.txtOtherClubs.selectedLineHeight = 0.0
            
            print("Selected String: \(selectedText) \n index: \(index)")
        }
        
        createClubButton.layer.masksToBounds = true
        createClubButton.layer.cornerRadius = 25
            
            
        createClubButton.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        mainContentView.setMainGradientBackground()
        controllersLyoutsAndGradients()
        txtFacebookLink.isHidden = true
        txtTwitterLink.isHidden = true
        txtLinkedInLink.isHidden = true
        txtInstagramLink.isHidden = true
        txtPublicLink.isHidden  = true
        txtPhoneNumber.isHidden = true
        
        editGlobalHeightContraints.constant = 0
        editLoyaltyHeihtConstariants.constant = 0
        editAppHeightConstraint.constant = 0
        editClubHieghtConstarints.constant = 0
        txtFacebookLink.selectedTitle = ""
        txtTwitterLink.selectedTitle = ""
        txtLinkedInLink.selectedTitle = ""
        txtInstagramLink.selectedTitle = ""
        txtPublicLink.selectedTitle = ""
        txtPhoneNumber.selectedTitle = ""

        txtFacebookLinkHeight.constant = 0
        txtTwtterLinkHeight.constant = 0
        txtLinkedInHeight.constant = 0
        txtInstgramLinkHeight.constant = 0
        txtPublicHeight.constant = 0
        txtPhoneNumberHeight.constant = 0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        createClubButton.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        mainContentView.setMainGradientBackground()
        controllersLyoutsAndGradients()
        editGlobalHeightContraints.constant = 0
        editLoyaltyHeihtConstariants.constant = 0
        editAppHeightConstraint.constant = 0
        editClubHieghtConstarints.constant = 0

        txtFacebookLink.selectedTitle = ""
        txtTwitterLink.selectedTitle = ""
        txtLinkedInLink.selectedTitle = ""
        txtInstagramLink.selectedTitle = ""
        txtPublicLink.selectedTitle = ""
        txtPhoneNumber.selectedTitle = ""
        txtFacebookLinkHeight.constant = 0
        txtTwtterLinkHeight.constant = 0
        txtLinkedInHeight.constant = 0
        txtInstgramLinkHeight.constant = 0
        txtPublicHeight.constant = 0
        txtPhoneNumberHeight.constant = 0
    }
}
//MARK:- Functions
extension createClubViewController
{
    func controllersLyoutsAndGradients()
    {
        editGlobalControl.layer.masksToBounds = true
        editGlobalControl.layer.cornerRadius = 23
        
        editGlobalControl.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        editClubControl.layer.masksToBounds = true
        editClubControl.layer.cornerRadius = 23
        
        editClubControl.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        editLoyalty.layer.masksToBounds = true
        editLoyalty.layer.cornerRadius = 23
        
        editLoyalty.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        editAppControl.layer.masksToBounds = true
        editAppControl.layer.cornerRadius = 23
        
        editAppControl.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        
    }
}

//MARK:- Actions
extension createClubViewController
{
    @IBAction func touchOnTrueButton(_ sender: UIButton) {
        editGlobalHeightContraints.constant = 46
        editLoyaltyHeihtConstariants.constant = 46
        editAppHeightConstraint.constant = 46
        editClubHieghtConstarints.constant = 46
        
        txtFacebookLink.isHidden = false
        txtTwitterLink.isHidden = false
        txtLinkedInLink.isHidden = false
        txtInstagramLink.isHidden = false
        txtPublicLink.isHidden  = false
        txtPhoneNumber.isHidden = false
        txtFacebookLinkHeight.constant = 42
        txtTwtterLinkHeight.constant = 42
        txtLinkedInHeight.constant = 42
        txtInstgramLinkHeight.constant = 42
        txtPublicHeight.constant = 42
        txtPhoneNumberHeight.constant = 42
        lblClubBackground.textColor = UIColor.black
        lblClubProfileIcon.textColor = UIColor.black
        lblCreateClub.textColor = UIColor.black
        lblCreateClub.text = "Edit Club"
        editClubView.backgroundColor = UIColor.white
        mainContentView.setWhiteGradientBackground()
        scrollView.backgroundColor = UIColor.white
        viewCreateClub.backgroundColor = UIColor.white
        labelChooseClubTypes.textColor = UIColor.black
        lblPublic.textColor = UIColor.black
        lblPrivate.textColor = UIColor.black
        lblAnonymous.textColor = UIColor.black
        lblRequiresInvite.textColor = UIColor.black
        lblRewards.textColor = UIColor.black
        createClubButton.setTitle("SAVE CHANGES", for: .normal)
        discriptionBottomView.backgroundColor = UIColor.systemGray
        welcometextBottomview.backgroundColor = UIColor.systemGray
        txtFacebookLink.textColor = UIColor.black
        txtTwitterLink.textColor = UIColor.black
        txtLinkedInLink.textColor = UIColor.black
        txtPhoneNumber.textColor = UIColor.black
        txtPublicLink.textColor = UIColor.black
        txtInstagramLink.textColor = UIColor.black
        txtClubNames.textColor = UIColor.black
        txtWelcomeText.textColor = UIColor.black
        txtCaptions.textColor = UIColor.black
        txtDescription.textColor = UIColor.black
        txtClubNames.selectedTitle = "CLUB NAME"
        txtClubNames.selectedTitleColor = UIColor.systemGray
        txtCaptions.selectedTitle = "CAPTION"
        txtCaptions.selectedTitleColor = UIColor.systemGray
    }
    @IBAction func touchOnBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func touchOnCreateClubVC(_ sender: Any) {
        
        
        editGlobalHeightContraints.constant = 0
        editLoyaltyHeihtConstariants.constant = 0
        editAppHeightConstraint.constant = 0
        editClubHieghtConstarints.constant = 0
        txtFacebookLinkHeight.constant = 0
        txtTwtterLinkHeight.constant = 0
        txtLinkedInHeight.constant = 0
        txtInstgramLinkHeight.constant = 0
        txtPublicHeight.constant = 0
        txtPhoneNumberHeight.constant = 0
        txtFacebookLink.isHidden = true
        txtTwitterLink.isHidden = true
        txtLinkedInLink.isHidden = true
        txtInstagramLink.isHidden = true
        txtPublicLink.isHidden  = true
        txtPhoneNumber.isHidden = true
        editClubView.backgroundColor = UIColor.white
        mainContentView.backgroundColor = UIColor.white
        viewCreateClub.backgroundColor = UIColor.black
        labelChooseClubTypes.textColor = UIColor.white
        lblPublic.textColor = UIColor.white
        lblPrivate.textColor = UIColor.white
        lblAnonymous.textColor = UIColor.white
        lblRequiresInvite.textColor = UIColor.white
        lblRewards.textColor = UIColor.white
        createClubButton.setTitle("CREATE CLUB NOW", for: .normal)
        scrollView.backgroundColor = UIColor.black
        editClubView.backgroundColor = UIColor.black
        lblCreateClub.textColor = UIColor.white
        lblCreateClub.text = "Create a club"
    }
    
    @IBAction func touchOnEditGlobalMenu(_ sender: UIControl) {
        print("Edit Global Sidebar Menu")
        let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let globalVc = storyboards.instantiateViewController(identifier: "editGlobalSideMenuController") as! editGlobalSideMenuController
        self.navigationController?.pushViewController(globalVc, animated: true)
    }
    @IBAction func touchOnClubPackages(_ sender: UIControl) {
        let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let globalVc = storyboards.instantiateViewController(identifier: "clubPackagesViewController") as! clubPackagesViewController
        self.navigationController?.pushViewController(globalVc, animated: true)
        
    }
    
    
    @IBAction func touchOnEditLoyalty(_ sender: UIControl) {
        let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let globalVc = storyboards.instantiateViewController(identifier: "loyaltyAndRewardController") as! loyaltyAndRewardController
        self.navigationController?.pushViewController(globalVc, animated: true)
        
    }
    @IBAction func touchOnEditAppColors(_ sender: UIControl) {
        let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let editAppColorVc = storyboards.instantiateViewController(identifier: "editAppColorVC") as! editAppColorVC
        self.navigationController?.pushViewController(editAppColorVc, animated: true)
       
    }
    
}
