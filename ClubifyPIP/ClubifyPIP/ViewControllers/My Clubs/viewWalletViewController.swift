//
//  viewWalletViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 07/09/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class viewWalletViewController: UIViewController {
    
    //MARK:- IBoutlets
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var buttonAddCoins: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewAddIcons: UIView!
    @IBOutlet weak var lblCoinBalance: UILabel!
    @IBOutlet weak var lblCurrencyBalance: UILabel!
    @IBOutlet weak var viewUpperSection: UIView!
    
    //MARK:- Variables
    
    var coinsTitle = ["Credit","Credit","Credit","Credit","Credit","Credit"]
    var numbersSubtitle = ["100 Coins","100 Coins","100 Coins","100 Coins","100 Coins","100 Coins"]
    var statusData = [[String:Any]]()
    var userData : [UserInfo] = []
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        viewAddIcons.setMainGradientBackground()
        viewUpperSection.setMainGradientBackground()
        buttonLayouts()
        guard let Xauthorization = UserDefaults.standard.string(forKey: "authToken") else { return }
        walletApi(Xauthorization: Xauthorization)
        transactionApi(Xauthorization: Xauthorization)
    }
}

//MARK:- Actions
extension viewWalletViewController
{
    @IBAction func touchOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Layouts

extension viewWalletViewController
{
    func buttonLayouts()
    {
        buttonAddCoins.layer.masksToBounds = true
        buttonAddCoins.layer.cornerRadius = 25
        buttonAddCoins.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
    
}

//MARK:- Tableview delegates and datasource
extension viewWalletViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "viewWalletCell",for: indexPath) as! viewWalletCell
        let dict = statusData[indexPath.row]
        print("tableview cell dict is:\(dict)")
        if dict["title"] == nil{
            cell.lblTitle.text = "Credit"
        }
        else
        {
            cell.lblTitle.text = dict["title"] as? String
            
        }
        cell.lblCoins.text = (dict["coins"] as? String)! + "  " + "coins"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- Api Calling

extension viewWalletViewController
{
    //MARK:- WalletApi
    func walletApi(Xauthorization:String)
    {
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : Xauthorization,
                                   "focus_club_id": "135"]
        let parameters = ["app_name" : "Clubify"] as [String:Any]
        let urlVal: String =  baseUrl.development.rawValue + "/monetization/wallet"
        print(urlVal)
        
        Alamofire.request(urlVal,
                          method: .get,
                          parameters: nil,encoding: URLEncoding.default,
                          headers: headers)
            .responseJSON {response in
                if let responseData = response.result.value as? [String:Any]{
                    print("responseData of owner is:\(responseData)")
                    let data = responseData["data"] as? [String:Any]
                    print(data as Any)
                    let account_balance = data!["account_balance"]
                    print(account_balance!)
                    self.lblCoinBalance.text = account_balance as? String
                    self.lblCurrencyBalance.text = "$" + "\(String(describing: account_balance!))"
                }
            }
        
    }
    
    //MARK:- TRANSACTION API
    func transactionApi(Xauthorization:String)
    {
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : Xauthorization,
                                   "focus_club_id": "135"]
        let parameters = ["app_name" : "Clubify"] as [String:Any]
        let urlVal: String =  baseUrl.development.rawValue + "/monetization/transactions"
        print(urlVal)
        
        
        
        Alamofire.request(urlVal, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers ).responseJSON { [self] response in
            
            print("### commonGETRequestCall  transactionApi response :: \(response)")
            if let responseData = response.result.value as? [String:Any]{
                print("responseData of owner is:\(responseData)")
                self.statusData = responseData["data"] as! [[String:Any]]
                
                if let statusData = responseData["data"] as? [[String:Any]] {
                    print("Array of transaction is:\(statusData)")
                    tblView.reloadData()
                }
            }
        }
    }
}

//ServiceCalls.sharedInstance.getApiResponseWithDictionary(urlVal: urlVal, method: .get, parameters: parameters, headers: headers) { (result) in
//    print("your tansaction result is:\(result)")
//}
