//
//  userSettingsViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 06/09/21.
//

import UIKit
import Alamofire
import SkyFloatingLabelTextField
import SwiftyJSON

class userSettingsViewController: UIViewController {
    
    //MARK:- IBoutlets
    
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var viewWalletControl: UIControl!
    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var txtGender: SkyFloatingLabelTextField!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtUserTextView: UITextView!
    
    //MARK:- Variables
    
    var usersData  = User()
    var userInfoData = UsersDataModel()
    
    //MARK:- LifeCycleMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        buttonLayouts()
        walletViewLayout()
        gradientBackgroud()
        
        setUserInfo()
        self.imgProfile.layer.masksToBounds = true
        self.imgProfile.layer.cornerRadius = 16
        userInfoAPI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
}


//MARK:- Actions
extension userSettingsViewController
{
    @IBAction func touchOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func touchOnLogout(_ sender: UIButton) {
        logOutApi()
    }
    
    @IBAction func touchOnViewWallet(_ sender: UIControl) {
        userUpdateAPI()
    }
}
//MARK:- Functions
extension userSettingsViewController
{
    //..gradient button backgoud function
    func gradientBackgroud()
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [hexStringToUIColor(hex: "30303F").cgColor,  hexStringToUIColor(hex: "000000").cgColor]
        gradient.locations = [0.6 , 1.0]
        gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.frame = self.mainContentView.layer.frame
        self.mainContentView.layer.insertSublayer(gradient, at: 0)
    }
    //..bottom button layouts functions
    func buttonLayouts()
    {
        buttonLogout.layer.masksToBounds = true
        buttonLogout.layer.cornerRadius = 25
        
        
        buttonLogout.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
    func walletViewLayout()
    {
        viewWalletControl.layer.masksToBounds = true
        viewWalletControl.layer.cornerRadius = 25
        
        viewWalletControl.setMainViewBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
}
//MARK:- usersetting api call

extension userSettingsViewController
{
    func setUserInfo(){
        
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : ServiceCalls.sharedInstance.Xauthorization!,
                                   "focus_club_id": "135"]
        
        let urlVal: String =  baseUrl.development.rawValue + "/user/info"
        print(urlVal)
        
        
        Alamofire.request(urlVal, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) -> Void in
            switch response.result {
            case .success:
                if let data = response.result.value as? Dictionary<String, AnyObject> {
                    print("userinfo data from resultvalue is:\(data)")
                    
                }
                break
            case .failure:
                print("Error")
            }
        }
    }
    //MARK:- LogOutApi
    
    func logOutApi()
    {
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : ServiceCalls.sharedInstance.Xauthorization!,
                                   "focus_club_id": "135"]
        let parameters = ["app_name" : "Clubify"] as [String:Any]
        let urlVal: String =  baseUrl.development.rawValue + "/users/logout/1"
        print(urlVal)
        
        
        Alamofire.request(urlVal, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers ).responseString { response in
            
            print("### commonGETRequestCall response :: \(response)")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(nextVC, animated: true)
            
        }
        
    }
}

//MARK:- discover club api calling
extension userSettingsViewController
{
    func userInfoAPI() {
        let urlString = "http://apidev.clubify.com/user/info"
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : ServiceCalls.sharedInstance.Xauthorization!,
                                   "focus_club_id": "135"]
        
        Alamofire.request(urlString,
                          method: .get,
                          parameters: nil,encoding: URLEncoding.default,
                          headers: headers)
            .responseJSON { response in
                if let responseData = response.result.value as? [String:Any]{
                    
                    if let data = responseData["data"] as? [String:Any]{
                        for (_ , value) in data
                        {
                            if let dataNew = value as? [String:Any]
                            {
                                let data1 = creatDictnory(value:dataNew as AnyObject)
                                guard let id = data1["id"] as? String else { return }
                                print("using key value loop id is:\(id)")
                                let usersData = UsersDataModel.init(id: id, firstname: data1["firstname"] as! String, username: data1["username"] as! String, profilepicture: data1["profilepicture"] as! String, biography: data1["biography"] as! String, lastname: data1["lastname"] as! String, gender: data1["gender"] as! String, email: data1["email"] as! String, phone: data1["phone"] as! String)
                                self.userInfoData = usersData
                                self.txtUserName.text = self.userInfoData.username
                                self.txtEmail.text = self.userInfoData.email
                                self.txtFirstName.text = self.userInfoData.firstname
                                self.txtLastName.text = self.userInfoData.lastname
                                self.txtPhone.text = self.userInfoData.phone
                                self.txtGender.text = self.userInfoData.gender
                                self.txtUserTextView.text = self.userInfoData.biography
                                print("array of userinfo is:\(self.userInfoData)")
                            }
                        }
                    }
                }
            }
    }
    
    //MARK:- ...Update api
    func userUpdateAPI() {
        let urlString = "http://apidev.clubify.com/user/update"
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : ServiceCalls.sharedInstance.Xauthorization!,
                                   "focus_club_id": "135"]
        
        let parameters = ["email":txtEmail.text!,
                          "username":txtUserName.text!,
                          "firstname":txtFirstName.text!,
                          "lastname": txtLastName.text!,
                          "gender": txtGender.text!,
                          "accounttypeid": "1" ,
                          "biography": txtUserTextView.text!,
                          "phone": txtPhone.text!,
                          "is_multiclub": "0",
                          "profile_video": "0"] as [String : Any]
        
        print(parameters)
        
        Alamofire.request(urlString, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                print("Done")
            case .failure(let error):
                print(error)
            }
        }
    }
}

