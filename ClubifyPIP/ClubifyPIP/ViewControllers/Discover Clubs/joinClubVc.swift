//
//  joinClubVc.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 26/08/21.
//

import UIKit

class joinClubVc: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonJoinClub: UIButton!
    @IBOutlet var mainContentView: UIView!
    @IBOutlet weak var imageCenterProfile: UIImageView!
    
    //MARK:- Variables
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        buttonLayouts()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        mainContentView.setMainGradientBackground()
        super.viewWillAppear(animated)
    }
}


//MARK:- Actions
@available(iOS 13.0, *)
extension joinClubVc
{
    @IBAction func touchOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func touchOnJoinClub(_ sender: UIButton) {
        let myClubStoryBoard = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let myClubvC = myClubStoryBoard.instantiateViewController(identifier: "myClubsViewController") as! myClubsViewController
        self.navigationController?.pushViewController(myClubvC, animated: true)
        
    }
}

//MARK:- functions
extension joinClubVc
{
    
    func buttonLayouts()
    {
        buttonJoinClub.layer.masksToBounds = true
        buttonJoinClub.layer.cornerRadius = 25
        
        imageCenterProfile.layer.masksToBounds = true
        imageCenterProfile.layer.cornerRadius = 12
        
        buttonJoinClub.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
}
