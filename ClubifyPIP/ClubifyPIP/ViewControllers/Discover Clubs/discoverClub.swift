//
//  discoverClub.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 25/08/21.
//

import Foundation
import UIKit
class discoverClubVc: UIViewController {

    //MARK:- IBoutlets
    @IBOutlet weak var collectionViewDiscover: UICollectionView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var collectionViewHasTag: UICollectionView!
    @IBOutlet weak var mainViewSearch: UIView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    //MARK:- Variable
    
    var hashTagArray = ["#adventure","#pop","#sports","#corporate"]
    var titleString = ["Bossibly","Inspired moms","Virtual Health","Sports Masters","Bossibly","Inspired moms","Virtual Health","Sports Masters","Bossibly","Inspired moms","Virtual Health","Sports Masters"]
    var subTittleString = ["FEMALE EMPOWERMENT","PARENTHOOD","HEALTH","SPORTS","FEMALE EMPOWERMENT","PARENTHOOD","HEALTH","SPORTS","FEMALE EMPOWERMENT","PARENTHOOD","HEALTH","SPORTS"]
    var discription = ["Femail LeaderShi1p.." ,"For the Mom..","Get Fit Today","Loved IT","Femail LeaderShi1p.." ,"For the Mom..","Get Fit Today","Loved IT","Femail LeaderShi1p.." ,"For the Mom..","Get Fit Today","Loved IT"]
    lazy   var searchBars:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 60, width: 200, height: 20))
    
    var searchController: UISearchController {
        
        let search = UISearchController(searchResultsController: nil)
        
        
        search.obscuresBackgroundDuringPresentation = false
        
        return search
    }
    //    MARK:- LycycleMethod

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 5
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        
        self.collectionViewHasTag.collectionViewLayout = layout

        viewSearch.layer.cornerRadius = 18
        searchTextField.backgroundColor = UIColor.white // Use anycolor that give you a 2d look.

            //To apply corner radius
        searchTextField.layer.cornerRadius = viewSearch.frame.size.height / 2

            //To apply border
        searchTextField.layer.borderWidth = 0.25
        searchTextField.layer.borderColor = UIColor.white.cgColor

            //To apply Shadow
        searchTextField.layer.shadowOpacity = 1
        searchTextField.layer.shadowRadius = 3.0
        searchTextField.layer.shadowOffset = CGSize.zero // Use any CGSize
        searchTextField.layer.shadowColor = UIColor.gray.cgColor

    }
    func hiddenSearchController() {
        navigationItem.searchController = nil
    }
    //..searchControllerFunction
    
    
    func showSearchController() {

        navigationItem.searchController = searchController

        navigationItem.hidesSearchBarWhenScrolling = true

        definesPresentationContext = true
    }

}
