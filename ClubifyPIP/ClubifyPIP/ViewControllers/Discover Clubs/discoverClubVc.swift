//
//  discoverClub.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 25/08/21.
//

import Foundation
import UIKit
import Alamofire

class discoverClubVc: UIViewController, UISearchBarDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    
    //MARK:- IBoutlets
    @IBOutlet weak var mainContentView: UIView!
    
    @IBOutlet weak var collectionViewDiscover: UICollectionView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var collectionViewHasTag: UICollectionView!
    @IBOutlet weak var mainViewSearch: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var serachBar: UISearchBar!
    
    //MARK:- Variable
    var searchController: UISearchController!
    
    var hashTagArray = ["#adventure","#pop","#sports","#corporate"]
    var titleString = ["Clubify Club","Contest Tests","Critter Care Brothers...","Everything Tennis"]
    var subTittleString = ["PARENTHOOD","PARENTHOOD","PARENTHOOD","PARENTHOOD"]
    var discription = ["Club" ,"Test","Critter Care Brothers","Everthing Tennis"]
    var arrClubData = [discoverId]()
    
    var responseDict = [String:Any]()
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()
    
    
    //    MARK:- LifecycleMethod
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        gradientBackgroud()
        
        
        title = "Dicover Clubs"
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.hidesSearchBarWhenScrolling = true
        navigationItem.searchController = searchController
        self.navigationController?.navigationBar.prefersLargeTitles = true
        configureSearchController()
        mainContentView.setMainGradientBackground()
        callAPI()
    }
    
    //..searchControllerFunction
    
    
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search here..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        // Place the search bar view to the tableview headerview.
    }
    
    func gradientBackgroud()
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [hexStringToUIColor(hex: "30303F").cgColor,  hexStringToUIColor(hex: "000000").cgColor]
        gradient.locations = [0.6 , 1.0]
        gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.frame = self.mainContentView.layer.frame
        self.mainContentView.layer.insertSublayer(gradient, at: 0)
    }
    @IBAction func tapOnBackButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- discover club api calling
extension discoverClubVc
{
    func callAPI() {
        let urlString = "http://apidev.clubify.com/clubs/public"
        var clubsData = [discoverId]()
        
        Alamofire.request(urlString,
                          method: .get,
                          parameters: nil,encoding: URLEncoding.default,
                          headers: nil)
            .responseJSON { response in
                if let responseData = response.result.value as? [String:Any]{
                    
                    if let data = responseData["data"] as? [String:Any]{
                        for (_ , value) in data
                        {
                            if let dataNew = value as? [String:Any]
                            {
                                let data1 = creatDictnory(value:dataNew as AnyObject)
                                guard let id = data1["id"] as? String else { return }
                                guard let profile_image_url = data1["profile_image"] else { return }
                                let clubData = discoverId.init(id: id, owner_id: (data1["owner_id"] as? String)!, type: data1["type"] as! String, name: data1["name"] as! String, subtitle: data1["subtitle"] as! String, description: data1["description"] as! String, anonymous:data1["anonymous"] as! String, requires_invite: data1["requires_invite"] as! String, profile_image: profile_image_url as! String)
                                clubsData.append(clubData)
                            }
                        }
                        if clubsData.count > 0
                        {
                            for dict in clubsData {
                                
                                self.arrClubData.append(dict)
                            }
                        }
                        
                        self.collectionViewHasTag.reloadData()
                        self.collectionViewDiscover.reloadData()
                    }
                }
            }
    }
}




