//
//  SideMenuViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 05/10/21.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSideMenu: UIButton!
    
    var arrSideMenuData = ["Members List","Private EVent"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func touchOnSideMenu(_ sender: UIButton) {
        
        let storyBoards = UIStoryboard(name: "SideMenu", bundle: nil)
        let secondVC = storyBoards.instantiateViewController(withIdentifier: "firstViewController")
            let contentViewController = UINavigationController(rootViewController: secondVC)
            sideMenuViewController?.setContentViewController(contentViewController, animated: true)
            sideMenuViewController?.hideMenuViewController()
    }
}
//TableviewDelegate and datasource

extension SideMenuViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuTableviewCell",for: indexPath) as! sideMenuTableviewCell
        cell.lblTitle.text = arrSideMenuData[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
//        {
//            let sideMenuSb = UIStoryboard(name: "VirtualHealth", bundle: nil)
//            let nextVc:ClubMemberListVC = sideMenuSb.instantiateViewController(identifier: "ClubMemberListVC") as! ClubMemberListVC
//            self.navigationController?.pushViewController(nextVc, animated: true)
        
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "VirtualHealth", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ClubMemberListVC") as! ClubMemberListVC
        let contentViewController = UINavigationController(rootViewController: viewController)

        UIApplication.shared.keyWindow?.rootViewController = viewController
        
        
        }
    }
    
}

