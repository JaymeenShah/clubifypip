//
//  File.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 28/09/21.
//

import Foundation
import AKSideMenu
import Foundation
import UIKit

final class RootViewController: AKSideMenu, AKSideMenuDelegate {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.menuPreferredStatusBarStyle = .lightContent
        self.contentViewShadowColor = .black
        self.contentViewShadowOffset = CGSize(width: 0, height: 0)
        self.contentViewShadowOpacity = 0.6
        self.contentViewShadowRadius = 12
        self.contentViewShadowEnabled = true

        self.backgroundImage = UIImage(named: "Stars")
        self.delegate = self
        let sideMenuStoryBoards = UIStoryboard(name: "SideMenu", bundle: nil)
        self.leftMenuViewController = sideMenuStoryBoards.instantiateViewController(withIdentifier: "leftMenuViewController")

        //        if let storyboard = self.storyboard {
        //            
        //            self.contentViewController = storyboard.instantiateViewController(withIdentifier: "contentViewController")
        //            self.leftMenuViewController = storyboard.instantiateViewController(withIdentifier: "leftMenuViewController")
        ////            self.rightMenuViewController = storyboard.instantiateViewController(withIdentifier: "rightMenuViewController")
        //        }
    }

    // MARK: - <AKSideMenuDelegate>

    public func sideMenu(_ sideMenu: AKSideMenu, willShowMenuViewController menuViewController: UIViewController) {
        debugPrint("willShowMenuViewController")
    }

    public func sideMenu(_ sideMenu: AKSideMenu, didShowMenuViewController menuViewController: UIViewController) {
        debugPrint("didShowMenuViewController")
    }

    public func sideMenu(_ sideMenu: AKSideMenu, willHideMenuViewController menuViewController: UIViewController) {
        debugPrint("willHideMenuViewController")
    }

    public func sideMenu(_ sideMenu: AKSideMenu, didHideMenuViewController menuViewController: UIViewController) {
        debugPrint("didHideMenuViewController")
    }
    
//    @IBAction func leftButtonAction(_ sender: UIBarButtonItem) {
//        self.sideMenuViewController?.presentLeftMenuViewController()
//    }
}



// MARK: - UIViewController+AKSideMenu

extension UIViewController {

    public var sideMenuViewController: AKSideMenu? {
        guard var iterator = self.parent else { return nil }
        guard let strClass = String(describing: type(of: iterator)).components(separatedBy: ".").last else { return nil }

        while strClass != nibName {
            if iterator is AKSideMenu {
                return iterator as? AKSideMenu
            } else if let parent = iterator.parent, parent != iterator {
                iterator = parent
            } else {
                break
            }
        }
        return nil
    }

    // MARK: - Public
    // MARK: - IBAction Helper methods

    @IBAction public func presentLeftMenuViewController(_ sender: AnyObject) {
        self.sideMenuViewController?.presentLeftMenuViewController()
    }

    @IBAction public func presentRightMenuViewController(_ sender: AnyObject) {
        self.sideMenuViewController?.presentRightMenuViewController()
    }
}
