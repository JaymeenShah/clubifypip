//
//  RegisterViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField





class RegisterViewController: UIViewController {
    //MARK:- IBOutlets
    
    @IBOutlet weak var buttonRegisterAccount: UIButton!
    
    @IBOutlet weak var dropDown: DropDown!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtOptionlInformation: SkyFloatingLabelTextField!
    @IBOutlet weak var mainContentView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        buttonLayouts()
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [hexStringToUIColor(hex: "30303F").cgColor,  hexStringToUIColor(hex: "000000").cgColor]
        gradient.locations = [0.6 , 1.0]
        gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.frame = self.mainContentView.layer.frame
        self.mainContentView.layer.insertSublayer(gradient, at: 0)
        
        dropDown.optionArray = ["Male", "Female"]
        
        dropDown.didSelect{(selectedText , index ,id) in
            //            self.optionalInformation.text = "\(selectedText)"
            self.txtOptionlInformation.placeholder = ""
            self.txtOptionlInformation.text = " "
            self.txtOptionlInformation.title = "Optional Information"
            self.txtOptionlInformation.isEnabled = false
            self.txtOptionlInformation.selectedLineColor = UIColor.clear
            self.txtOptionlInformation.lineColor = UIColor.clear
            self.txtOptionlInformation.selectedLineHeight = 0.0
            
            print("Selected String: \(selectedText) \n index: \(index)")
        }
        
    }
    
    
    
}

//MARK:- Global hex color
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
//MARK:- APIHandling
extension RegisterViewController
{
    func setRegisterAPICall(username:String,email:String,password:String,firstname:String,lastname:String,gender:String)
    {
        let paramsDict = [
            "username": username,
            "email": email,
            "password": password,
            "firstname": firstname,
            "lastname": lastname,
            "gender": "Male",
            "accounttypeid": "1",
            "app_name": "Clubify"
        ] as [String:Any]

//        var loginData = [user]()
        
        let urlVal: String =  baseUrl.development.rawValue + "/user/create"
        print(urlVal)
        ServiceCalls.sharedInstance.PostAPICallWithHeaderValues(url: urlVal, postData: paramsDict, headersData: nil) { (sucess, resultValue) in
            print(resultValue)
            if sucess == true
            {
                if let data = resultValue["data"] as? [String:Any]
                {
                    
                    print(data)
                    guard let message = resultValue["message"] else { return }
                    guard let users =  data["user"] as? [String:Any] else { return }
                    guard let id = users["id"] else { return }
//                    let loginReport = user.init(id: id as! String,authorization:  data["Authorization"] as! String, message: message as! String)
                    print("Your login suceess is :\(resultValue)")
//                    loginData.append(loginReport)
//                    print("Your login Array is:\(loginData)")
                    let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
                    let nextVc = storyboards.instantiateViewController(identifier: "myClubsViewController") as! myClubsViewController
                    self.navigationController?.pushViewController(nextVc, animated: true)
                }
                
            }
            else if let result : [ String:Any ] = resultValue["error"] as? [String: Any]
            {
                print("Error is:\(result)")
            }
        }
        
    }
    
}




