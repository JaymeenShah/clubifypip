//
//  ForgotPasswordViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 23/08/21.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var recoverPassWordView: UIView!
    @IBOutlet weak var txtResetPassword: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        
        buttonLayouts()
        showAnimate()
        
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        setForgotPasswordAPICall(email: txtResetPassword.text!)
    }
    @IBAction func passvordRecoveryValidation(_ sender: SkyFloatingLabelTextField) {
        validateEmailTextFieldWithText(email: txtResetPassword.text)
    }
    
    func validateEmailTextFieldWithText(email: String?) {
        guard let email = email else {
            txtResetPassword.errorMessage = nil
            txtResetPassword.title = "Email"
            txtResetPassword.selectedTitle = "Email"
            
            return
        }
        
        if email.isEmpty {
            txtResetPassword.errorMessage = nil
        } else if !validateEmail(email) {
            txtResetPassword.errorMessage = NSLocalizedString(
                "Email not valid",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtResetPassword.errorMessage = nil
            txtResetPassword.title = "Email"
            txtResetPassword.selectedTitle = "Email"
        }
    }
    
    // MARK: - validation
    
    func validateEmail(_ candidate: String) -> Bool {
        
        // NOTE: validating email addresses with regex is usually not the best idea.
        // This implementation is for demonstration purposes only and is not recommended for production use.
        // Regex source and more information here: http://emailregex.com
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
}

//MARK:- TextFieldValidation Function

extension ForgotPasswordViewController
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Validate the email field
        if textField == txtResetPassword {
            passvordRecoveryValidation(txtResetPassword)
        }
        
        // When pressing return, move to the next field
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
}
//MARK:- Functions
extension ForgotPasswordViewController
{
    func buttonLayouts()
    {
        
        // Do any additional setup after loading the view.
        btnSend.layer.cornerRadius = 20
        btnSend.layer.masksToBounds = true
        btnSend.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green: 107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        recoverPassWordView.layer.cornerRadius = 12
        recoverPassWordView.layer.masksToBounds = true
        
        
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
    
}
//MARK:- APIHandling
extension ForgotPasswordViewController
{
    func setForgotPasswordAPICall(email:String)
    {
        let paramsDict = [
            "email": email,
            "app_name": "Clubify"
        ] as [String:Any]
        
        //        var loginData = [user]()
        
        let urlVal: String =  baseUrl.development.rawValue + "/users/recoverpassword"
        print(urlVal)
        ServiceCalls.sharedInstance.PostAPICallWithHeaderValues(url: urlVal, postData: paramsDict, headersData: nil) { (sucess, resultValue) in
            print(resultValue)
            if sucess == true
            {
                if let data = resultValue["data"] as? [String:Any]
                {
                    
                    print(data)
                    guard let statuscode = resultValue["status"] else { return }
                    print(statuscode)
                    guard let message = resultValue["message"] else { return }
                    guard let users =  data["user"] as? [String:Any] else { return }
                    guard let id = users["id"] else { return }
                    //                    let loginReport = user.init(id: id as! String,authorization:  data["Authorization"] as! String, message: message as! String)
                    //                    print("Your login suceess is :\(resultValue)")
                    //                    loginData.append(loginReport)
                    //                    print("Your login Array is:\(loginData)")
                    let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
                    let nextVc = storyboards.instantiateViewController(identifier: "myClubsViewController") as! myClubsViewController
                    self.navigationController?.pushViewController(nextVc, animated: true)
                }
                
            }
            else if let result : [ String:Any ] = resultValue["error"] as? [String: Any]
            {
                print("Error is:\(result)")
            }
        }
        
    }
    
}


