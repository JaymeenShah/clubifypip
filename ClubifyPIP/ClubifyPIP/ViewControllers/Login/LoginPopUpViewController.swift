//
//  LoginPopUpViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.
//

import UIKit
import FBSDKLoginKit
import AuthenticationServices

protocol FirstViewControllerProtocol {
    // Use protocol/delegate to communicate within two view controllers
    func dismiss()
}

class LoginPopUpViewController: UIViewController {
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var loginPopUpView: UIView!
    @IBOutlet weak var buttonAppleLogin: UIButton!
    
    @IBOutlet weak var buttonEmailLogin: UIButton!
    @IBOutlet weak var buttonFacebookLogin: UIButton!
    
    //MARK:- Variable
    
    var delegate: FirstViewControllerProtocol?
    
    let baseurl = UserDefaults.standard.string(forKey: "baseurl")
    let myView = SpinnerView(frame: CGRect(x: screenSize.width/2, y: screenSize.height/2, width: 50, height: 50))
    let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)

    //MARK:- LifeCycleMethods
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Your latest base usrl is:\(String(describing: baseurl))")
        showAnimate()
        buttonLayouts()
        checkStatusOfAppleSignIn()
        activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        activityIndicator.style = .large
        activityIndicator.color = .red
        view.addSubview(activityIndicator)
        if Reachabilitties.isConnectedToNetwork() {
            // Go ahead and fetch your data from the internet
            // ...
        } else {
            print("Internet connection not available")
            
            
            let alert = UIAlertController(title: "No Internet connection", message: "Please ensure you are connected to the Internet", preferredStyle:.alert)
            
            present(alert, animated: true, completion: nil)
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.view.addSubview(myView)

        self.removeAnimate()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.removeAnimate()
    }
    @objc func didTapView(_ sender: UITapGestureRecognizer) {
        removeAnimate()
        activityIndicator.stopAnimating()
    }
}

//MARK:- Functions
extension LoginPopUpViewController
{
    func buttonLayouts()
    {
        // Do any additional setup after loading the view.
        loginPopUpView.layer.masksToBounds = true
        loginPopUpView.layer.cornerRadius = 12
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        buttonAppleLogin.layer.cornerRadius = 22.5
        buttonAppleLogin.layer.masksToBounds = true
        
        buttonFacebookLogin.layer.cornerRadius = 22.5
        buttonFacebookLogin.layer.masksToBounds = true
        buttonEmailLogin.layer.cornerRadius = 22.5
        buttonEmailLogin.layer.masksToBounds = true
        
        buttonEmailLogin.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green: 107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
    //MARK:- checkStatus for Apple Login
    
    func checkStatusOfAppleSignIn()
    {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: "\(String(describing: UserDefaults.standard.value(forKey: "User_AppleID")))") { (credentialState, error) in
            
            switch credentialState {
            case .authorized:
                let message = UserDefaults.standard.string(forKey: "User_AppleID")
                let refreshAlert = UIAlertController(title: "You logged in with", message: message, preferredStyle: UIAlertController.Style.alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    print("Handle Ok logic here")
                }))
                
                self.present(refreshAlert, animated: true, completion: nil)
                break
            default:
                break
            }
        }
    }
}

//MARK:-Actions
extension LoginPopUpViewController
{
    @IBAction func touchOnLoginViaEmail(_ sender: UIButton) {
        
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginEmailViewController") as! LoginEmailViewController
        
        self.addChild(popvc)
        popvc.view.frame = self.view.frame
        self.view.addSubview(popvc.view)
        popvc.didMove(toParent: self)
        
    }
    
    @IBAction func touchOnAplleSignIn(_ sender: Any) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
        
    }
    
    @IBAction func tapOnLoginWithFaceBook(_ sender: Any) {
        activityIndicator.startAnimating()
        let fbLoginManager : LoginManager = LoginManager()
        
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }            
         }
        //        fbLoginManager.logOut()
    }
    //MARK:- Functions
    func getFBUserData(){
        print(AccessToken.current)
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start { (connection, result, error) in
                print(result!)
                var fbAccessToken = ""
                
                if error != nil {
                    print("----------ERROR-----------")
                    print(error!)
                    return
                }
                
                if AccessToken.current?.tokenString != nil {
                    fbAccessToken = AccessToken.current!.tokenString
                    print(fbAccessToken)
                    UserDefaults.standard.set(fbAccessToken, forKey: "fbAccessToken")
                    print(fbAccessToken)
                    let userData = result as! NSDictionary
                    let email = userData["email"] as? String
                    guard let firstName = userData["first_name"] as? String  else { return }
                    guard let lastName = userData["last_name"] as? String  else { return }
                    guard let id = userData["id"] as? String else { return }
                    self.setLoginAPICall(type: "facebook", email: email!, password: fbAccessToken)
                    
                    var pictureUrl = ""
                    if let picture = userData["picture"] as? NSDictionary, let data = picture["data"] as? NSDictionary, let url = data["url"] as? String {
                        pictureUrl = url
                        print(pictureUrl)
                    }
                    print("Email is \(String(describing: email))")
                    print("fname is \(String(describing: firstName))")
                    print("lname is \(String(describing: lastName))")
                    print("id is \(String(describing: id))")
                    UserDefaults.standard.set(email, forKey: "email")
                    UserDefaults.standard.set(firstName, forKey: "firstName")
                    UserDefaults.standard.set(lastName, forKey: "lastName")
                    UserDefaults.standard.set(id, forKey: "id")
                    
                    print("profile is \(pictureUrl)")
                    
                }
            }
        }
    }
}

extension LoginPopUpViewController:ASAuthorizationControllerDelegate
{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization)
    {
        switch authorization.credential {
        
        case let credentials as ASAuthorizationAppleIDCredential:
            DispatchQueue.main.async {
                
                if "\(credentials.user)" != "" {
                    
                    UserDefaults.standard.set("\(credentials.user)", forKey: "User_AppleID")
                }
                if credentials.email != nil {
                    
                    UserDefaults.standard.set("\(credentials.email!)", forKey: "email")
                }
                if credentials.fullName!.givenName != nil {
                    
                    UserDefaults.standard.set("\(credentials.fullName!.givenName!)", forKey: "User_FirstName")
                }
                if credentials.fullName!.familyName != nil {
                    
                    UserDefaults.standard.set("\(credentials.fullName!.familyName!)", forKey: "User_LastName")
                }
                UserDefaults.standard.synchronize()
            }
            
        case let credentials as ASPasswordCredential:
            DispatchQueue.main.async {
                
                if "\(credentials.user)" != "" {
                    
                    UserDefaults.standard.set("\(credentials.user)", forKey: "User_AppleID")
                }
                if "\(credentials.password)" != "" {
                    
                    UserDefaults.standard.set("\(credentials.password)", forKey: "User_Password")
                }
                UserDefaults.standard.synchronize()
                guard let apple_email = UserDefaults.standard.string(forKey: "email") else { return }
                guard let apple_password = UserDefaults.standard.string(forKey: "User_Password") else { return }
                
                self.setLoginAPICall(type: "apple", email: apple_email, password: apple_password)
                
            }
            
        default :
            let alert: UIAlertController = UIAlertController(title: "Apple Sign In", message: "Something went wrong with your Apple Sign In!", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            break
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error)
    {
        let alert: UIAlertController = UIAlertController(title: "Error", message: "\(error.localizedDescription)", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK:- ASAuthorizationControllerPresentationContextProviding delegate
extension LoginPopUpViewController: ASAuthorizationControllerPresentationContextProviding
{
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
    
}

//MARK:- APIHandling
extension LoginPopUpViewController
{
    func setLoginAPICall(type:String,email:String,password:String)
    {
        let paramsDict = [
            "type":type,
            "email":email,
            "password":password
        ] as [String:Any]
    
        let urlVal: String =  baseUrl.development.rawValue + "/users/login"
        print(urlVal)
        ServiceCalls.sharedInstance.PostAPICallWithHeaderValues(url: urlVal, postData: paramsDict, headersData: nil) { (sucess, resultValue) in
            if sucess == true
            {
                if let data = resultValue["data"] as? [String:Any]
                {
                    
                    print(data)
                    let data1 = creatDictnory(value: data as AnyObject)
                    print("Login new detail is:\(data1)")
                    guard let message = data1["message"] else { return }
                    guard let users =  data["user"] as? [String:Any] else { return }
                    guard let id = users["id"] else { return }
                    let Authorization = data["Authorization"]
                    print(Authorization)
                    print("Your login suceess is :\(resultValue)")
                    let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
                    let nextVc = storyboards.instantiateViewController(identifier: "myClubsViewController") as! myClubsViewController
                    self.navigationController?.pushViewController(nextVc, animated: true)
                }
                
            }
            else if let result : [ String:Any ] = resultValue["error"] as? [String: Any]
            {
                print("Error is:\(result)")
            }
        }
        
    }
    
    
}



