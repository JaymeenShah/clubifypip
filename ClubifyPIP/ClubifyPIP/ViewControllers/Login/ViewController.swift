//
//  ViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- IBoutlets
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var discoverClubButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    private var customView: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var conditionalView: UIView!
    @IBOutlet weak var buttonProd: UIButton!
    @IBOutlet weak var buttonUAT: UIButton!
    @IBOutlet weak var buttonDev: UIButton!
    
    //MARK:- Variables
    let myView = SpinnerView(frame: CGRect(x: screenSize.width/2, y: screenSize.height/2, width: 50, height: 50))

    var centerImgArray = [UIImage(named: "Center-1"),UIImage(named: "Center-2"),UIImage(named: "Center-3"),UIImage(named: "Center-4"),]
    var titleArray = ["YOUR POT OF GOLD","STEP 1"," STEP 2","STEP 3"]
    var subtitleArray = ["APP & COMMUNITY","BUILD YOUR APP TEMPLATE","ADD CONTENT AND INVITE FRIENDS","GET  YOUR APP & INVITE COMNUNITY"]
    var descriptionArray = ["","","","WE HELP YOU   LAUNCH."]
    var flag = false
    var flag1 = false
    var flag2 = false
    //MARK:- LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonLayouts()
        pageControl.numberOfPages = centerImgArray.count
        collectionView.delegate = self
        conditionalView.isHidden = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        
        // Configure Tap Gesture Recognizer
        tapGestureRecognizer.numberOfTapsRequired = 10
        
        // Add Tap Gesture Recognizer
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        
        // Configure Tap Gesture Recognizer
        tapGestureRecognizer.numberOfTapsRequired = 10
        
        // Add Tap Gesture Recognizer
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func didTapView(_ sender: UITapGestureRecognizer) {
        print("You have cleard 10 Taps")
        conditionalView.isHidden = false
        conditionalView.layer.masksToBounds = true
        conditionalView.layer.cornerRadius = 16
    }
   
    
}

//MARK:- Actions
extension ViewController
{
    @IBAction func touchOnDiscoverClubs(_ sender: Any) {
        print("Present")
        let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let discoverVc:discoverClubVc = storyboards.instantiateViewController(identifier: "discoverClubVc") as! discoverClubVc
        self.navigationController?.pushViewController(discoverVc, animated: true)
    }
    
    
    
    @IBAction func tapOnProduct(_ sender: UIButton) {
        if flag == false
        {
            sender.setBackgroundImage(UIImage(named: "radio_on"), for: UIControl.State.normal)
            
            buttonUAT.setBackgroundImage(UIImage(named: "radio_off"), for: UIControl.State.normal)
            buttonDev.setBackgroundImage(UIImage(named: "radio_off"), for: UIControl.State.normal)
            
            flag = true
            flag1 = false
            flag2 = false
            productionApiCall()
            UserDefaults.standard.set(baseUrl.production.rawValue, forKey: "baseurl")
            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginPopUpViewController") as! LoginPopUpViewController
            
            self.addChild(popvc)
            popvc.view.frame = self.view.frame
            self.view.addSubview(popvc.view)
            popvc.didMove(toParent: self)
        }
        
        
    }
    
    
    @IBAction func tapOnUAT(_ sender: UIButton) {
        if flag1 == false
        {
            
            buttonProd.setBackgroundImage(UIImage(named: "radio_off"), for: UIControl.State.normal)
            buttonDev.setBackgroundImage(UIImage(named: "radio_off"), for: UIControl.State.normal)
            
            sender.setBackgroundImage(UIImage(named: "radio_on"), for: UIControl.State.normal)
            flag1 = true
            flag = false
            flag2 = false
            uatApiCall()
            UserDefaults.standard.set(baseUrl.uat.rawValue, forKey: "baseurl")
            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginPopUpViewController") as! LoginPopUpViewController
            
            self.addChild(popvc)
            popvc.view.frame = self.view.frame
            self.view.addSubview(popvc.view)
            popvc.didMove(toParent: self)
        }
        
    }
    
    
    
    @IBAction func tapOnDev(_ sender: UIButton) {
        if flag2 == false
        {
            buttonProd.setBackgroundImage(UIImage(named: "radio_off"), for: UIControl.State.normal)
            
            buttonUAT.setBackgroundImage(UIImage(named: "radio_off"), for: UIControl.State.normal)
            sender.setBackgroundImage(UIImage(named: "radio_on"), for: UIControl.State.normal)
            flag1 = false
            flag = false
            flag2 = true
            developmentApiCall()
            UserDefaults.standard.set(baseUrl.development.rawValue, forKey: "baseurl")
            let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginPopUpViewController") as! LoginPopUpViewController
            
            self.addChild(popvc)
            popvc.view.frame = self.view.frame
            self.view.addSubview(popvc.view)
            popvc.didMove(toParent: self)
            
        }
    }
    @IBAction func touchOnLogin(_ sender: UIButton) {
        myView.isHidden = false
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginPopUpViewController") as! LoginPopUpViewController
        
        self.addChild(popvc)
        popvc.view.frame = self.view.frame
        self.view.addSubview(popvc.view)
        popvc.didMove(toParent: self)
        
    }
}


//MARK:- CollectionViewDelegate
extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return  centerImgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CenterCell", for: indexPath) as! CenterCell
        cell.centerImgView.image = centerImgArray[indexPath.row]
        cell.lblTitle.text = titleArray[indexPath.row]
        cell.lblSubTitle.text = subtitleArray[indexPath.row]
        cell.lblDescription.text = descriptionArray[indexPath.row]
        return cell
    }
    
}

//MARK:- Functions

extension ViewController
{
    func buttonLayouts()
    {
        loginButton.layer.cornerRadius = 22
        loginButton.layer.masksToBounds = true
        
        discoverClubButton.layer.cornerRadius = 22
        discoverClubButton.layer.masksToBounds = true
        loginButton.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green: 107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        discoverClubButton.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green: 107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        
    }
    
}
//MARK:- baseurl integration

extension ViewController
{
    //..MARK:- prounction api
    func productionApiCall ()
    {
       
        
        let urlVal: String = baseUrl.production.rawValue
        print(urlVal)
        ServiceCalls.sharedInstance.commonGETRequestCall(urlStr: urlVal) { (success, response) in
            if success == true
            {
                print("Your production response is:\(response)")
            }
           
        }
        
    }
    //..MARK:- developement api

    func developmentApiCall ()
    {
       
        
        let urlVal: String = baseUrl.development.rawValue
        print(urlVal)
        ServiceCalls.sharedInstance.commonGETRequestCall(urlStr: urlVal) { (success, response) in
            if success == true
            {
                print("Your developement response is:\(response)")
            }
            
        }
        
    }
    //..MARK:- UAT api

    func uatApiCall ()
    {
       
        
        let urlVal: String = baseUrl.uat.rawValue
        print(urlVal)
        ServiceCalls.sharedInstance.commonGETRequestCall(urlStr: urlVal) { (success, response) in
            if success == true
            {
                print("Your UAT response is:\(response)")
            }
            
        }
        
    }
}





