//
//  LoginEmailViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class LoginEmailViewController: UIViewController,FirstViewControllerProtocol ,UITextFieldDelegate{
    func dismiss() {
        firstViewController?.view.removeFromSuperview()
        firstViewController?.removeFromParent()
    }
    
    //MARK:-IBoutlets
    @IBOutlet weak var loginEmailPopView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    
    weak var firstViewController: LoginPopUpViewController?
    var delegate: FirstViewControllerProtocol?
    let baseurl = UserDefaults.standard.string(forKey: "baseurl")
//    let loginArray = [user]()
    var authToken : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        buttonLayouts()
        showAnimate()
        txtPassword.isSecureTextEntry = true
        txtUserName.delegate = self
        txtPassword.delegate = self
        txtUserName.title = "USERNAME"
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        view.addGestureRecognizer(tapGestureRecognizer)
        txtUserName.text = "Jenna"
        txtPassword.text = "Abc123!!"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        txtUserName.selectedTitle = "USERNAME"
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.removeAnimate()
    }
    @objc func didTapView(_ sender: UITapGestureRecognizer) {
        removeAnimate()
    }
}
//MARK:- APIHandling
extension LoginEmailViewController
{
    func APICall ()
    {
        
        let paramsDict = [
            "type":"username",
            "username":txtUserName.text!,
            "password":txtPassword.text!
        ] as [String : Any]
//        var loginData = [user]()
        
        let urlVal: String =  baseUrl.development.rawValue + "/users/login"
        print(urlVal)
        ServiceCalls.sharedInstance.PostAPICallWithHeaderValues(url: urlVal, postData: paramsDict, headersData: nil) { (sucess, resultValue) in
            if sucess == true
            {
                if let data = resultValue["data"] as? [String:Any]
                {
                    let data1 = creatDictnory(value: data as AnyObject)
                    print("Login new detail is:\(data1)")
                    guard let message = resultValue["message"] else { return }
                    let users =  data["user"] as? [String:Any]
                    guard let id = users!["id"] else { return }
                    guard let Authorization = data["Authorization"] else { return }
//                    let loginReport = user.init(id: id as! String,authorization:  data["Authorization"] as! String, message: message as! String)
                    print("Your login suceess is :\(resultValue)")
//                    loginData.append(loginReport)
//                    print("Your login Array is:\(loginData)")
//                    self.authToken = loginReport.authorization
//                    print(self.authToken)
                    UserDefaults.standard.set(Authorization, forKey: "authToken")
//                    let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
//                    let nextVc = storyboards.instantiateViewController(identifier: "myClubsViewController") as! myClubsViewController
//                    self.navigationController?.pushViewController(nextVc, animated: true)
                    self.setUserInfo(Xauthorization:self.authToken)
                }
                
            }
            else if let result : [ String:Any ] = resultValue["error"] as? [String: Any]
            {
                print("Error is:\(result)")
            }
        }
        
    }
    
    func setUserInfo(Xauthorization:String){
    
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                           "X-authorization" : Xauthorization,
                           "focus_club_id": "1830"]
        
        let urlVal: String =  baseUrl.development.rawValue + "/user/info"
        print(urlVal)
        
        ServiceCalls.sharedInstance.commonApiCall(url: urlVal, postData: nil, headersData: headers) { (success, resultValue) in
            print(resultValue)
            if success == true
            {
                
            }
            else
            {
                
            }
        }

        
}
}





