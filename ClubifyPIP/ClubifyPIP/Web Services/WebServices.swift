//
//  WebServices.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/10/21.
//


import UIKit
import Alamofire
import MobileCoreServices
import DataCache

class WebServices: NSObject {
    
    static let sharedInstance : WebServices = {
        let instance = WebServices()
        return instance
    }()
    
    //-------------------------------------------------------------------------------------------------
    enum WSPrintData {
        case requestURL
        case requestParams
        case requestParamsString
        case requestHeaders
        case requestResponse
        case requestJsonResponse
        case responseError
        case httpRequestError
        case uploadingProgress
    }
    
    //MARK:  Variables
    let shouldPrintRequestData : Bool = true
    
    //MARK:  Print Request Data
    func printAPIData(_ printDataType: WSPrintData, printData: AnyObject?) {
        
        if !shouldPrintRequestData { return }
        
        switch printDataType {
        case .requestURL:
            if let requestURL = printData as? String{ print("Request Url : \(requestURL)") }
        case .requestParams:
            if let requestParams = printData as? Parameters{ print("Request Params : \(requestParams)") }
        case .requestParamsString:
            if let requestParams = printData as? String{ print("Request Params : \(requestParams)") }
        case .requestHeaders:
            if let requestHeaders = printData as? HTTPHeaders{ print("Request Header : \(requestHeaders)") }
        case .requestResponse:
            if let response = printData as? Dictionary<String, Any>{ print("Response : \(response)") }
        case .responseError:
            if let responseError = printData as? Error{ print("Response Error : \(responseError.localizedDescription)") }
        case .httpRequestError:
            if let httpError = printData as? Error{ print("failure Http : \(httpError.localizedDescription)") }
        case .requestJsonResponse:
            if let response = printData as? Dictionary<String, Any>{
                let responseDict = response as NSDictionary
                let jsonData = try! JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                let jsonString = String(data: jsonData, encoding: .utf8)
                print("Json Response : \(String(describing: jsonString!))")
            }
        case .uploadingProgress:
            if let progress = printData as? Double{ print("Uploading Progress : \(createString(value: progress as AnyObject))") }
        }
        
    }
    
    //MARK:  Call Post API
    func requestPostAPI(requestURL: String,
                        parameters: Parameters?,
                        completionHandler: @escaping (_ responseObject: Dictionary<String, Any>?, _ error: Error?) -> Void) {
        
        //Set Request Headers
        let requestHeader: HTTPHeaders = setHeadersForAPI()
        
        var paraString : String? = nil
        if parameters != nil {
            paraString = getJsonStringFromDictionary(aParameters: parameters!)
        }
        
        //Post Request
        self.requestAPIWith(strRequestUrl: requestURL, httpMethod: .post, apiParams: [:], encoding: (paraString == nil) ? URLEncoding.default : paraString, requestHeader: requestHeader) { (response, error) in
            completionHandler(response, error)
        }
    }
    
    //MARK:  Call API with dynamic HTTPMethod
    func requestAPI(requestURL: String,
                    httpMethod: HTTPMethod,
                    parameters: Parameters?,
                    completionHandler: @escaping (_ responseObject: Dictionary<String, Any>?, _ error: Error?) -> Void) {
        
        //Set Request Headers
        let requestHeader: HTTPHeaders = setHeadersForAPI()
        
        var paraString : String? = nil
        if parameters != nil {
            paraString = getJsonStringFromDictionary(aParameters: parameters!)
        }
        
        //Post Request
        self.requestAPIWith(strRequestUrl: requestURL, httpMethod: httpMethod, apiParams: [:], encoding: (paraString == nil) ? URLEncoding.default : paraString, requestHeader: requestHeader) { (response, error) in
            completionHandler(response, error)
        }
    }
    
    //MARK:  Call Get API
    func requestGetAPI(requestURL: String,
                       completionHandler: @escaping (_ responseObject: Dictionary<String, Any>?, _ error: Error?) -> Void) {
        
        //Set Request Headers
        let requestHeader: HTTPHeaders = setHeadersForAPI()
        
        let parameters = ["app_name": IN_APPPURCHASE_APP_NAME]

        //Get Request
        self.requestAPIWith(strRequestUrl: requestURL, httpMethod: .get, apiParams: parameters, encoding: URLEncoding.default , requestHeader: requestHeader) { (response, error) in
            completionHandler(response, error)
        }
//        self.requestAPIWith(strRequestUrl: requestURL, httpMethod: .get, requestHeader: requestHeader) { (response, error) in
//            completionHandler(response, error)
//        }
    }
    
    //MARK:  Call Request API
    func requestAPIWith(strRequestUrl: String,
                        httpMethod: HTTPMethod,
                        apiParams: Parameters? = nil,
                        encoding: ParameterEncoding? = URLEncoding.default,
                        requestHeader: HTTPHeaders? = nil,
                        completionHandler: @escaping (_ responseObject: Dictionary<String, Any>?, _ error: Error?) -> Void ){
        
        if !(appDelegate.manager?.isReachable)! {
            let error = NSError(domain: "", code: 505, userInfo: [NSLocalizedDescriptionKey : "The network connection was lost."])
            completionHandler(nil,error)
            return
        }
        
        self.printAPIData(.requestParamsString, printData: encoding as AnyObject)
        self.printAPIData(.requestURL, printData: strRequestUrl as AnyObject)
        self.printAPIData(.requestHeaders, printData: requestHeader as AnyObject)
        
        request(strRequestUrl,
                method: httpMethod,
                parameters: [:],
                encoding: encoding!,
                headers: requestHeader).responseJSON { response in
                    switch(response.result) {
                    case .success(_):
                        do {
                            let JSON = try JSONSerialization.jsonObject(with:                                                                          response.data! as Data, options:JSONSerialization.ReadingOptions(rawValue: 0))
                            if let dict: Dictionary = JSON as? [String : AnyObject] {
                                self.printAPIData(.requestJsonResponse, printData: dict as AnyObject)
                                completionHandler(dict, nil)
                            }
                        }catch let JSONError as NSError {
                            self.printAPIData(.responseError, printData: JSONError as AnyObject)
                        }
                        break
                    case .failure(_):
                        self.printAPIData(.httpRequestError, printData: response.result.error as AnyObject)
                        completionHandler(nil,(response.result.error as NSError?)!)
                        break
                    }
        }
    }
    
    //-------------------------------------------------------------------------------------------------
    
    //MARK:
    func LoginWebEervice(parameters : [String : Any], completionHandler:@escaping (_ result: User,_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(User(),0,"The network connection was lost.".localized.localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: WebURL.login as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(WebURL.login, method: .post, parameters: nil, encoding: paraString!, headers: ["Content-Type":"application/json"]).responseJSON { (response) in
            switch response.result {
            case .success:
                if let JSON : NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestResponse, printData: JSON as AnyObject)
                    
                    if let success = JSON.object(forKey: "success") as? Int {
                        if success == 1 {
                            let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                            let AuthorizationToken = createString(value: (data as AnyObject).object(forKey: "Authorization") as AnyObject)
                            UserDefaults.Main.set(AuthorizationToken, forKey: .sessionToken)

                            let userData = creatDictnory(value:data.object(forKey: "user") as AnyObject)
                            let userId = createString(value: (userData as AnyObject).object(forKey: "id") as AnyObject)
                            let userprofile = User.init(id:userId, sessionToken: AuthorizationToken , username:"", firstname:"" , lastname: "", profileImageURL:"" , email:"" , gender:"" ,bio : "", isMultiClub: "", phone: "", firebaseId: "", firebasePassword: "", membershipLevel: "", profileVideoURL: "")

                            completionHandler(userprofile,200,(JSON.object(forKey: "message") as? String)!)
                        }else {
                            completionHandler(User(),success ,(JSON.object(forKey: "message") as? String)!)
                        }
                    }else {
                        completionHandler(User(),(JSON.object(forKey: "status") as? Int)! ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(User(), 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(User(),0,error.localizedDescription)
                break
            }
        }
    }
    
    func LoginWithUserData(url:String, completionHandler:@escaping (_ result: User,_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(User(),0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let response = response.result.value as? [String: Any] ?? [:]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let dicData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let data: NSDictionary = creatDictnory(value:dicData.object(forKey: "user") as AnyObject)
                        let userId = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                        let userName = createString(value: (data as AnyObject).object(forKey: "username") as AnyObject)
                        let email = createString(value: (data as AnyObject).object(forKey: "email") as AnyObject)
                        let firstName = createString(value: (data as AnyObject).object(forKey: "firstname") as AnyObject)
                        let lastName = createString(value: (data as AnyObject).object(forKey: "lastname") as AnyObject)
                        let profilePic = createString(value: (data as AnyObject).object(forKey: "profilepicture") as AnyObject)
                        let gender = createString(value: (data as AnyObject).object(forKey: "gender") as AnyObject)
                        let biography = createString(value: (data as AnyObject).object(forKey: "biography") as AnyObject)
                        let isMultiClub = createString(value: (data as AnyObject).object(forKey: "is_multiclub") as AnyObject)
                        let phone = createString(value: (data as AnyObject).object(forKey: "phone") as AnyObject)
                        let firebaseId = createString(value: (data as AnyObject).object(forKey: "firebase_id") as AnyObject)
                        let firebasePassword = createString(value: (data as AnyObject).object(forKey: "firebase_password") as AnyObject)
                        let membershipLevel = createString(value: (data as AnyObject).object(forKey: "membership_level") as AnyObject)
                        let profileVideo = createString(value: (data as AnyObject).object(forKey: "profile_video") as AnyObject)


                        if isMultiClub == "" || isMultiClub == nil {
                            isMultiClub == "0"
                        }
                        
                        if let dicData = response["data"] as? [String: Any] {
                            if let data = dicData["user"] as? [String: Any] {
                                let userprofile = (User.init(fromDictionary: data))
                                completionHandler(userprofile,200,(JSON.object(forKey: "message") as? String)!)
                            }
                        } else {
                            completionHandler(User(),success ,(JSON.object(forKey: "message") as? String)!)
                        }

                       // let userprofile = User.init(fromDictionary: <#T##[String : Any]#>)//User.init(id:userId, sessionToken: getUserAuthToken(), username: userName, firstname: firstName, lastname:lastName , profileImageURL: profilePic, email:email , gender:gender,bio : biography, isMultiClub: isMultiClub, phone: phone, firebaseId: firebaseId, firebasePassword: firebasePassword, membershipLevel: membershipLevel)
                        //completionHandler(userprofile,200,(JSON.object(forKey: "message") as? String)!)
                    }else {
                        completionHandler(User(),success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
//                    completionHandler(User(), 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(User(),0,error.localizedDescription)
                break
            }
        })
    }
    
    func RegisterUserWebEervice(email:String,password: String, username: String,firstname:String,lastname: String, gender: String,accounttypeid:String, completionHandler:@escaping (_ result: User,_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(User(),0,"The network connection was lost.".localized)
            return
        }
        
        let parameters = ["email": email,
                          "password": password,
                          "username": username,
                          "firstname": firstname,
                          "lastname": lastname,
                          "gender": gender,
                          "accounttypeid": accounttypeid]
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)

        self.printAPIData(.requestURL, printData: WebURL.userCreate as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(WebURL.userCreate, method: .post, parameters: [:],encoding: paraString!, headers: ["Content-Type":"application/json"]).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let AuthorizationToken = createString(value: (data as AnyObject).object(forKey: "Authorization") as AnyObject)
                        let userData = creatDictnory(value:data.object(forKey: "user") as AnyObject)
                        let userId = createString(value: (userData as AnyObject).object(forKey: "id") as AnyObject)
                        let userprofile = User.init(id:userId, sessionToken: AuthorizationToken , username:"", firstname:"" , lastname: "", profileImageURL:"" , email:"" , gender:"",bio : "biography", isMultiClub: "", phone: "",firebaseId: "", firebasePassword: "", membershipLevel: "", profileVideoURL: "")
                        completionHandler(userprofile,200,(JSON.object(forKey: "message") as? String)!)
                    }else {
                        completionHandler(User(),success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(User(), 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(User(),0,error.localizedDescription)
                break
            }
        })
    }
    
    func EditUserProfileWebEervice(authToken : String,parameters : [String : Any],userid : String,profileUrl : String ,completionHandler:@escaping (_ result: User,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(User(),0,"The network connection was lost.".localized)
            return
        }
     
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: WebURL.userEditProfile as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(WebURL.userEditProfile, method: .put, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let response = response.result.value as? [String: Any] ?? [:]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let userName = createString(value: (data as AnyObject).object(forKey: "username") as AnyObject)
                        let email = createString(value: (data as AnyObject).object(forKey: "email") as AnyObject)
                        let firstName = createString(value: (data as AnyObject).object(forKey: "firstname") as AnyObject)
                        let lastName = createString(value: (data as AnyObject).object(forKey: "lastname") as AnyObject)
                        let gender = createString(value: (data as AnyObject).object(forKey: "gender") as AnyObject)
                        let biography = createString(value: (data as AnyObject).object(forKey: "biography") as AnyObject)
                        let isMultiClub = createString(value: (data as AnyObject).object(forKey: "is_multiclub") as AnyObject)
                        let phone = createString(value: (data as AnyObject).object(forKey: "phone") as AnyObject)
                        let firebaseId = createString(value: (data as AnyObject).object(forKey: "firebase_id") as AnyObject)
                        let firebasePassword = createString(value: (data as AnyObject).object(forKey: "firebase_password") as AnyObject)

                       // let userprofile = User.init(id:userid, sessionToken: authToken, username: userName, firstname: firstName, lastname:lastName , profileImageURL: profileUrl, email:email , gender:gender,bio : biography, isMultiClub: isMultiClub, phone: phone,firebaseId: firebaseId, firebasePassword: firebasePassword, membershipLevel: "")
                       
                        if let data = response["data"] as? [String: Any] {
                            let userprofile = (User.init(fromDictionary: data))
                            userprofile.userID = userid
                            userprofile.profileImageURL = profileUrl
                            completionHandler(userprofile,200,(JSON.object(forKey: "message") as? String)!)
                        } else {
                            completionHandler(User(),success ,(JSON.object(forKey: "message") as? String)!)
                        }

                        //completionHandler(userprofile,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(User(),success ,(JSON.object(forKey: "message") as? String)!)
                    }
                } else {
                    completionHandler(User(), 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(User(),0,"Something went wrong. Please try again")
                } else {
                    completionHandler(User(),0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func RegisterUserImageUpload(mediaType: String, imageData : Data,completionHandler:@escaping (_ profileImage: String,_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("",0,"The network connection was lost.".localized)
            return
        }
        
        let boundary = "Boundary-\(UUID().uuidString)"
        let strURL = WebURL.userProfileUpload + "/\(mediaType)"//added type to URL to check profile pic or cover video
        let URL = try! URLRequest(url: strURL , method: .post, headers: ["Content-Type" :"multipart/form-data; boundary=\(boundary)","X-authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()])
        
        self.printAPIData(.requestURL, printData: strURL as AnyObject)
        
        upload(
            multipartFormData: { multipartFormData in
                // Append Image to multipart form data.
                if mediaType == "video" {
                    multipartFormData.append(imageData, withName:"fileToUpload", fileName: "Sample1.mp4", mimeType: "video/mp4")
                } else {
                    multipartFormData.append(imageData, withName:"fileToUpload", fileName: "toBeDetermined.jpg", mimeType: "image/jpg")
                }
        },with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        self.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                    })
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let JSON:NSDictionary = response.result.value as? NSDictionary {
                                
                                self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                                
                                let success = JSON.object(forKey: "status") as! Int
                                if success == 200 {
                                    let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                    var profileUrl = ""
                                    if mediaType == "image" {
                                        profileUrl = createString(value: (data as AnyObject).object(forKey: "profilepicture") as AnyObject)
                                    } else {
                                        profileUrl = createString(value: (data as AnyObject).object(forKey: "profile_video") as AnyObject)
                                    }
                                    completionHandler(profileUrl,200,(JSON.object(forKey: "message") as? String)!)

                                }else {
                                    completionHandler("",success ,(JSON.object(forKey: "message") as? String)!)
                                }
                            }else {
                                completionHandler("", 0,"No Data Found".localized)
                            }
                            break
                        case .failure(let error):
                            self.printAPIData(.httpRequestError, printData: error as AnyObject)
                            completionHandler("",0,error.localizedDescription)
                            break
                        }
                    }
                case .failure(let encodingError):
                    self.printAPIData(.httpRequestError, printData: encodingError as AnyObject)
                    break
                }
        })
    }
    
    func UploadPhotoForContest(mediaData : Data,completionHandler:@escaping (_ photoUrl: String,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("",0,"The network connection was lost.".localized)
            return
        }
        
        let URL = try! URLRequest(url: WebURL.contestUpload, method: .post, headers: ["X-Authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()])
        
        self.printAPIData(.requestURL, printData: WebURL.contestUpload as AnyObject)
        
        upload(
            multipartFormData: { multipartFormData in
                // Append Image to multipart form data.
                multipartFormData.append(mediaData, withName:"contest_image", fileName: "toBeDetermined.jpg", mimeType: "image/jpg")
        },with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        self.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                    })
                    upload.responseJSON { response in
                        
                        switch response.result {
                        case .success:
                            if let JSON:NSDictionary = response.result.value as? NSDictionary {
                                
                                self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                                
                                let success = JSON.object(forKey: "status") as! Int
                                if success == 200 {
                                    let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                    let mediaUrl = createString(value: (data as AnyObject).object(forKey: "contest_image") as AnyObject)
                                    completionHandler(mediaUrl,200,(JSON.object(forKey: "message") as? String)!)
                                }else if success == 401  {
                                    AppState.sharedInstance.joinClubLogIn = "0"
                                    //UserDefaults.Main.set(false, forKey: .autoLogin)
                                    appDelegate.LogOutDevice()
                                }else {
                                    completionHandler("",success ,(JSON.object(forKey: "message") as? String)!)
                                }
                            }else {
                                completionHandler("", 0,"No Data Found".localized)
                            }
                            break
                        case .failure(let error):
                            self.printAPIData(.httpRequestError, printData: error as AnyObject)
                            if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                                completionHandler("",0,"Something went wrong. Please try again")
                            } else {
                                completionHandler("",0,error.localizedDescription)
                            }
                            break
                        }
                    }
                case .failure(let encodingError):
                    self.printAPIData(.httpRequestError, printData: encodingError as AnyObject)
                    break
                }
        })
    }
    
    func GalleryMediaUpload(aRequestUrl : String, mediaType :String ,mediaData : Data,fileName : String,completionHandler:@escaping (_ videoId: String,_ mediaUrl: String,_ code: Int, _ message: String, _ response: NSDictionary ) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("","",0,"The network connection was lost.".localized, NSDictionary())
            return
        }
        
        self.printAPIData(.requestURL, printData: aRequestUrl as AnyObject)
        print("Media Type : \(mediaType)")
        print("File Name : \(fileName)")
        
        let boundary = "Boundary-\(UUID().uuidString)"
        let URL = try! URLRequest(url: aRequestUrl, method: .post, headers: ["Content-Type" :"multipart/form-data; boundary=\(boundary)","X-authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()])
        
        var fileExtention = "application/pdf"
        
        if mediaType == "file" {
            fileExtention = fileName.fileExtension()
            
            if fileExtention == "html" {
                fileExtention = "text/html"
            }else if fileExtention == "pdf" {
                fileExtention = "application/pdf"
            }else if fileExtention == "doc" {
                fileExtention = "application/msword"
            }else if fileExtention == "docx" {
                fileExtention = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            }else if fileExtention == "txt" {
                fileExtention = "text/plain"
            }
        }
        
        print(fileName + " " + fileExtention)
        
        upload(
            multipartFormData: { multipartFormData in
                // Append Image to multipart form data.
                if mediaType == "image" {
                    multipartFormData.append(mediaData, withName:"fileToUpload", fileName: "toBeDetermined.jpg", mimeType: "image/jpg")
                }else if mediaType == "file" {
                    multipartFormData.append(mediaData, withName:"fileToUpload", fileName: fileName, mimeType: fileExtention)
                } else if mediaType == "6" {
                    
                    multipartFormData.append(mediaData, withName:"fileToUpload", fileName: "toBeDetermined.mp3", mimeType: "audio/mp3")
                } else {
                    multipartFormData.append(mediaData, withName:"fileToUpload", fileName: "Sample1.mp4", mimeType: "video/mp4")
                }
        },with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        self.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                    })
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let JSON:NSDictionary = response.result.value as? NSDictionary {
                                
                                self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                                
                                let success = JSON.object(forKey: "status") as! Int
                                if success == 200 {
                                    let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                    var mediaId = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                                    let mediaIUrl = createString(value: (data as AnyObject).object(forKey: "image") as AnyObject)
                                    if mediaId.isEmpty {
                                       mediaId = createString(value: (data as AnyObject).object(forKey: "file_id") as AnyObject)
                                    }
                                    completionHandler(mediaId,mediaIUrl,200,(JSON.object(forKey: "message") as? String)!,JSON)
                                }else if success == 401 {
                                    AppState.sharedInstance.joinClubLogIn = "0"
                                    //UserDefaults.Main.set(false, forKey: .autoLogin)
                                    appDelegate.LogOutDevice()
                                }else {
                                    completionHandler("","",success ,(JSON.object(forKey: "message") as? String)!,NSDictionary())
                                }
                            }else {
                                completionHandler("","", 0,"No Data Found".localized,NSDictionary())
                            }
                            break
                        case .failure(let error):
                            self.printAPIData(.httpRequestError, printData: error as AnyObject)
                            if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                                completionHandler("","",0,"Something went wrong. Please try again",NSDictionary())
                            } else {
                                completionHandler("","",0,error.localizedDescription,NSDictionary())
                            }
                            break
                        }
                    }
                case .failure(let encodingError):
                    self.printAPIData(.httpRequestError, printData: encodingError as AnyObject)
                    break
                }
        })
    }
    
    func joinChatDetails(clubId : String,parameters :[String : Any], url: String , completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)

        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func SetMediaDetails(clubId : String,parameters :[String : Any], url: String , completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)

        request(url , method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
//                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func sendSMS(clubId : String,parameters :[String : Any], url: String , completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
//            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url , method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    } else {
                        
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                        
                    }
                }else {
//                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    func getSMSDetails(phoneNo: String, clubId : String,parameters :[String : Any], url: String , completionHandler:@escaping (_ code: Int, _ message: String, _ resultData : [SMSChatDetails]) -> Void ) {
        
        var result: [SMSChatDetails] = []

        if !(appDelegate.manager?.isReachable)! {
//            completionHandler(0,"The network connection was lost.".localized,result)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url , method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "SMSChatList-\(phoneNo)")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    } else {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        if feedData.count > 0 {
                            for data in feedData {
                                let dataDict = creatDictnory(value:data as AnyObject)
                                let clubMemberList = SMSChatDetails.init(dictJSON: creatDictnory(value: dataDict))
                                result.append(clubMemberList)
                            }
                            completionHandler(success ,(JSON.object(forKey: "message") as? String)!, result)

                        } else {
                            completionHandler(success ,(JSON.object(forKey: "message") as? String)!, result)
                        }
                    }
                }else {
//                    completionHandler(0,"No Data Found".localized, result)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again", result)
                } else {
                    completionHandler(0,error.localizedDescription, result)
                }
                
                break
            }
        })
    }
    
    func SetContestDetails(clubId : String,parameters :[String : Any], url: String , completionHandler:@escaping (_ code: Int, _ message: String, _ resultData : ContestDetails) -> Void ) {
        
        var result = ContestDetails()

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized,ContestDetails())
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url , method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    let responseDict = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    } else {
                        if let data = responseDict?["data"] as? [String: Any] {
                            result = (ContestDetails.init(fromDictionary: data))
                            completionHandler(success ,(JSON.object(forKey: "message") as? String)!, result)
                        } else {
                            completionHandler(success ,(JSON.object(forKey: "message") as? String)!, result)
                        }
                    }
                }else {
                    completionHandler(0,"No Data Found".localized, ContestDetails())
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again", ContestDetails())
                } else {
                    completionHandler(0,error.localizedDescription, ContestDetails())
                }
                
                break
            }
        })
    }
    
    func trackProductClicks(clubId : String,parameters :[String : Any], url: String , completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url , method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func SetEventDetails(clubId : String,parameters :[String : Any], url: String , completionHandler:@escaping (_ code: Int, _ message: String, _ resultData : ClubEvent) -> Void ) {
        
        var result = ClubEvent()

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized, ClubEvent())
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url , method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        let eventData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        result = ClubEvent.init(dictJSON: eventData)
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!, result)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized, ClubEvent())
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again", ClubEvent())
                } else {
                    completionHandler(0,error.localizedDescription, ClubEvent())
                }
                
                break
            }
        })
    }
    
    func getSharableMediaGallery(url:String, completionHandler:@escaping (_ result: GalleryMedia,_ code: Int, _ message: String) -> Void ) {
        var dict_galleryData = GalleryMedia()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(dict_galleryData,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                        
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let mediaData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        
                        if mediaData.count > 0 {
                            
                            for data in mediaData {
                                let mediaData = GalleryMedia.init(dictJSON: creatDictnory(value: data as AnyObject))
                                dict_galleryData = mediaData
                            }
                        }
                        completionHandler(dict_galleryData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(dict_galleryData,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(dict_galleryData,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                //if error.localizedDescription .contains("JSON could not be serialized because of error")
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(dict_galleryData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(dict_galleryData,0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getAllMedia(url:String, completionHandler:@escaping (_ result: [GalleryMedia],_ morepages: String ,_ code: Int, _ message: String, _ response: NSDictionary) -> Void ) {
        var arr_galleryData = [GalleryMedia]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arr_galleryData,"0",0,"The network connection was lost.".localized, NSDictionary())
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let responseDict = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    if AppState.sharedInstance.galleryDocumentType == "all" {
                        print("webservice")
                        
                        var courseIdentifier = "mediaGalleryList-all-\(String(describing: getClubId()))"
                        if !AppState.sharedInstance.folderId.isEmpty {
                            courseIdentifier = "mediaGalleryList-all-\(AppState.sharedInstance.folderId)"
                        } else if AppState.sharedInstance.menuUniqueId == "dynamic_course" && AppState.sharedInstance.isDynamicCourse {
                            print("dynamic")
                            courseIdentifier = "mediaGalleryList-all-\(String(describing: AppState.sharedInstance.dynamicCourseData?.id ?? getClubId()))"
                        }
                        print(courseIdentifier)

                        let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                        DataCache.instance.write(data: dataValue, forKey: courseIdentifier)
                    } else {
                        //Cache
                        print("webservice")
                        print("mediaGalleryList-\(AppState.sharedInstance.galleryDocumentType)")

                        DataCache.instance.clean(byKey: "mediaGalleryList-\(AppState.sharedInstance.galleryDocumentType)")
                        let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: responseDict!)
                        DataCache.instance.write(data: dataValue, forKey: "mediaGalleryList-\(AppState.sharedInstance.galleryDocumentType)")
                    }
                    //"mediaGalleryList-all-\(folderData.id)"
                        
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let mediaData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if mediaData.count > 0 {
                            
                            for data in mediaData {
                                let mediaData = GalleryMedia.init(dictJSON: creatDictnory(value: data as AnyObject))
                                arr_galleryData.append(mediaData)
                            }
                        }
                        completionHandler(arr_galleryData,morePages,200,(JSON.object(forKey: "message") as? String)!, JSON)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arr_galleryData,"0",success,(JSON.object(forKey: "message") as? String)!, NSDictionary())
                    }
                }else {
                    completionHandler(arr_galleryData,"0",0,"No Data Found".localized, NSDictionary())
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                //if error.localizedDescription .contains("JSON could not be serialized because of error")
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arr_galleryData,"0",0,"Something went wrong. Please try again", NSDictionary())
                } else {
                    completionHandler(arr_galleryData,"0",0,error.localizedDescription, NSDictionary())
                }

                break
            }
        })
    }
    
    func EditMediaDetails(requestUrl :String,parameters : [String : Any], completionHandler:@escaping ( _ mediaData: GalleryMedia,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(GalleryMedia(),0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)

        request(requestUrl , method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let mediaData = GalleryMedia.init(dictJSON: data)
                        completionHandler(mediaData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(GalleryMedia(),success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(GalleryMedia(),0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(GalleryMedia(),0,"Something went wrong. Please try again")
                } else {
                    completionHandler(GalleryMedia(),0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func DeleteSidebarMenuDetails(requestUrl :String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        
        request(requestUrl , method: .delete, parameters: [:],headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    
    func DeleteChatRoom(requestUrl :String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        
        request(requestUrl , method: .delete, parameters: [:],headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func deleteSMS(requestUrl :String, parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }

        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(requestUrl, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        completionHandler(200 ,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func DeleteMediaDetails(requestUrl :String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        
        request(requestUrl , method: .get, parameters: [:],headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func webservice_joinPlan(requestUrl :String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        
        request(requestUrl , method: .get, parameters: [:],headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "success") as? Int
                    let status = JSON.object(forKey: "status") as? Int
                    if success == 1{
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == nil{
                        completionHandler(status! ,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401 || success == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success! ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func getUserFeedData(url:String, completionHandler:@escaping (_ result: ClubFeedsElement ,_ code: Int, _ message: String) -> Void) {
        
        var arrfeedData = ClubFeedsElement()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrfeedData,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    //let success = JSON.object(forKey: "status") as! Int
                    if JSON != nil {
                        //let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        arrfeedData = ClubFeedsElement.init(dictJSON: JSON)
                        completionHandler(arrfeedData,200,"Feed data found")
                    }else {
                        completionHandler(arrfeedData,0,"No Feed Data Found")
                    }
                }else {
                    completionHandler(arrfeedData,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrfeedData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrfeedData,0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func getAllClubFeeds(url:String, completionHandler:@escaping (_ result: [ClubFeedsElement],_ morepages: String ,_ code: Int, _ message: String, _ canpostfeeds: Bool) -> Void) {
        
        var arrfeedData = [ClubFeedsElement]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrfeedData,"0",0,"The network connection was lost.".localized,false)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let responseDict = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        let canpostfeeds = JSON.object(forKey: "canpostfeeds") as? Bool

                        if let data = responseDict?["data"] as? [[String: Any]] {

                            //Cache
                            let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: responseDict!)
                            DataCache.instance.write(data: dataValue, forKey: "clubFeedList-\(String(describing: AppState.sharedInstance.ClubList?.id))")

                            for prodDetail in data {
                                arrfeedData.append(ClubFeedsElement.init(dictJSON: creatDictnory(value: prodDetail as AnyObject)))
                            }
                        }
                        
                        completionHandler(arrfeedData,morePages,200,(JSON.object(forKey: "message") as? String)!, canpostfeeds ?? false)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrfeedData,"0",success,(JSON.object(forKey: "message") as? String)!, false)
                    }
                }else {
                    completionHandler(arrfeedData,"0",0,"No Data Found".localized, false)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrfeedData,"0",0,"Something went wrong. Please try again", false)
                } else {
                    completionHandler(arrfeedData,"0",0,error.localizedDescription, false)
                }
                break
            }
        })
    }
    
    //CommentListing
    func addComment(url:String,  parameters : [String : Any] ,completionHandler:@escaping ( _ feedCommentId: String, _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("0",0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        let method = HTTPMethod.post
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success(let resultData):
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int

                    if success == 200 {
                        let data = JSON.object(forKey: "data") as! [String: Any]
                        let feedCmtId = data["feed_comment_id"] as? String
                        completionHandler(feedCmtId ?? "0", 200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler("0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler("0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler("0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler("0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func editComment(url:String,  parameters : [String : Any] ,completionHandler:@escaping ( _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        let method = HTTPMethod.put
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success(let resultData):
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func deleteComment(url:String,  parameters : [String : Any] ,completionHandler:@escaping ( _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        let method = HTTPMethod.delete
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success(let resultData):
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    //Add Reply
    func addReply(url:String,  parameters : [String : Any] ,completionHandler:@escaping ( _ feedReplyId: String, _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("0",0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        let method = HTTPMethod.post
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success(let resultData):
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    
                    if success == 200 {
                        let data = JSON.object(forKey: "data") as! [String: Any]
                        let feedCmtId = data["reply_id"] as? String
                        completionHandler(feedCmtId ?? "0", 200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler("0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler("0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler("0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler("0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getAllFeedComments(url:String, completionHandler:@escaping (_ result: [Comments],_ morepages: Int ,_ code: Int, _ message: String) -> Void) {
        
        var arrfeedData: [Comments] = []
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrfeedData,0,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let responseDict = response.result.value as? [String: Any] {
                    print("=======Result============")
                    print(responseDict)
                    
                    let status = responseDict["status"] as? Int
                    let morePages = responseDict["more_pages"] as? Int
                    
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: responseDict)
                    DataCache.instance.write(data: dataValue, forKey: "ClubCommentList-\(AppState.sharedInstance.feedId)")

                    if status == 200 {
                        if let data = responseDict["data"] as? [[String: Any]] {
                            for prodDetail in data {
                                arrfeedData.append(Comments(fromDictionary: prodDetail))
                            }
                        }
                        //arrfeedData.sort(by: { Int($0.id ?? "0")! > Int($1.id)! })
                        completionHandler(arrfeedData, morePages!, 200, (responseDict["message"] as? String)!)

                    } else if status == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(arrfeedData,0,status ?? 0,(responseDict["message"] as? String)!)
                    }

                } else {
                    completionHandler(arrfeedData, 0, 0, "No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrfeedData,0,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrfeedData,0,0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func updateClubFeed(urlRequest : String, type :String ,club_id :String,description :String ,image : String , video : String,completionHandler:@escaping (_ clubFeedData: ClubFeedsElement,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(ClubFeedsElement(),0,"The network connection was lost.".localized)
            return
        }
        
        let parameters = ["type": type,
                          "club_id": club_id,
                          "description": description,
                          "image": image,
                          "video": video]
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)

        self.printAPIData(.requestURL, printData: urlRequest as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(urlRequest , method: .put, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let clubFeedData = ClubFeedsElement.init(dictJSON: data)
                        completionHandler(clubFeedData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(ClubFeedsElement(),success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(ClubFeedsElement(),0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(ClubFeedsElement(),0,"Something went wrong. Please try again")
                } else {
                    completionHandler(ClubFeedsElement(),0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func deleteClubFeed(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func updateFeedMediaDetails(postId: String, parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        let strURL = "\(WebURL.updateClubFeedCreate)\(postId)"
        
        self.printAPIData(.requestURL, printData: strURL as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        
        request(strURL , method: .put, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func SetFeedMediaDetails(parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String, _ feedID: String? ) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized, nil)
            return
        }

        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: WebURL.addClubFeedCreate as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(WebURL.addClubFeedCreate , method: .put, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,
                                          (JSON.object(forKey: "message") as? String)!,
                                          (JSON.object(forKey: "data") as? Dictionary<String, String>)?["feed_id"] as? String)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized, nil)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again", nil)
                } else {
                    completionHandler(0,error.localizedDescription, nil)
                }

                break
            }
        })
    }
    
    func GetallMyClubs(isClubType: String, url:String, completionHandler:@escaping (_ clubData: [ClubList],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        
        var arrClubData = [ClubList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrClubData,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
          
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "MyClubList-\(isClubType)")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
//                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        for dict in feedData {
                            let data1 = creatDictnory(value:dict as AnyObject)
                            let clubData = ClubList.init(dictJSON: data1)
                            arrClubData.append(clubData)
                        }
//                        for (_, value) in feedData {
//                            if let data = value as? [String: Any] {
//
//                                let data1 = creatDictnory(value:data as AnyObject)
//                                let clubData = ClubList.init(dictJSON: data1)
//                                arrClubData.append(clubData)
//                            }
//                        }
                        //arrClubData.sort(by: { Int($0.id)! > Int($1.id)! })
                        completionHandler(arrClubData,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    } else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrClubData,"0",success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrClubData,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(arrClubData,"0",0,error.localizedDescription)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrClubData,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrClubData,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func GetAllNewDiscoverClubs(url:String, completionHandler:@escaping (_ clubData: [CategoryWiseClubList],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        
        var arrClubData = [CategoryWiseClubList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrClubData,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "MultiClubList")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)

//                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        if feedData.count > 0 {
                            for data in feedData {
                                let folderData = CategoryWiseClubList.init(fromDictionary: creatDictnory(value:data as AnyObject))
                                arrClubData.append(folderData)
                            }
                        }
                        /*for (_, value) in feedData {
                            if let data = value as? [String: Any] {
                                let data1 = creatDictnory(value:data as AnyObject)
                                let clubData = CategoryWiseClubList.init(fromDictionary: data1)
                                arrClubData.append(clubData)
                            }
                        }*/
                        //arrClubData.sort(by: { Int($0.id)! < Int($1.id)! })
                        completionHandler(arrClubData,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    } else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrClubData,"0",success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrClubData,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(arrClubData,"0",0,error.localizedDescription)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrClubData,"0",0,"Something went wrong. Please try again")

                } else {
                    completionHandler(arrClubData,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func GetPointValue(url:String, completionHandler:@escaping (_ clubData: PointDetails,_ code: Int, _ message: String) -> Void) {
        
        var arrClubData = PointDetails()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(PointDetails(),0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let responseDict = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    //Cache
                    DataCache.instance.clean(byKey: "pointSystem")
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: responseDict!)
                    DataCache.instance.write(data: dataValue, forKey: "pointSystem")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let data = feedData as? [String: Any]
                        arrClubData = PointDetails.init(fromDictionary: data!)
                        completionHandler(arrClubData,200,(JSON.object(forKey: "message") as? String)!)
                    } else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrClubData,success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrClubData, 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                //completionHandler(arrClubData,"0",0,error.localizedDescription)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrClubData,0,"Something went wrong. Please try again")

                } else {
                    completionHandler(arrClubData,0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func GetAllDiscoverClubs(url:String, completionHandler:@escaping (_ clubData: [ClubList],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        
        var arrClubData = [ClubList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrClubData,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    DataCache.instance.clean(byKey: "DiscoverClubs")
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "DiscoverClubs")
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        for (_, value) in feedData {
                            if let data = value as? [String: Any] {
                                let data1 = creatDictnory(value:data as AnyObject)
                                let clubData = ClubList.init(dictJSON: data1)
                                arrClubData.append(clubData)
                            }
                        }
                        arrClubData.sort(by: { Int($0.id)! < Int($1.id)! })
                        completionHandler(arrClubData,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    } else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrClubData,"0",success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrClubData,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                //completionHandler(arrClubData,"0",0,error.localizedDescription)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrClubData,"0",0,"Something went wrong. Please try again")

                } else {
                    completionHandler(arrClubData,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func joinClub(url:String,completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)

        request(url, method: .put, parameters: nil, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let status = JSON.object(forKey: "status") as? Int
                    let success = JSON.object(forKey: "success") as? Int
                    
                    if status == 200 || success == 1 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == nil{
                        completionHandler(status! ,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success! ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"un-authorized not public")
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func createClub(parameters : [String : Any], completionHandler:@escaping (_ clubId: String ,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("0",0,"The network connection was lost.".localized)
            return
        }

        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: WebURL.createClub as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(WebURL.createClub, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let clubId = createString(value: (data as AnyObject).object(forKey: "club_id") as AnyObject)
                        completionHandler(clubId,200 ,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler("0",success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
//                    completionHandler("0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler("0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler("0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func followUnfollowUser(urlRequest :String,parameters : [String : Any], completionHandler:@escaping (_ response: [String: Any], _ code: Int, _ message: String) -> Void) {

        if !(appDelegate.manager?.isReachable)! {
            completionHandler([:],0,"The network connection was lost.".localized)
            return
        }

        let paraString = getJsonStringFromDictionary(aParameters: parameters)

        self.printAPIData(.requestURL, printData: urlRequest as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)

        request(urlRequest, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let responseDict = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        if let data = responseDict?["data"] as? [String: Any] {
                            completionHandler(data, 200 ,(JSON.object(forKey: "message") as? String)!)
                        }
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler([:], success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler([:], 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler([:], 0,"Something went wrong. Please try again")
                } else {
                    completionHandler([:], 0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func forwardAudioMessage(urlRequest :String,parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }

        let paraString = getJsonStringFromDictionary(aParameters: parameters)

        self.printAPIData(.requestURL, printData: urlRequest as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)

        request(urlRequest, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        completionHandler(200 ,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func uploadAudioMessage(urlRequest :String, mediaData : Data,parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)

        self.printAPIData(.requestURL, printData: urlRequest as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        let boundary = "Boundary-\(UUID().uuidString)"
        
        let URL = try! URLRequest(url: urlRequest ,method: .post, headers: ["Content-Type" :"multipart/form-data; boundary=\(boundary)","X-Authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()])
        
        upload( multipartFormData: { multipartFormData in
            multipartFormData.append(mediaData, withName:"fileToUpload", fileName: "toBeDetermined.mp3", mimeType: "audio/mp3")
            var strVal: String = ""
            for (key, value) in parameters {
                if value is [String] {
                    let arrList:[String] = (value as? [String])!
                    do {
                        let data = try JSONSerialization.data(withJSONObject: arrList, options: .fragmentsAllowed)
                        multipartFormData.append(data, withName: key)
                    } catch  {
                    }
                } else {
                    strVal = (value as? String)!
                    multipartFormData.append(strVal.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        },with: URL,
          encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    WebServices.sharedInstance.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        if let JSON:NSDictionary = response.result.value as? NSDictionary {
                            
                            self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                            
                            let success = JSON.object(forKey: "status") as! Int
                            if success == 200 {
                                let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                completionHandler(200 ,(JSON.object(forKey: "message") as? String)!)
                            }else if success == 401  {
                                AppState.sharedInstance.joinClubLogIn = "0"
                                //UserDefaults.Main.set(false, forKey: .autoLogin)
                                appDelegate.LogOutDevice()
                            }else {
                                completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                            }
                        }else {
//                            completionHandler(0,"No Data Found".localized)
                        }
                        break
                    case .failure(let error):
                        self.printAPIData(.httpRequestError, printData: error as AnyObject)
                        if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                            completionHandler(0,"Something went wrong. Please try again")
                        } else {
                            completionHandler(0,error.localizedDescription)
                        }
                        
                        break
                    }
                }
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func EditClubDetails(urlRequest :String,parameters : [String : Any], completionHandler:@escaping (_ clubData: ClubList ,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(ClubList(),0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)

        self.printAPIData(.requestURL, printData: urlRequest as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        let boundary = "Boundary-\(UUID().uuidString)"
        
        let URL = try! URLRequest(url: urlRequest ,method: .post, headers: ["Content-Type" :"multipart/form-data; boundary=\(boundary)","X-Authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()])
        
        upload( multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if value is Int {
                    multipartFormData.append(("\(value)").data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                } else {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        },with: URL,
          encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    WebServices.sharedInstance.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        if let JSON:NSDictionary = response.result.value as? NSDictionary {
                            
                            self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                            
                            let success = JSON.object(forKey: "status") as! Int
                            if success == 200 {
                                let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                let clubData = ClubList.init(dictJSON: data)
                                completionHandler(clubData,200 ,(JSON.object(forKey: "message") as? String)!)
                            }else if success == 401  {
                                AppState.sharedInstance.joinClubLogIn = "0"
                                //UserDefaults.Main.set(false, forKey: .autoLogin)
                                appDelegate.LogOutDevice()
                            }else {
                                completionHandler(ClubList(),success ,(JSON.object(forKey: "message") as? String)!)
                            }
                        }else {
                            completionHandler(ClubList(), 0,"No Data Found".localized)
                        }
                        break
                    case .failure(let error):
                        self.printAPIData(.httpRequestError, printData: error as AnyObject)
                        if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                            completionHandler(ClubList(),0,"Something went wrong. Please try again")
                        } else {
                            completionHandler(ClubList(),0,error.localizedDescription)
                        }
                        
                        break
                    }
                }
            case .failure(let encodingError):
                self.printAPIData(.httpRequestError, printData: encodingError as AnyObject)
                if encodingError.localizedDescription.contains("JSON") {
                    completionHandler(ClubList(),0,"Something went wrong. Please try again")
                } else {
                    completionHandler(ClubList(),0,encodingError.localizedDescription)
                }
                break
            }
        })
    }
    
    func UploadPhotoForClubCreate(mediaType :String ,mediaData : Data,clubId : String ,completionHandler:@escaping (_ photoUrl: String,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("",0,"The network connection was lost.".localized)
            return
        }
        
        let URL = try! URLRequest(url: WebURL.UploadInCreateClub + clubId + mediaType, method: .post, headers: ["X-Authorization" : getUserAuthToken(),"focus_club_id": getWhiteLabelAppId()])
        
        self.printAPIData(.requestURL, printData: WebURL.UploadInCreateClub as AnyObject)
        
        upload(
            multipartFormData: { multipartFormData in
                // Append Image to multipart form data.
                multipartFormData.append(mediaData, withName:"fileToUpload", fileName: "toBeDetermined.jpg", mimeType: "image/jpg")
        },with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        self.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                    })
                    upload.responseJSON { response in
                        
                        switch response.result {
                        case .success:
                            if let JSON:NSDictionary = response.result.value as? NSDictionary {
                                
                                self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                                
                                let success = JSON.object(forKey: "status") as! Int
                                if success == 200 {
                                    let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                    var mediaUrl = ""
                                    if (data.object(forKey: "profile_image") != nil) {
                                        mediaUrl = createString(value: (data as AnyObject).object(forKey: "profile_image") as AnyObject)
                                    }else {
                                        mediaUrl = createString(value: (data as AnyObject).object(forKey: "background_image") as AnyObject)
                                    }
                                    completionHandler(mediaUrl,200,(JSON.object(forKey: "message") as? String)!)
                                }else if success == 401  {
                                    AppState.sharedInstance.joinClubLogIn = "0"
                                    //UserDefaults.Main.set(false, forKey: .autoLogin)
                                    appDelegate.LogOutDevice()
                                }else {
                                    completionHandler("",success ,(JSON.object(forKey: "message") as? String)!)
                                }
                            }else {
                                completionHandler("", 0,"No Data Found".localized)
                            }
                            break
                        case .failure(let error):
                            self.printAPIData(.httpRequestError, printData: error as AnyObject)
                            if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                                completionHandler("",0,"Something went wrong. Please try again")
                            } else {
                                completionHandler("",0,error.localizedDescription)
                            }
                            break
                        }
                    }
                case .failure(let encodingError):
                    self.printAPIData(.httpRequestError, printData: encodingError as AnyObject)
                    break
                }
        })
    }
    
    func LeaveClub(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .put, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
//                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func deleteClub(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func webservice_getAllClubFolders(url:String, completionHandler:@escaping (_ folderslistData: [FolderList],_ morePage: String ,_ code: Int, _ message: String, _ JSON: NSDictionary) -> Void ) {
        
        var arrFolderData = [FolderList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrFolderData,"0",0,"The network connection was lost.".localized,NSDictionary())
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let responseDict = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    //Cache
                    var courseIdentifier = "clubFolderList-\(String(describing: AppState.sharedInstance.ClubList?.id ?? "0"))"
                    
                    if !AppState.sharedInstance.folderId.isEmpty {
                       
                        courseIdentifier = "clubFolderList-\(AppState.sharedInstance.folderId)"

                    } else if AppState.sharedInstance.menuUniqueId == "dynamic_course" && AppState.sharedInstance.isDynamicCourse {
                        
                        courseIdentifier = "clubFolderList-\(String(describing: AppState.sharedInstance.dynamicCourseData?.id ?? AppState.sharedInstance.ClubList?.id))"
                    }
                    
                    DataCache.instance.clean(byKey: courseIdentifier)
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: responseDict!)
                    DataCache.instance.write(data: dataValue, forKey: courseIdentifier)

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let folderData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        
                        
                        if folderData.count > 0 {
                            for data in folderData {
                                let folderData = FolderList.init(dictJSON: creatDictnory(value:data as AnyObject))
                                arrFolderData.append(folderData)
                            }
                        }
                        arrFolderData.sort(by: { Int($0.id)! < Int($1.id)! })
                        completionHandler(arrFolderData,morePages,200,(JSON.object(forKey: "message") as? String)!, JSON)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrFolderData,"0",success ,(JSON.object(forKey: "message") as? String)!,NSDictionary())
                    }
                }else {
                    completionHandler(arrFolderData,"0", 0,"No Data Found".localized,NSDictionary())
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                //completionHandler(arrFolderData,"0",0,error.localizedDescription,NSDictionary())
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrFolderData,"0",0,"Something went wrong. Please try again",NSDictionary())
                } else {
                    completionHandler(arrFolderData,"0",0,error.localizedDescription,NSDictionary())
                }

                break
            }
        })
    }
    
    func webService_saveSortingSetting(url:String,parameters:[String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func webService_createFolder(url:String,parameters:[String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func webService_UpdateFolder(url:String,parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                    completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func deleteClubFolder(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getAllFolderMedia(url:String, completionHandler:@escaping (_ result: [GalleryMedia],_ morepages: String ,_ code: Int, _ message: String) -> Void) {
        
        var arr_galleryData = [GalleryMedia]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arr_galleryData,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let mediaData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        if mediaData.count > 0 {
                            for data in mediaData {
                                let mediaData = GalleryMedia.init(dictJSON: creatDictnory(value: data as AnyObject))
                                arr_galleryData.append(mediaData)
                            }
                        }
                        completionHandler(arr_galleryData,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arr_galleryData,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arr_galleryData,"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arr_galleryData,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arr_galleryData,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func webService_InviteMembers(parameter: [String : Any] , completionHandler:@escaping (_ code: Int, _ message: String, _ response: [String: Any]) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized, [:])
            return
        }
        
        let strPerPage = parameter["email"] as? [String]
        
        let paraString = getJsonStringFromDictionary(aParameters: parameter)
        
        self.printAPIData(.requestURL, printData: WebURL.inviteMembers as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
//        let manager = Alamofire.SessionManager.default
//        manager.session.configuration.timeoutIntervalForRequest = 180
        
        var request = URLRequest(url: URL.init(string: WebURL.inviteMembers)!)
        request.httpMethod = "POST"
        request.timeoutInterval = 1000 // Set your timeout interval here.
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(getUserAuthToken(), forHTTPHeaderField: "X-Authorization")
        request.addValue(getWhiteLabelAppId(), forHTTPHeaderField: "focus_club_id")

        let data = (paraString?.data(using: .utf8))! as Data
        request.httpBody = data
        
        Alamofire.request(request as URLRequestConvertible).responseJSON { (responseData) -> Void in
//            if((responseData.result.value) != nil) { //Check for response
//                print(responseData.result.value!)
//            }
            
            switch responseData.result {
            case .success:
                if let JSON:NSDictionary = responseData.result.value as? NSDictionary {
                    
                    let response = responseData.result.value as? [String: Any] ?? [:]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "success") as? Int
                    let status = JSON.object(forKey: "status") as? Int
                    if success == 200 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!, response)
                    }else if success == nil {
                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!, response)
                    }else if status == 401 || success == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!, [:])
                    }
                }else {
                    completionHandler(0,"No Data Found".localized, [:])
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again", [:])
                } else {
                    completionHandler(0,error.localizedDescription, [:])
                }

                break
            }
        }

//        manager.request(WebURL.inviteMembers , method: .post, parameters: [:],encoding: paraString!, headers: ["Content-Type":"application/json","X-authorization" : Authentictoken, "focus_club_id": getWhiteLabelAppId()]).responseJSON(completionHandler: { (response) in
//
//            switch response.result {
//            case .success:
//                if let JSON:NSDictionary = response.result.value as? NSDictionary {
//
//                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
//
//                    let success = JSON.object(forKey: "success") as? Int
//                    let status = JSON.object(forKey: "status") as? Int
//                    if success == 1 {
//                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
//                    }else if success == nil {
//                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!)
//                    }else {
//                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!)
//                    }
//                }else {
//                    completionHandler(0,"No Data Found".localized)
//                }
//                break
//            case .failure(let error):
//                self.printAPIData(.httpRequestError, printData: error as AnyObject)
//                completionHandler(0,error.localizedDescription)
//                break
//            }
//        })
    }
    
    func GetLeaderboardPointsDetails(url:String, completionHandler:@escaping (_ clubData: [LeaderboardPointsDetails],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [LeaderboardPointsDetails]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "LeaderboardPointsDetails")
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if feedData.count > 0 {
                            for data in feedData {
                                let dataDict = creatDictnory(value:data as AnyObject)
                                let clubMemberList = LeaderboardPointsDetails.init(dictJSON: creatDictnory(value: dataDict))
                                arrMemberList.append(clubMemberList)
                            }
                        }
                       
                        completionHandler(arrMemberList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func getChatRoomList(url:String, completionHandler:@escaping (_ clubData: [ChatRoomDetails],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [ChatRoomDetails]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let responseDict = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "ChatRoomList")
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if feedData.count > 0 {
                            for data in feedData {
                                let dataDict = creatDictnory(value:data as AnyObject)
                                let clubMemberList = ChatRoomDetails.init(dictJSON: creatDictnory(value: dataDict))
                                arrMemberList.append(clubMemberList)
                            }
                        }
                       
                        completionHandler(arrMemberList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func getYouTubeList(url:String, completionHandler:@escaping (_ clubData: [YouTubeVideoDetails] ,_ nextPageToken: String,_ code: Int, _ message: String) -> Void) {
        var arrYouTubeVideoList = [YouTubeVideoDetails]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrYouTubeVideoList,"",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: [ "X-authorization" : getUserAuthToken(), "focus_club_id": WHITELABEL_APP_ID.isEmpty ? "\(AppState.sharedInstance.ClubList?.id ?? "1")" : WHITELABEL_APP_ID]).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "YouTubeVideoList-\(AppState.sharedInstance.clubPowerUpWebPage?.id ?? "0")")
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        
                        let nextPageToken = JSON.object(forKey: "nextPageToken") as? String

                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        if feedData.count > 0 {
                            for data in feedData {
                                let dataDict = creatDictnory(value:data as AnyObject)
                                let clubMemberList = YouTubeVideoDetails.init(dictJSON: creatDictnory(value: dataDict))
                                arrYouTubeVideoList.append(clubMemberList)
                            }
                        }
                       
                        completionHandler(arrYouTubeVideoList,nextPageToken ?? "",200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrYouTubeVideoList,"",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrYouTubeVideoList,"", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrYouTubeVideoList,"",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrYouTubeVideoList,"",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func getSMSChatList(url:String, completionHandler:@escaping (_ clubData: [SMSChatDetails] ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [SMSChatDetails]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "SMSChatList")
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        if feedData.count > 0 {
                            for data in feedData {
                                let dataDict = creatDictnory(value:data as AnyObject)
                                let clubMemberList = SMSChatDetails.init(dictJSON: creatDictnory(value: dataDict))
                                arrMemberList.append(clubMemberList)
                            }
                        }
                       
                        completionHandler(arrMemberList,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList, 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func fetchNotificationsList(url:String, completionHandler:@escaping (_ clubData: [NotificationDetails],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrNotificationList = [NotificationDetails]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrNotificationList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        //let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        if feedData.count > 0 {
                            for data in feedData {
                                let notificationDetail = creatDictnory(value:data as AnyObject)
                                arrNotificationList.append(NotificationDetails(dictJSON: notificationDetail))
                            }
                        }
                        
                        completionHandler(arrNotificationList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrNotificationList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrNotificationList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrNotificationList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrNotificationList,"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func getFollowerList(url:String, completionHandler:@escaping (_ clubData: [UserDetail],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [UserDetail]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    let responseDict = response.result.value as? [String: Any]

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        //let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        if let members = responseDict?["data"] as? [[String: Any]] {
                            for memberDetail in members {
                                arrMemberList.append(UserDetail(fromDictionary: memberDetail))
                            }
                        }
                        completionHandler(arrMemberList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    func getLikesMemberList(url:String, completionHandler:@escaping (_ clubData: [UserDetail],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [UserDetail]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    let responseDict = response.result.value as? [String: Any]

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        //let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        if let members = responseDict?["data"] as? [[String: Any]] {
                            for memberDetail in members {
                                arrMemberList.append(UserDetail(fromDictionary: memberDetail))
                            }
                        }
                        completionHandler(arrMemberList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func GetLeaderboardMemberList(url:String, completionHandler:@escaping (_ clubData: [LeaderboardMembers],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [LeaderboardMembers]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "MemberListForLeaderboard")
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if feedData.count > 0 {
                            for data in feedData {
                                let dataDict = creatDictnory(value:data as AnyObject)
                                let clubMemberList = LeaderboardMembers.init(dictJSON: creatDictnory(value: dataDict))
                                arrMemberList.append(clubMemberList)
                            }
                        }
                       
                        completionHandler(arrMemberList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func GetChatMemberList(url:String, completionHandler:@escaping (_ clubData: [MemberList],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [MemberList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "MemberListForChat")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if feedData.count > 0 {
                            for data in feedData {
                                let dataDict = creatDictnory(value:data as AnyObject)
                                let clubMemberList = MemberList.init(dictJSON: creatDictnory(value: dataDict))
                                arrMemberList.append(clubMemberList)
                            }
                        }
                        completionHandler(arrMemberList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    func GetClubMemberList(url:String, completionHandler:@escaping (_ clubData: [MemberList],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        var arrMemberList = [MemberList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrMemberList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    
                    //let response = response.result.value as? [String: Any] ?? [:]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "MemberList")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        var arra_keys = feedData.allKeys as! [String]
                        arra_keys.sort {
                            return $0 < $1
                        }
                        
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        if arra_keys.count > 0 {
                            for i in 0...arra_keys.count - 1 {
                                let strKey = arra_keys[i] as String
                                let data = creatDictnory(value:feedData[strKey] as AnyObject)
                                if let dataId = data["id"] as? String {
                                    let clubMemberList = MemberList.init(dictJSON: creatDictnory(value: data))
                                    arrMemberList.append(clubMemberList)
                                }
                            }
                        }
                        completionHandler(arrMemberList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrMemberList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrMemberList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrMemberList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrMemberList,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    //Initialize Firebase Chat
    func initializeFirebaseChat(params:[String:Any], url:String, completionHandler:@escaping (_ response: [String: Any],_ code: Int, _ message: String) -> Void) {
        
        var arrPowerUpList = [ClubPowerUp]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler([:],0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: params)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
                
        var request = URLRequest(url: URL.init(string: url)!)
        request.httpMethod = "POST"
        request.timeoutInterval = 1000 // Set your timeout interval here.
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(getUserAuthToken(), forHTTPHeaderField: "X-Authorization")
        request.addValue(getWhiteLabelAppId(), forHTTPHeaderField: "focus_club_id")

        let data = (paraString?.data(using: .utf8))! as Data
        request.httpBody = data
        
        Alamofire.request(request as URLRequestConvertible).responseJSON { (response) -> Void in
                
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let response = response.result.value as? [String: Any] ?? [:]
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        completionHandler(response,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler([:],success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler([:], 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler([:],0,"Something went wrong. Please try again")
                } else {
                    completionHandler([:],0,error.localizedDescription)
                }
                
                break
            }
        }
    }
    //Create Firebase User
    func loginFirebaseUser(params:[String:Any],url:String, completionHandler:@escaping (_ response: [String: Any],_ code: Int, _ message: String) -> Void) {
        
        var arrPowerUpList = [ClubPowerUp]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler([:],0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: params)

        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)

        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let response = response.result.value as? [String: Any] ?? [:]
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        completionHandler(response,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler([:],success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler([:], 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler([:],0,"Something went wrong. Please try again")
                } else {
                    completionHandler([:],0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func GetPowerUpList(url:String, completionHandler:@escaping (_ clubPowerupData: [ClubPowerUp],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        
        var arrPowerUpList = [ClubPowerUp]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrPowerUpList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if feedData.count > 0 {
                            for data in feedData {
                                let pageDic = creatDictnory(value:(data as AnyObject).object(forKey: "page") as AnyObject)
                                let listPageDic = creatDictnory(value:(data as AnyObject).object(forKey: "listpage") as AnyObject)
                                
                                let listingPages = listPage.init(dictJSON: listPageDic)
                                let pages = listPage.init(dictJSON: pageDic)
                                
                                let mainPowerUps = ClubPowerUp.init(listpage: listingPages, page: pages)
                                arrPowerUpList.append(mainPowerUps)
                            }
                        }
                        completionHandler(arrPowerUpList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrPowerUpList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrPowerUpList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrPowerUpList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrPowerUpList,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    // _ result: TaskList,
    func readUnreadNotificationStatus(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .post, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                     if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    // _ result: TaskList,
    func finishUnFinishTaskList(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                     if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func pushNotificationUpdateAuth(url:String,parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .put, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                    completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func clearPushNotificationCount(url:String,completionHandler:@escaping (_ code: Int, _ message: String) -> Void ) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func DeleteVoiceMail(requestUrl :String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        
        request(requestUrl , method: .delete, parameters: [:],headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func getVoicemailList(url :String,parameters : [String : Any], completionHandler:@escaping ( _ result: VoiceMail, _ morepages: String, _ code: Int, _ message: String) -> Void) {
                
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(VoiceMail(),"0",0,"The network connection was lost.")
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .get, parameters: [:],encoding: "", headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success( _):
                
                if let JSON = response.result.value as? [String: Any] {

                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON["status"] as? Int
                    let morePages = JSON["more_pages"] as? String

                    if success == 200 {
                        completionHandler(VoiceMail(fromDictionary: JSON),morePages ?? "0", 200,(JSON["message"] as? String)!)

//                        if let data = JSON["data"] as? [[String: Any]], data.count > 0 {
//                        }
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(VoiceMail(),"0",success!,(JSON["message"] as? String)!)
                    }
                }else {
                    completionHandler(VoiceMail(),"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(VoiceMail(),"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(VoiceMail(),"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func getProductCategories(url :String,parameters : [String : Any], completionHandler:@escaping ( _ result: [ParentCategoryDetail] , _ code: Int, _ message: String) -> Void) {
        
        var parentCategoryList = [ParentCategoryDetail]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(parentCategoryList,0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .get, parameters: [:],encoding: "", headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success( _):
                
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "ClubPlanList")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let planData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        if planData.count > 0 {
//                            for i in 0...arra_keys.count - 1 {
                            for dict in planData {
                                let data = dict as? [String: Any]
                                parentCategoryList.append(ParentCategoryDetail.init(fromDictionary: data!))//.init(dictJSON: currentPlanData))
                            }
                        }

                        completionHandler(parentCategoryList,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(parentCategoryList,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(parentCategoryList,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(parentCategoryList,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(parentCategoryList,0,error.localizedDescription)
                }

                break
            }
        })
    }
    func getClubPlans(url :String,parameters : [String : Any], completionHandler:@escaping ( _ result: [ClubPlans] , _ currentClubDetails: ClubPlanDetail , _ morepages: String, _ code: Int, _ message: String) -> Void) {
        
        var clubPlaList = [ClubPlans]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(clubPlaList,ClubPlanDetail(),"0",0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success( _):
                
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "ClubPlanList")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let currentPlanData = creatDictnory(value:JSON.object(forKey: "currentPlanofMember") as AnyObject)

                        //ClubPlan Data Set
//                        let clubPlanModel = ClubPlanDetail.init(dictJSON: currentPlanData)
                        let clubPlanModel = ClubPlanDetail.initialize()
                        
//                        let planData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let planData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
//                        var arra_keys = planData.allKeys as! [String]
//                        //For Array Sorting
//                        arra_keys.sort {
//                            return $0 < $1
//                        }

                        if planData.count > 0 {
//                            for i in 0...arra_keys.count - 1 {
                            for dict in planData {
//                                let strKey = arra_keys[i] as String
                                //let data = creatDictnory(value:planData[strKey] as AnyObject)
                                //creatDictnory(value:(data as AnyObject).object(forKey: "page") as AnyObject)
                                let authorityData = creatDictnory(value: (dict as AnyObject).object(forKey: "clubAuthority") as AnyObject)
                                let clubPlanData = creatDictnory(value: (dict as AnyObject).object(forKey: "clubPlan") as AnyObject)
                                let screenPermissionsData = creatDictnory(value: (dict as AnyObject).object(forKey: "screenPermissions") as AnyObject)
                                
                                //ClubAuthority Data Set
                                let autData = ClubAuthority.init(dictJSON: authorityData)
                                
                                //ClubPlan Data Set
                                let clubPlanModel = ClubPlanDetail.init(dictJSON: clubPlanData)
                                
                                //ScreenPermission Data Set
                                let ScreenPermissionModel = ScreenPermissions.init(dictJSON: screenPermissionsData)
                                
                                let clubPlansModel = ClubPlans.init(clubAuth: autData, clubPlanDetail: clubPlanModel, screenPermissions: ScreenPermissionModel)
                                
                                clubPlaList.append(clubPlansModel)
                            }
                            
                        }
//                        completionHandler(clubPlaList,clubPlanModel,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(clubPlaList,ClubPlanDetail(),"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(clubPlaList,ClubPlanDetail(),"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(clubPlaList,ClubPlanDetail(),"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(clubPlaList,ClubPlanDetail(),"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getClubPlansForPredefinedChat(url :String,parameters : [String : Any], completionHandler:@escaping ( _ result: [ClubPlanDetail] , _ currentClubDetails: ClubPlanDetail , _ morepages: String, _ code: Int, _ message: String) -> Void) {
        
        var clubPlaList = [ClubPlanDetail]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(clubPlaList,ClubPlanDetail(),"0",0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success( _):
                
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "ClubPlanList")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let currentPlanData = creatDictnory(value:JSON.object(forKey: "currentPlanofMember") as AnyObject)

                        //ClubPlan Data Set
                        let clubPlanModel = ClubPlanDetail.init(dictJSON: currentPlanData)

//                        let planData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let planData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
//                        var arra_keys = planData.allKeys as! [String]
//                        //For Array Sorting
//                        arra_keys.sort {
//                            return $0 < $1
//                        }

                        if planData.count > 0 {
//                            for i in 0...arra_keys.count - 1 {
                            for dict in planData {
//                                let strKey = arra_keys[i] as String
                                //let data = creatDictnory(value:planData[strKey] as AnyObject)
                                //creatDictnory(value:(data as AnyObject).object(forKey: "page") as AnyObject)
                                //let authorityData = creatDictnory(value: (dict as AnyObject).object(forKey: "clubAuthority") as AnyObject)
                                let clubPlanData = creatDictnory(value: (dict as AnyObject).object(forKey: "clubPlan") as AnyObject)
                                //let screenPermissionsData = creatDictnory(value: (dict as AnyObject).object(forKey: "screenPermissions") as AnyObject)
                                
                                //ClubAuthority Data Set
                                //let autData = ClubAuthority.init(dictJSON: authorityData)
                                
                                //ClubPlan Data Set
                                let clubPlanModel = ClubPlanDetail.init(dictJSON: dict as! NSDictionary)//(dictJSON: clubPlanData)
                                
                                //ScreenPermission Data Set
                                //let ScreenPermissionModel = ScreenPermissions.init(dictJSON: screenPermissionsData)
                                
                                //let clubPlansModel = ClubPlans.init(clubAuth: autData, clubPlanDetail: clubPlanModel, screenPermissions: ScreenPermissionModel)
                                
                                clubPlaList.append(clubPlanModel)
                            }
                        }
                        completionHandler(clubPlaList,clubPlanModel,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(clubPlaList,ClubPlanDetail(),"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(clubPlaList,ClubPlanDetail(),"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(clubPlaList,ClubPlanDetail(),"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(clubPlaList,ClubPlanDetail(),"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func createNewClubPlan(url:String,parameters : [String : Any],isFromAdd :Bool ,completionHandler:@escaping ( _ planId : String, _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("",0,"The network connection was lost.".localized)
            return
        }
        
        func convertParams(aParameters : [String : Any]) -> String {
            var paramJsonString = ""
            do {
                let jsonData = try JSONSerialization.data(withJSONObject:aParameters, options:[])
                paramJsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            } catch {
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
            }
            return paramJsonString
        }
        
        let paraString = convertParams(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
       
        var method = HTTPMethod.post
        if !isFromAdd {
            method = HTTPMethod.put
        }
        
        request(url, method: method, parameters: [:],encoding: paraString, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success(let resultData):
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let planId = createString(value: (data as AnyObject).object(forKey: "planid") as AnyObject)
                        completionHandler(planId,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler("",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler("",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler("",0,"Something went wrong. Please try again")
                } else {
                    completionHandler("",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func addImageInPlan(requestUrl :String ,mediaType :String ,mediaData : Data,clubId : String,planId : String,fileName : String,completionHandler:@escaping (_ mediaUrl: String,_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("",0,"The network connection was lost.".localized)
            return
        }
        
        let boundary = "Boundary-\(UUID().uuidString)"
        let URL = try! URLRequest(url: requestUrl + clubId + "/" + planId, method: .post, headers: ["Content-Type" :"multipart/form-data; boundary=\(boundary)","X-authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()])
        
        self.printAPIData(.requestURL, printData: requestUrl as AnyObject)
        
        upload(
            multipartFormData: { multipartFormData in
                // Append Image to multipart form data.
                multipartFormData.append(mediaData, withName:"fileToUpload", fileName: "toBeDetermined.jpg", mimeType: "image/jpg")
        },with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        self.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                    })
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let JSON:NSDictionary = response.result.value as? NSDictionary {
                                
                                self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                                
                                let success = JSON.object(forKey: "status") as! Int
                                if success == 200 {
                                    let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                    let mediaUrl = createString(value: (data as AnyObject).object(forKey: "imageurl") as AnyObject)
                                    completionHandler(mediaUrl,200,(JSON.object(forKey: "message") as? String)!)
                                }else if success == 401  {
                                    AppState.sharedInstance.joinClubLogIn = "0"
                                    //UserDefaults.Main.set(false, forKey: .autoLogin)
                                    appDelegate.LogOutDevice()
                                }else {
                                    completionHandler("",success ,(JSON.object(forKey: "message") as? String)!)
                                }
                            }else {
                                completionHandler("", 0,"No Data Found".localized)
                            }
                            break
                        case .failure(let error):
                            self.printAPIData(.httpRequestError, printData: error as AnyObject)
                            if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                                completionHandler("",0,"Something went wrong. Please try again")
                            } else {
                                completionHandler("",0,error.localizedDescription)
                            }

                            break
                        }
                    }
                case .failure(let encodingError):
                    self.printAPIData(.httpRequestError, printData: encodingError as AnyObject)
                    break
                }
        })
    }
    
    func receiptVerification(url:String,parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        print("focus_club_id: \(getWhiteLabelAppId())")
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: ["X-Authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()]).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }

    func addCoinsInUserAccount(url :String,parameters : [String : Any], completionHandler:@escaping ( _ result: TaskList, _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(TaskList(),0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
       
        request(url, method: .put, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let taskListData = TaskList.init(dictJSON: data)
                        completionHandler(taskListData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(TaskList(),success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(TaskList(),0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(TaskList(),0,"Something went wrong. Please try again")
                } else {
                    completionHandler(TaskList(),0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    //Startup page
    func addUpdateStartupPage(url :String, parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getDefaultStartupPage(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let walletData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getUserPointDetails(url:String, completionHandler:@escaping (_ WalletDetails: Int,_ code: Int, _ message: String) -> Void) {
        
        //var result = UserPointDetails()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let response = response.result.value as? [String: Any]

                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        
                        let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: response!)
                        DataCache.instance.write(data: dataValue, forKey: "pointDetails")

                        
                        if let pointsData = JSON["data"] as? [String: Any] {
                            appDelegate.totalPoints = pointsData["total_points"] as! Int
                        }
                        //                        let dataDict = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        //                        let poinSummary = creatArray(value:dataDict.object(forKey: "point_summary") as AnyObject)
                        //
                        //                        if poinSummary.count > 0 {
                        //                            for data in poinSummary {
                        //                                result = UserPointDetails.init(dictJSON: creatDictnory(value: data as AnyObject))
                        //                            }
                        //                        }
                        completionHandler(appDelegate.totalPoints,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(0,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getWalletDetails(url:String, completionHandler:@escaping (_ WalletDetails: WalletDetails,_ code: Int, _ message: String) -> Void) {
        
        var result = WalletDetails()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(WalletDetails(),0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
                
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let response = response.result.value as? [String: Any]
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        //Cache
                        let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: response!)
                        DataCache.instance.write(data: dataValue, forKey: "walletDetails")

                        let walletData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
//                        if walletData.count > 0 {
//                            for data in walletData {
                                result = WalletDetails.init(dictJSON: creatDictnory(value: walletData as AnyObject))
//                            }
//                        }
                        completionHandler(result,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(WalletDetails(),success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(WalletDetails(),0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(WalletDetails(),0,"Something went wrong. Please try again")
                } else {
                    completionHandler(WalletDetails(),0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getWalletTransaction(url:String, completionHandler:@escaping (_ result: [WalletDetails],_ morepages: String ,_ code: Int, _ message: String) -> Void) {
        
        var result = [WalletDetails]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(result,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    let response = response.result.value as? [String: Any]
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: response!)
                    DataCache.instance.write(data: dataValue, forKey: "TransactionDetails")
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let mainData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if mainData.count > 0 {
                            for data in mainData {
                                let details = WalletDetails.init(dictJSON: creatDictnory(value: data as AnyObject))
                                result.append(details)
                            }
                        }
                        
                        completionHandler(result,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(result,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(result,"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(result,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(result,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func webservice_purchaseIndividualItems(url:String,parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
       
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func purchaseItemsStatus(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func upgradeClubPlans(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let status = JSON.object(forKey: "status") as? Int
                    let success = JSON.object(forKey: "success") as? Int

                    if success == 1 && success != nil {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if status != nil{
                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401 || status == 400 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func purchaseGallery(url:String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let status = JSON.object(forKey: "status") as? Int
                    let success = JSON.object(forKey: "success") as? Int
                    
                    if success == 1 && success != nil {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if status != nil{
                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401 || status == 400 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }

    
    func getRequeresInvites(url:String, isReceivedInvites : Bool? = false,  completionHandler:@escaping (_ result: [RequestInvitesList],_ morepages: String ,_ code: Int, _ message: String) -> Void) {
        
        var result = [RequestInvitesList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(result,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "InvitationList")

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let planData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if isReceivedInvites! {
                            for (_, objReceiveInvites) in creatArray(value: JSON.object(forKey: "data") as AnyObject).enumerated() {
                                if let receiveInvite = objReceiveInvites as? NSDictionary {
                                    result.append(RequestInvitesList.init(dictJSON: receiveInvite))
                                }
                            }
                        }else{
                            var arra_keys = planData.allKeys as! [String]
                            //For Array Sorting
                            arra_keys.sort {
                                return $0 < $1
                            }
                            
                            if arra_keys.count > 0 {
                                for i in 0...arra_keys.count - 1 {
                                    let strKey = arra_keys[i] as String
                                    let data = creatDictnory(value:planData[strKey] as AnyObject)
                                    let details = RequestInvitesList.init(dictJSON: data)
                                    result.append(details)
                                }
                            }
                        }
                        
                        completionHandler(result,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(result,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(result,"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(result,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(result,"0",0,error.localizedDescription)
                }
                break
            }
        })
    }

    func webservice_watchMedia(url:String,parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "success") as? Int
                    let status = JSON.object(forKey: "status") as? Int
                    
                    if success == 1 || status == 200 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func webservice_gainPointsForSharingPost(url:String,parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "success") as? Int
                    let status = JSON.object(forKey: "status") as? Int
                    
                    if success == 1 || status == 200 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func webservice_viewGalleryItems(url:String,parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "success") as? Int
                    let status = JSON.object(forKey: "status") as? Int
                    
                    if success == 1 || status == 200 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func webservice_inviteRequestReply(url:String,parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "success") as? Int
                    let status = JSON.object(forKey: "status") as? Int
                    
                    if success == 1 || status == 200 {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!)
                    }else if status == 401 || status == 400 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(status!,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func getSingleClubDetails(url:String, completionHandler:@escaping (_ clubData: ClubList , _ code: Int, _ message: String) -> Void) {
        
        let arrClubData = ClubList()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrClubData,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        data.setValue(data.value(forKey: "num_of_members"), forKey: "num_members")
                        let clubData = ClubList.init(dictJSON: data)
                        completionHandler(clubData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrClubData,success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrClubData, 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrClubData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrClubData,0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    //Forgot Password
    func webservice_forgotPassword(url :String, parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String, _ isCompetible : Bool) -> Void) {
        
        let isCompetible : Bool = true
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized, isCompetible)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: ["Content-Type":"application/json"]).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as? Int
                    if success == 200 && success != nil {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else if success == nil {
                        completionHandler(0,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else {
                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }
                }else {
                    completionHandler(0,"No Data Found",isCompetible)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(0,error.localizedDescription,isCompetible)
                break
            }
        })
    }
    
    func registerNewUser(url :String,parameters : [String : Any], completionHandler:@escaping ( _ auth: String,_ userid: String, _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("","",0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
       
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: ["Content-Type":"application/json","focus_club_id": getWhiteLabelAppId()]).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let AuthorizationToken = createString(value: (data as AnyObject).object(forKey: "Authorization") as AnyObject)
                        let userData = creatDictnory(value:data.object(forKey: "user") as AnyObject)
                        let userId = createString(value: (userData as AnyObject).object(forKey: "id") as AnyObject)
                        completionHandler(AuthorizationToken,userId,200,(JSON.object(forKey: "message") as? String)!)
                    }else {
                        completionHandler("","",success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler("","",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler("","",0,error.localizedDescription)
                break
            }
        })
    }
    
    func getSentInvites(url:String, completionHandler:@escaping (_ result: [RequestInvitesList],_ morepages: String ,_ code: Int, _ message: String) -> Void) {
        
        var result = [RequestInvitesList]()
        
        if !(appDelegate.manager?.isReachable)! {
                        completionHandler(result,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let planData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        var arra_keys = planData.allKeys as! [String]
                        //For Array Sorting
                        arra_keys.sort {
                            return $0 < $1
                        }
                        
                        if arra_keys.count > 0 {
                            for i in 0...arra_keys.count - 1 {
                                let strKey = arra_keys[i] as String
                                let data = creatDictnory(value:planData[strKey] as AnyObject)
                                let details = RequestInvitesList.init(dictJSON: data)
                                result.append(details)
                            }
                        }
                        
                        completionHandler(result,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(result,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(result,"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(result,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(result,"0",0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func getAllInvitesClubs(url:String, completionHandler:@escaping (_ clubData: [ClubList] ,_ code: Int, _ message: String) -> Void) {
        
        var arrClubData = [ClubList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrClubData,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)

        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        for (_, value) in feedData {
                            if let data = value as? [String: Any] {
                                let data1 = creatDictnory(value:data as AnyObject)
                                let clubData = ClubList.init(dictJSON: data1)
                                arrClubData.append(clubData)
                            }
                        }
                        arrClubData.sort(by: { Int($0.id)! < Int($1.id)! })
                        completionHandler(arrClubData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrClubData,success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrClubData,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrClubData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrClubData,0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func getAllPowerList(url:String, completionHandler:@escaping (_ clubPowerupData: [ClubPowerUpList],_ morePage: String ,_ code: Int, _ message: String) -> Void) {
        
        var arrPowerUpList = [ClubPowerUpList]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrPowerUpList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "PowerUpsList")
                    
                    let success = JSON.object(forKey: "status") as? Int
                    if success == 200 {
                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        for (_, value) in feedData {
                            if let data = value as? [String: Any] {
                                if let dataId = data["id"] as? String {
                                    print(dataId)
                                    let powerUpList = ClubPowerUpList.init(dictJSON: creatDictnory(value: data as AnyObject))
                                    arrPowerUpList.append(powerUpList)
                                }
                            }
                        }
                        arrPowerUpList.sort(by: { Int($0.id)! < Int($1.id)! })
                        completionHandler(arrPowerUpList,"0",200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrPowerUpList,"0",success!,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrPowerUpList,"0", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrPowerUpList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrPowerUpList,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func buyPowerUpFromList(url :String,parameters : [String : Any], completionHandler:@escaping (_ subscrib_id: String , _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("",0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let subscription_id = createString(value: (JSON as AnyObject).object(forKey: "subscription_id") as AnyObject)
                        completionHandler(subscription_id,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler("",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler("",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler("",0,"Something went wrong. Please try again")
                } else {
                    completionHandler("",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getPowerupSettings(url:String, completionHandler:@escaping (_ clubData: ClubPowerUpSettings , _ code: Int, _ message: String) -> Void) {
        
        var arrPowerUpData = ClubPowerUpSettings()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrPowerUpData,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let mainDict = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        var data = creatDictnory(value:mainDict.object(forKey: "0") as AnyObject)
                        var id = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                        if id.isEmpty {
                            data = mainDict
                        }
                        id = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                        let club_id = createString(value: (data as AnyObject).object(forKey: "club_id") as AnyObject)
                        let title = createString(value: (data as AnyObject).object(forKey: "title") as AnyObject)
                        let address = createString(value: (data as AnyObject).object(forKey: "address") as AnyObject)
                        let created = createString(value: (data as AnyObject).object(forKey: "created") as AnyObject)
                        let deleted = createString(value: (data as AnyObject).object(forKey: "deleted") as AnyObject)
                        let name = createString(value: (data as AnyObject).object(forKey: "name") as AnyObject)
                        let caption = createString(value: (data as AnyObject).object(forKey: "caption") as AnyObject)
                        arrPowerUpData = ClubPowerUpSettings.init(id: id, club_id: club_id, title: title, address: address, created: created, deleted: deleted,name :name,caption: caption)
                        completionHandler(arrPowerUpData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrPowerUpData,success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrPowerUpData, 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrPowerUpData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrPowerUpData,0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func addUpdatePowerupSetting(url :String,parameters : [String : Any],aMethod : String, completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
       
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        var method = HTTPMethod.put
        
        if aMethod == "POST" {
            method = HTTPMethod.post
        }
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func joinClubContest(url :String, parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String, _ isCompetible : Bool) -> Void) {
        
        let isCompetible : Bool = true
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized, isCompetible)
            return
        }
            
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as? Int
                    if success == 200 && success != nil {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else if success == nil {
                        completionHandler(0,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else {
                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }
                }else {
                    completionHandler(0,"No Data Found",isCompetible)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(0,error.localizedDescription,isCompetible)
                break
            }
        })
    }
    
    func referFriendAPICall(url :String, parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String, _ isCompetible : Bool) -> Void) {
        
        let isCompetible : Bool = true
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized, isCompetible)
            return
        }
            
         let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
         self.printAPIData(.requestURL, printData: url as AnyObject)
         self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as? Int
                    if success == 200 && success != nil {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else if success == nil {
                        completionHandler(0,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else {
                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }
                }else {
                    completionHandler(0,"No Data Found",isCompetible)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(0,error.localizedDescription,isCompetible)
                break
            }
        })
    }
    
    func productPackage_BuyNow(url :String, parameters : [String : Any],completionHandler:@escaping (_ code: Int, _ message: String, _ isCompetible : Bool) -> Void) {
        
        let isCompetible : Bool = true
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized, isCompetible)
            return
        }
        
        //let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        //self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as? Int
                    if success == 200 && success != nil {
                        completionHandler(200,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else if success == nil {
                        completionHandler(0,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }else {
                        completionHandler(success!,(JSON.object(forKey: "message") as? String)!,isCompetible)
                    }
                }else {
                    completionHandler(0,"No Data Found",isCompetible)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                completionHandler(0,error.localizedDescription,isCompetible)
                break
            }
        })
    }
    
    func getAllContestList(url:String, completionHandler:@escaping (_ result: [ContestDetails],_ morepages: String ,_ code: Int, _ message: String) -> Void) {
        
        var arrStoreList = [ContestDetails]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrStoreList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                //if let JSON:NSDictionary = response.result.value as? NSDictionary {
                if let JSON = response.result.value as? [String: Any] {
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    //Cache
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                    DataCache.instance.write(data: dataValue, forKey: "contestList")

                    let success = JSON["status"] as? Int
                    if success == 200 {
                        let morePages = JSON["more_pages"] as? String
                        if let products = JSON["data"] as? [[String: Any]] {
                            

                            for prodDetail in products {
                                arrStoreList.append(ContestDetails(fromDictionary: prodDetail))
                            }
                        }
                        
                        completionHandler(arrStoreList,morePages ?? "0",200,(JSON["message"] as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrStoreList,"0",success ?? 0,(JSON["message"] as? String)!)
                    }
                }else {
                    completionHandler(arrStoreList,"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrStoreList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrStoreList,"0",0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    func getAllProductServiceList(url:String, completionHandler:@escaping (_ productDetail: ProductServiceDetail,_ memberEditableListings: String, _ result: [ProductServiceDetail],_ morepages: Int ,_ code: Int, _ message: String) -> Void) {
        
        var arrStoreList = [ProductServiceDetail]()
        var prodDetails = ProductServiceDetail()

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(ProductServiceDetail(),"0",arrStoreList,0,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                //if let JSON:NSDictionary = response.result.value as? NSDictionary {
                if let JSON = response.result.value as? [String: Any] {
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON["status"] as? Int
                    if success == 200 {
                        let morePages = JSON["more_pages"] as? Int
                        let member_editable_listings = JSON["member_editable_listings"] as? String

                        if let productDetails = JSON["product_details"] as? [String: Any] {
                            
                            prodDetails = ProductServiceDetail(fromDictionary: productDetails)
                        }
                        
                        if let products = JSON["data"] as? [[String: Any]] {
                            
                            //Cache
                            let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: JSON)
                            DataCache.instance.write(data: dataValue, forKey: "productServiceList-\(AppState.sharedInstance.productPackagePage?.id ?? "0")")

                            for prodDetail in products {
                                arrStoreList.append(ProductServiceDetail(fromDictionary: prodDetail))
                            }
                        }
                        
                        completionHandler(prodDetails,member_editable_listings ?? "0", arrStoreList,morePages ?? 0,200,(JSON["message"] as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(ProductServiceDetail(),"0",arrStoreList,0,success ?? 0,(JSON["message"] as? String)!)
                    }
                }else {
                    completionHandler(ProductServiceDetail(),"0",arrStoreList,0,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(ProductServiceDetail(),"0",arrStoreList,0,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(ProductServiceDetail(),"0",arrStoreList,0,0,error.localizedDescription)
                }
                
                break
            }
        })
    }
    
    func getAllPowerUpStoreList(url:String, completionHandler:@escaping (_ result: [ClubStore],_ morepages: String ,_ code: Int, _ message: String) -> Void) {
        
        var arrStoreList = [ClubStore]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrStoreList,"0",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let mainData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if mainData.count > 0 {
                            for data in mainData {
                                let id = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                                let club_id = createString(value: (data as AnyObject).object(forKey: "club_id") as AnyObject)
                                let title = createString(value: (data as AnyObject).object(forKey: "title") as AnyObject)
                                let description = createString(value: (data as AnyObject).object(forKey: "description") as AnyObject)
                                let image = createString(value: (data as AnyObject).object(forKey: "image") as AnyObject)
                                let cost = createString(value: (data as AnyObject).object(forKey: "cost") as AnyObject)
                                let created = createString(value: (data as AnyObject).object(forKey: "created") as AnyObject)
                                let modified = createString(value: (data as AnyObject).object(forKey: "modified") as AnyObject)
                                let deleted = createString(value: (data as AnyObject).object(forKey: "deleted") as AnyObject)
                                let category_id = createString(value: (data as AnyObject).object(forKey: "category_id") as AnyObject)
                                let category_name = createString(value: (data as AnyObject).object(forKey: "category_name") as AnyObject)
                                let clubStoreData = ClubStore.init(id: id, club_id: club_id, title: title, desc: description, image: image, cost: cost, created: created, modified: modified, deleted: deleted,category_id : category_id,category_name : category_name)
                                arrStoreList.append(clubStoreData)
                            }
                        }
                        completionHandler(arrStoreList,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrStoreList,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrStoreList,"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrStoreList,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrStoreList,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func addNewClubStore(url :String, parameters : [String : Any],aMethod : String, completionHandler:@escaping ( _ result: ClubStore, _ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(ClubStore(),0,"The network connection was lost.".localized)
            return
        }
        
        var method = HTTPMethod.put
        if aMethod == "POST" {
            method = HTTPMethod.post
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
       
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let id = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                        let club_id = createString(value: (data as AnyObject).object(forKey: "club_id") as AnyObject)
                        let title = createString(value: (data as AnyObject).object(forKey: "title") as AnyObject)
                        let description = createString(value: (data as AnyObject).object(forKey: "description") as AnyObject)
                        let image = createString(value: (data as AnyObject).object(forKey: "image") as AnyObject)
                        let cost = createString(value: (data as AnyObject).object(forKey: "cost") as AnyObject)
                        let created = createString(value: (data as AnyObject).object(forKey: "created") as AnyObject)
                        let modified = createString(value: (data as AnyObject).object(forKey: "modified") as AnyObject)
                        let deleted = createString(value: (data as AnyObject).object(forKey: "deleted") as AnyObject)
                        let category_id = createString(value: (data as AnyObject).object(forKey: "category_id") as AnyObject)
                        let category_name = createString(value: (data as AnyObject).object(forKey: "category_name") as AnyObject)
                        let clubStoreData = ClubStore.init(id: id, club_id: club_id, title: title, desc: description, image: image, cost: cost, created: created, modified: modified, deleted: deleted,category_id : category_id,category_name : category_name)
                        completionHandler(clubStoreData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(ClubStore(),success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(ClubStore(),0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(ClubStore(),0,"Something went wrong. Please try again")
                } else {
                    completionHandler(ClubStore(),0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func getExtraSideMenuItems(url:String, completionHandler:@escaping (_ title: String, _ webdata: [WebPageData],_ code: Int, _ message: String) -> Void) {
        
        var arrWebData = [WebPageData]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("Club Store",arrWebData,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let storeData = creatDictnory(value:JSON.object(forKey: "store_data") as AnyObject)
                        let webPageDictData = creatDictnory(value:JSON.object(forKey: "webpage_data") as AnyObject)
                        let taskListDictData = creatDictnory(value:JSON.object(forKey: "tasklist_data") as AnyObject)
                        let data = creatDictnory(value:storeData.object(forKey: "data") as AnyObject)
                        let clubStoreTitle = createString(value: (data as AnyObject).object(forKey: "title") as AnyObject)
                        let arrWebPageData = creatArray(value:webPageDictData.object(forKey: "data") as AnyObject)
                        let arrTaskListData = creatArray(value:taskListDictData.object(forKey: "data") as AnyObject)

                        if arrWebPageData.count > 0 {
                            for webData in arrWebPageData {
                                let webTitle = createString(value: (webData as AnyObject).object(forKey: "title") as AnyObject)
                                let webAddress = createString(value: (webData as AnyObject).object(forKey: "address") as AnyObject)
                                let webAllData = WebPageData.init(id: "", club_id: "", title: webTitle, addressUrl: webAddress, created: "", deleted: "", status: "", course: "", navigation: "", externalBrowser: "", type: "")
                                arrWebData.append(webAllData)
                            }
                        }
                        
                        if arrTaskListData.count > 0 {
                            for taskListData in arrTaskListData {
                                let taskName = createString(value: (taskListData as AnyObject).object(forKey: "name") as AnyObject)
                                let taskCaption = createString(value: (taskListData as AnyObject).object(forKey: "caption") as AnyObject)
                                
                                let taskListData = WebPageData.init(id: "", club_id: "", title: taskName, addressUrl: taskCaption, created: "taskList", deleted: "", status: "", course: "", navigation: "", externalBrowser: "", type: "") // Add static taskList For filter array in didselect.
                                arrWebData.append(taskListData)
                            }
                        }
                        completionHandler(clubStoreTitle,arrWebData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler("Club Store",arrWebData,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler("Club Store",arrWebData,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler("Club Store",arrWebData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler("Club Store",arrWebData,0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getCategories(url:String, completionHandler:@escaping (_ result: [Categories],_ code: Int, _ message: String) -> Void) {
        
        var arrCatogoriesList = [Categories]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(arrCatogoriesList,0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        for (_, value) in feedData {
                            if let data = value as? [String: Any] {
                                if let dataId = data["id"] as? String {
                                    print(dataId)
                                    let id = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                                    let club_id = createString(value: (data as AnyObject).object(forKey: "club_id") as AnyObject)
                                    let type = createString(value: (data as AnyObject).object(forKey: "type") as AnyObject)
                                    let name = createString(value: (data as AnyObject).object(forKey: "name") as AnyObject)
                                    let created = createString(value: (data as AnyObject).object(forKey: "created") as AnyObject)
                                    let deleted = createString(value: (data as AnyObject).object(forKey: "deleted") as AnyObject)
                                    let categoryData = Categories.init(id: id, club_id: club_id, type: type, name: name, created: created, deleted: deleted)
                                    arrCatogoriesList.append(categoryData)
                                }
                            }
                        }

                        completionHandler(arrCatogoriesList,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(arrCatogoriesList,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(arrCatogoriesList,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(arrCatogoriesList,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(arrCatogoriesList,0,error.localizedDescription)
                }
                break
            }
        })
    }
    
    func addProductCategories(url:String,parameters : [String : Any],aMethod : String , completionHandler:@escaping (_ result: Categories,_ code: Int, _ message: String) -> Void) {
        
        var catogoriesData = Categories()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(catogoriesData,0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        var method = HTTPMethod.put
        if aMethod == "POST" {
            method = HTTPMethod.post
        }
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        let data = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let id = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                        let club_id = createString(value: (data as AnyObject).object(forKey: "club_id") as AnyObject)
                        let type = createString(value: (data as AnyObject).object(forKey: "type") as AnyObject)
                        let name = createString(value: (data as AnyObject).object(forKey: "name") as AnyObject)
                        let created = createString(value: (data as AnyObject).object(forKey: "created") as AnyObject)
                        let deleted = createString(value: (data as AnyObject).object(forKey: "deleted") as AnyObject)
                        catogoriesData = Categories.init(id: id, club_id: club_id, type: type, name: name, created: created, deleted: deleted)
                        completionHandler(catogoriesData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(catogoriesData,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(catogoriesData,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(catogoriesData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(catogoriesData,0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func getAllWebpages(url:String, completionHandler:@escaping (_ result: [WebPageData],_ code: Int, _ message: String) -> Void) {

        var webPageData = [WebPageData]()

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(webPageData,0,"The network connection was lost.".localized)
            return
        }

        self.printAPIData(.requestURL, printData: url as AnyObject)

        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {

                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        //                        let feedData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let feedData = creatArray(value:JSON.object(forKey: "data") as AnyObject)
                        //                        for (_, value) in feedData {
                        //                            if let data = value as? [String: Any] {
                        for data in feedData {
                            //                                if let dataId = data["id"] as? String {
                            //                                    print(dataId)
                            let id = createString(value: (data as AnyObject).object(forKey: "id") as AnyObject)
                            let club_id = createString(value: (data as AnyObject).object(forKey: "club_id") as AnyObject)
                            let title = createString(value: (data as AnyObject).object(forKey: "title") as AnyObject)
                            let addressUrl = createString(value: (data as AnyObject).object(forKey: "address") as AnyObject)
                            let created = createString(value: (data as AnyObject).object(forKey: "created") as AnyObject)
                            let deleted = createString(value: (data as AnyObject).object(forKey: "deleted") as AnyObject)
                            let status = createString(value: (data as AnyObject).object(forKey: "status") as AnyObject)
                            let course = createString(value: (data as AnyObject).object(forKey: "course") as AnyObject)
                            let navigation = createString(value: (data as AnyObject).object(forKey: "navigation") as AnyObject)
                            let externalBrowser = createString(value: (data as AnyObject).object(forKey: "external_browser") as AnyObject)
                            let type = createString(value: (data as AnyObject).object(forKey: "type") as AnyObject)

                            let webData = WebPageData.init(id: id, club_id: club_id, title: title, addressUrl: addressUrl, created: created, deleted: deleted, status: status, course: course, navigation: navigation, externalBrowser: externalBrowser, type: type)
                            webPageData.append(webData)
                        }
                        //                            }
                        //                        }
                        completionHandler(webPageData,200,(JSON.object(forKey: "message") as? String)!)
                    } else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()

                    }else {

                        completionHandler(webPageData,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(webPageData,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(webPageData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(webPageData,0,error.localizedDescription)
                }

                break
            }
        })
    }
        
    func webservice_withdrawMoney(url :String,parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
 
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    //MARK:  API Call For Stripe Get TOken

    func getNewStripeToken(url:String, completionHandler:@escaping (_ token: String ,_ code: Int, _ message: String) -> Void) {
                
        if !(appDelegate.manager?.isReachable)! {
            completionHandler("",0,"The network connection was lost.".localized)
            return
        }
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {

                       let token = createString(value:JSON.object(forKey: "data") as AnyObject)
                       
                        completionHandler(token,200,(JSON.object(forKey: "message") as? String)!)
                    } else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler("",success ,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler("", 0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                //completionHandler(arrClubData,"0",0,error.localizedDescription)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler("",0,"Something went wrong. Please try again")

                } else {
                    completionHandler("",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
//MARK:  API Call For Create Club Points
    func createClubPointsAPICall(url:String,parameters : [String : Any],aMethod : String , completionHandler:@escaping (_ result: Categories,_ code: Int, _ message: String) -> Void) {
        
        var catogoriesData = Categories()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(catogoriesData,0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        self.printAPIData(.requestURL, printData: url as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        var method = HTTPMethod.put
        if aMethod == "POST" {
            method = HTTPMethod.post
        }
        
        request(url, method: method, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        completionHandler(catogoriesData,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(catogoriesData,success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(catogoriesData,0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(catogoriesData,0,"Something went wrong. Please try again")
                } else {
                    completionHandler(catogoriesData,0,error.localizedDescription)
                }
                break
            }
        })
    }

    func getAgoraToken(parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ token: String?) -> Void) {

        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }

        let paraString = getJsonStringFromDictionary(aParameters: parameters)

        self.printAPIData(.requestURL, printData: WebURL.addLiveChatToken as AnyObject)
        self.printAPIData(.requestParamsString, printData: paraString as AnyObject)

        request(WebURL.addLiveChatToken , method: .post, parameters: parameters, encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {

                    self.printAPIData(.requestJsonResponse, printData: JSON as AnyObject)

                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401  {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        //UserDefaults.Main.set(false, forKey: .autoLogin)
                        appDelegate.LogOutDevice()
                    } else {
                        completionHandler(success , JSON.object(forKey: "data") as? String)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                self.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") || error.localizedDescription.contains("serialized") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }

                break
            }
        })
    }
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
}

extension String {
    
    func fileName() -> String {
        if let fileNameWithoutExtension = NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent {
            return fileNameWithoutExtension
        } else {
            return ""
        }
    }
    
    func fileExtension() -> String {
        if let fileExtension = NSURL(fileURLWithPath: self).pathExtension {
            return fileExtension
        } else {
            return ""
        }
    }
}

