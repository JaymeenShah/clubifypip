//
//  WebServicesEvent.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 19/10/21.
//

import UIKit
import Alamofire
import DataCache

class WebServicesEvent: NSObject {
   
    static let sharedInstanceEvent : WebServicesEvent = {
        let instance = WebServicesEvent()
        return instance
    }()
    
    func webservice_createClubEvent(url :String, parameters : [String : Any],imageData : Data, completionHandler:@escaping (_ resultData : ClubEvent, _ code: Int, _ message: String) -> Void) {
        
        var result = ClubEvent()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(result,0,"The network connection was lost.".localized)
            return
        }
        
        WebServices.sharedInstance.printAPIData(.requestURL, printData: url as AnyObject)
        
        let boundary = "Boundary-\(UUID().uuidString)"
        
        let URL = try! URLRequest(url: url ,method: .post, headers: ["Content-Type" :"multipart/form-data; boundary=\(boundary)","X-authorization" : getUserAuthToken(), "focus_club_id": getWhiteLabelAppId()])
        
        upload( multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if key == "membership_level" {
                    if value is [String] {
                        var bValue: [String] = []
                        bValue = (value as? [String])!
                        for index in 0..<bValue.count {
                            var strVal: String = ""
                            strVal = bValue[index]
                            multipartFormData.append(strVal.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: "membership_level[]")
                        }
                    }
                } else {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
            multipartFormData.append(imageData, withName:"icon", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpg")
        },with: URL,
          encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    WebServices.sharedInstance.printAPIData(.uploadingProgress, printData: Progress.fractionCompleted as AnyObject)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        if let JSON:NSDictionary = response.result.value as? NSDictionary {
                            
                            WebServices.sharedInstance.printAPIData(.requestResponse, printData: JSON as AnyObject)
                            
                            let success = JSON.object(forKey: "status") as! Int
                            if success == 200 {
                                let eventData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                                result = ClubEvent.init(dictJSON: eventData)
                                completionHandler(result,200,(JSON.object(forKey: "message") as? String)!)
                            }else if success == 401 || success == 400 {
                                AppState.sharedInstance.joinClubLogIn = "0"
                                
                                appDelegate.LogOutDevice()
                            }else {
                                completionHandler(result,success ,(JSON.object(forKey: "message") as? String)!)
                            }
                        }else {
                            completionHandler(result,0,"No Data Found".localized)
                        }
                        break
                    case .failure(let error):
                        WebServices.sharedInstance.printAPIData(.httpRequestError, printData: error as AnyObject)
                        if error.localizedDescription.contains("JSON") {
                            completionHandler(result,0,"Something went wrong. Please try again")
                        } else {
                            completionHandler(result,0,error.localizedDescription)
                        }

                        break
                    }
                }
            case .failure(let encodingError):
                WebServices.sharedInstance.printAPIData(.httpRequestError, printData: encodingError as AnyObject)
                break
            }
        })
    }
    
    func getCalenderEventList(url:String, completionHandler:@escaping (_ result: [ClubEvent],_ morepages: String ,_ code: Int, _ message: String) -> Void) {
        
        var result = [ClubEvent]()
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(result,"0",0,"The network connection was lost.".localized)
            return
        }
        
        WebServices.sharedInstance.printAPIData(.requestURL, printData: url as AnyObject)
        
        request(url, method: .get, parameters: [:], headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    WebServices.sharedInstance.printAPIData(.requestResponse, printData: JSON as AnyObject)
                    let responseDict = response.result.value as? [String: Any]
                    //Cache
                    let keyForCache = url.contains("upcoming") ? "FutureEventList" : "EventList"
                    let dataValue: Data = NSKeyedArchiver.archivedData(withRootObject: responseDict)
                    DataCache.instance.write(data: dataValue, forKey: keyForCache)

                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 200 {
                        //let eventData = creatDictnory(value:JSON.object(forKey: "data") as AnyObject)
                        let morePages = createString(value:JSON.object(forKey: "more_pages") as AnyObject)
                        
                        if let response = response.result.value as? [String: Any] {
                            if let data = response["data"] as? [[String: Any]] {
                                for prodDetail in data {
                                    let data = creatDictnory(value:prodDetail as AnyObject)
                                    result.append(ClubEvent.init(dictJSON: data))
                                }
                            }
                        }
                        
                        print(result)
//                        if result.count > 0 {
//                            result.sort(by: { Int($0.eventId)! < Int($1.eventId)! })
//                            result.sort(by: { Int($0.eventId)! > Int($1.eventId)! })
//                        }
                        completionHandler(result,morePages,200,(JSON.object(forKey: "message") as? String)!)
                    }else if success == 401 || success == 400 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        
                        appDelegate.LogOutDevice()
                    }else {
                        completionHandler(result,"0",success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(result,"0",0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                WebServices.sharedInstance.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") {
                    completionHandler(result,"0",0,"Something went wrong. Please try again")
                } else {
                    completionHandler(result,"0",0,error.localizedDescription)
                }

                break
            }
        })
    }
    
    func webservice_joinClubEvent(url :String,parameters : [String : Any], completionHandler:@escaping (_ code: Int, _ message: String) -> Void) {
        
        if !(appDelegate.manager?.isReachable)! {
            completionHandler(0,"The network connection was lost.".localized)
            return
        }
        
        let paraString = getJsonStringFromDictionary(aParameters: parameters)
        
        WebServices.sharedInstance.printAPIData(.requestURL, printData: url as AnyObject)
        WebServices.sharedInstance.printAPIData(.requestParamsString, printData: paraString as AnyObject)
        
        request(url, method: .post, parameters: [:],encoding: paraString!, headers: setHeadersForAPI()).responseJSON(completionHandler: { (response) in

            switch response.result {
            case .success:
                if let JSON:NSDictionary = response.result.value as? NSDictionary {
                    
                    WebServices.sharedInstance.printAPIData(.requestResponse, printData: JSON as AnyObject)
                    
                    let success = JSON.object(forKey: "status") as! Int
                    if success == 401 {
                        AppState.sharedInstance.joinClubLogIn = "0"
                        
                        appDelegate.LogOutDevice()
                    }else {
                    completionHandler(success,(JSON.object(forKey: "message") as? String)!)
                    }
                }else {
                    completionHandler(0,"No Data Found".localized)
                }
                break
            case .failure(let error):
                WebServices.sharedInstance.printAPIData(.httpRequestError, printData: error as AnyObject)
                if error.localizedDescription.contains("JSON") {
                    completionHandler(0,"Something went wrong. Please try again")
                } else {
                    completionHandler(0,error.localizedDescription)
                }
                break
            }
        })
    }
}

