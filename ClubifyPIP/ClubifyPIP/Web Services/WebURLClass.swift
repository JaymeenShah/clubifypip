//
//  WebURLClass.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
//MARK: - Web Services Constant
public class WebURLClass : NSObject {
    
    public var login:String = mailBaseURL + "/users/login"
    public var loginUserInfo:String = mailBaseURL + "/user/info"
    public var userCreate:String = mailBaseURL + "/user/create"
    public var userEditProfile:String = mailBaseURL + "/user/update"
    public var userProfileUpload:String = mailBaseURL + "/file/user/upload"
    public var galleryFileUpload = mailBaseURL + "/files/gallery/upload/"
    public var galleryFileDetails = mailBaseURL + "/files/gallery/create/"
    public var galleryMediaFiles = mailBaseURL + "/files/gallery/"
    public var galleryMediaUpdate = mailBaseURL + "/files/gallery/update/"
    public var galleryMediaDelete = mailBaseURL + "/files/gallery/deactivate/"
    public var purchaseGallery = mailBaseURL + "/user/purchase/gallery/" //Change:
    
    //V2
//    public var purchaseGallery_V2 = mailBaseURL + "/user/v2/purchase/gallery/" //Change:
    public var purchaseGallery_V2 = mailBaseURL + "/user/v3/purchase/gallery/" //Change:

    public var getAllclubFeeds = mailBaseURL + "/user/feeds/"
    public var addClubFeedMedia = mailBaseURL + "/file/feed/upload/"
    public var addClubFeedCreate = mailBaseURL + "/user/feed/create"
    public var getMyMemberClubs = mailBaseURL + "/clubs/memberof/"
    public var getMyOwnerClubs = mailBaseURL + "/clubs/ownerof/"
    public var getDiscoverClubs = mailBaseURL + "/clubs/public/"
    public var JoinedClub = mailBaseURL + "/clubs/join/"
    public var createClub = mailBaseURL + "/clubs/create"
    public var UploadInCreateClub = mailBaseURL + "/file/club/upload/"
    public var UpdateClub = mailBaseURL + "/clubs/"
    public var UpdateClubFeed = mailBaseURL + "/user/feed/update/"
    public var deleteClubFeed = mailBaseURL + "/user/feed/deactivate/"
    public var leaveClub = mailBaseURL + "/club/leave/"
    public var deleteClub = mailBaseURL + "/club/deactivate/"
    public var createFolder = mailBaseURL + "/gallery/create/"
    public var getAllFolder = mailBaseURL + "/gallery/"
    public var deleteFolder = mailBaseURL + "/gallery/deactivate/"
    public var UpdateFolder = mailBaseURL + "/gallery/update/"
    public var getFolderMedias = mailBaseURL + "/resources/"
    public var inviteMembers = mailBaseURL + "/clubs/invite/request"
    public var clubMembers = mailBaseURL + "/club/members/"
    //Get Club Members for Voicemail
    public var getClubMembersForVoicemail = mailBaseURL + "/get/club/members/"
    
    public var getAllPowerUps = mailBaseURL + "/additionalpages/"
    public var getAllPowerUpList = mailBaseURL + "/clubs/powerups/"
    public var createClubPowerUP = mailBaseURL + "/additionalpage/create/"
    public var updateClubPowerUP = mailBaseURL + "/additionalpage/update/"
    public var deleteClubPowerUP = mailBaseURL + "/additionalpages/deactivate/"
    public var getTaskListClubPowerUp = mailBaseURL + "/additionalpages/task/"
    public var createTaskClubPowerUp = mailBaseURL + "/additionalpage/task/create/"
    public var getProductListClubPowerUp = mailBaseURL + "/additionalpages/product/"
    public var createProductListClubPowerUp = mailBaseURL + "/additionalpage/product/create/"
    public var deleteProductListClubPowerUp = mailBaseURL + "/additionalpages/product/deactivate/"
    public var updateProductListClubPowerUp = mailBaseURL + "/additionalpage/product/update/"
    public var finishClubPowerUpTaskList = mailBaseURL + "/additionalpages/task/finish/"
    public var unFinishClubPowerUpTaskList = mailBaseURL + "/additionalpages/task/unfinish/"
    public var updatePushAuth = mailBaseURL + "/notification/push/key"
    public var getClubAllPlans = mailBaseURL + "/clubplan/clubplans/"
    public var createClubAllPlans = mailBaseURL + "/clubplan/create"
    public var deleteClubPlan = mailBaseURL + "/clubplan/deactivate/"
    public var updateClubPlan = mailBaseURL + "/clubplan/"
    public var joinClubPlan = mailBaseURL + "/clubplan/join/"
    public var updateAuthority = mailBaseURL + "/clubplan/authority/"
    public var updateScreenpermissions = mailBaseURL + "/clubplan/screenpermissions/"
    public var uploadImageOnPlan = mailBaseURL + "/file/plan/upload/"
    public var uploadProfileImageOnPlan = mailBaseURL + "/file/plan/profile_icon/upload/"
    public var IAPReceiptVerification = mailBaseURL + "/monetization/ios/verify"
    public var addCoinsToAccount = mailBaseURL + "/monetization/tokens/add"
    public var broadCastNotification = mailBaseURL + "notification/push/broadcast"
    public var getWalletDetails = mailBaseURL + "/monetization/wallet"
    public var getWalletTransactions = mailBaseURL + "/monetization/transactions/"
    public var buyIndividualItems = mailBaseURL + "/user/buy/gallery_resource"
    public var checkItemPurchase = mailBaseURL + "/user/purchase/check/"
   
    public var upgradeClubPlan = mailBaseURL + "/clubplan/upgrade/"
    public var getInvitesList = mailBaseURL + "/club/inviterequests/"
    public var inviteRequestReply = mailBaseURL + "/clubs/inviterequest/reply/"
    public var getClubDetails = mailBaseURL + "/club/"
    public var getSentInvitedList = mailBaseURL + "/club/allinvitationrequests/"
    public var getInvitesClubs = mailBaseURL + "/clubs/invites"
    public var inviteReply = mailBaseURL + "/clubs/invite/reply"
    public var buyPowerUp = mailBaseURL + "/clubs/buy/powerups"
    public var getPowerUpWebSetting = mailBaseURL + "/clubs/powerup_webpage/"
    public var getPowerUpTaskSetting = mailBaseURL + "/clubs/powerup_tasklist/"
    public var getPowerUpStoreSetting = mailBaseURL + "/clubs/powerup_store/"
    public var AddPowerUpTaskList = mailBaseURL + "/clubs/add/powerup_tasklist"
    public var AddPowerUpWeb = mailBaseURL + "/clubs/add/powerup_webpage"
    public var AddPowerUpClubStore = mailBaseURL + "/clubs/add/powerup_store"
    public var UpdatePowerUpTaskList = mailBaseURL + "/clubs/update/powerup_tasklist"
    public var UpdatePowerUpWeb = mailBaseURL + "/clubs/update/powerup_webpage"
    public var UpdatePowerUpClubStore = mailBaseURL + "/clubs/update/powerup_store"
    public var getListOfStoreProduct = mailBaseURL + "/storeproducts/"
    public var addNewClubStore = mailBaseURL + "/storeproducts/create"
    public var updateClubStore = mailBaseURL + "/storeproducts/update"
    public var deleteClubStore = mailBaseURL + "/storeproduct/delete/"
    public var getExtraSideMenu = mailBaseURL + "/additional_sidemenu/"
    public var getProductCategory = mailBaseURL + "/storeproduct/categories/"
    public var addProductCategory = mailBaseURL + "/storeproducts/categories/create"
    public var updateProductCategory = mailBaseURL + "/storeproducts/categories/update"
    public var deleteProductCategory = mailBaseURL + "/storeproduct/categories/delete/"
    public var getClubCategory = mailBaseURL + "/clubs/categories"
    public var getPowerUpWebPage = mailBaseURL + "/clubs/powerup_webpage/"
    public var deleteWebpage = mailBaseURL + "/clubs/delete/powerup_webpage/"
    public var disablePowerUp = mailBaseURL + "/clubs/disable/powerups"
    public var createEvent = mailBaseURL + "/clubevent/create"
    public var deleteEvent = mailBaseURL + "/clubevent/delete/"
    public var updateEvent = mailBaseURL + "/clubevent/update/"
    public var eventListing = mailBaseURL + "/clubevent/clubevents/"
    public var withdrawCoins = mailBaseURL + "/withdraw_coins_request"
    public var joinClubEvent = mailBaseURL + "/clubevent/join"
    public var redeemCodeSend = mailBaseURL + "/referral/apply"
    public var clearNotiCount = mailBaseURL + "/notification/clear_count"
    public var mutePost = mailBaseURL + "/user/feed/markasmute"
    public var reportPost = mailBaseURL + "/user/feed/report"
    public var likeUnlikePost = mailBaseURL + "/user/feed/markaslike"
    public var blockMember = mailBaseURL + "/club/memberstatus/"
    public var requestAccess = mailBaseURL + "/club/requestaccess/"
    public var logOut = mailBaseURL + "/users/logout/1"
    public var getSideMenu = mailBaseURL + "/additional_sidemenu/"
    public var updateSideMenu = mailBaseURL + "/clubs/update/side_menu"
    public var reorderSideMenu = mailBaseURL + "/clubs/side_menu/reorder"
    public var forgotPassword = mailBaseURL + "/users/recoverpassword"
    public var syncContacts = mailBaseURL + "/clubs/invite/synccontacts"
     
    //Comments and Reply calls
    public var commentList = mailBaseURL + "/user/feedcomment/"
    public var deleteComment = mailBaseURL + "/user/feedcomment/delete"
    public var editComment = mailBaseURL + "/user/feedcomment/edit"
    public var addReply = mailBaseURL + "/user/feedcommentReply/create"
    public var editReply = mailBaseURL + "/user/feedcommentReply/edit"
    public var deleteReply = mailBaseURL + "/user/feedcommentReply/delete"
    public var addComment = mailBaseURL + "/user/feedcomment/create"
    
    //Default Startup Page
    public var addUpdateStartupPage = mailBaseURL + "/default_startup_screen"
    public var getStartupPage = mailBaseURL + "/additional_sidemenu/"
    
    //Add Product/Service
    public var setProductService = mailBaseURL + "/set_product_package"
    public var productServiceFileUpload = mailBaseURL + "/file/product/upload/"
    public var addProductDetails = mailBaseURL + "/add_product_details"
    public var getProductDetails = mailBaseURL + "/get_product_details/"
    public var updateProductDetails = mailBaseURL + "/update_product_details"
    public var deleteProductDetails = mailBaseURL + "/delete_product_details/"
    
    //Create Event
    public var eventFileUpload = mailBaseURL + "/file/upload"
    
    //Update Feed
    public var updateClubFeedCreate = mailBaseURL + "/user/feed/update/"
    
    //WebPage listing
    public var powerupWebpageListing = mailBaseURL + "/clubs/powerup_webpage/"
    
    //Product Package listing
    public var powerupProductListing = mailBaseURL + "/clubs/product_packages/"
    
    //Refer a friend listing.
    
    public var referFriendListing = mailBaseURL + "/clubs/invite_friend_screen/"
    
    public var commonListing = mailBaseURL + "/"
    
    //CallToAction Button API for Product
    public var productPackageBuyNow = mailBaseURL + "/product_packages/buy_now/"
    
    //V2
    public var productPackageBuyNow_V2 = mailBaseURL + "/product_packages/v2/buy_now/"

    //New Discover Club API call
    public var getNewDiscoverClub = mailBaseURL + "/clubs/category"
    
    //New Club List API call - White labelled app
    public var getMulticlub = mailBaseURL + "/clubs/multiclub/"
    
    //Get Audio Message list - Team Loop - White labelled app/Voicemail
    //    public var getAudioMessage = mailBaseURL + "/getAudioMsgsV2/"
    //    public var uploadSendAudioMessage = mailBaseURL + "/sendAudioMsg/upload/"
    
    public var getAudioMessage = mailBaseURL + "/getAudioMsgsV3/"
    public var uploadSendAudioMessage = mailBaseURL + "/sendAudioMsgV3/upload/"
    
    public var forwardAudioMessage = mailBaseURL + "/forward/audio/message"
    public var deleteAudioMessage = mailBaseURL + "/deleteAudioMessage/"
    
    //Delete Sidebar Menu items
    public var deleteSidebarMenu = mailBaseURL + "/delete/sidemenu/"
    
    //Contest listing - Giveaway screen
    public var getContests = mailBaseURL + "/club/contest/"
    
    //Create Contest Upload
    public var contestUpload = mailBaseURL + "/club/contest/upload"
    
    //API:- /club/contest/create/
    public var contestCreate = mailBaseURL + "/club/contest/create"
    
    //API:- club/contest/update/(contest_id)
    public var contestUpdate = mailBaseURL + "/club/contest/update/"
    
    //API:- /club/points/create
    public var pointsCreate = mailBaseURL + "/club/points/create"
    
    //API:- /club/points/details/80
    public var fetchPointsUpdate = mailBaseURL + "/club/points/details/"
    
    //API:- clubcontest/join
    public var joinClubContest = mailBaseURL + "/clubcontest/join"

    //API:- /get/user/points/(club_Id)
    public var getUserPoints = mailBaseURL + "/get/user/points/"
    
    //API:- /clubs/update/invite_refer_screen
    public var updateInviteReferScreen = mailBaseURL + "/clubs/update/invite_refer_screen"

    //API:- /club/watch
    public var watchScreen = mailBaseURL + "/club/watch"

    //API:- refereFriend
    public var refereFriend = mailBaseURL + "/clubs/add/refere_friend"
    
    //API:- addDynamicCourse
    public var addDynamicCourse = mailBaseURL + "/add/dynamic/course"

    //API:- addDynamicCourse
    public var dynamicCourseListing = mailBaseURL + "/clubs/dynamic_course/"

    //API:- addDynamicCourse
    public var  createFirebaseLogin = mailBaseURL + "/firebase/create/loginuser"

    //API:- addDynamicCourse
    public var initializeFirebaseChat = mailBaseURL + "/firebase/chat/initilize"
    
    //API:- getMembersForChat
    public var getMembersForChat = mailBaseURL + "/get/chat/club/members/"

    //API:- getMembersForLeaderboard
    public var getMembersForLeaderboard = mailBaseURL + "/get/members/leader/board/"
    
    //API:- getMembersForLeaderboard
    public var getPointsDetailsForLeaderboard = mailBaseURL + "/leader/board/points/details/"

    //API:- createChatRoom
    public var createChatRoom = mailBaseURL + "/predefined/chat/create"
    
    //API:- get ChatRoom list
    public var getChatRooms = mailBaseURL + "/predefined/chat/"

    //API:- updateChatRoom
    public var updateChatRoom = mailBaseURL + "/predefined/chat/update"
    
    //API:- deleteChatRoom
    public var deleteChatRoom = mailBaseURL + "/predefined/chat/delete/"//(/:clubId)(/:predefined_chat_id)"

    //API:- deleteChatRoom
    public var getClubPlansWithMembers = mailBaseURL + "/predefined/clubplans/"
    
    //API:- Get Products Categories in Product/Service
    public var getProductServiceCategories = mailBaseURL + "/product/load/categories/"
    
    //API:- Join Chat Room
    public var joinChatRoom = mailBaseURL + "/join/predefined_chat/"
    
    //API:- Get User Feed Details
    public var getUserFeedDetails = mailBaseURL + "/get/user/feed/"

    //API:- Mark Comment Like
    public var markCommentLike = mailBaseURL + "/user/comment/markaslike"

    //API:- Mark Reply Like
    public var markReplyLike = mailBaseURL + "/user/reply/markaslike"

    //API:- Track Product Clicks
    public var trackProductClicks = mailBaseURL + "/update/product/clicks"
    
    //API:- Track Gallery Items Views
    public var viewGalleryItems = mailBaseURL + "/update/gallery/item/views"
    
    
    //API:- Fetch the new stripe token
    public var getTokenStripe = mailBaseURL + "/user/user_request/new_request"

    //API:- Fetch the SMS Listing
    public var getSMSList = mailBaseURL + "/get/club/sms/"

    //API:- Fetch the SMS Detail
    public var getSMSDetails = mailBaseURL + "/club/sms/details"

    //API:- Send the new SMS
    public var sendSMS = mailBaseURL + "/club/send/sms"

    //API:- Send the new SMS
    public var gainPointForSharingFeed = mailBaseURL + "/gain/points/share/feed"
    
    //API:- Fetching video list from YouTube URL
    public var getYouTubeVideo = mailBaseURL + "/get/youtube/video/"
    
    //API:- Saving the sort setting as per Folder wise
    public var saveSortingSettings = mailBaseURL + "/update/folder/sorting/settings"

    //API:- Generate Agora Live token
    public var addLiveChatToken = mailBaseURL + "/get/live_chat_token"
    
    //API:- Send broadcast SMS
    public var broadcastSMS = mailBaseURL + "/club/broadcast/sms"

    //API:- Follow User
    public var followUser = mailBaseURL + "/follow/user"

    //API:- get Followers Feed
    public var getFollowersFeed = mailBaseURL + "/get/followers/feed/"
     
    //API:- get Member Profile Gallery
    public var galleryMemberList = mailBaseURL + "/member/files/gallery/"
    
    //API:- combine All Media items and Folders inside course
    public var combineGalleryMediaFilesFolder = mailBaseURL + "/resources/files/gallery/"
    
    //API:- to fetch the gallery details once the share link is pressed inside the app
    public var shareableFilesGallery = mailBaseURL + "/shareable/files/gallery"

    //API:- to upload file details to server
    public var fileIntentToUpload = mailBaseURL + "/file/intent_to_upload"
    
    //API:- to send file upload status
    public var fileUploadStatus = mailBaseURL + "/file/upload/update_file_status"
    
    //API:- Fetch liked user list
    public var getlikeUserList = mailBaseURL + "/feed/like/users/"
    
    //API:- Fetch Followers list
    public var getFollowerList = mailBaseURL + "/user/followers/"
    
    //API:- notification Activities
    public var getNotificationList = mailBaseURL + "/notification/activities/"
    
    //API:- Delete SMS
    public var deleteSMS = mailBaseURL + "/club/twilio/sms/delete"
    
    //API:- Read/Unread Notification
    public var readUnreadNotification = mailBaseURL + "/notification/"
    
    //API:-  Upcoming/Future Club Events
    public var  upcomingClubevents = mailBaseURL + "/upcoming/clubevents/"
    
    //API:-  Reorder Gallery Resource
    public var  reorderGalleryResource = mailBaseURL + "/resources/files/gallery/reorder"
}
