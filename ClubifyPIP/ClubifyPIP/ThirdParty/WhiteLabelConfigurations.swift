//
//  WhiteLabelConfigurations.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/10/21.
//


import Foundation

enum appConstantType {
    case oneCoin
    case tenCoins
    case thirtyCoins
    case hundredCoins
    case twoHundredCoins
    case threeHundredCoins
    case fiveHundredCoins
    case twelveHundredCoins
    
    //iLead update
    case fourCoin
    case twelveCoins
    case hundredandthreeCoins
    case hundredandNintyNineCoins
    case twoHundredEightyFiveCoins
    case fourHundredCoins
    case eightHundredCoins
    //
    case whiteLblAppId
    case whiteLblAppUserId
    case whiteLblAppName
    case appGoogleApiKey
    case fireBaseGoogleServiceFileName
    case googleClientId
    case dropBoxId
    case iTunesLink
    case inAppPurchaseAppName
    case useIAP
}

enum appBundleIds : String {
    //TODO: If you change any bundle identifire than please change below data also
    case clubify = "com.mobilithink.clubify"
    case sorinClub = "com.mobilithink.sorinClub"
    case cccClub = "com.mobilithink.CCCClub"
    case volleyBall = "com.mobilithink.volleyball101club"
    case actioneer = "com.mobilithink.actioneer"
    case deenSquadClub = "com.mobilithink.deensquadclub"
    case newToCanadaClub = "com.mobilithink.newtocanada"
    case ictForum = "com.mobilithink.ICTForum"
    case cyberExperts = "com.mobilithink.CyberExpert"
    case adultify = "com.mobilithink.adultify"
    case ptsd = "com.mobilithink.ptsd"
    case ilead = "com.mobilithink.ilead"
    case yourtechcoach = "com.mobilithink.yourtechcoach"
    case myhotperks = "com.mobilithink.myhotperks"
    case iag = "com.mobilithink.iag"
    case teamloop = "com.mobilithink.teamloop"
    case virtualhealthcoach = "com.mobilithink.virtualhealthcoach"
    case vuca = "com.mobilithink.vucacoach"
    case socialarchitect = "com.mobilithink.socialarchitect"
    case idealwealthgrower = "com.mobilithink.idealwealthgrower"
    case pharmakonect = "com.mobilithink.pharmakonect"
    case nbda = "com.mobilithink.nbda"
    case inspiredmoms = "com.mobilithink.inspiredmoms"
    case peersondemand = "com.mobilithink.peersondemand"
    case sportsmasters = "com.mobilithink.sportsmasters"
    case engineeryourmission = "com.mobilithink.engineeryourmission"
    case bossibly = "com.mobilithink.bossibly"
    case caba = "com.mobilithink.caba"
    case suttongroup = "com.mobilithink.suttongroup"
    case nlcapitalgroup = "com.mobilithink.nlcapitalgroup"
    case ideserveitnow = "com.mobilithink.ideserveitnow"
    case urbanfitmag = "com.mobilithink.urbanfitmag"
    case risebreakfree = "com.mobilithink.risebreakfree"
    case rodneylavoie = "com.mobilithink.rodneylavoie"
    case podcommander = "com.mobilithink.podcommander"
    case workishell = "com.mobilithink.workishell"
    case homicity = "com.mobilithink.homicity"
    case crittercarebrothers = "com.mobilithink.crittercarebrothers"
    case yashcollagen = "com.mobilithink.yascollagen"
    case resetwithmiranda = "com.mobilithink.resetwithmiranda"
    case everythingtennis = "com.mobilithink.everythingtennis"
    case creatorsuncensored = "com.mobilithink.creatorsuncensored"

}

//Clubify Constants

var One_Coins = getConstantValue(.oneCoin)
var Ten_Coins = getConstantValue(.tenCoins)
var Thirty_Coins = getConstantValue(.thirtyCoins)
var Hundreds_Coins = getConstantValue(.hundredCoins)
var TwoHundreds_Coins = getConstantValue(.twoHundredCoins)
var ThreeHundreds_Coins = getConstantValue(.threeHundredCoins)
var FiveHundreds_Coins = getConstantValue(.fiveHundredCoins)
var TwelveHundreds_Coins = getConstantValue(.twelveHundredCoins)

//iLead app updates
var four_Coins = getConstantValue(.fourCoin)
var Twelve_Coins = getConstantValue(.twelveCoins)
var Hundredandthree_Coins = getConstantValue(.hundredandthreeCoins)
var HundredandNintyNine_Coins = getConstantValue(.hundredandNintyNineCoins)
var TwoHundredEightyFiveCoins_Coins = getConstantValue(.twoHundredEightyFiveCoins)
var FourHundredCoins_Coins = getConstantValue(.fourHundredCoins)
var EightHundreds_Coins = getConstantValue(.eightHundredCoins)
/////

var WHITELABEL_APP_ID = getConstantValue(.whiteLblAppId)
var WHITELABEL_APP_USER_ID = getConstantValue(.whiteLblAppUserId)
var WHITELABEL_APP_NAME = getConstantValue(.whiteLblAppName)
var appGoogleApiKey = getConstantValue(.appGoogleApiKey)
var FIRE_BASE_GOOGLE_SERVICE_FILE_NAME = getConstantValue(.fireBaseGoogleServiceFileName)
var GOOGLE_CLIENT_ID = getConstantValue(.googleClientId)
var DROPBOX_ID = getConstantValue(.dropBoxId)
var ITUNES_LINK = getConstantValue(.iTunesLink)
var IN_APPPURCHASE_APP_NAME = getConstantValue(.inAppPurchaseAppName)

//-------------------------------------------------------------------------

func getConstantValue(_ constantType : appConstantType) -> String {
    
    let bundleId = Bundle.main.bundleIdentifier
    
    switch bundleId {
    case appBundleIds.clubify.rawValue:
        switch constantType {
        case .oneCoin: return "com.clubify.C1Coin"
        case .tenCoins: return "com.clubify.C10Coins"
        case .thirtyCoins: return "com.clubify.C30Coins"
        case .hundredCoins: return "com.clubify.C100Coins"
        case .twoHundredCoins: return "com.clubify.C200Coin"
        case .threeHundredCoins: return "com.clubify.C300Coins"
        case .fiveHundredCoins: return "com.clubify.C500Coins"
        case .twelveHundredCoins: return "com.clubify.C1200Coins"
        case .whiteLblAppId: return "" //i.e. Club Id
        case .whiteLblAppUserId: return "" // i.e. Club User Id
        case .whiteLblAppName: return ""
        case .appGoogleApiKey: return "AIzaSyBTSPryanCCBmxJKLmxALGi2Ro0eN0j0hg"
        case .fireBaseGoogleServiceFileName: return "GoogleService-Info"
        case .googleClientId: return "173949952092-vci453glmsl421gom63i6blgv0grkln7.apps.googleusercontent.com"
        case .dropBoxId: return "osjw0c421es3agl"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id882297946"
        case .inAppPurchaseAppName: return "Clubify"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.sorinClub.rawValue:
        switch constantType {
        case .oneCoin: return "com.sorins.C1Coin"
        case .tenCoins: return "com.sorins.C10Coins"
        case .thirtyCoins: return "com.sorins.C30Coins"
        case .hundredCoins: return "com.sorins.C100Coins"
        case .twoHundredCoins: return "com.sorins.C200Coins"
        case .threeHundredCoins: return "com.sorins.C300Coins"
        case .fiveHundredCoins: return "com.sorins.C500Coins"
        case .twelveHundredCoins: return "com.sorins.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "9" : "18") //i.e. Club Id prod "9" and dev  "18"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "28" : "36") // i.e. Club User Id prod "28" and dev  "36"
        case .whiteLblAppName: return "Sorins Real Estate Club!"
        case .appGoogleApiKey: return "AIzaSyBNr1Dgn8jo7JYZcoXjXmUemPews7l1PAQ"
        case .fireBaseGoogleServiceFileName: return "GoogleService-SorinClub-Info"
        case .googleClientId: return "240075460028-tir89oip8q1ic5agavq7egc51q35ib48.apps.googleusercontent.com"
        case .dropBoxId: return "s9ko3zciulebtx1"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1403360074"
        case .inAppPurchaseAppName: return "SorinClub"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.cccClub.rawValue:
        switch constantType {
        case .oneCoin: return "com.ccc.C1Coin"
        case .tenCoins: return "com.ccc.C10Coins"
        case .thirtyCoins: return "com.ccc.C30Coins"
        case .hundredCoins: return "com.ccc.C100Coins"
        case .twoHundredCoins: return "com.ccc.C200Coins"
        case .threeHundredCoins: return "com.ccc.C300Coins"
        case .fiveHundredCoins: return "com.ccc.C500Coins"
        case .twelveHundredCoins: return "com.ccc.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "10" : "20") //i.e. Club Id prod "10" and dev  "20"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "30" : "40") // i.e. Club User Id prod "30" and dev  "40"
        case .whiteLblAppName: return "CCC!"
        case .appGoogleApiKey: return "AIzaSyBR1QErHu9ST3D4toznO6NEO10tiS5atX0"
        case .fireBaseGoogleServiceFileName: return "GoogleService-CCC!-Info"
        case .googleClientId: return "615227395573-mds68d7am4v9oaq437qbcv91r5mi9dcq.apps.googleusercontent.com"
        case .dropBoxId: return "69gd6ci3c49xyot"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1417013384"
        case .inAppPurchaseAppName: return "CCC"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.volleyBall.rawValue:
        switch constantType {
        case .oneCoin: return "com.vollyball101.C1Coin"
        case .tenCoins: return "com.vollyball101.C10Coins"
        case .thirtyCoins: return "com.vollyball101.C30Coins"
        case .hundredCoins: return "com.vollyball101.C100Coins"
        case .twoHundredCoins: return "com.vollyball101.C200Coins"
        case .threeHundredCoins: return "com.vollyball101.C300Coins"
        case .fiveHundredCoins: return "com.vollyball101.C500Coins"
        case .twelveHundredCoins: return "com.vollyball101.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "24" : "1") //i.e. Club Id prod "24" and dev  "1"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "61" : "3") // i.e. Club User Id prod "61" and dev  "3"
        case .whiteLblAppName: return "VolleyBall 101"
        case .appGoogleApiKey: return "AIzaSyAGGgIAdta4Xx3Uzc-JfiAOJk7_K8gT6fI"
        case .fireBaseGoogleServiceFileName: return "GoogleService-VolleyBall101-Info"
        case .googleClientId: return "379756817435-maod472hpa37jvhev52qn36r3fon05n2.apps.googleusercontent.com"
        case .dropBoxId: return "m1b5u08tvsnu2ia"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1359120151"
        case .inAppPurchaseAppName: return "Volleyball"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.actioneer.rawValue:
        switch constantType {
        case .oneCoin: return "com.actioneer.C1Coin"
        case .tenCoins: return "com.actioneer.C10Coins"
        case .thirtyCoins: return "com.actioneer.C30Coins"
        case .hundredCoins: return "com.actioneer.C100Coins"
        case .twoHundredCoins: return "com.actioneer.C200Coins"
        case .threeHundredCoins: return "com.actioneer.C300Coins"
        case .fiveHundredCoins: return "com.actioneer.C500Coins"
        case .twelveHundredCoins: return "com.actioneer.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "5" : "7") //i.e. Club Id prod "5" and dev  "7"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "12" : "6") // i.e. Club User Id prod "12" and dev  "6"
        case .whiteLblAppName: return "Be an Actioneer!"
        case .appGoogleApiKey: return "AIzaSyCRkKnSLcCCpRy1J5Z67zxWI00RJS6TfUY"
        case .fireBaseGoogleServiceFileName: return "GoogleService-BeAnActioneer-Info"
        case .googleClientId: return "361270969413-gdi2ih0qed0snto03q0uurqfljbivs14.apps.googleusercontent.com"
        case .dropBoxId: return "0kjoxcx52oqi9br"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1388208601"
        case .inAppPurchaseAppName: return "Actioneer"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.deenSquadClub.rawValue:
        switch constantType {
        case .oneCoin: return "com.deensquadclub.C1Coin"
        case .tenCoins: return "com.deensquadclub.C10Coins"
        case .thirtyCoins: return "com.deensquadclub.C30Coins"
        case .hundredCoins: return "com.deensquadclub.C100Coins"
        case .twoHundredCoins: return "com.deensquadclub.C200Coins"
        case .threeHundredCoins: return "com.deensquadclub.C300Coins"
        case .fiveHundredCoins: return "com.deensquadclub.C500Coins"
        case .twelveHundredCoins: return "com.deensquadclub.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "3" : "8") //i.e. Club Id For Dev = "8" And Prod = "3"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "9" : "16") // i.e. Club User Id For Dev = "16"  And Prod = "9"
        case .whiteLblAppName: return "Deen Squad Club"
        case .appGoogleApiKey: return "AIzaSyCPOBx-izDkuYT7xcJoRvMcB6QF8-MHksc"
        case .fireBaseGoogleServiceFileName: return "GoogleService-DeenSquadClub-Info"
        case .googleClientId: return "644125844572-35t5lomvt6ddkm5hcrlrr12cf926nb2s.apps.googleusercontent.com"
        case .dropBoxId: return "i8u2qno75ced5fr"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1384407929"
        case .inAppPurchaseAppName: return "Deensquad"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.newToCanadaClub.rawValue:
        switch constantType {
        case .oneCoin: return "com.newToCanada.C1Coin"
        case .tenCoins: return "com.newToCanada.C10Coins"
        case .thirtyCoins: return "com.newToCanada.C30Coins"
        case .hundredCoins: return "com.newToCanada.C100Coins"
        case .twoHundredCoins: return "com.newToCanada.C200Coins"
        case .threeHundredCoins: return "com.newToCanada.C300Coins"
        case .fiveHundredCoins: return "com.newToCanada.C500Coins"
        case .twelveHundredCoins: return "com.newToCanada.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "33" : "61") //TODO: Need Change in prod//i.e. Club Id For Dev = "8" And Prod = "3"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "117" : "381") //TODO: Need Change in prod // i.e. Club User Id For Dev = "16"
        case .whiteLblAppName: return "New to Canada"
        case .appGoogleApiKey: return "AIzaSyCAsvRWdp4Yky3MNCYljtOH7-9sJ220nDk"
        case .fireBaseGoogleServiceFileName: return "GoogleService-NewToCanada-Info"
        case .googleClientId: return "657118557905-nu6rf0itdgnr6dbe2fm85f4q3nu3tfb5.apps.googleusercontent.com"
        case .dropBoxId: return "2ent3ajp24y1bpa"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1448200727"
        case .inAppPurchaseAppName: return "New_To_Canada_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.ictForum.rawValue:
        switch constantType {
        case .oneCoin: return "com.ICTForum.C1Coin"
        case .tenCoins: return "com.ICTForum.C10Coins"
        case .thirtyCoins: return "com.ICTForum.C30Coins"
        case .hundredCoins: return "com.ICTForum.C100Coins"
        case .twoHundredCoins: return "com.ICTForum.C200Coins"
        case .threeHundredCoins: return "com.ICTForum.C300Coins"
        case .fiveHundredCoins: return "com.ICTForum.C500Coins"
        case .twelveHundredCoins: return "com.ICTForum.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "44" : "128") //TODO: Need Change in prod//i.e. Club Id For Dev = "128" And Prod = "44"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "244" : "1604") //TODO: Need Change in prod // i.e. Club User Id For Dev = "78" and Prod = "39"
        case .whiteLblAppName: return "ICT Forum"
        case .appGoogleApiKey: return "AIzaSyC6kFNkOupNXAy0S_zli3qhe3kvxXvEYuY"
        case .fireBaseGoogleServiceFileName: return "GoogleService-ICTForum-Info"
        case .googleClientId: return "533520805853-v7gfk3e35u1u8itv9i00og60qmrrms97.apps.googleusercontent.com"
        case .dropBoxId: return "ifr01npqv86i9vz"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1455087284"
        case .inAppPurchaseAppName: return "ICT_Forum_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.cyberExperts.rawValue:
        switch constantType {
        case .oneCoin: return "com.CyberExperts.C1Coin"
        case .tenCoins: return "com.CyberExperts.C10Coins"
        case .thirtyCoins: return "com.CyberExperts.C30Coins"
        case .hundredCoins: return "com.CyberExperts.C100Coins"
        case .twoHundredCoins: return "com.CyberExperts.C200Coins"
        case .threeHundredCoins: return "com.CyberExperts.C300Coins"
        case .fiveHundredCoins: return "com.CyberExperts.C500Coins"
        case .twelveHundredCoins: return "com.CyberExperts.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "41" : "129") //TODO: Need Change in prod//i.e. Club Id For Dev = "128" And Prod = "41"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "191" : "1606") //TODO: Need Change in prod // i.e. Club User Id For Dev = "78" and Prod = "39"
        case .whiteLblAppName: return "CyberExperts"
        case .appGoogleApiKey: return "AIzaSyA6vpiIZrurjbOam83zSqqhHeesNJUprhQ"
        case .fireBaseGoogleServiceFileName: return "GoogleService-CyberExperts-Info"
        case .googleClientId: return "384502842939-2efk2a6itevkqmcn9a665lll74vi0nsc.apps.googleusercontent.com"
        case .dropBoxId: return "99fefwxngzl7q29"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1455310691"
        case .inAppPurchaseAppName: return "Cyber_Experts_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.adultify.rawValue:
        switch constantType {
        case .oneCoin: return "com.adultify.C1Coin"
        case .tenCoins: return "com.adultify.C10Coins"
        case .thirtyCoins: return "com.adultify.C30Coins"
        case .hundredCoins: return "com.adultify.C100Coins"
        case .twoHundredCoins: return "com.adultify.C200Coins"
        case .threeHundredCoins: return "com.adultify.C300Coins"
        case .fiveHundredCoins: return "com.adultify.C500Coins"
        case .twelveHundredCoins: return "com.adultify.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "56" : "132") //TODO: Need Change in prod//i.e. Club Id For Dev = "128" And Prod = "41"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "328" : "1619") //TODO: Need Change in prod // i.e. Club User Id For Dev = "78" and Prod = "39"
        case .whiteLblAppName: return "Adultify"
        case .appGoogleApiKey: return "AIzaSyDDlbycPUY71EWDUnHzQPWQJnODNaDUnJ0"
        case .fireBaseGoogleServiceFileName: return "GoogleService-Adultify-Info"
        case .googleClientId: return "811534486929-ui1md241kpvk66hb75bp7guebvdsa1lk.apps.googleusercontent.com"
        case .dropBoxId: return "51bo7d6b2yc6ede"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1462171096"
        case .inAppPurchaseAppName: return "Adultify_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.ptsd.rawValue:
        switch constantType {
        case .oneCoin: return "com.ptsd.C1Coin"
        case .tenCoins: return "com.ptsd.C10Coins"
        case .thirtyCoins: return "com.ptsd.C30Coins"
        case .hundredCoins: return "com.ptsd.C100Coins"
        case .twoHundredCoins: return "com.ptsd.C200Coins"
        case .threeHundredCoins: return "com.ptsd.C300Coins"
        case .fiveHundredCoins: return "com.ptsd.C500Coins"
        case .twelveHundredCoins: return "com.ptsd.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "52" : "133") //TODO: Need Change in prod//i.e. Club Id For Dev = "128" And Prod = "41"
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "191" : "1606") //TODO: Need Change in prod // i.e. Club User Id For Dev = "78" and Prod = "39"
        case .whiteLblAppName: return "PTSDuh"
        case .appGoogleApiKey: return "AIzaSyDoS1sIxyx7y3OtcDi-qztu6RbnZ0s1PFg"
        case .fireBaseGoogleServiceFileName: return "GoogleService-ptsd-Info"
        case .googleClientId: return "169775565212-4l6cp0dq3cv94hckk59q8h5or50n65lj.apps.googleusercontent.com"
        case .dropBoxId: return "yc067mea94kfx3s"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1461742946"
        case .inAppPurchaseAppName: return "PTSD_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.ilead.rawValue:
        switch constantType {
        case .oneCoin: return ""
        case .tenCoins: return ""
        case .hundredCoins: return ""
        case .twoHundredCoins: return ""
        case .threeHundredCoins: return ""
        case .fiveHundredCoins: return ""
        case .twelveHundredCoins: return ""
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "64" : "172")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "453" : "1740")
        case .whiteLblAppName: return "iLead"
        case .appGoogleApiKey: return "AIzaSyBDLEGBOiuJGNvATaYksrrszBbbhG8P21A"
        case .fireBaseGoogleServiceFileName: return "GoogleService-iLead-Info"
        case .googleClientId: return "367754950401-li5c6fh7i9gr7sq8ednjtifgkb0o18bb.apps.googleusercontent.com"
        case .dropBoxId: return "g851esr4n1jzqzq"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1472987062"
        case .inAppPurchaseAppName: return "Ilead_Club"
            
        case .fourCoin: return "com.iLead.C4Coin"
        case .twelveCoins: return "com.iLead.C12Coin"
        case .thirtyCoins: return "com.iLead.C30Coin"
        case .hundredandthreeCoins: return "com.iLead.C103Coin"
        case .hundredandNintyNineCoins:return "com.iLead.C199Coin"
        case .twoHundredEightyFiveCoins:return "com.iLead.C285Coin"
        case .fourHundredCoins:return "com.iLead.C400Coin"
        case .eightHundredCoins:return "com.iLead.C800Coin"
        default: return ""
        }
    case appBundleIds.yourtechcoach.rawValue:
        switch constantType {
        case .oneCoin: return "com.yourtechcoach.C1Coin"
        case .tenCoins: return "com.yourtechcoach.C10Coins"
        case .thirtyCoins: return "com.yourtechcoach.C30Coins"
        case .hundredCoins: return "com.yourtechcoach.C100Coins"
        case .twoHundredCoins: return "com.yourtechcoach.C200Coins"
        case .threeHundredCoins: return "com.yourtechcoach.C300Coins"
        case .fiveHundredCoins: return "com.yourtechcoach.C500Coins"
        case .twelveHundredCoins: return "com.yourtechcoach.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "61" : "316")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "39" : "1507")
        case .whiteLblAppName: return "The Success Club"
        case .appGoogleApiKey: return "AIzaSyBgJ3dPAO_SYtPTG2I0CFoDnxpfBJqGzYo"
        case .fireBaseGoogleServiceFileName: return "GoogleService-YourTechCoach-Info"
        case .googleClientId: return "357807585607-tm45oqh67agcrqlgrak1mu22gmiqm2a7.apps.googleusercontent.com"
        case .dropBoxId: return "5ht4rnit1lybxbc"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1477146159"
        case .inAppPurchaseAppName: return "Your_Tech_Coach_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.myhotperks.rawValue:
        switch constantType {
        case .oneCoin: return "com.myhotperks.C1Coin"
        case .tenCoins: return "com.myhotperks.C10Coins"
        case .thirtyCoins: return "com.myhotperks.C30Coins"
        case .hundredCoins: return "com.myhotperks.C100Coins"
        case .twoHundredCoins: return "com.myhotperks.C200Coins"
        case .threeHundredCoins: return "com.myhotperks.C300Coins"
        case .fiveHundredCoins: return "com.myhotperks.C500Coins"
        case .twelveHundredCoins: return "com.myhotperks.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "77" : "161")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "558" : "1671")
        case .whiteLblAppName: return "My Hot Perks"
        case .appGoogleApiKey: return "AIzaSyARAxsHniePhMELM4IE-RnJDf-XLB5YvHw"
        case .fireBaseGoogleServiceFileName: return "GoogleService-MyHotPerks-Info"
        case .googleClientId: return "283352749625-rsusq11o63rfn9q3dl2jtiu55re1ocml.apps.googleusercontent.com"
        case .dropBoxId: return "5433roe79ajipzk"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1477146159"
        case .inAppPurchaseAppName: return "Myhotperks_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.iag.rawValue:
        switch constantType {
        case .oneCoin: return "com.iag.C1Coin"
        case .tenCoins: return "com.iag.C10Coins"
        case .thirtyCoins: return "com.iag.C30Coins"
        case .hundredCoins: return "com.iag.C100Coins"
        case .twoHundredCoins: return "com.iag.C200Coins"
        case .threeHundredCoins: return "com.iag.C300Coins"
        case .fiveHundredCoins: return "com.iag.C500Coins"
        case .twelveHundredCoins: return "com.iag.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "78" : "162")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "558" : "1671")
        case .whiteLblAppName: return "IAG"
        case .appGoogleApiKey: return "AIzaSyBEjKDASRw3VkbXQBKh9Bck1F-_7sR69Rw"
        case .fireBaseGoogleServiceFileName: return "GoogleService-Info-IAG"
        case .googleClientId: return "60587431360-3rdh94u7359ujj02l4qbp774teit4o0p.apps.googleusercontent.com"
        case .dropBoxId: return "37wu1vbbmb3h190"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1481570614"
        case .inAppPurchaseAppName: return "IAG_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.teamloop.rawValue:
        switch constantType {
        case .oneCoin: return "com.teamloop.C1Coin"
        case .tenCoins: return "com.teamloop.C10Coins"
        case .thirtyCoins: return "com.teamloop.C30Coins"
        case .hundredCoins: return "com.teamloop.C100Coins"
        case .twoHundredCoins: return "com.teamloop.C200Coins"
        case .threeHundredCoins: return "com.teamloop.C300Coins"
        case .fiveHundredCoins: return "com.teamloop.C500Coins"
        case .twelveHundredCoins: return "com.teamloop.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "88" : "191")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "628" : "1813")
        case .whiteLblAppName: return "team loop"
        case .appGoogleApiKey: return "AIzaSyD2BT2eCrdd6zp1alqwpUu8VnjPfxk3oO8"
        case .fireBaseGoogleServiceFileName: return "GoogleService-TeamLoop-Info"
        case .googleClientId: return "353317805834-5840do10l5j45qofvf82le8nu2f30949.apps.googleusercontent.com"
        case .dropBoxId: return "g36sejudrvp0r3n"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1491050162"
        case .inAppPurchaseAppName: return "Teamloop_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.virtualhealthcoach.rawValue:
        switch constantType {
        case .oneCoin: return "com.virtualhealthcoach.C1Coin"
        case .tenCoins: return "com.virtualhealthcoach.C10Coins"
        case .thirtyCoins: return "com.virtualhealthcoach.C30Coins"
        case .hundredCoins: return "com.virtualhealthcoach.C100Coins"
        case .twoHundredCoins: return "com.virtualhealthcoach.C200Coins"
        case .threeHundredCoins: return "com.virtualhealthcoach.C300Coins"
        case .fiveHundredCoins: return "com.virtualhealthcoach.C500Coins"
        case .twelveHundredCoins: return "com.virtualhealthcoach.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "89" : "192")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "630" : "1830")
        case .whiteLblAppName: return "Virtual Health Coach"
        case .appGoogleApiKey: return "AIzaSyD3kUKicLmu-TVWsp0_4GFIAbcyToCH0N4"
        case .fireBaseGoogleServiceFileName: return "GoogleService-VirtualHealthCoach-Info"
        case .googleClientId: return "1018528391567-9evcduou7ufa0gf5ng60t9a5m0o5bsjm.apps.googleusercontent.com"
        case .dropBoxId: return "xb9hv70jcexovq0"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1491177332"
        case .inAppPurchaseAppName: return "VirtualHealthCoach_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.vuca.rawValue:
        switch constantType {
        case .oneCoin: return "com.vuca.C1Coin"
        case .tenCoins: return "com.vuca.C10Coins"
        case .thirtyCoins: return "com.vuca.C30Coins"
        case .hundredCoins: return "com.vuca.C100Coins"
        case .twoHundredCoins: return "com.vuca.C200Coins"
        case .threeHundredCoins: return "com.vuca.C300Coins"
        case .fiveHundredCoins: return "com.vuca.C500Coins"
        case .twelveHundredCoins: return "com.vuca.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "95" : "231")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "646" : "1878")
        case .whiteLblAppName: return "VUCA Coach"
        case .appGoogleApiKey: return "AIzaSyB_Y8LCA7RoJXEPO54KdaPoO5_PH6qxbQM"
        case .fireBaseGoogleServiceFileName: return "GoogleService-VUCA-Info"
        case .googleClientId: return "423230164660-jbt6nu4crpok4bpmcqm8h55vjib2c3ps.apps.googleusercontent.com"
        case .dropBoxId: return "ksiosurr2ydiami"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1495283416"
        case .inAppPurchaseAppName: return "VUCACoach_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.socialarchitect.rawValue:
        switch constantType {
        case .oneCoin: return "com.socialarchitect.C1Coin"
        case .tenCoins: return "com.socialarchitect.C10Coins"
        case .thirtyCoins: return "com.socialarchitect.C30Coins"
        case .hundredCoins: return "com.socialarchitect.C100Coins"
        case .twoHundredCoins: return "com.socialarchitect.C200Coins"
        case .threeHundredCoins: return "com.socialarchitect.C300Coins"
        case .fiveHundredCoins: return "com.socialarchitect.C500Coins"
        case .twelveHundredCoins: return "com.socialarchitect.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "102" : "248")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "697" : "1929")
        case .whiteLblAppName: return "The Social Architect"
        case .appGoogleApiKey: return "AIzaSyCvzDXZkO7Af5zbnHgjSCTzYndIDp3NIkg"
        case .fireBaseGoogleServiceFileName: return "GoogleService-SocialArchitect-Info"
        case .googleClientId: return "520648380996-lvh52erf6hpeeb5bvcccf7hd40n54ovr.apps.googleusercontent.com"
        case .dropBoxId: return "wxiizytusf0rp9k"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1507905929"
        case .inAppPurchaseAppName: return "socialarchitect_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.idealwealthgrower.rawValue:
        switch constantType {
        case .oneCoin: return "com.iwg.C1Coin"
        case .tenCoins: return "com.iwg.C10Coins"
        case .thirtyCoins: return "com.iwg.C30Coins"
        case .hundredCoins: return "com.iwg.C100Coins"
        case .twoHundredCoins: return "com.iwg.C200Coins"
        case .threeHundredCoins: return "com.iwg.C300Coins"
        case .fiveHundredCoins: return "com.iwg.C500Coins"
        case .twelveHundredCoins: return "com.iwg.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "107" : "262")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "707" : "1957")
        case .whiteLblAppName: return "Ideal Wealth Grower"
        case .appGoogleApiKey: return "AIzaSyCSFpiM6H5IF6ezD2xSwU6TStml-EWE22M"
        case .fireBaseGoogleServiceFileName: return "GoogleService-IWG-Info"
        case .googleClientId: return "1025176339058-pmkpk701bpa94u0ufg15i0djkgebek93.apps.googleusercontent.com"
        case .dropBoxId: return "ep84vomwkxde49i"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1512363340"
        case .inAppPurchaseAppName: return "iwg_Club"//iwg_Club
        
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.pharmakonect.rawValue:
        switch constantType {
        case .oneCoin: return "com.pharmakonect.C1Coin"
        case .tenCoins: return "com.pharmakonect.C10Coins"
        case .thirtyCoins: return "com.pharmakonect.C30Coins"
        case .hundredCoins: return "com.pharmakonect.C100Coins"
        case .twoHundredCoins: return "com.pharmakonect.C200Coins"
        case .threeHundredCoins: return "com.pharmakonect.C300Coins"
        case .fiveHundredCoins: return "com.pharmakonect.C500Coins"
        case .twelveHundredCoins: return "com.pharmakonect.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "108" : "277")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "727" : "1999")
        case .whiteLblAppName: return "PharmaKonect"
        case .appGoogleApiKey: return "AIzaSyCKWABWOR5FlMADVW8PbCs48XDLvqiK0CM"
        case .fireBaseGoogleServiceFileName: return "GoogleService-PharmaKonect-Info"
        case .googleClientId: return "457453117800-vcdrfovkf2i4q3n8rgcqc6libhjkcbcj.apps.googleusercontent.com"
        case .dropBoxId: return "h4heskqjklf7o11"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1515680505"
        case .inAppPurchaseAppName: return "PharmaKonect_Club"//iwg_Club
        
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.nbda.rawValue:
        switch constantType {
        case .oneCoin: return "com.nbda.C1Coin"
        case .tenCoins: return "com.nbda.C10Coins"
        case .thirtyCoins: return "com.nbda.C30Coins"
        case .hundredCoins: return "com.nbda.C100Coins"
        case .twoHundredCoins: return "com.nbda.C200Coins"
        case .threeHundredCoins: return "com.nbda.C300Coins"
        case .fiveHundredCoins: return "com.nbda.C500Coins"
        case .twelveHundredCoins: return "com.nbda.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "113" : "285")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "743" : "2014")
        case .whiteLblAppName: return "NBDA"
        case .appGoogleApiKey: return "AIzaSyB59PbzJvyi8UzBmTc5ycRjKgvZu7bkDWg"
        case .fireBaseGoogleServiceFileName: return "GoogleService-NBDA-Info"
        case .googleClientId: return "545445495513-3oqf3u4o7b4ur6p690p9ns73dch5hdmh.apps.googleusercontent.com"
        case .dropBoxId: return "ehsrwrwei3qf2lo"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1516437326"
        case .inAppPurchaseAppName: return "NBDA_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.inspiredmoms.rawValue:
        switch constantType {
        case .oneCoin: return "com.inspiredMoms.C1Coin"
        case .tenCoins: return "com.inspiredMoms.C10Coins"
        case .thirtyCoins: return "com.inspiredMoms.C30Coins"
        case .hundredCoins: return "com.inspiredMoms.C100Coins"
        case .twoHundredCoins: return "com.inspiredMoms.C200Coins"
        case .threeHundredCoins: return "com.inspiredMoms.C300Coins"
        case .fiveHundredCoins: return "com.inspiredMoms.C500Coins"
        case .twelveHundredCoins: return "com.inspiredMoms.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "120" : "295")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "743" : "2014")
        case .whiteLblAppName: return "Inspired Mommy"
        case .appGoogleApiKey: return "AIzaSyCZJwO2A6oppU9S1ilGK4F6NkPF0gpY4ic"
        case .fireBaseGoogleServiceFileName: return "GoogleService-InspiredMoms-Info"
        case .googleClientId: return "221985334501-dgn9pnj7g6qv56ds1rp61655p3oih89i.apps.googleusercontent.com"
        case .dropBoxId: return "qdopwwcvcbbq29j"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1524193908"
        case .inAppPurchaseAppName: return "InspiredMoms_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.peersondemand.rawValue:
        switch constantType {
        case .oneCoin: return "com.peersondemand.C1Coin"
        case .tenCoins: return "com.peersondemand.C10Coins"
        case .thirtyCoins: return "com.peersondemand.C30Coins"
        case .hundredCoins: return "com.peersondemand.C100Coins"
        case .twoHundredCoins: return "com.peersondemand.C200Coins"
        case .threeHundredCoins: return "com.peersondemand.C300Coins"
        case .fiveHundredCoins: return "com.peersondemand.C500Coins"
        case .twelveHundredCoins: return "com.peersondemand.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "127" : "309")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "906" : "2158")
        case .whiteLblAppName: return "PeersOnDemand"
        case .appGoogleApiKey: return "AIzaSyAK9VMiRDuqpEuS6jCJAl8kYgVNtDIxq7Q"
        case .fireBaseGoogleServiceFileName: return "GoogleService-PeersOnDemand-Info"
        case .googleClientId: return "733828763359-h3r7pug7f1c68dfu02cpg4nukh3a3qob.apps.googleusercontent.com"
        case .dropBoxId: return "6zzxzycib18yrt2"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1533655439"
        case .inAppPurchaseAppName: return "PeersOnDemand_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.sportsmasters.rawValue:
        switch constantType {
        case .oneCoin: return "com.sportsmasters.C1Coin"
        case .tenCoins: return "com.sportsmasters.C10Coins"
        case .thirtyCoins: return "com.sportsmasters.C30Coins"
        case .hundredCoins: return "com.sportsmasters.C100Coins"
        case .twoHundredCoins: return "com.sportsmasters.C200Coins"
        case .threeHundredCoins: return "com.sportsmasters.C300Coins"
        case .fiveHundredCoins: return "com.sportsmasters.C500Coins"
        case .twelveHundredCoins: return "com.sportsmasters.C1200Coins"
        case .whiteLblAppId: return (mailBaseURL == baseUrl.production.rawValue ? "128" : "310")
        case .whiteLblAppUserId: return (mailBaseURL == baseUrl.production.rawValue ? "958" : "2168")
        case .whiteLblAppName: return "SportsMasters"
        case .appGoogleApiKey: return "AIzaSyDaKZHd5OGNvSfwvrvOgs445wYp-bqndok"
        case .fireBaseGoogleServiceFileName: return "GoogleService-SportsMasters-Info"
        case .googleClientId: return "1037981197141-38n7o6sirhpkp2a56f39fpel65uj8c1j.apps.googleusercontent.com"
        case .dropBoxId: return "uo8moa29savqf9s"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1534475833"
        case .inAppPurchaseAppName: return "SportsMasters_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.engineeryourmission.rawValue:
        switch constantType {
        case .oneCoin: return "com.engineeryourmission.C1Coin"
        case .tenCoins: return "com.engineeryourmission.C10Coins"
        case .thirtyCoins: return "com.engineeryourmission.C30Coins"
        case .hundredCoins: return "com.engineeryourmission.C100Coins"
        case .twoHundredCoins: return "com.engineeryourmission.C200Coins"
        case .threeHundredCoins: return "com.engineeryourmission.C300Coins"
        case .fiveHundredCoins: return "com.engineeryourmission.C500Coins"
        case .twelveHundredCoins: return "com.engineeryourmission.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "133"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "318"
            } else {
                return "317"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1073"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2205"
            } else {
                return "2195"
            }
        case .whiteLblAppName: return "Engineer Your Mission"
        case .appGoogleApiKey: return "AIzaSyAbTbE5U9pxUlRFUkcSJwIxaetN5qaxCJw"
        case .fireBaseGoogleServiceFileName: return "GoogleService-EngineerYourMission-Info"
        case .googleClientId: return "730484460041-q2ihlehug5fjjdh4a2itfupvtjj12de2.apps.googleusercontent.com"
        case .dropBoxId: return "1hgrxauwvlmeiyb"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1541142899"
        case .inAppPurchaseAppName: return "EngineerYourMission_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.bossibly.rawValue:
        switch constantType {
        case .oneCoin: return "com.bossibly.C1Coin"
        case .tenCoins: return "com.bossibly.C10Coins"
        case .thirtyCoins: return "com.bossibly.C30Coins"
        case .hundredCoins: return "com.bossibly.C100Coins"
        case .twoHundredCoins: return "com.bossibly.C200Coins"
        case .threeHundredCoins: return "com.bossibly.C300Coins"
        case .fiveHundredCoins: return "com.bossibly.C500Coins"
        case .twelveHundredCoins: return "com.bossibly.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "135"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "318"
            } else {
                return "319"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1079"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2196"
            } else {
                return "2211"
            }
        case .whiteLblAppName: return "Bossibly"
        case .appGoogleApiKey: return "AIzaSyCDdRS12CQyhn4e9otL0_yqnXaoQe9OQ8E"
        case .fireBaseGoogleServiceFileName: return "GoogleService-Bossibly-Info"
        case .googleClientId: return "911975128452-9fao6btt9p6n0pu4n19grbriu2ldb59n.apps.googleusercontent.com"
        case .dropBoxId: return "l5bx1qqc70yhywt"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1541579567"
        case .inAppPurchaseAppName: return "Bossibly_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.caba.rawValue:
        switch constantType {
        case .oneCoin: return "com.caba.C1Coin"
        case .tenCoins: return "com.caba.C10Coins"
        case .thirtyCoins: return "com.caba.C30Coins"
        case .hundredCoins: return "com.caba.C100Coins"
        case .twoHundredCoins: return "com.caba.C200Coins"
        case .threeHundredCoins: return "com.caba.C300Coins"
        case .fiveHundredCoins: return "com.caba.C500Coins"
        case .twelveHundredCoins: return "com.caba.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "136"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "320"
            } else {
                return "320"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1083"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2213"
            } else {
                return "2199"
            }
        case .whiteLblAppName: return "CABA"
        case .appGoogleApiKey: return "AIzaSyCl1XgxCJ05ShneJ2Leg7XKRU897uIe7cU"
        case .fireBaseGoogleServiceFileName: return "GoogleService-CABA-Info"
        case .googleClientId: return "456299460924-5lm0v4hu6og72kdu6cp25gad26pimkun.apps.googleusercontent.com"
        case .dropBoxId: return "jj67z2gh689w938"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1542344682"
        case .inAppPurchaseAppName: return "Caba_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.suttongroup.rawValue:
        switch constantType {
        case .oneCoin: return "com.suttongroup.C1Coin"
        case .tenCoins: return "com.suttongroup.C10Coins"
        case .thirtyCoins: return "com.suttongroup.C30Coins"
        case .hundredCoins: return "com.suttongroup.C100Coins"
        case .twoHundredCoins: return "com.suttongroup.C200Coins"
        case .threeHundredCoins: return "com.suttongroup.C300Coins"
        case .fiveHundredCoins: return "com.suttongroup.C500Coins"
        case .twelveHundredCoins: return "com.suttongroup.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "138"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "321"
            } else {
                return "321"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1089"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2214"
            } else {
                return "2200"
            }
        case .whiteLblAppName: return "Sutton Group"
        case .appGoogleApiKey: return "AIzaSyAXhAEoX6x3TkMhQRp1YHkq6-ezrUGwl90"
        case .fireBaseGoogleServiceFileName: return "GoogleService-SG-Info"
        case .googleClientId: return "1087784234505-0v5apvdpmhksfej0arjh4u0n6lk63ie4.apps.googleusercontent.com"
        case .dropBoxId: return "h5ab1wq80fjqau4"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1543169591"
        case .inAppPurchaseAppName: return "SG_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.nlcapitalgroup.rawValue:
        switch constantType {
        case .oneCoin: return "com.nlcapitalgroup.C1Coin"
        case .tenCoins: return "com.nlcapitalgroup.C10Coins"
        case .thirtyCoins: return "com.nlcapitalgroup.C30Coins"
        case .hundredCoins: return "com.nlcapitalgroup.C100Coins"
        case .twoHundredCoins: return "com.nlcapitalgroup.C200Coins"
        case .threeHundredCoins: return "com.nlcapitalgroup.C300Coins"
        case .fiveHundredCoins: return "com.nlcapitalgroup.C500Coins"
        case .twelveHundredCoins: return "com.nlcapitalgroup.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "144"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "324"
            } else {
                return "324"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1168"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2219"
            } else {
                return "2204"
            }
        case .whiteLblAppName: return "NL Capital Group"
        case .appGoogleApiKey: return "AIzaSyDqLx1yiSecBrlyh99ilkxgrj9YL9XKtcA"
        case .fireBaseGoogleServiceFileName: return "GoogleService-NLCapitalGroup-Info"
        case .googleClientId: return "822971660986-mje7381ukpg0hibf865hjrn9ngqag3ar.apps.googleusercontent.com"
        case .dropBoxId: return "jdc3nqcwx0c5v9k"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1545882540"
        case .inAppPurchaseAppName: return "NLCapital_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.ideserveitnow.rawValue:
        switch constantType {
        case .oneCoin: return "com.ideserveitnow.C1Coin"
        case .tenCoins: return "com.ideserveitnow.C10Coins"
        case .thirtyCoins: return "com.ideserveitnow.C30Coins"
        case .hundredCoins: return "com.ideserveitnow.C100Coins"
        case .twoHundredCoins: return "com.ideserveitnow.C200Coins"
        case .threeHundredCoins: return "com.ideserveitnow.C300Coins"
        case .fiveHundredCoins: return "com.ideserveitnow.C500Coins"
        case .twelveHundredCoins: return "com.ideserveitnow.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "143"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "323"
            } else {
                return "323"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1167"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2218"
            } else {
                return "2203"
            }
        case .whiteLblAppName: return "iDeserveIt Now"
        case .appGoogleApiKey: return "AIzaSyCllotQsaCVP9WaNWCs3183gennC0e9We4"
        case .fireBaseGoogleServiceFileName: return "GoogleService-iDeserveIt Now-Info"
        case .googleClientId: return "521794568161-an18q74igft40ksi4hhtfc1598o2nrss.apps.googleusercontent.com"
        case .dropBoxId: return "dw67rqgripl30ry"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1545916582"
        case .inAppPurchaseAppName: return "IDeserverItNow_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.urbanfitmag.rawValue:
        switch constantType {
        case .oneCoin: return "com.urbanfitmag.C1Coin"
        case .tenCoins: return "com.urbanfitmag.C10Coins"
        case .thirtyCoins: return "com.urbanfitmag.C30Coins"
        case .hundredCoins: return "com.urbanfitmag.C100Coins"
        case .twoHundredCoins: return "com.urbanfitmag.C200Coins"
        case .threeHundredCoins: return "com.urbanfitmag.C300Coins"
        case .fiveHundredCoins: return "com.urbanfitmag.C500Coins"
        case .twelveHundredCoins: return "com.urbanfitmag.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "151"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "347"
            } else {
                return "347"//"332"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1191"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2224"
            } else {
                return "2209"
            }
        case .whiteLblAppName: return "UrbanFitMag"
        case .appGoogleApiKey: return "AIzaSyDmvV2mlryQf_7sYCX6ivQJ0N86GHJwo1Y"
        case .fireBaseGoogleServiceFileName: return "GoogleService-UrbanFitMag-Info"
        case .googleClientId: return "106749285053-7b23tqmt96vetgvcmhi5dtqqfs4mecqr.apps.googleusercontent.com"
        case .dropBoxId: return "q4kfxkom3adcyxx"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1548230616"
        case .inAppPurchaseAppName: return "UrbanFitMag_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.risebreakfree.rawValue:
        switch constantType {
        case .oneCoin: return "com.risebreakfree.C1Coin"
        case .tenCoins: return "com.risebreakfree.C10Coins"
        case .thirtyCoins: return "com.risebreakfree.C30Coins"
        case .hundredCoins: return "com.risebreakfree.C100Coins"
        case .twoHundredCoins: return "com.risebreakfree.C200Coins"
        case .threeHundredCoins: return "com.risebreakfree.C300Coins"
        case .fiveHundredCoins: return "com.risebreakfree.C500Coins"
        case .twelveHundredCoins: return "com.risebreakfree.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "155"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "351"
            } else {
                return "333"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1222"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2228"
            } else {
                return "2215"
            }
        case .whiteLblAppName: return "Rise & Break Free"
        case .appGoogleApiKey: return "AIzaSyC1ECStuqCvIp9hvXKBz7vmniUGpzFJCqo"
        case .fireBaseGoogleServiceFileName: return "GoogleService-Rise&BreakFree-Info"
        case .googleClientId: return "509835422792-jvh6erech3mm3gbt1ldvpva0vbnmh11n.apps.googleusercontent.com"
        case .dropBoxId: return "rgladoe0trpdobk"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1551277769"
        case .inAppPurchaseAppName: return "Rise&BreakFree_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.rodneylavoie.rawValue:
        switch constantType {
        case .oneCoin: return "com.rodneylavoie.C1Coin"
        case .tenCoins: return "com.rodneylavoie.C10Coins"
        case .thirtyCoins: return "com.rodneylavoie.C30Coins"
        case .hundredCoins: return "com.rodneylavoie.C100Coins"
        case .twoHundredCoins: return "com.rodneylavoie.C200Coins"
        case .threeHundredCoins: return "com.rodneylavoie.C300Coins"
        case .fiveHundredCoins: return "com.rodneylavoie.C500Coins"
        case .twelveHundredCoins: return "com.rodneylavoie.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "157"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "356"//"334"
            } else {
                return "356"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1248"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2224"//"2232"
            } else {
                return "2224"
            }
        case .whiteLblAppName: return "Rodney Lavoie"
        case .appGoogleApiKey: return "AIzaSyBnvdNeStbf0A-XDuOGg_onk7uHprO--Ns"
        case .fireBaseGoogleServiceFileName: return "GoogleService-RodneyLavoie-Info"
        case .googleClientId: return "439632386058-45aqjrciedj8t22g9to8bfc4ka70f51p.apps.googleusercontent.com"
        case .dropBoxId: return "sdo57la17w6kxok"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1552668939"
        case .inAppPurchaseAppName: return "RodneyLavoie_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.podcommander.rawValue:
        switch constantType {
        case .oneCoin: return "com.podcommander.C1Coin"
        case .tenCoins: return "com.podcommander.C10Coins"
        case .thirtyCoins: return "com.podcommander.C30Coins"
        case .hundredCoins: return "com.podcommander.C100Coins"
        case .twoHundredCoins: return "com.podcommander.C200Coins"
        case .threeHundredCoins: return "com.podcommander.C300Coins"
        case .fiveHundredCoins: return "com.podcommander.C500Coins"
        case .twelveHundredCoins: return "com.podcommander.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "165"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "373"
            } else {
                return "345"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1255"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2232"
            } else {
                return "2251"
            }
        case .whiteLblAppName: return "Pod Commander"
        case .appGoogleApiKey: return "AIzaSyBFTjsI3Mnslw-6kP0aUd3V8OsXT_U3zdU"
        case .fireBaseGoogleServiceFileName: return "GoogleService-PodCommander-Info"
        case .googleClientId: return "185418683738-jqee3p54pfldusvcb68klt9h97nnc6d8.apps.googleusercontent.com"
        case .dropBoxId: return "an71256uh2wuzbm"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1559166078"
        case .inAppPurchaseAppName: return "PodCommander_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.workishell.rawValue:
        switch constantType {
        case .oneCoin: return "com.workishell.C1Coin"
        case .tenCoins: return "com.workishell.C10Coins"
        case .thirtyCoins: return "com.workishell.C30Coins"
        case .hundredCoins: return "com.workishell.C100Coins"
        case .twoHundredCoins: return "com.workishell.C200Coins"
        case .threeHundredCoins: return "com.workishell.C300Coins"
        case .fiveHundredCoins: return "com.workishell.C500Coins"
        case .twelveHundredCoins: return "com.workishell.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "170"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "378"
            } else {
                return "378"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1383"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2389"
            } else {
                return "2389"
            }
        case .whiteLblAppName: return "Work is Hell"
        case .appGoogleApiKey: return "AIzaSyCu0oMJ0GdS4xTec7FCwgeDeSTEx6j1Q1I"
        case .fireBaseGoogleServiceFileName: return "GoogleService-WorkIsHell-Info"
        case .googleClientId: return "163632458063-v1goe9q0jbbs87o4ja1esv8cuo027h48.apps.googleusercontent.com"
        case .dropBoxId: return "vi3hadtjbllrqu0"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1563877531"
        case .inAppPurchaseAppName: return "WorkisHell_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.homicity.rawValue:
        switch constantType {
        case .oneCoin: return "com.homicity.C1Coin"
        case .tenCoins: return "com.homicity.C10Coins"
        case .thirtyCoins: return "com.homicity.C30Coins"
        case .hundredCoins: return "com.homicity.C100Coins"
        case .twoHundredCoins: return "com.homicity.C200Coins"
        case .threeHundredCoins: return "com.homicity.C300Coins"
        case .fiveHundredCoins: return "com.homicity.C500Coins"
        case .twelveHundredCoins: return "com.homicity.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "171"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "379"
            } else {
                return "379"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1089"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2214"
            } else {
                return "2214"
            }
        case .whiteLblAppName: return "Homicity"
        case .appGoogleApiKey: return "AIzaSyDJHYL_-Zxs_wA54frLAkcj-ZDGNn6CL8w"
        case .fireBaseGoogleServiceFileName: return "GoogleService-Homicity-Info"
        case .googleClientId: return "853980857558-luqa09lj4rbbt2n6binh52tt8hk7ehel.apps.googleusercontent.com"
        case .dropBoxId: return "4bhb90pgohifccf"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1563901217"
        case .inAppPurchaseAppName: return "Homicity_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.crittercarebrothers.rawValue:
        switch constantType {
        case .oneCoin: return "com.crittercarebrothers.C1Coin"
        case .tenCoins: return "com.crittercarebrothers.C10Coins"
        case .thirtyCoins: return "com.crittercarebrothers.C30Coins"
        case .hundredCoins: return "com.crittercarebrothers.C100Coins"
        case .twoHundredCoins: return "com.crittercarebrothers.C200Coins"
        case .threeHundredCoins: return "com.crittercarebrothers.C300Coins"
        case .fiveHundredCoins: return "com.crittercarebrothers.C500Coins"
        case .twelveHundredCoins: return "com.crittercarebrothers.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "175"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "392"
            } else {
                return "392"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1487"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2429"
            } else {
                return "2429"
            }
        case .whiteLblAppName: return "Critter Care Brothers"
        case .appGoogleApiKey: return "AIzaSyBswX1Ln-fTRc31nd_0TK31i5nkJyemAlU"
        case .fireBaseGoogleServiceFileName: return "GoogleService-CritterCareBrothers-Info"
        case .googleClientId: return "812840510361-4sb7ple8ochfiu1scq5abfl37sv83qcv.apps.googleusercontent.com"
        case .dropBoxId: return "qlombhyfl4kgouu"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1572594745"
        case .inAppPurchaseAppName: return "Crittercarebrothers_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.yashcollagen.rawValue:
        switch constantType {
        case .oneCoin: return "com.yascollagen.C1Coin"
        case .tenCoins: return "com.yascollagen.C10Coins"
        case .thirtyCoins: return "com.yascollagen.C30Coins"
        case .hundredCoins: return "com.yascollagen.C100Coins"
        case .twoHundredCoins: return "com.yascollagen.C200Coins"
        case .threeHundredCoins: return "com.yascollagen.C300Coins"
        case .fiveHundredCoins: return "com.yascollagen.C500Coins"
        case .twelveHundredCoins: return "com.yascollagen.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "177"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "346"
            } else {
                return "393"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1501"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2255"
            } else {
                return "2438"
            }
        case .whiteLblAppName: return "YAS Collagen"
        case .appGoogleApiKey: return "AIzaSyBN5royo9nTWokK5WVOuPkovAwcBPF90bg"
        case .fireBaseGoogleServiceFileName: return "GoogleService-YASCollagen-Info"
        case .googleClientId: return "1090640109680-l172ekaq4t6iek40ktds550ja0hfbovs.apps.googleusercontent.com"
        case .dropBoxId: return "1d1z94p3v0ayjrt"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1573400428"
        case .inAppPurchaseAppName: return "YASCollagen_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.resetwithmiranda.rawValue:
        switch constantType {
        case .oneCoin: return "com.resetwithmiranda.C1Coin"
        case .tenCoins: return "com.resetwithmiranda.C10Coins"
        case .thirtyCoins: return "com.resetwithmiranda.C30Coins"
        case .hundredCoins: return "com.resetwithmiranda.C100Coins"
        case .twoHundredCoins: return "com.resetwithmiranda.C200Coins"
        case .threeHundredCoins: return "com.resetwithmiranda.C300Coins"
        case .fiveHundredCoins: return "com.resetwithmiranda.C500Coins"
        case .twelveHundredCoins: return "com.resetwithmiranda.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "180"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "395"
            } else {
                return "395"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1560"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2478"
            } else {
                return "2478"
            }
        case .whiteLblAppName: return "Reset with Miranda"
        case .appGoogleApiKey: return "AIzaSyBN5royo9nTWokK5WVOuPkovAwcBPF90bg"
        case .fireBaseGoogleServiceFileName: return "GoogleService-ResetwithMiranda-Info"
        case .googleClientId: return "267703479939-6nhe2svrp24ce13jn0rlat24k159jodl.apps.googleusercontent.com"
        case .dropBoxId: return "a2ysrc2gx4cmsil"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1580019027"
        case .inAppPurchaseAppName: return "ResetwithMiranda_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.everythingtennis.rawValue:
        switch constantType {
        case .oneCoin: return "com.everythingtennis.C1Coin"
        case .tenCoins: return "com.everythingtennis.C10Coins"
        case .thirtyCoins: return "com.everythingtennis.C30Coins"
        case .hundredCoins: return "com.everythingtennis.C100Coins"
        case .twoHundredCoins: return "com.everythingtennis.C200Coins"
        case .threeHundredCoins: return "com.everythingtennis.C300Coins"
        case .fiveHundredCoins: return "com.everythingtennis.C500Coins"
        case .twelveHundredCoins: return "com.everythingtennis.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "181"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "396"
            } else {
                return "396"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "1564"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2481"
            } else {
                return "2481"
            }
        case .whiteLblAppName: return "Everything Tennis"
        case .appGoogleApiKey: return "AIzaSyDZF-SZZaDThrK1OYUDx97_f4Y9imxrfro"
        case .fireBaseGoogleServiceFileName: return "GoogleService-EverythingTennis-Info"
        case .googleClientId: return "421116251946-olaj28tk6pn5hb16tefpst08ug6kg2pm.apps.googleusercontent.com"
        case .dropBoxId: return "fvhokxxjjyhpsi0"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1579880563"
        case .inAppPurchaseAppName: return "EverythingTennis_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    case appBundleIds.creatorsuncensored.rawValue:
        switch constantType {
        case .oneCoin: return "com.creatorsuncensored.C1Coin"
        case .tenCoins: return "com.creatorsuncensored.C10Coins"
        case .thirtyCoins: return "com.creatorsuncensored.C30Coins"
        case .hundredCoins: return "com.creatorsuncensored.C100Coins"
        case .twoHundredCoins: return "com.creatorsuncensored.C200Coins"
        case .threeHundredCoins: return "com.creatorsuncensored.C300Coins"
        case .fiveHundredCoins: return "com.creatorsuncensored.C500Coins"
        case .twelveHundredCoins: return "com.creatorsuncensored.C1200Coins"
        case .whiteLblAppId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "185"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "402"
            } else {
                return "402"
            }
        case .whiteLblAppUserId:
            if mailBaseURL == baseUrl.production.rawValue {
                return "2115"
            } else if mailBaseURL == baseUrl.development.rawValue {
                return "2508"
            } else {
                return "2508"
            }
        case .whiteLblAppName: return "Creators Uncensored"
        case .appGoogleApiKey: return "AIzaSyANUtf68wLlJ5DHqBOa_5-7azhIkwOKuu4"
        case .fireBaseGoogleServiceFileName: return "GoogleService-CreatorsUncensored-Info"
        case .googleClientId: return "977151275261-icp42hf41urp8ser7e03ruhfh0jqmq7b.apps.googleusercontent.com"
        case .dropBoxId: return "tc6tzgf6kktmhdl"
        case .iTunesLink: return "itms-apps://itunes.apple.com/app/bars/id1588881980"
        case .inAppPurchaseAppName: return "CreatorsUncensored_Club"
            
        case .fourCoin: return ""
        case .twelveCoins: return ""
        case .hundredandthreeCoins: return ""
        case .hundredandNintyNineCoins:return ""
        case .twoHundredEightyFiveCoins:return ""
        case .fourHundredCoins:return ""
        case .eightHundredCoins:return ""
        default: return ""
        }
    default: return ""
    }
}

func getBGImageName() -> String {
    
    let bundleId = Bundle.main.bundleIdentifier
    
    switch bundleId {
    case appBundleIds.sorinClub.rawValue: return "bg_screen_sorin_club"
    case appBundleIds.cccClub.rawValue: return "bg_screen_CCC"
    case appBundleIds.volleyBall.rawValue: return "bg_VolleyBall"
    case appBundleIds.actioneer.rawValue: return "bg_screen_actioneer"
    case appBundleIds.deenSquadClub.rawValue: return "bg_screen_deen_squad"
    case appBundleIds.newToCanadaClub.rawValue: return "bg_screen_NewToCanada"
    case appBundleIds.ictForum.rawValue: return "bg_screen_ICTForum"
    case appBundleIds.cyberExperts.rawValue: return "bg_screen_CyberExperts"
    case appBundleIds.adultify.rawValue: return "bg_screen_Adultify"
    case appBundleIds.ptsd.rawValue: return "bg_screen_PTSD"
    case appBundleIds.ilead.rawValue: return "bg_screen_iLead"
    case appBundleIds.yourtechcoach.rawValue: return "bg_screen_YourTechCoach"
    case appBundleIds.myhotperks.rawValue: return "bg_screen_MyHotPerks"
    case appBundleIds.iag.rawValue: return "bg_screen_IAG"
    case appBundleIds.teamloop.rawValue: return "bg_screen_TeamLoop"
    case appBundleIds.virtualhealthcoach.rawValue: return "bg_screen_VirtualHealthCoach"
    case appBundleIds.vuca.rawValue: return "bg_screen_vuca"
    case appBundleIds.socialarchitect.rawValue: return "bg_screen_social"
    case appBundleIds.idealwealthgrower.rawValue: return "bg_screen_iwg"
    case appBundleIds.pharmakonect.rawValue: return "bg_screen_pharmakonect"
    case appBundleIds.nbda.rawValue: return "bg_screen_nbda"
    case appBundleIds.inspiredmoms.rawValue: return "bg_screen_InspiredMoms"
    case appBundleIds.peersondemand.rawValue: return "bg_screen_PeersOnDemand"
    case appBundleIds.sportsmasters.rawValue: return "bg_screen_SportsMasters"
    case appBundleIds.engineeryourmission.rawValue: return "bg_screen_EngineerYourMission"
    case appBundleIds.bossibly.rawValue: return "bg_screen_Bossibly"
    case appBundleIds.caba.rawValue: return "bg_screen_CABA"
    case appBundleIds.suttongroup.rawValue: return "bg_screen_SG"
    case appBundleIds.nlcapitalgroup.rawValue: return "bg_screen_NLCapitalGroup"
    case appBundleIds.ideserveitnow.rawValue: return "bg_screen_IDeserveItNow"
    case appBundleIds.urbanfitmag.rawValue: return "bg_screen_UrbanFitMag"
    case appBundleIds.risebreakfree.rawValue: return "bg_screen_Rise&BreakFree"
    case appBundleIds.rodneylavoie.rawValue: return "bg_screen_RodneyLavoie"
    case appBundleIds.podcommander.rawValue: return "bg_screen_PodCommander"
    case appBundleIds.workishell.rawValue: return "bg_screen_WorkisHell"
    case appBundleIds.homicity.rawValue: return "bg_screen_Homicity"
    case appBundleIds.crittercarebrothers.rawValue: return "bg_screen_Crittercarebrothers"
    case appBundleIds.crittercarebrothers.rawValue: return "bg_screen_YASCollagen"
    case appBundleIds.resetwithmiranda.rawValue: return "bg_screen_ResetwithMiranda"
    case appBundleIds.everythingtennis.rawValue: return "bg_screen_EverythingTennis"
    case appBundleIds.creatorsuncensored.rawValue: return "bg_screen_CreatorsUncensored"

    default: return ""
    }
}

func getClubProfIconImageName() -> String {
    
    let bundleId = Bundle.main.bundleIdentifier
    
    switch bundleId {
    case appBundleIds.sorinClub.rawValue: return "Club_Profile_Icon_sorin_club"
    case appBundleIds.cccClub.rawValue: return "Club_Profile_Icon_CCC"
    case appBundleIds.volleyBall.rawValue: return "Club_Profile_Icon_VolleyBall"
    case appBundleIds.actioneer.rawValue: return "Club_Profile_Icon_actioneer"
    case appBundleIds.deenSquadClub.rawValue: return "Club_Profile_Icon_deen_squad"
    case appBundleIds.newToCanadaClub.rawValue: return "Club_Profile_Icon_NewToCanada"
    case appBundleIds.ictForum.rawValue: return "Club_Profile_Icon_ICTForum"
    case appBundleIds.cyberExperts.rawValue: return "Club_Profile_Icon_CyberExperts"
    case appBundleIds.adultify.rawValue: return "Club_Profile_Icon_Adultify"
    case appBundleIds.ptsd.rawValue: return "Club_Profile_Icon_PTSD"
    case appBundleIds.ilead.rawValue: return "Club_Profile_Icon_iLead"
    case appBundleIds.yourtechcoach.rawValue: return "Club_Profile_Icon_YourTechCoach"
    case appBundleIds.myhotperks.rawValue: return "Club_Profile_Icon_MyHotPerks"
    case appBundleIds.iag.rawValue: return "Club_Profile_Icon_IAG"
    case appBundleIds.teamloop.rawValue: return "Club_Profile_Icon_TeamLoop"
    case appBundleIds.virtualhealthcoach.rawValue: return "Club_Profile_Icon_VirtualHealthCoach"
    case appBundleIds.vuca.rawValue: return "Club_Profile_Icon_vuca"
    case appBundleIds.socialarchitect.rawValue: return "Club_Profile_Icon_social"
    case appBundleIds.idealwealthgrower.rawValue: return "Club_Profile_Icon_iwg"
    case appBundleIds.pharmakonect.rawValue: return "Club_Profile_Icon_pharmakonect"
    case appBundleIds.nbda.rawValue: return "Club_Profile_Icon_nbda"
    case appBundleIds.inspiredmoms.rawValue: return "Club_Profile_Icon_InspiredMoms"
    case appBundleIds.peersondemand.rawValue: return "Club_Profile_Icon_PeersOnDemand"
    case appBundleIds.sportsmasters.rawValue: return "Club_Profile_Icon_SportsMasters"
    case appBundleIds.engineeryourmission.rawValue: return "Club_Profile_Icon_EngineerYourMission"
    case appBundleIds.bossibly.rawValue: return "Club_Profile_Icon_Bossibly"
    case appBundleIds.caba.rawValue: return "Club_Profile_Icon_CABA"
    case appBundleIds.suttongroup.rawValue: return "Club_Profile_Icon_SG"
    case appBundleIds.nlcapitalgroup.rawValue: return "Club_Profile_Icon_NLCapitalGroup"
    case appBundleIds.ideserveitnow.rawValue: return "Club_Profile_Icon_IDeserveItNow"
    case appBundleIds.urbanfitmag.rawValue: return "Club_Profile_Icon_UbanFitMag"
    case appBundleIds.risebreakfree.rawValue: return "Club_Profile_Icon_Rise&BreakFree"
    case appBundleIds.rodneylavoie.rawValue: return "Club_Profile_Icon_RodneyLavoie"
    case appBundleIds.podcommander.rawValue: return "Club_Profile_Icon_PodCommander"
    case appBundleIds.workishell.rawValue: return "Club_Profile_Icon_WorkisHell"
    case appBundleIds.homicity.rawValue: return "Club_Profile_Icon_Homicity"
    case appBundleIds.crittercarebrothers.rawValue: return "Club_Profile_Icon_Crittercarebrothers"
    case appBundleIds.crittercarebrothers.rawValue: return "Club_Profile_Icon_YASCollagen"
    case appBundleIds.resetwithmiranda.rawValue: return "Club_Profile_Icon_ResetwithMiranda"
    case appBundleIds.everythingtennis.rawValue: return "Club_Profile_Icon_EverythingTennis"
    case appBundleIds.creatorsuncensored.rawValue: return "Club_Profile_Icon_CreatorsUncensored"

    default: return ""
    }
}

func getAppIconImageName() -> String {
    
    let bundleId = Bundle.main.bundleIdentifier
    
    switch bundleId {
    case appBundleIds.sorinClub.rawValue: return "app_icon_sorinclub"
    case appBundleIds.cccClub.rawValue: return "app_icon_cccclub"
    case appBundleIds.volleyBall.rawValue: return "app_icon_volleyball"
    case appBundleIds.actioneer.rawValue: return "app_icon_actioneer"
    case appBundleIds.deenSquadClub.rawValue: return "app_icon_deensquad"
    case appBundleIds.newToCanadaClub.rawValue: return "app_icon_newtocanada"
    case appBundleIds.ictForum.rawValue: return "app_icon_ictforum"
    case appBundleIds.cyberExperts.rawValue: return "app_icon_cyberexperts"
    case appBundleIds.adultify.rawValue: return "app_icon_adultify"
    case appBundleIds.ptsd.rawValue: return "app_icon_ptsd"
    case appBundleIds.ilead.rawValue: return "app_icon_ilead"
    case appBundleIds.yourtechcoach.rawValue: return "app_icon_yourtechcoach"
    case appBundleIds.myhotperks.rawValue: return "app_icon_myhotperks"
    case appBundleIds.iag.rawValue: return "app_icon_iag"
    case appBundleIds.teamloop.rawValue: return "app_icon_teamloop"
    case appBundleIds.virtualhealthcoach.rawValue: return "app_icon_vhc"
    case appBundleIds.vuca.rawValue: return "app_icon_vuca"
    case appBundleIds.socialarchitect.rawValue: return "app_icon_social"
    case appBundleIds.idealwealthgrower.rawValue: return "app_icon_iwg"
    case appBundleIds.pharmakonect.rawValue: return "app_icon_pharmakonect"
    case appBundleIds.nbda.rawValue: return "app_icon_nbda"
    case appBundleIds.inspiredmoms.rawValue: return "app_icon_InspiredMoms"
    case appBundleIds.peersondemand.rawValue: return "app_icon_PeersOnDemand"
    case appBundleIds.sportsmasters.rawValue: return "app_icon_SportsMasters"
    case appBundleIds.engineeryourmission.rawValue: return "app_icon_EngineerYourMission"
    case appBundleIds.bossibly.rawValue: return "app_icon_Bossibly"
    case appBundleIds.caba.rawValue: return "app_icon_CABA"
    case appBundleIds.suttongroup.rawValue: return "app_icon_SG"
    case appBundleIds.nlcapitalgroup.rawValue: return "app_icon_NLCapitalGroup"
    case appBundleIds.ideserveitnow.rawValue: return "app_icon_IDeserveItNow"
    case appBundleIds.urbanfitmag.rawValue: return "app_icon_UrbanFitMag"
    case appBundleIds.risebreakfree.rawValue: return "app_icon_Rise&BreakFree"
    case appBundleIds.rodneylavoie.rawValue: return "app_icon_RodneyLavoie"
    case appBundleIds.podcommander.rawValue: return "app_icon_PodCommander"
    case appBundleIds.workishell.rawValue: return "app_icon_WorkisHell"
    case appBundleIds.homicity.rawValue: return "app_icon_Homicity"
    case appBundleIds.crittercarebrothers.rawValue: return "app_icon_Crittercarebrothers"
    case appBundleIds.crittercarebrothers.rawValue: return "app_icon_YASCollagen"
    case appBundleIds.resetwithmiranda.rawValue: return "app_icon_ResetwithMiranda"
    case appBundleIds.everythingtennis.rawValue: return "app_icon_EverythingTennis"
    case appBundleIds.creatorsuncensored.rawValue: return "app_icon_CreatorsUncensored"

    default: return ""
    }
}
