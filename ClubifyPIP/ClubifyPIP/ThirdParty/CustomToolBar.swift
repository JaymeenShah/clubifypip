//
//  CustomToolBarDelegate.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/10/21.
//

import UIKit
import Foundation

@objc protocol CustomToolBarDelegate {
    @objc optional func getSegmentIndex(segmentIndex : Int,selectedTextField :UITextField)
    func closeKeyBoard()
}

class CustomToolBar: UIToolbar {
    
    var delegate1: CustomToolBarDelegate?
    var txtField = UITextField()
   
    private lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        //#3D6FFF, rgb(61,111,255)
        //#AA39FE, rgb(170,57,254)
        if WHITELABEL_APP_NAME == "Sutton Group" {
            l.colors = [UIColor.init(hex: "D20201").cgColor, UIColor.init(hex: "D20201").cgColor]
        } else {
            
            if WHITELABEL_APP_NAME.isEmpty {
                l.colors = [UIColor.gradientBtnFirst().cgColor, UIColor.gradientBtnSecond().cgColor]
            } else {
                let strStartColor: String = UserDefaults.standard.string(forKey:"StartColor") ?? ColorConstant.clubifyDefaultBtnStartColor
                let strEndColor: String = UserDefaults.standard.string(forKey:"EndColor") ?? ColorConstant.clubifyDefaultBtnEndColor
                let strColorType: String = UserDefaults.standard.string(forKey:"ColorType") ??
                    "1"
//                if strColorType == "1" {
//                    l.colors = [UIColor.init(hex: strStartColor).cgColor, UIColor.init(hex: strEndColor).cgColor]
//                } else {
//                    l.colors = [UIColor.init(hex: strStartColor).cgColor, UIColor.init(hex: strStartColor).cgColor]
//                }

//                if Helper.sharedInstance.isSolidOrGradientColor == "1" {
//                    l.colors = [UIColor.init(hex: Helper.sharedInstance.buttonColorStartColor).cgColor, UIColor.init(hex: Helper.sharedInstance.buttonColorEndColor).cgColor]
//                } else {
//                    l.colors = [UIColor.init(hex: Helper.sharedInstance.buttonColorStartColor).cgColor, UIColor.init(hex: Helper.sharedInstance.buttonColorStartColor).cgColor]
//                }
            }
        }
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        //l.cornerRadius = 16
        layer.insertSublayer(l, at: 0)
        return l
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

     public init(frame: CGRect ,isSegment : Bool) {     // for using CustomView in code

        super.init(frame: frame)
        
        self.frame = frame
        self.barStyle = UIBarStyle.default
        self.isTranslucent = true
        //self.barTintColor = UIColor.appGradiantButton()//AppTheme()
        self.tintColor = UIColor.white

        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
       
        if isSegment {
            previousButton.customView = self.prevNextSegment()
        }
        self.setItems([previousButton, spaceButton, doneButton], animated: false)
        self.isUserInteractionEnabled = true
        self.sizeToFit()
    }
    required init(coder aCoder: NSCoder) {
        super.init(coder: aCoder)!
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.NexaRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSAttributedString.Key.font: barbuttonFont, NSAttributedString.Key.foregroundColor:UIColor.clear], for: .disabled)
        if(DeviceTypes.IS_IPHONE_6P)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 150, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 150, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: .valueChanged)
        return prevNextSegment;
    }
    
    @objc func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            delegate1?.getSegmentIndex!(segmentIndex: 1, selectedTextField: txtField)
        }else
        {
           delegate1?.getSegmentIndex!(segmentIndex:0, selectedTextField: txtField)
        }
    }
    @objc func resignKeyboard()
    {
      delegate1?.closeKeyBoard()
    }
}

@IBDesignable
class UICustomSwitch : UISwitch {

    @IBInspectable var OnColor: UIColor! = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 1.0)//green
    @IBInspectable var OffColor: UIColor! = UIColor.lightGray
//    @IBInspectable var Scale: CGFloat! = 1.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUpCustomUserInterface()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpCustomUserInterface()
    }

    func setUpCustomUserInterface() {

        //clip the background color
        self.layer.cornerRadius = 16
        self.layer.masksToBounds = true

        //Scale down to make it smaller in look
        self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0);

        //add target to get user interation to update user-interface accordingly
        self.addTarget(self, action: #selector(UICustomSwitch.updateUI), for: .valueChanged)

        //set onTintColor : is necessary to make it colored
        self.onTintColor = self.OnColor

        //setup to initial state
        self.updateUI()
    }

    //to track programatic update
    override func setOn(_ on: Bool, animated: Bool) {
        super.setOn(on, animated: true)
        updateUI()
    }

    //Update user-interface according to on/off state
    @objc func updateUI() {
        if self.isOn == true {
            self.backgroundColor = self.OnColor
        }
        else {
            self.backgroundColor = self.OffColor
        }
    }
}
