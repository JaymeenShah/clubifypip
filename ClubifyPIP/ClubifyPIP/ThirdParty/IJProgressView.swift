import UIKit

open class IJProgressView {
    
    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    open class var shared: IJProgressView {
        struct Static {
            static let instance: IJProgressView = IJProgressView()
        }
        return Static.instance
    }
    
    open func showProgressView(_ view: UIView) {
        containerView.frame = view.frame
        appDelegate.isProgress = true
        //        containerView.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.size.width, height: ScreenSize.HEIGHT)
        containerView.center = view.center
        // containerView.backgroundColor = UIColor(hex: 0xffffff, alpha: 0.3)
        
        containerView.isUserInteractionEnabled = false
        containerView.backgroundColor = UIColor.AppTransparentGrey()
        
        progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        progressView.center = view.center
        progressView.backgroundColor = UIColor.black
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.style = .whiteLarge
        activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
        
        progressView.addSubview(activityIndicator)
        containerView.addSubview(progressView)
        view.addSubview(containerView)
        view.bringSubviewToFront(containerView)
        view.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
    }
    
    open func hideProgressView() {
        appDelegate.isProgress = false
        containerView.superview?.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
        containerView.removeFromSuperview()
    }
}

extension UIColor {
    
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
//
//  IJProgressView.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 19/10/21.
//

import Foundation
