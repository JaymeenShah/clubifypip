//
//  ViewController.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.
//

import UIKit

class PageViewController: UIViewController {

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

//MARK:- Functions
extension PageViewController
{
    func setBtnGradientBackground(colorTrail: UIColor, colorLead: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorLead.cgColor, colorTrail.cgColor]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
