//
//  ClubEvent.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/10/21.
//

import Foundation
class ClubEvent: NSObject {
    
    var id : String = ""
    var club_id: String = ""
    var event_start_date: String = ""
    var event_end_date: String = ""
    var name: String = ""
    var desc: String = ""
    var icon : String = ""
    var amount: String = ""
    var created_by: String = ""
    var created: String = ""
    var deleted: String = ""
    var videoLink: String = ""
    var address: String = ""
    var longitude: String = ""
    var latitude: String = ""
    var number_of_member: String = ""
    var number_of_seat_left: String = ""
    var isJoined = Bool()
    var membershipLevel: [MembershipLevel] = []
    var link: String = ""
    var eventId: String = ""
    var points: String = ""
    var caption: String = ""
    var coHostName: String = ""
    var locationName: String = ""
    var upgrade_package: String = ""
    
    override init() {
        
    }
    
    init(dictJSON:NSDictionary) {
        
        self.upgrade_package = dictJSON.getString(key: "upgrade_package")
        self.caption = dictJSON.getString(key: "caption")
        self.coHostName = dictJSON.getString(key: "cohost_name")
        self.locationName = dictJSON.getString(key: "location_name")

        self.points = dictJSON.getString(key: "points")
        self.eventId = dictJSON.getString(key: "event_id")
        self.id = dictJSON.getString(key: "id")
        self.club_id = dictJSON.getString(key: "club_id")
        self.event_start_date = dictJSON.getString(key: "event_start_date")
        self.event_end_date = dictJSON.getString(key: "event_end_date")
        self.name = dictJSON.getString(key: "name")
        self.desc = dictJSON.getString(key: "description")
        self.icon = dictJSON.getString(key: "icon")
        self.amount = dictJSON.getString(key: "amount")
        self.created_by = dictJSON.getString(key: "created_by")
        self.created = dictJSON.getString(key: "created")
        self.deleted = dictJSON.getString(key: "deleted")
        self.videoLink = dictJSON.getString(key: "video_conference_link")
        self.address = dictJSON.getString(key: "address")
        self.longitude = dictJSON.getString(key: "longitude")
        self.latitude = dictJSON.getString(key: "latitude")
        self.number_of_member = dictJSON.getString(key: "number_of_member")
        self.number_of_seat_left = dictJSON.getString(key: "number_of_seat_left")
        self.isJoined = dictJSON.getBool(key: "is_joined")
        self.link = dictJSON.getString(key: "link")

        if let data = dictJSON["membership_level"] as? [[String: Any]] {
            var arrMembershipLevel: [MembershipLevel] = []
            for permData in data {
                arrMembershipLevel.append(MembershipLevel(fromDictionary: permData))
            }
            membershipLevel = arrMembershipLevel
        }

    }
}
