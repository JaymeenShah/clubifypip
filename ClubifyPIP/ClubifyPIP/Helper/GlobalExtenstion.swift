//
//  GlobalExtenstion.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 24/08/21.
//

import Foundation
import UIKit

//MARK:- UIButtonsExtension

extension UIButton
{
    func setBtnGradientBackground(colorLead: UIColor, colorTrail: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorLead.cgColor, colorTrail.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
//MARK:- String extensiokn
extension String {
var isEmail: Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: self)
 }
}
//MARK:- Gradient Backgroundview Extension
extension UIView
{
    func setMainGradientBackground() {
        let colorTop =  UIColor(red: 48.0/255.0, green: 48.0/255.0, blue: 63.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue:0.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = bounds                
        layer.insertSublayer(gradientLayer, at:0)
    }
    func setSeconLayerMainGradientBackground() {
        let colorTop =  UIColor(red: 48.0/255.0, green: 48.0/255.0, blue: 63.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue:0.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at:0)
    }
    func setWhiteGradientBackground() {
        let colorTop =  UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue:255.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at:0)
    }
    
    func setMainSectionGradientBackground() {
        let colorTop =  UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue:0.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at:0)
    }
    func setCellGradientBackground() {
        let colorTop =  UIColor(red: 39.0/255.0, green: 121.0/255.0, blue: 165.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 75/255.0, green: 71.0/255.0, blue:92.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = bounds
                
        layer.insertSublayer(gradientLayer, at:0)
    }
    
    func setMainViewBackground(colorLead: UIColor, colorTrail: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorLead.cgColor, colorTrail.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
func getJsonStringFromDictionary(aParameters : [String : Any]) -> String? {
    var paramJsonString = ""
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: aParameters, options: .prettyPrinted)
        let theJSONText = String(data: jsonData, encoding: .utf8)
        paramJsonString = theJSONText!
    } catch {
        print(error.localizedDescription)
    }
    return paramJsonString
}


//Dictionary Extension
extension NSDictionary {
    
    //MARK: - Get String From Dictionary
    func getString(key:String) -> String {
        
        if let value = self[key] {
            
            let string = NSString.init(format: "%@", value as! CVarArg) as String
            if (string.lowercased() == "null" || string.lowercased() == "nil" || string.lowercased() == "<null>") {
                return ""
            }
            return string.removeWhiteSpace()
        }
        return ""
    }
    //MARK: - Get Bool From Dictionary
    func getBool(key:String) -> Bool {
        
        if let value = self[key] {
            
            let string = NSString.init(format: "%@", value as! CVarArg) as String
            if (string.lowercased() == "null" || string == "nil") {
                return false
            }
            if (string.isNumber) {
                
                return Bool(NSNumber(integerLiteral: Int(string)!))
            } else if (string.lowercased() == "false" || string == "0") {
                return false
                
            } else if (string.lowercased() == "true" || string == "1") {
                return true
                
            } else {
                return false
            }
        }
        return false
    }
    //MARK: - Get Int From Dictionary
    func getInt(key:String) -> Int {
        
        if let value = self[key] {
            
            let string = NSString.init(format: "%@", value as! CVarArg) as String
            if (string == "null" || string == "NULL" || string == "nil") {
                return 0
            }
            
            if (string.isNumber) {
                
                return Int(string)!
            } else {
                return 0
            }
            
        }
        return 0
    }
    //MARK: - Get Double From Dictionary
    func getDouble(key:String) -> Double {
        
        if let value = self[key] {
            
            let string = NSString.init(format: "%@", value as! CVarArg) as String
            if (string == "null" || string == "NULL" || string == "nil") {
                return Double(0.0)
            }
            if (string.isFloat) {
                
                return Double(string)!
            } else {
                return Double(0.0)
            }
        }
        return Double(0.0)
    }
    //MARK: - Get Float From Dictionary
    func getFloat(key:String) -> Float {
        
        if let value = self[key] {
            
            let string = NSString.init(format: "%@", value as! CVarArg) as String
            if (string == "null" || string == "NULL" || string == "nil") {
                return Float(0.0)
            }
            if (string.isFloat) {
                
                return Float(string)!
            } else {
                return Float(0.0)
            }
        }
        return Float(0.0)
    }
    //MARK: - Get Dictionary From Dictionary
    func getDictionary(key:String) -> NSDictionary {
        
        if let value = self[key] as? NSDictionary {
            
            let string = NSString.init(format: "%@", value as CVarArg) as String
            if (string == "null" || string == "NULL" || string == "nil") {
                return NSDictionary()
            }
            return value
        }
        return NSDictionary()
    }
    //MARK: - Get Array From Dictionary
    func getArray(key:String) -> NSArray {
        
        if let value = self[key] as? NSArray {
            
            let string = NSString.init(format: "%@", value as CVarArg) as String
            if (string == "null" || string == "NULL" || string == "nil") {
                return NSArray()
            }
            return value
        }
        return NSArray()
    }
}

public protocol Localizable {
    var localized: String { get }
}


extension String: Localizable {
    public var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
//String Extension

extension String {
    
    //Remove white space in string
    func removeWhiteSpace() -> String {
        //        return self.trimmingCharacters(in: .whitespaces)
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    //Check string is number or not
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
    
    //Check string is Float or not
    var isFloat : Bool {
        get{
            
            if Float(self) != nil {
                return true
            }else {
                return false
            }
        }
    }
    
    //Convert HTML to string
    init?(htmlEncodedString: String) {
        
        guard htmlEncodedString.data(using: .utf8) != nil else {
            return nil
        }
        
        
        self.init("attributedString.string")
    }
   
}

extension UIColor{
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let red = (rgbValue & 0xff0000) >> 16
        let green = (rgbValue & 0xff00) >> 8
        let blue = rgbValue & 0xff
        
        self.init(
            red: CGFloat(red) / 0xff,
            green: CGFloat(green) / 0xff,
            blue: CGFloat(blue) / 0xff, alpha: 1
        )
    }
    
    class func AppTheme() -> UIColor{
        return UIColor(red: 35.0 / 255.0, green: 99.0 / 255.0, blue: 208.0 / 255.0, alpha: 1.0)
    }
    class func LightAppTheme() -> UIColor{
        return UIColor(red: 251.0 / 255.0, green: 117.0 / 255.0, blue: 43.0 / 255.0, alpha: 0.5)
    }
    class func AppSkyGreen() -> UIColor{
        return UIColor(red: 24.0 / 255.0, green: 199.0 / 255.0, blue: 187.0 / 255.0, alpha: 1.0)
    }
    class func AppTransparentGrey() -> UIColor{
        return UIColor(red: 116.0 / 255.0, green: 116.0 / 255.0, blue: 116.0 / 255.0, alpha: 0.4)
    }
    class func AppGrey() -> UIColor{
        return UIColor(red: 213.0 / 255.0, green: 212.0 / 255.0, blue: 213.0 / 255.0, alpha: 1.0)
    }
    class func placeholderColor() -> UIColor{
        return UIColor(red: 201.0 / 255.0, green: 201.0 / 255.0, blue: 206.0 / 255.0, alpha: 1.0)
    }
    class func connectGrey() -> UIColor{
        return UIColor(red: 146.0 / 255.0, green: 150.0 / 255.0, blue: 159.0 / 255.0, alpha: 1.0)
    }
    class func textfieldBorader() -> UIColor{
        return UIColor(red: 216.0 / 255.0, green: 215.0 / 255.0, blue: 215.0 / 255.0, alpha: 1.0)
    }
    class func AppGradiantGreen1() -> UIColor{
        return UIColor(red: 48.0 / 255.0, green: 213.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    }
    class func AppGradiantGreen2() -> UIColor{
        return UIColor(red: 42.0 / 255.0, green: 184.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
    }
    class func AppGradiantBlue1() -> UIColor{
        //        return UIColor(red: 43.0 / 255.0, green: 101.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
        //        return UIColor(red: 78.0 / 255.0, green: 78.0 / 255.0, blue: 93.0 / 255.0, alpha: 1.0)
        //return UIColor(red: 63 / 255.0, green: 64 / 255.0, blue: 80 / 255.0, alpha: 1.0)
        return UIColor.init(hex: "30303F")
        
    }
    
    class func GradientBG() -> UIColor{
        return UIColor(red: 32.0 / 255.0, green: 32.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }
    class func AppGradiantBlue1ForFB() -> UIColor{
        return UIColor(red: 43.0 / 255.0, green: 101.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    class func AppGradiantBlue2ForFB() -> UIColor{
        return UIColor(red: 48.0 / 255.0, green: 144.0 / 255.0, blue: 199.0 / 255.0, alpha: 1.0)
    }
    
    class func AppGradiantBlue2() -> UIColor{
        //return UIColor(red: 78.0 / 255.0, green: 78.0 / 255.0, blue: 93.0 / 255.0, alpha: 1.0)
        //        return UIColor(red: 48.0 / 255.0, green: 144.0 / 255.0, blue: 199.0 / 255.0, alpha: 1.0)
        // return UIColor(red: 48 / 255.0, green: 48 / 255.0, blue: 63 / 255.0, alpha: 1.0)
        //return UIColor(red: 63 / 255.0, green: 64 / 255.0, blue: 80 / 255.0, alpha: 1.0)
        return UIColor.init(hex: "30303F")
        
    }
    class func appTextfieldWhiteBg() -> UIColor{
        return UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    class func appTextfieldTextColor() -> UIColor{
        return UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    class func appsLabelTextColor() -> UIColor{
        return UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    class func switchGreenColor() -> UIColor{
        return UIColor(red: 69.0 / 255.0, green: 211.0 / 255.0, blue: 91.0 / 255.0, alpha: 1.0)
    }
    
    class func appGradiantButton() -> UIColor{
        //return UIColor.init(hex: "00A3D9")//rgb(0,163,217)//#00B3E1
        //        return UIColor.init(red: 0/255, green: 179/255, blue: 225/255, alpha: 1.0)
        return UIColor.init(hex: "00B3E1")
    }
    
    class func appIconsColor() -> UIColor{
        return UIColor.init(hex: "DDDDEC")//#92929E
    }
    
    class func appIconsDarkColor() -> UIColor{
        return UIColor.init(hex: "82828D")//"DDDDEC")//#92929E
    }
    
    //DDDDEC
    //Course Gradient Colors
    
    //Sky Blue
    class func courseSkyBlueTop() -> UIColor{
        return UIColor.init(hex: "007DB4")
    }
    class func courseSkyBlueBottom() -> UIColor{
        return UIColor.init(hex: "00A1DA")
    }
    
    //Yellow Ocher
    class func courseYellowOcherTop() -> UIColor{
        return UIColor.init(hex: "9E854B")
    }
    class func courseYellowOcherBottom() -> UIColor{
        return UIColor.init(hex: "D8B061")
    }
    
    //Violet
    class func courseVioletTop() -> UIColor{
        return UIColor.init(hex: "8C05BA")
    }
    class func courseVioletBottom() -> UIColor{
        return UIColor.init(hex: "C222F6")
    }
    
    //Slate Gray
    class func courseSlateGrayTop() -> UIColor{
        return UIColor.init(hex: "708798")
    }
    class func courseSlateGrayBottom() -> UIColor{
        return UIColor.init(hex: "89A7BA")
    }
    
    //Dark Green
    class func courseDarkGreenTop() -> UIColor{
        return UIColor.init(hex: "6D836F")
    }
    class func courseDarkGreenBottom() -> UIColor{
        return UIColor.init(hex: "88B18D")
    }
    
    //Red
    class func courseRedTop() -> UIColor{
        return UIColor.init(hex: "A12C30")
    }
    class func courseRedBottom() -> UIColor{
        return UIColor.init(hex: "D0363A")
    }
    
    //l.colors = [UIColor.init(hex: "3D6FFF").cgColor, UIColor.init(hex: "AA39FE").cgColor]
    class func gradientBtnFirst() -> UIColor{
        return UIColor.init(hex: ColorConstant.clubifyDefaultBtnStartColor)
    }

    class func gradientBtnSecond() -> UIColor{
        return UIColor.init(hex: ColorConstant.clubifyDefaultBtnEndColor)
    }

}
struct ColorConstant {
    static let blackColor = UIColor.black;
    static let clubifyDefaultBtnStartColor = "3D6FFF"
    static let clubifyDefaultBtnEndColor = "AA39FE"
    static let clubifyDefaultBtnColor = "FFFFFF"
    static var clubifyDefaultHighLightColor = "A667FF"//"3C3D57"//"5A426A"//"A667FF"

}
