//
//  RegisterViewController + Extension.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 24/08/21.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

//MARK:-Actions
extension RegisterViewController
{
    
    //..backbuttonAction
    @IBAction func touchOnBack(_ sender: Any) {
        self.removeAnimate()
        
    }
    
    @IBAction func validateEmailField(_ sender: SkyFloatingLabelTextField) {
        validateEmailTextFieldWithText(email: txtEmail.text)
        
    }
    
    @IBAction func validateUserNameField(_ sender: SkyFloatingLabelTextField) {
        validateUserNameTextFieldWithText(UserName: txtUserName.text)
    }
    
    
    @IBAction func validateFirstNameField(_ sender: SkyFloatingLabelTextField) {
        validateFirstNameTextFieldWithText(firstName: txtFirstName.text)
    }
    
    
    @IBAction func validateLastNameField(_ sender: SkyFloatingLabelTextField) {
        validateLastNameTextFieldWithText(lastName: txtLastName.text)
    }
    
    
    @IBAction func validatePasswordField(_ sender: SkyFloatingLabelTextField) {
        validatePasswordTextFieldWithText(password: txtPassword.text)
    }
    
    @IBAction func validateConfirmPasswordField(_ sender: SkyFloatingLabelTextField) {
        validateConfirmTextFieldWithText(confirmPassword: txtConfirmPassword.text)
    }
    
    @IBAction func validateOptionalInformation(_ sender: SkyFloatingLabelTextField) {
        validateOptionalInformationWithText(optionalInformation: txtOptionlInformation.text)
    }
    
    @IBAction func touchOnRegisterAccount(_ sender: UIButton) {
        if txtEmail.text == ""
        {
            txtEmail.errorMessage = "Please Enter Required filled.."
        }
        if txtUserName.text == ""
        {
            txtUserName.errorMessage = "Please Enter Required filled.."
        }
        if txtFirstName.text == ""
        {
            txtFirstName.errorMessage = "Please Enter Required filled.."
        }
        if txtLastName.text == ""
        {
            txtLastName.errorMessage = "Please Enter Required filled.."
        }
        if txtPassword.text == ""
        {
            txtPassword.errorMessage = "Please Enter Required filled.."
        }
        if txtConfirmPassword.text == ""
        {
            txtConfirmPassword.errorMessage = "Please Enter Required filled.."
        }
        if txtOptionlInformation.text == ""
        {
            txtOptionlInformation.errorMessage = "Please Enter Required filled.."
        }
        if txtOptionlInformation.text == dropDown.text
        {
            txtOptionlInformation.errorMessage = " "
            txtOptionlInformation.title = "Optional Information"
        }
        setRegisterAPICall(username: txtUserName.text!, email: txtEmail.text!, password: txtPassword.text!, firstname: txtFirstName.text!, lastname: txtLastName.text!, gender: "Male")
    }
}


//MARK:-  Funcions
extension RegisterViewController
{
    //MARK:-..textfield Delegate function

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Validate the email field
        if textField == txtEmail
        {
            validateEmailField(txtUserName)
        }
        else if textField == txtUserName {
            
        }
        else if textField == txtFirstName {
            
        }
        else if textField == txtLastName {
            
        }
        else if textField == txtPassword
        {
            validatePasswordField(txtPassword)
        }
        else if textField == txtConfirmPassword {
            
        }
        // When pressing return, move to the next field
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    func buttonLayouts()
    {
        buttonRegisterAccount.layer.masksToBounds = true
        buttonRegisterAccount.layer.cornerRadius = 25
        
        buttonRegisterAccount.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green:107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
    }
    
//MARK:-..animate Pop up view function
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    //MARK:-..remove Pop up view function

    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
    //MARK:- ..Email validation function

    func validateEmailTextFieldWithText(email: String?) {
        guard let email = email else {
            txtEmail.errorMessage = nil
            txtEmail.title = "Email"
            txtEmail.selectedTitle = "Email"
            
            return
        }
        
        if email.isEmpty {
            txtEmail.errorMessage = nil
        } else if !validateEmail(email) {
            txtEmail.errorMessage = NSLocalizedString(
                "Email not valid",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtEmail.errorMessage = nil
            txtEmail.title = "Email"
            txtEmail.selectedTitle = "Email"
        }
    }
    //MARK:- ..UserName validation function

    func validateUserNameTextFieldWithText(UserName: String?) {
        guard let UserName = UserName else {
            txtUserName.errorMessage = nil
            txtUserName.title = "Username"
            txtUserName.selectedTitle = "Username"
            
            return
        }
    
        if UserName.isEmpty {
            txtUserName.errorMessage = nil
        } else if txtUserName.text == "" {
            txtUserName.errorMessage = NSLocalizedString(
                "Please Enter Required Field**",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtUserName.errorMessage = nil
            txtUserName.title = "Username"
            txtUserName.selectedTitle = "Username"
        }
    }
    //MARK:- FirstName validation function

    func validateFirstNameTextFieldWithText(firstName: String?) {
        guard let firstName = firstName else {
            txtFirstName.errorMessage = nil
            txtFirstName.title = "First Name"
            txtFirstName.selectedTitle = "First Name"
            
            return
        }
        
        if firstName.isEmpty {
            txtFirstName.errorMessage = nil
        } else if txtFirstName.text == "" {
            txtFirstName.errorMessage = NSLocalizedString(
                "Please Enter Required Field**",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtFirstName.errorMessage = nil
            txtFirstName.title = "First Name"
            txtFirstName.selectedTitle = "First Name"
            
        }
        
    }
    //MARK:-..Last Name validation function

    func validateLastNameTextFieldWithText(lastName: String?) {
        guard let lastName = lastName else {
            txtLastName.errorMessage = nil
            txtLastName.title = "Last Name"
            txtLastName.selectedTitle = "Last Name"
            
            return
        }
        
        if lastName.isEmpty {
            txtLastName.errorMessage = nil
        } else if txtLastName.text == "" {
            txtLastName.errorMessage = NSLocalizedString(
                "Please Enter Required Field**",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtLastName.errorMessage = nil
            txtLastName.title = "Last Name"
            txtLastName.selectedTitle = "Last Name"
            
        }
        
    }
    
    //MARK:- Password Cofirmation validation function

    func validatePasswordTextFieldWithText(password: String?) {
        guard let password = password else {
            txtPassword.errorMessage = nil
            txtPassword.title = "Password"
            txtPassword.selectedTitle = "Password"

            return
        }
        
        if password.isEmpty {
            txtPassword.errorMessage = nil
        } else if password.count < 6 {
            txtPassword.errorMessage = NSLocalizedString(
                "Password Must be Atleast sixth characters",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtPassword.errorMessage = nil
            txtPassword.title = "Password"
            txtPassword.selectedTitle = "Password"

        }
        
        
    }
    //MARK:- Confirm password validation function

    func validateConfirmTextFieldWithText(confirmPassword: String?) {
        guard let confirmPassword = confirmPassword else {
            txtConfirmPassword.errorMessage = nil
            txtConfirmPassword.title = "Confirm Password"
            txtConfirmPassword.selectedTitle = "Confirm Password"
            
            return
        }
        
        if confirmPassword.isEmpty {
            txtConfirmPassword.errorMessage = nil
        } else if txtConfirmPassword.text == ""
        {
            txtConfirmPassword.errorMessage = NSLocalizedString(
                "Please Enter Required Field**",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        }
        else if txtPassword.text != txtConfirmPassword.text
        {
            txtConfirmPassword.errorMessage = NSLocalizedString(
                "Confirm Password was wrong",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
            }
        
        else if confirmPassword.count < 6 {
            txtPassword.errorMessage = NSLocalizedString(
                "Password Must be Atleast sixth characters",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        }
        else {
            txtConfirmPassword.errorMessage = nil
            txtConfirmPassword.title = "Confirm Password"
            txtConfirmPassword.selectedTitle = "Confirm Password"
            
        }
        
        
    }
    
    //MARK:- Password Cofirmation validation function

    func validateOptionalInformationWithText(optionalInformation: String?) {
        guard let optionalInformation = optionalInformation else {
            txtOptionlInformation.errorMessage = nil
            txtOptionlInformation.title = "Optional Information"
            txtOptionlInformation.selectedTitle = "Optional Information"

            return
        }
        
        if optionalInformation.isEmpty {
            txtOptionlInformation.errorMessage = nil
        }
        if txtOptionlInformation.text == dropDown.text
        {
            txtOptionlInformation.errorMessage = nil
            txtOptionlInformation.title = "Optional Information"

        }
        else {
            txtOptionlInformation.errorMessage = nil
            txtOptionlInformation.title = "Optional Information"
            txtOptionlInformation.selectedTitle = "Optional Information"

        }
        
        
    }
    // MARK: - validation
    
    func validateEmail(_ candidate: String) -> Bool {
        
        // NOTE: validating email addresses with regex is usually not the best idea.
        // This implementation is for demonstration purposes only and is not recommended for production use.
        // Regex source and more information here: http://emailregex.com
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
}
