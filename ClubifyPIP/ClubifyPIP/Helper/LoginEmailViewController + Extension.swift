//
//  LoginEmailViewController + Extension.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 24/08/21.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

//MARK:- TextfieldDelegate
extension LoginEmailViewController
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Validate the email field
        if textField == txtUserName {
            validateEmailField(txtUserName)
        }
        
        if textField == txtPassword
        {
            validatePasswordField(txtPassword)
        }
        // When pressing return, move to the next field
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }

}


//MARK:- Actions

@available(iOS 13.0, *)
extension LoginEmailViewController{
    //..RegisterbuttonAction
    @IBAction func touchOnRegisterNow(_ sender: UIButton) {
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        
        self.addChild(popvc)
        
        popvc.view.frame = self.view.frame
        
        self.view.addSubview(popvc.view)
        
        popvc.didMove(toParent: self)
    }
    
    @IBAction func touchOnLoginButton(_ sender: UIButton) {
        APICall()
    
        let storyboards = UIStoryboard(name: "DiscoverClub", bundle: nil)
        let nextVc = storyboards.instantiateViewController(identifier: "myClubsViewController") as! myClubsViewController
        self.navigationController?.pushViewController(nextVc, animated: true)
        
    }
    @IBAction func touchOnForgotPassword(_ sender: UIButton) {
        self.removeAnimate()
    }
    
    @IBAction func validateEmailField(_ sender: SkyFloatingLabelTextField) {
        validateEmailTextFieldWithText(email: txtUserName.text)
    }
    
    
    @IBAction func validatePasswordField(_ sender: SkyFloatingLabelTextField) {
        validatePasswordTextFieldWithText(password: txtPassword.text)
    }
}
//MARK:- Functions
extension LoginEmailViewController
{
    
    func buttonLayouts()
    {
        
        // Do any additional setup after loading the view.
        loginEmailPopView.layer.masksToBounds = true
        loginEmailPopView.layer.cornerRadius = 12
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        loginButton.setBtnGradientBackground(colorLead: UIColor.init(displayP3Red: 81/255, green: 107/255, blue: 250/255, alpha: 1.0), colorTrail: UIColor.init(displayP3Red: 162/255, green: 71/255, blue:250/255, alpha: 1.0))
        
        loginButton.layer.cornerRadius = 22.5
        loginButton.layer.masksToBounds = true
     
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
    // MARK:-  Email  validation

    func validateEmailTextFieldWithText(email: String?) {
        guard let email = email else {
            txtUserName.errorMessage = nil
            txtUserName.title = "USERNAME"
            txtUserName.selectedTitle = "USERNAME"

            return
        }

        if email.isEmpty {
            txtUserName.errorMessage = nil
        } else if !validateEmail(email) {
            txtUserName.errorMessage = NSLocalizedString(
                "Email not valid",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtUserName.errorMessage = nil
            txtUserName.title = "USERNAME"
            txtUserName.selectedTitle = "USERNAME"

        }

    }
    // MARK:- Password  validation

    
    func validatePasswordTextFieldWithText(password: String?) {
        guard let password = password else {
            txtPassword.errorMessage = nil
            return
        }

        if password.isEmpty {
            txtPassword.errorMessage = nil
        } else if password.count < 6 {
            txtPassword.errorMessage = NSLocalizedString(
                "Password Must be Atleast sixth characters",
                tableName: "SkyFloatingLabelTextField",
                comment: " "
            )
        } else {
            txtPassword.errorMessage = nil
            txtPassword.title = "PASSWORD"
        }
    }
    
    
    // MARK: - validation

    func validateEmail(_ candidate: String) -> Bool {

        // NOTE: validating email addresses with regex is usually not the best idea.
        // This implementation is for demonstration purposes only and is not recommended for production use.
        // Regex source and more information here: http://emailregex.com

        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
}


