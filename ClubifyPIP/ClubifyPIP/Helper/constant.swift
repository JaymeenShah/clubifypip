//
//  constant.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 25/08/21.
//

import Foundation
import UIKit

let App_Delegate = UIApplication.shared.delegate as! AppDelegate
let UserDefault = UserDefaults.standard
let screenSize: CGRect = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
var sessionId                   = "sessionId"
let selectedCity                = "selectedCity"
let visitCountOfFavourite       = "visitCountOfFavourite"

let Terms_Condition = "https://www.muskegonnightout.com/terms"
let P_Policy = "https://www.muskegonnightout.com/privacy"

struct ScreenSize
{
    
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
struct DeviceTypes
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_6S = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6S_P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_7 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_8 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_8P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_SE = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    
    static var IS_IPHONE_XS  =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static var IS_IPHONE_12_MINI  =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static var IS_IPHONE_12  =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 844.0
    static var IS_IPHONE_12_PRO_MAX  =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 926.0
    static var IS_IPHONE_11_PRO  =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1125.0

    static var isiPhoneX_OR_XS  :   Bool   = 812 == UIScreen.main.bounds.size.height ? true:false
    static var isiPhoneXR_OR_XSMAX  :   Bool   = 896 == UIScreen.main.bounds.size.height ? true:false
    static let IS_IPAD_AIR = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1024
    static let IS_IPAD_MINI = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1024
    static let IS_IPAD_PRO = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1336.0
}
//MARK:- Create Dictionary
func creatDictnory(value: AnyObject) -> NSMutableDictionary
{
    var tempDict = NSMutableDictionary()
    
    if let DictData: NSDictionary = value as? NSDictionary
    {
        tempDict = NSMutableDictionary.init()
        tempDict.addEntries(from:DictData as! [AnyHashable : Any])
    }
    else if let _: NSNull = value as? NSNull
    {
        tempDict = NSMutableDictionary.init()
    }
    
    return tempDict
}
//MARK: - check string nil
func createString(value: AnyObject) -> String
{
    var returnString: String = ""
    if let str: String = value as? String
    {
        returnString = str
    }
    else if let str: Int = value as? Int
    {
        returnString = String.init(format: "%d", str)
    }
        
    else if let _: NSNull = value as? NSNull
    {
        returnString = String.init(format: "")
    }
    return returnString
}
//MARK: - Font Layout
struct FontName {
    //Font Name List
    static let DKNewBeginnings = "DKNewBeginnings"
    static let RobotoRegular = "Roboto-Regular"
    static let NexaBold = "Nexa-Bold"
    static let NexaRegular = "NexaRegular"
    
    static let SFProDisplayLight = "SFProDisplay-Light"
    static let SFProDisplaySemibold = "SFProDisplay-Semibold"
    static let SFProDisplayMedium = "SFProDisplay-Medium"
    static let SFProDisplayRegular = "SFProDisplay-Regular"
    static let SFProDisplayBold = "SFProDisplay-Bold"
    static let SFProDisplayHeavy = "SFProDisplay-Heavy"
    static let SFProTextRegular = "SFProText-Regular"
    static let SFProTextSemibold = "SFProText-Semibold"
    static let SFProTextBold = "SFProText-Bold"
    static let SFProTextMedium = "SF-Pro-Text-Medium"

    static let fontNavBar: UIFont? = UIFont(name: SFProTextBold, size: 15.0)
    static let fontNavBarIPad: UIFont? = UIFont(name: SFProTextBold, size: 22.0)
    
    static let fontButton: UIFont? = UIFont(name: NexaBold, size: 18.0)
    static let fontButtonIPad: UIFont? = UIFont(name: NexaBold, size: 30.0)
    
}
