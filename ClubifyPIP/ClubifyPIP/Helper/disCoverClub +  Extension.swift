//
//  disCoverClub +  Extension.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 25/08/21.
//

import Foundation
import UIKit
import SDWebImage

//MARK:- CollectionviewDelegate and CollectionviewDatasource

extension discoverClubVc:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch  collectionView {
        case collectionViewHasTag:

            return arrClubData.count
        case collectionViewDiscover:

            return arrClubData.count

        default:
            return arrClubData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch  collectionView {
        case collectionViewHasTag:
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "hashTagCell", for: indexPath) as! hashTagCell
            cellA.lblHashTag.text = arrClubData[indexPath.row].name            
            return cellA
        case collectionViewDiscover:
            
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "dicoverClubCell", for: indexPath) as! dicoverClubCell
            cellA.lblTitle.text =  arrClubData[indexPath.row].name
            cellA.lblSubtitle.text =  arrClubData[indexPath.row].subtitle
            cellA.lblDescription.text =  arrClubData[indexPath.row].description
            print(arrClubData[indexPath.row].profile_image)
            if let url: NSURL = NSURL(string: arrClubData[indexPath.row].profile_image) {
                cellA.imgProfile.sd_setImage(with: url as URL) { (image, error, cache, urls) in
                    if (error != nil) {
                        cellA.imgProfile.image = UIImage(named: "placeholder")
                    } else {
                        cellA.imgProfile.image = image
                    }
                }
            }
            return cellA
        default:
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "dicoverClubCell", for: indexPath) as! dicoverClubCell
            return cellA
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView
        {
        case collectionViewDiscover:
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "joinClubVc") as! joinClubVc
            self.navigationController?.pushViewController(controller, animated: true)
            
        default:
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "joinClubVc") as! joinClubVc
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    
}
//MARK:- CollectionviewFlowLayoutDelegate
extension discoverClubVc:UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case collectionViewHasTag:
            if DeviceTypes.IS_IPHONE_12
            {
                return CGSize(width: 95, height: 40)
                
            }
            else{
                return CGSize(width: 90, height: 40)
                
            }
        case collectionViewDiscover:
            
            if  DeviceTypes.IS_IPHONE_12
            {
                return CGSize(width: 175, height: 255)
                
            }
            
            else
            {
                return CGSize(width: 175, height: 255)
                
            }
            
        default:
            let calWidth = collectionView.frame.size.width / 2 - 20
            let calHeight = collectionView.frame.size.height / 1.5
            return CGSize(width: calWidth, height: calHeight)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        switch collectionView {
        case collectionViewDiscover:
            if DeviceTypes.IS_IPHONE_12
            {
                return 5
                
            }
            else
            {
                return 5
            }
        default:
            return 0
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        switch collectionView {
        
        case collectionViewDiscover:
            if DeviceTypes.IS_IPHONE_12
            {
                return 5
                
            }
            return 0
       
        default:
            return 0
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        switch collectionView {
        case collectionViewHasTag:
            if DeviceTypes.IS_IPHONE_12
            {
                return UIEdgeInsets(top: 3, left:12, bottom: 8, right: -14)
                
            }
            return UIEdgeInsets(top: 3, left:5, bottom: 8, right: 14)
            
        case collectionViewDiscover:
            if DeviceTypes.IS_IPHONE_12
            {
                return UIEdgeInsets(top: 3, left:14, bottom: 8, right: 14)
                
            }
            return UIEdgeInsets(top: 18, left: 0, bottom: 8, right: 0)
            
        default:
            
            return UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        }
    }
    
}
