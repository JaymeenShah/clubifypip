//
//  enum.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 01/09/21.
//

import Foundation
//MARK:- Defining Base URL
enum baseUrl : String {
    case production = "https://api.clubify.com"
    case development = "http://apidev.clubify.com"
    case uat = "http://apiut.clubify.com"
}
enum appTheme : String {
    case Light = "1"
    case Dark = "0"
}
