//
//  Helper.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 08/10/21.
//

import Foundation
import UIKit
import AVFoundation
import QuartzCore
import Alamofire

//MARK: - Current Open Screen For Push Notification
enum CurrentOpenScreen : String {
    case ClubFeed = "ClubFeed"
    case Wallet = "Wallet"
    case ClubEvents = "ClubEvent"
    case ClubListHome = "ClubListHome"
    case InviteReceive = "InviteReceive"
    case JoinClubDetail = "JoinClubDetail"
    case VisualVoicemail = "VisualVoicemail"
    case ClubFeedDetail = "ClubFeedDetail"
    case FeedLiked = "FeedLiked"
    case FeedCommetCreated = "feedCommetCreated"
    case FeedCommetReplyCreated = "feedCommetReplyCreated"
    case GalleryCreated = "galleryCreated"
    case GalleryResourceCreated = "galleryResourceCreated"
    case ProductCreated = "productCreated"
    case ContestCreated = "contestCreated"
    case NewMessage = "NewMessage"
    case CommentReplyLiked = "commentReplyLiked"
    case ProductPurchased = "productPurchased"

}
func setHeadersForAPI() -> HTTPHeaders {
    print("focus_club_id---- \(getWhiteLabelAppId())")
    let headers: HTTPHeaders = ["Content-Type":"application/json" ,
                                "X-authorization" : getUserAuthToken(),
                                "focus_club_id": getWhiteLabelAppId()]
    return headers
}
func getWhiteLabelAppId() -> String {
    return WHITELABEL_APP_ID.isEmpty ? "1" : WHITELABEL_APP_ID
}
//MARK: - User Authentication Token
// Fetch User Authentication Token
func getUserAuthToken() -> String {
    
    //    if !UserDefaults.Main.string(forKey: .sessionToken).isEmpty {
    //        let authToken: String = UserDefaults.Main.string(forKey: .sessionToken)
    ////        AppState.sharedInstance.user?.sessionToken = authToken
    ////        let sessionToken = AppState.sharedInstance.user?.sessionToken ?? "0"
    //        print("sessionToken---- \(sessionToken)")
    //        return sessionToken
    //    }
    return "0"
}


extension UIImageView {

    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
        self.tintColorDidChange()
    }
    
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}

extension String {
    
    var convertToPrice: String? {
        guard let value = Double(self) else { return self }
        let divided = (value / 100) * 100
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencySymbol = ""
        //currencyFormatter.decimalSeparator = "."
        currencyFormatter.minimumFractionDigits = 0//(value.contains(".00")) ? 0 : 2
        currencyFormatter.maximumFractionDigits = 2
        
        if let priceString = currencyFormatter.string(for: divided) {
            return priceString
        }
        return self
    }
    
    func decode() -> String {
        let data = self.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII) ?? self
    }
    
    func encode() -> String {
        let data = self.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decodeUsingText() -> String {
        let data = self.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII) ?? self
    }
    
    func encodeUsingText() -> String {
        let data = self.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }

}

@IBDesignable
class GradientBGView: UIView {
    
    @IBInspectable var startColor:   UIColor = .AppGradiantBlue1() { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .black { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override public class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    var isDarkTheme = false
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        if !isDarkTheme {
            let strTheme: String = UserDefaults.standard.string(forKey:"lightTheme") ?? ""
            if !strTheme.isEmpty {
                let isLightTheme = UserDefaults.standard.string(forKey:"lightTheme")
                if isLightTheme == "1" {
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
                    gradientLayer.colors = [UIColor.white.cgColor, UIColor.white.cgColor]
                } else {
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
                    gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
                }
            } else {
                UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
                UserDefaults.standard.set("0", forKey: "lightTheme")
            UserDefaults.standard.synchronize()
                gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            }
        } else {
            gradientLayer.colors = [UIColor.AppGradiantBlue1().cgColor, UIColor.black.cgColor]
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}

//MARK:  Add your userdefault key here
extension UserDefaults {
    
    struct Main : UserDefaultable {
        private init() { }
        
        enum BoolDefaultKey : String {
            case autoLogin
            case isSplash
            case storeLoginInfo
            case isToggleList
            case isToggleMyClubs
        }
        
        enum FloatDefaultKey:String {
            case floatKey
        }
        
        enum DoubleDefaultKey: String {
            case doubleKey
        }
        
        enum IntegerDefaultKey: String {
            case IntKey
        }
        
        enum StringDefaultKey: String {
            case username
            case password
            case deviceToken
            case baseURL
            case lightTheme
            case sessionToken
            case appHightLightColor
            
        }
        
        enum URLDefaultKey: String {
            case urlKey
        }
        
        enum ObjectDefaultKey: String {
            case Profile
            case ChatUser
        }
    }
}
// MARK: -  User Defaults Types
protocol UserDefaultable : KeyNamespaceable {
    
    associatedtype BoolDefaultKey : RawRepresentable
    associatedtype FloatDefaultKey : RawRepresentable
    associatedtype StringDefaultKey : RawRepresentable
    associatedtype IntegerDefaultKey : RawRepresentable
    associatedtype ObjectDefaultKey : RawRepresentable
    associatedtype DoubleDefaultKey : RawRepresentable
    associatedtype URLDefaultKey : RawRepresentable
    
}
protocol KeyNamespaceable {
    
}

extension KeyNamespaceable {
    private static func namespace(_ key: String) -> String {
        return "\(Self.self).\(key)"
    }
    
    static func namespace<T: RawRepresentable>(_ key: T) -> String where T.RawValue == String {
        return namespace(key.rawValue)
    }
}
extension UserDefaultable where BoolDefaultKey.RawValue == String,FloatDefaultKey.RawValue == String,StringDefaultKey.RawValue == String, IntegerDefaultKey.RawValue == String,ObjectDefaultKey.RawValue == String,DoubleDefaultKey.RawValue == String,URLDefaultKey.RawValue == String {
    
    static func set(_ bool: Bool, forKey key: BoolDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(bool, forKey: key)
    }
    
    static func bool(forKey key: BoolDefaultKey) -> Bool {
        let key = namespace(key)
        return UserDefaults.standard.bool(forKey: key)
    }
    
    static func set(_ float: Float, forKey key: FloatDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(float, forKey: key)
    }
    
    static func float(forKey key: FloatDefaultKey) -> Float {
        let key = namespace(key)
        return UserDefaults.standard.float(forKey: key)
    }
    static func set(_ string: String, forKey key: StringDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(string, forKey: key)
    }
    
    static func string(forKey key: StringDefaultKey) -> String {
        let key = namespace(key)
        return UserDefaults.standard.string(forKey:key) == nil ? "" : UserDefaults.standard.string(forKey:key)!
        // When getting nil its return double quotes
    }
    
    static func set(_ integer: Int, forKey key: IntegerDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(integer, forKey: key)
    }
    
    static func integer(forKey key: IntegerDefaultKey) -> Int {
        let key = namespace(key)
        return UserDefaults.standard.integer(forKey: key)
    }
    static func set(_ object: Any, forKey key: ObjectDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(object, forKey: key)
    }
    
    static func object(forKey key: ObjectDefaultKey) -> Any? {
        let key = namespace(key)
        return UserDefaults.standard.object(forKey: key)
    }
    
    static func set(_ double: Double, forKey key: DoubleDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(double, forKey: key)
    }
    
    static func double(forKey key: DoubleDefaultKey) -> Double {
        let key = namespace(key)
        return UserDefaults.standard.double(forKey: key)
    }
    
    static func set(_ url: URL, forKey key: URLDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(url, forKey: key)
    }
    
    static func url(forKey key: URLDefaultKey) -> URL? {
        let key = namespace(key)
        return UserDefaults.standard.url(forKey: key)
    }
    
    // For Remove User Default
    static func removeObj(forKey key:BoolDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey:key)
    }
    static func removeObj(forKey key:FloatDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey:key)
    }
    static func removeObj(forKey key:StringDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey:key)
    }
    static func removeObj(forKey key:IntegerDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey:key)
    }
    static func removeObj(forKey key:ObjectDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey:key)
    }
    static func removeObj(forKey key:DoubleDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey:key)
    }
    static func removeObj(forKey key:URLDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey:key)
    }
}


func creatArray(value: AnyObject) -> NSMutableArray
{
    var tempArray = NSMutableArray()
    
    if let arrData: NSArray = value as? NSArray
    {
        tempArray = NSMutableArray.init(array: arrData)
    }
    else if let _: NSNull = value as? NSNull
    {
        tempArray = NSMutableArray.init()
    }
    
    return tempArray
}
//MARK:- Localized Functions
struct LocalizeKeys {
    
    //Common
    static let yes : String = "Yes".localized
    static let no : String = "No".localized
    static let ok : String = "OK".localized
    static let cancel : String = "Cancel".localized
    static let locContinue : String = "Continue".localized //Added loc cause continue can't be used as identifire.
    static let success : String = "Success".localized
    static let alert : String = "Alert".localized
    static let upgradeLife : String = "Upgrade Your Life".localized
    
    static let functionalityUnderDev : String = "Functionality_Under_Development".localized
    static let later : String = "Later".localized
    static let update : String = "Update".localized
    
    //ClubFeedVC
    static let blockUser : String = "Block User".localized
    static let mute : String = "Mute".localized
    static let report : String = "Report".localized
    static let mutePostTitle : String = "Mute_Post_Title".localized
    static let mutePostDesc : String = "Mute_Post_Desc".localized
    static let reportPostTitle : String = "Report_Post_Title".localized
    static let reportPostDesc : String = "Report_Post_Desc".localized
    static let alertNoRights : String = "Alert_No_Rights".localized
    static let edit : String = " Edit ".localized
    static let delete : String = "Delete".localized
    static let message : String = "Message".localized
    
    
    //MemberList
    static let removeUserAlertTitle : String = "Remove_User_Title".localized
    static let removeUserAlertDesc : String = "Remove_User_Desc".localized
    
    //JoinClubDetailsVC
    static let request : String = "Request".localized
    static let requestAccessDescription : String = "Request_Access_Description".localized
    static let requestAccessSent : String = "Request_Access_Sent".localized
    static let requestAccessSentPopUpAlert : String = "Request_Access_Sent_PopUp_Alert".localized
    
    //Create Event
    static let alertAddManualAddress : String = "Alert_Add_Manual_Address".localized
    static let useGoogleMaps : String = "Use_Google_Maps".localized
    
    //Edit Side Menu
    static let UpdateMenuTitle : String = "Update_menu_title".localized
    static let saveSideMenuChangesAlert : String = "Save_SideMenu_Changes_Alert".localized
    
    //Create Update Club
    static let alertBlankClubCategory : String = "Alert_Blank_Club_Category".localized
    
    //Invitation VC
    static let alertUpgradePowerUp : String = "Alert_Upgrade_PowerUp".localized
}
//MARK: - alert sheet function
func showAlertAction(_ viewController : UIViewController ,title : String, message : String, cancelTitle : String, cancelCompletion : @escaping (()->()), okTitle : String, okCompletion : @escaping (()->())) {
    let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .alert)
    if !cancelTitle.isEmpty{
        let alertCancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (UIAlertAction) in
            cancelCompletion()
        }
        actionSheet.addAction(alertCancelAction)
    }
    
    if !okTitle.isEmpty{
        let alertokAction = UIAlertAction(title: okTitle, style: .default) { (UIAlertAction) in
            okCompletion()
        }
        actionSheet.addAction(alertokAction)
    }
    
    viewController.present(actionSheet, animated: true, completion: nil)
}

//MARK: - Fetching Club Id
func getClubId() -> String {
    let clubId = AppState.sharedInstance.ClubList?.id ?? "1"
    return clubId
    
}
func setNoDataFoundLabelString(firstLine: String, secondLine: String)-> NSMutableAttributedString {
    
    let strFirstLine = firstLine + "\r\r"
    
    var fontFirst: UIFont?
    var fontSecond: UIFont?
    fontFirst = UIFont.init(name: FontName.SFProTextBold, size: 16)
    fontSecond = UIFont.init(name: FontName.SFProTextRegular, size: 12)
    //    if checkIfiPad() {
    //        fontFirst = UIFont.init(name: FontName.SFProTextBold, size: 20)
    //        fontSecond = UIFont.init(name: FontName.SFProTextRegular, size: 16)
    //        
    //    } else {
    //        fontFirst = UIFont.init(name: FontName.SFProTextBold, size: 16)
    //        fontSecond = UIFont.init(name: FontName.SFProTextRegular, size: 12)
    //        
    //    }
    var firstColor: UIColor?
    var secondColor: UIColor?
    
    let strTheme: String = UserDefaults.standard.string(forKey:"lightTheme") ?? ""
    if !strTheme.isEmpty {
        let isLightTheme = UserDefaults.standard.string(forKey:"lightTheme")
        if isLightTheme == "1" {
            firstColor = UIColor.black
            secondColor = UIColor.darkGray
        } else {
            firstColor = UIColor.white
            secondColor = UIColor.lightText
        }
    } else {
        UserDefaults.standard.set("0", forKey: "lightTheme")
            UserDefaults.standard.synchronize()
        firstColor = UIColor.white
        secondColor = UIColor.lightText
    }
    
    let attributedText = NSMutableAttributedString(string: strFirstLine, attributes: [NSAttributedString.Key.font: fontFirst!, NSAttributedString.Key.foregroundColor: firstColor!])
    
    attributedText.append(NSAttributedString(string: secondLine, attributes: [NSAttributedString.Key.font: fontSecond!, NSAttributedString.Key.foregroundColor: secondColor]))
    return attributedText
}
var alertokAction = UIAlertAction()
var textFieldCoupon: UITextField?
func showAlert(title: NSString, message: String) {
    
    let alertController = UIAlertController(title: title as String, message: message, preferredStyle:UIAlertController.Style.alert)

    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
    { action -> Void in
      // Put your code here
    })
    
    var viewController = UIApplication.shared.keyWindow?.rootViewController
    while viewController?.presentedViewController != nil {
        viewController = viewController?.presentedViewController
    }

    viewController?.present(alertController, animated: true, completion: nil)

    //UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.present(alertController, animated: true, completion: nil)

    //let obj = UIAlertView(title: title as String, message: message, delegate: nil, cancelButtonTitle:"OK")
    //obj.show()
}

func showAlertForCouponCode(_ viewController : UIViewController ,title : String, message : String, cancelTitle : String, cancelCompletion : @escaping (()->()), okTitle : String, okCompletion : @escaping (_ couponCode: String ) -> Void ) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let alertCancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (UIAlertAction) in
        cancelCompletion()
    }

    alertokAction = UIAlertAction(title: okTitle, style: .default, handler: { [weak alert] (_) in
        textFieldCoupon = alert?.textFields![0] // Force unwrapping because we know it exists.
        print("Text field: \(String(describing: textFieldCoupon?.text))")
        if textFieldCoupon?.text == "" || textFieldCoupon?.text == nil || textFieldCoupon?.text!.count == 0 {
            //alertokAction.isEnabled = false
            showAlert(title: "Alert", message: "Please enter coupon code")
        } else {
            //alertokAction.isEnabled = true
            okCompletion(textFieldCoupon?.text ?? "")
        }
    })

    //2. Add the text field. You can configure it however you need.
    alert.addTextField { (textField) in
        textFieldCoupon = textField
        textFieldCoupon?.text = ""
        textFieldCoupon?.placeholder = "Enter Coupon Code"
        textFieldCoupon?.delegate = viewController as? UITextFieldDelegate
        //textChanged()
    }
    
    alert.addAction(alertokAction)
    //alertokAction.isEnabled = false
    alert.addAction(alertCancelAction)
    
    viewController.present(alert, animated: true, completion: nil)
}
//MARK:- Get Apphightlight Color
func getAppHightLightColor() -> String {
    
    if !UserDefaults.Main.string(forKey: .appHightLightColor).isEmpty {
        let apphighlightcolor: String = UserDefaults.Main.string(forKey: .appHightLightColor)
        return apphighlightcolor
    }
   else{
        return ColorConstant.clubifyDefaultHighLightColor
    }
}

/// Request URLs
enum RequestURL: String {
    case appleSandbox = "https://sandbox.itunes.apple.com/verifyReceipt"
    case appleProduction = "https://buy.itunes.apple.com/verifyReceipt"
}
func UTCToCurrentDateFormatWithOutput(date:String, aDateFormate : String, outDateFormat: String) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = aDateFormate
//    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.timeZone = TimeZone.current

    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = outDateFormat
    var str = ""
    if dt != nil
    {
        str = dateFormatter.string(from: dt!)
    }
    return str
}
func setCurrencyIconIfStripe(imageView: UIImageView) {
    if isStripeEnabled() {
        let currencyIconURL = AppState.sharedInstance.ClubList?.icon_url ?? ""
        imageView.sd_setImage(with: URL.init(string: currencyIconURL), placeholderImage: UIImage.init(named: "icon_dollar"), options: .refreshCached)
    } else {
        imageView.image = #imageLiteral(resourceName: "icon_clubifyGold") //#imageLiteral(resourceName: "Coin_Cources")
    }
}

//MARK: - App CLub Theme
func isStripeEnabled() -> Bool {
    print("IAP----- \(String(describing: AppState.sharedInstance.ClubList?.ios_use_iap))")
    if AppState.sharedInstance.ClubList?.ios_use_iap == "1" {
        return false
    }
    return true
}

extension UIView
{
    //Set Border Color
    @IBInspectable var borderColor:UIColor {
        set {
            self.layer.borderColor = newValue.cgColor
        }
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
    }
    
    
    //Set Border Width
    @IBInspectable var borderWidth:CGFloat {
        set {
            self.layer.borderWidth = newValue
        }
        get {
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat {
        set {
            self.layer.cornerRadius = newValue
        }
        get {
            return self.layer.cornerRadius
        }
    }
    
}
protocol Blurable {
    var layer: CALayer { get }
    var subviews: [UIView] { get }
    var frame: CGRect { get }
    var superview: UIView? { get }
    func removeFromSuperview()
    
    func blur(blurRadius: CGFloat)
}
//MARK:- Check If device is iPad or iPhone
func checkIfiPad() -> Bool {
    
    if DeviceTypes.IS_IPAD_AIR || DeviceTypes.IS_IPAD_PRO || DeviceTypes.IS_IPAD_MINI {
        return true
    }
    return false
}
extension Blurable {
    func blur(blurRadius: CGFloat) {
        if self.superview == nil {
            return
        }
        UIGraphicsBeginImageContextWithOptions(CGSize(width: frame.width, height: frame.height), false, 1)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let blur = CIFilter(name: "CIGaussianBlur"),
            let this = self as? UIView else {
                return
        }
        blur.setValue(CIImage(image: image!), forKey: kCIInputImageKey)
        blur.setValue(blurRadius, forKey: kCIInputRadiusKey)
        let ciContext  = CIContext(options: nil)
        let result = blur.value(forKey: kCIOutputImageKey) as! CIImage
        let boundingRect = CGRect(x:0,
                                  y: 0,
                                  width: frame.width,
                                  height: frame.height)
        let cgImage = ciContext.createCGImage(result, from: boundingRect)
        let filteredImage = UIImage(cgImage: cgImage!)
        let blurOverlay = BlurOverlay()
        
        blurOverlay.image = filteredImage
        blurOverlay.contentMode = UIView.ContentMode.left
        if let superview = superview as? UIStackView,
            let index = (superview as UIStackView).arrangedSubviews.index(of: this) {
            removeFromSuperview()
            superview.insertArrangedSubview(blurOverlay, at: index)
        }
        else {
            blurOverlay.frame.origin = frame.origin
            UIView.transition(from: this,
                              to: blurOverlay,
                              duration: 0.2,
                              options: .curveEaseIn,
                              completion: nil)
        }
        objc_setAssociatedObject(this,
                                 &BlurableKey.blurable,
                                 blurOverlay,
                                 objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}
// MARK: - Convert String To Date
func getUTCDateFromString(aDate : String , aDateFormate : String) -> Date
{
    //    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = aDateFormate
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    return formatter.date(from: aDate)!
}

func getCalenderDateFromString(aDate : String , aDateFormate : String) -> Date
{
    //    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = aDateFormate
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    return formatter.date(from: aDate)!
}


func getCurrentDateInString(aDateFormate : String) -> String
{
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = aDateFormate
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    return formatter.string(from:date)
}
func getCalenderDateInString(aDateFormate : String) -> String
{
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    return formatter.string(from:date)
}
func UTCToLocalDateFormat(date:String, aDateFormate : String) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = aDateFormate
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    var str = ""
    if dt != nil
    {
        str = dateFormatter.string(from: dt!)
    }
    return str
}
class BlurOverlay: UIImageView {
}

struct BlurableKey {
    static var blurable = "blurable"
}

extension UIView: Blurable {
    func applyGradient(colours: [UIColor], force: Bool = false) -> Void {
        if self.layer.sublayers?.first is CAGradientLayer, !force {
            return
        }

        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.frame
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.cornerRadius = 0
        self.layer.insertSublayer(gradient, at: 0)
    }
    func applyGradientView(colours: [UIColor]) {
        
        var colours: [UIColor] = []
        let strTheme: String = UserDefaults.standard.string(forKey:"lightTheme") ?? ""
        if !strTheme.isEmpty {
            let isLightTheme = UserDefaults.standard.string(forKey:"lightTheme")
            if isLightTheme == "1" {
                colours = [UIColor.init(white: 1.0, alpha: 0.3), UIColor.white]
            } else {
                colours = [UIColor.clear, UIColor.black]
            }
        } else {
            UserDefaults.standard.set("0", forKey: "lightTheme")
            UserDefaults.standard.synchronize()
            colours = [UIColor.clear, UIColor.black]
        }
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame.size = self.frame.size
        //gradient.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        DispatchQueue.main.async {
            self.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    func applyGradientViewCell(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame.size = self.frame.size//CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.1, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.1, y: 1.0)
        self.layer.insertSublayer(gradient, at: 0)
    }
}
func UTCToLocal(date:String) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    var str = ""
    if dt != nil
    {
        str = dateFormatter.string(from: dt!)
    }
    return str
    
}
func getFormatedDate(_ datetodisplay:String, numericDates:Bool ,dateFormate:String ) -> String {
    
    let display = UTCToLocal(date: datetodisplay)
    
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = formatter.date(from: display)
    if date != nil
    {
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = dateFormate
        let strDate = formatter.string(from: date!)
        return strDate
    }
    return ""
}
func createStringToint(value: AnyObject) -> Int
{
    var returnString: Int = 0
    
    if value as! String == "" {
        returnString = 0
    }else {
        returnString = Int(value as! String)!
    }
    
    return returnString
}

class MTTextFieldSimple: UITextField
{
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 30)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)//bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func awakeFromNib() {
        //        if checkIfiPad(){
        //            self.font = self.font?.withSize((self.font?.pointSize)! * CGFloat(DeviceScaleiPad.SCALE_X))
        //        }else {
        //            self.font = self.font?.withSize((self.font?.pointSize)! * DeviceScale.SCALE_X)
        //        }
        
        self.borderColor = UIColor.init(hex: "D5D4D5")
        self.borderWidth = 1.0
        self.cornerRadius = 15
        
        self.textColor = .black
        var placeholderText = ""
        if self.placeholder != nil{
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderColor()])
        
        self.clearButtonMode = .whileEditing
        //        if #available(iOS 13.0, *) {
        //            if self.traitCollection.userInterfaceStyle == .dark {
        //                if let clearButton = self.value(forKey: "_clearButton") as? UIButton {
        //                    // Create a template copy of the original button image
        //                    let templateImage =  clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        //
        //                    // Set the template image copy as the button image
        //                    clearButton.setImage(templateImage, for: .normal)
        //                    clearButton.setImage(templateImage, for: .highlighted)
        //
        //                    // Finally, set the image color
        //                    clearButton.tintColor = .black
        //                }
        //
        //            } else {
        //                if let clearButton = self.value(forKey: "_clearButton") as? UIButton {
        //                    // Create a template copy of the original button image
        //                    let templateImage =  clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        //
        //                    // Set the template image copy as the button image
        //                    clearButton.setImage(templateImage, for: .normal)
        //                    clearButton.setImage(templateImage, for: .highlighted)
        //
        //                    // Finally, set the image color
        //                    clearButton.tintColor = .darkGray
        //                }
        //            }
        //        }
    }
}
//MARK: - MTLabel
class MTLabel : UILabel {
    override func awakeFromNib() {
        
        //Font size auto resizing in different devices
        if checkIfiPad(){
            self.font = self.font.withSize(self.font.pointSize * CGFloat(10))
            self.textColor = UIColor.white
            
        }else {
            self.font = self.font.withSize(self.font.pointSize * CGFloat(10))
            self.textColor = UIColor.white
        }
    }
}
