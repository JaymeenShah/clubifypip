//
//  AppState.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 11/10/21.
//

import Foundation
import UIKit
import Chat21

class AppState: NSObject {
//    //Logged in User ID    //Set at Login. Read only from that point on.
    //Updated if user settings suscesfully updated
    private var _user:User?
    var user: User? {
        set { _user = newValue! }
        get { return (_user) }
    }
    private var _chatUser:ChatUser?
    var chatUser: ChatUser? {
        set { _chatUser = newValue! }
        get { return (_chatUser) }
    }


    //Currently Acessed Club Owner or Member - Club Data Full Instance
    private var _ClubList:ClubList?
    var ClubList: ClubList? {
        set { _ClubList = newValue }
        get { return _ClubList }
    }
    
    //Currently Used ClubPlan as Member for screen permissions and club authority
    //or as owner, when editing clubplans, this will hold currently selected clubplan
    private var _clubPowerUp:[ClubPowerUp]?
    var clubPowerUp: [ClubPowerUp]? {
        set { _clubPowerUp = newValue }
        get { return _clubPowerUp! }
    }

    
    private var _clubPowerUpWebPage:WebPageData?
    var clubPowerUpWebPage: WebPageData? {
        set { _clubPowerUpWebPage = newValue }
        get { return _clubPowerUpWebPage! }
    }
    
    private var _productPackagePage:WebPageData?
    var productPackagePage: WebPageData? {
        set { _productPackagePage = newValue }
        get { return _productPackagePage! }
    }

    private var _referData:WebPageData?
    var referData: WebPageData? {
        set { _referData = newValue }
        get { return _referData! }
    }
    
    private var _dynamicCourseData:WebPageData?
       var dynamicCourseData: WebPageData? {
           set { _dynamicCourseData = newValue }
           get { return _dynamicCourseData! }
       }
    
    //JoinLoginState - 1 if login presented after JoinClub selected from Discvoer CLubs
    //used for dialog navigation, and UI button labels and elements
    //eventually will replace with a login dialog pop-up for JoinClub Screen? TBD
    private var _joinClubLogIn:String? = "0"
    var joinClubLogIn: String {
        set { _joinClubLogIn = newValue }
        get { return _joinClubLogIn! }
    }
    private var _galleryDocumentType:String? = ""
    var galleryDocumentType: String {
        set { _galleryDocumentType = newValue }
        get { return _galleryDocumentType! }
    }
    override init() {
        super.init()
    }
    var strProductPackageId: String = ""
    var menuReferenceId: String = ""
    var menuUniqueId: String = ""
    var isReferAFriend: Bool = false
    var isDynamicCourse: Bool = false
    var isRightPanDisable: Bool = false
    var folderId: String = ""
    var feedId: String = ""
    var history:[String] = []
    var menuId: String = ""
    var sortBy: String = ""

    var shareLinkData: [String: Any] = [:]
   
    class var sharedInstance: AppState {
        struct Singleton {
            static let instance = AppState()
        }
        return Singleton.instance
    }
}

    
