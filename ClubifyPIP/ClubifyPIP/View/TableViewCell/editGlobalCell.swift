//
//  editGlobalCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 17/09/21.
//

import UIKit
import Alamofire
class editGlobalCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var editSwitch: UISwitch!
    @IBOutlet weak var imgCoins: UIImageView!

  
    //MARK:- Init Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func updateSideMenu(_ sender: Any) {
        
        let isActive = editSwitch.isOn ? "1" : "0"
        updateSideMenu(is_active: isActive, club_id: "192")
    }
    
    
}

//MARK:- update SideMenuApi
extension editGlobalCell
{
    func updateSideMenu(is_active:String,club_id:String){
        
        let headers:HTTPHeaders = ["Content-Type":"application/json" ,
                                   "X-authorization" : ServiceCalls.sharedInstance.Xauthorization!,
                                   "focus_club_id": "135"]
        let parameters = [
            "menu_name" : "Club Wall",
            "menu_id" : "13055",
            "is_active" : is_active,
            "club_id" : club_id
          ]
        
        let urlVal: String =  baseUrl.development.rawValue + "/clubs/update/side_menu"
        print(urlVal)
        Alamofire.request(urlVal,
                          method: .put,
                          parameters: parameters,encoding: JSONEncoding.default,
                          headers: headers)
            .responseJSON {response in
                if let responseData = response.result.value as? [String:Any]{
                    
                    print("update data of owner is:\(responseData)")
                    
                }
            }
    }

}
