//
//  loyalty&RewardCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/09/21.
//

import UIKit

class loyaltyAndRewardCell: UITableViewCell {

    //MARK:-IBoutles
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var colorSlider: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
