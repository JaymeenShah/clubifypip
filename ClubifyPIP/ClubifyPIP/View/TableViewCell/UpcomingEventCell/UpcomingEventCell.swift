//
//  UpcomingEventCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 19/10/21.
//

import UIKit
import BubblePictures

class UpcomingEventCell: UITableViewCell {

    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblEventPage: UILabel!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnWebLink: UIButton!
    @IBOutlet weak var heightConstraintBtnJoined: NSLayoutConstraint!

    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var viewPoints: UIView!
    @IBOutlet weak var viewCoins: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var blurEffectView: UIVisualEffectView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!

    @IBOutlet weak var heightConstraintEventPage: NSLayoutConstraint!
    @IBOutlet weak var imgPoints: UIImageView!
    @IBOutlet weak var imgCoins: UIImageView!

    @IBOutlet weak var viewForLock: UIView!
    @IBOutlet weak var btnLockIcon: UIButton!

    @IBOutlet weak var lblEventPageTitle: UILabel!

    var pictures = [BPCellConfigFile]()
    var picturesMembership = [MembershipLevel]()
    var strCost: String = ""
    
    weak var delegate: FutureEventPictureSelectedProtocol?
    
    var bubblePictures: BubblePictures!
    var layoutConfigurator = BPLayoutConfigurator(
        backgroundColorForTruncatedBubble: UIColor.appGradiantButton(),//UIColor.white,
        fontForBubbleTitles: UIFont(name: FontName.SFProTextBold, size: 12.0)!,
        colorForBubbleBorders: UIColor.white,
        colorForBubbleTitles: UIColor.white,
        maxCharactersForBubbleTitles: 3,
        maxNumberOfBubbles: 5,
        displayForTruncatedCell: nil,
        widthForBubbleBorders: 0,
        bubbleImageContentMode: .scaleAspectFit,
        distanceInterBubbles: 12,
        direction: .leftToRight,
        alignment: .left)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewForLock.isHidden = true
        btnLockIcon.isHidden = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

@objc protocol FutureEventPictureSelectedProtocol {
    func pictureSelected(_ item : Int, _ arrMembership: [MembershipLevel], _ cost: String)
}

extension UpcomingEventCell: BPDelegate {
    func didSelectTruncatedBubble() {
        print("Selected truncated bubble")
    }
    
    func didSelectBubble(at index: Int) {
        print(index)
        if self.delegate != nil {
            self.delegate?.pictureSelected(index, picturesMembership, strCost)
        }
    }
}

