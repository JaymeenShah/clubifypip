//
//  clubPackageCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/09/21.
//

import UIKit

class clubPackageCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCoins: UILabel!
    @IBOutlet weak var imgCoins: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
