//
//  viewWalletCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 07/09/21.
//

import UIKit

class viewWalletCell: UITableViewCell {

    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCoins: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCoins: UILabel!
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        mainContentView.setMainGradientBackground()
        mainContentView.backgroundColor = UIColor.clear
    }

   
}
