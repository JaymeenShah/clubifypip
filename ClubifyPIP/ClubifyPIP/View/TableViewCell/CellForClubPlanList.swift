//
//  CellForClubPlanList.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 19/10/21.
//

import UIKit

class CellForClubPlanList:UITableViewCell{
    
    //MARK:  Protperties
    @IBOutlet weak var lblTitle: MTLabel!
    @IBOutlet weak var lblCost: MTLabel!
    @IBOutlet weak var imgPlan: UIImageView!
    @IBOutlet weak var imgTickMark: UIImageView!
    @IBOutlet weak var imgTickMarkControl: UIControl!
    @IBOutlet weak var imgCoins: UIImageView!
    
    
    // MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        //setCurrencyIconIfStripe(imageView: self.imgCoins)
    }
    
    func configureCell() {
        self.imgPlan.layer.cornerRadius = 15.0
        self.imgPlan.clipsToBounds = true
    }
}
