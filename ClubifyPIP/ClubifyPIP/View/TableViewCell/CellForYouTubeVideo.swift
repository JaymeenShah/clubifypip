//
//  CellForYouTubeVideo.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 06/10/21.
//

import UIKit

class CellForYouTubeVideo: UITableViewCell {
    
    //MARK:-  Properties
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPublishTime: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgThumb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
