//
//  dicoverClubCell.swift
//  collectionViewVertically28
//
//  Created by Plutus-MacMini on 25/08/21.
//

import UIKit

class dicoverClubCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var viewHint: UIView!
    @IBOutlet weak var viewAllLables: UIView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var viewBackgrounBlurr: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()

        self.viewHint.layer.masksToBounds = true
        viewHint.layer.cornerRadius = 20
        viewHint.layer.shadowColor = UIColor.gray.cgColor
        if !UIAccessibility.isReduceTransparencyEnabled {
            viewBackgrounBlurr.backgroundColor = .clear
            
            let blurEffect = UIBlurEffect(style:.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.viewBackgrounBlurr.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            viewBackgrounBlurr.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        } else {
            viewBackgrounBlurr.backgroundColor = .white
        }

    }
}
