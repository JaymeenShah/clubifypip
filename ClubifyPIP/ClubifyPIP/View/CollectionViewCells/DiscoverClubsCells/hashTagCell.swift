//
//  hashTagCell.swift
//  collectionViewVertically28
//
//  Created by Plutus-MacMini on 28/05/21.
//

import UIKit

class hashTagCell: UICollectionViewCell {
    
    @IBOutlet weak var viewHasTag: UIView!
    
    @IBOutlet weak var lblHashTag: UILabel!
    
    @IBOutlet weak var mainContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewHasTag.layer.cornerRadius = 20

      
    }
}
