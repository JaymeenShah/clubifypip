//
//  collectionViewClub.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 25/08/21.
//

import UIKit

class collectionViewClubCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var viewBackgrounBlurr: UIView!
    override func awakeFromNib() {
    }
}
