//
//  hasTagCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 25/08/21.
//

import UIKit

class hasTagCell: UICollectionViewCell {
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var lblHasTag: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainContentView.layer.cornerRadius = 40
      
        self.mainContentView.layer.masksToBounds = false
        self.mainContentView.layer.shadowOffset = CGSize.zero
        self.mainContentView.layer.shadowOpacity = 1.0
        mainContentView.layer.shadowRadius = 3.0
        mainContentView.layer.shadowColor = UIColor.gray.cgColor
    }
}
