//
//  CellForMembersList.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 07/10/21.
//

import UIKit
import DataCache

class CellForMembersList: UICollectionViewCell {
    
    //MARK:  Properties
    @IBOutlet weak var lblUserName: NBLabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnBlockUser: UIButton?
    @IBOutlet weak var imgPlanIcon: UIImageView!
    @IBOutlet weak var lblPlanName: NBLabel!
    
    //MARK:
    override func prepareForReuse() {
        self.imgProfile.image = #imageLiteral(resourceName: "placeholder")
        super.prepareForReuse()
    }
}
