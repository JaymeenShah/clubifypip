//
//  CenterCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.
//

import UIKit

class CenterCell: UICollectionViewCell {
    
    @IBOutlet weak var centerImgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
