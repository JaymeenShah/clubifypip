//
//  memberClubCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 27/08/21.
//

import UIKit

class memberClubCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var clubCellContentView: UIView!
    @IBOutlet weak var imgMemeberCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        clubCellContentView.layer.masksToBounds = true
        clubCellContentView.layer.cornerRadius = 12
    }
}
