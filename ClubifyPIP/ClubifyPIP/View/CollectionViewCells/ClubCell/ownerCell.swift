//
//  ownerCell.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 27/08/21.
//

import UIKit

class ownerCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var clubCellContentView: UIView!
    @IBOutlet weak var imgMemeberCell: UIImageView!
    @IBOutlet weak var buttonSetting: UIButton!
    @IBOutlet weak var viewSetting: UIControl!
    @IBOutlet weak var bottomBackGroundView: UIView!
    @IBOutlet weak var buttonSettingIcon: UIButton!
    @IBOutlet weak var settingControl: UIControl!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgMemeberCell.layer.masksToBounds = true
        imgMemeberCell.layer.cornerRadius = 16
        settingControl.layer.masksToBounds = true
        settingControl.layer.cornerRadius = 15
        settingControl.layer.borderWidth = 1.5
        settingControl.layer.borderColor = UIColor.white.cgColor

        
        
    }
}
