//
//  AppDelegate.swift
//  ClubifyPIP
//
//  Created by Plutus-MacBook on 20/08/21.

import UIKit
import FBSDKLoginKit
import IQKeyboardManagerSwift
import GoogleSignIn
import SwiftyDropbox
import Alamofire
import Firebase
import UserNotifications
import FirebaseMessaging
import StoreKit
import GoogleMaps
import GooglePlaces
import Chat21
import DataCache
import AKSideMenu
import AppTrackingTransparency
import PlacesPicker


var mailBaseURL:String = baseUrl.production.rawValue



let appDelegate = UIApplication.shared.delegate as! AppDelegate
var WebURL : WebURLClass = WebURLClass()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    //MARK:  Variables
    var manager = NetworkReachabilityManager()
    var window: UIWindow?
    var isProgress = false
    var isFromFacebook = false
    var isFromGoogle = false
    var isDiscoverFromLogin = false
    var isJoinClubDeepLimk = false
    var isFromJoinNow = false
    internal var shouldRotate = false
    var lastPowerPage = "0"
    var userCoins = "0"
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    var isFirstTimeUser = false
    let receiptURL = Bundle.main.appStoreReceiptURL
    var currentOpenScreen : String = ""
    var isFromPinterest : Bool = false
    var notificationType : String = ""
    var isDevUrl : Bool = false
    let locationManager = CLLocationManager()
    var moveToClubWall:(() ->Void)?
    var upgradeYourPackage:(() ->Void)?
    var refreshSideMenuList:(() ->Void)?
    var sideMenuData = [SideMenu]()
    var openMenuUniqueId : String = ""
    
    var openDefaultStartupPage: String = ""
    var strDefaultMenuId: String = ""
    var strDefaultMenuName: String = ""
    
    var setMenuTitle:(() ->Void)?
    var isOneClubOnly = false
    var navigationController = UINavigationController()
    var totalPoints = 0
    var reached = false
    var launchURL: URL?
    var isFromMemberOf = false
    var notificationClubData = ClubList()
    var watchVideoBy90Percent = false
    var isFromRegisterScreen = false
    
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
          
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        return true
    }
          
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {

        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func LogOutDevice() {
        removeDataOnLogout()
    }

    func removeDataOnLogout() {
        
        AppState.sharedInstance.ClubList?.clubTheme = "0"
        AppState.sharedInstance.history.removeAll()
        GIDSignIn.sharedInstance()?.signOut()
//        self.removeDeviceTokenFromFirebase()
        DropboxClientsManager.authorizedClient = nil
        UserDefaults.standard.removeObject(forKey: "WelcomeTextAlert")
        UserDefaults.standard.removeObject(forKey: "FirstTimeAlert")
        UserDefaults.standard.removeObject(forKey: "buttonColor")
        UserDefaults.standard.removeObject(forKey: "lightTheme")
        UserDefaults.standard.synchronize()
        DataCache.instance.clean(byKey: "contestList")
        DataCache.instance.clean(byKey: "clubFeedList-\(String(describing: AppState.sharedInstance.ClubList?.id))")
        DataCache.instance.clean(byKey: "sideMenu")
        DataCache.instance.clean(byKey: "walletDetails")
        DataCache.instance.clean(byKey: "pointDetails")
        DataCache.instance.clean(byKey: "DiscoverClubs")
        DataCache.instance.clean(byKey: "MemberListForChat")
        DataCache.instance.cleanAll()
        
        UserDefaults.Main.removeObj(forKey: .Profile)
        UserDefaults.Main.removeObj(forKey: .sessionToken)

        AppState.sharedInstance.user = User()
    //    let nextVC = storyboard.instantiateViewController(withIdentifier: "navLogin") as! UINavigationController
    //    UIApplication.shared.keyWindow?.rootViewController = nextVC
    //    self.window?.rootViewController = nextVC
    //    self.window?.makeKeyAndVisible()

    }


}

